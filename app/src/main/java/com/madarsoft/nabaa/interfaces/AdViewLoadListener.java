package com.madarsoft.nabaa.interfaces;

import android.view.View;

public interface AdViewLoadListener {
	void onAdLoaded();
	void onAdClosed();
	void  onAdError();
	

}
