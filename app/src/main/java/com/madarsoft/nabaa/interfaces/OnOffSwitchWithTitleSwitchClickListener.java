package com.madarsoft.nabaa.interfaces;

/**
 * Created by ZProf on 02/06/2016.
 */
public interface OnOffSwitchWithTitleSwitchClickListener {
    void onSwitchClick();
}
