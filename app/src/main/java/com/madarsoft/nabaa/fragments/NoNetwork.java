package com.madarsoft.nabaa.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.controls.MainControl;

/**
 * Created by Colossus on 12-Apr-16.
 */
@SuppressLint("ValidFragment")
public class NoNetwork extends MadarFragment {

    private final String id;
    OnInterrnetChangeListener onInterrnetChangeListener;
    private RelativeLayout refresh;

    public NoNetwork(String id) {
        this.id = id;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        refresh = (RelativeLayout) getActivity().findViewById(R.id.no_network_button_view);
        final MainControl mainControl = new MainControl(getActivity());

        refresh.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           if (!mainControl.checkInternetConnection()) {
                                               Toast.makeText(getActivity(), R.string.no_internet, Toast.LENGTH_SHORT).show();
                                           } else {
                                               FragmentManager manager = getActivity().getSupportFragmentManager();
                                               FragmentTransaction transaction = manager.beginTransaction();
                                               NewsDetail2 newsDetail2 = new NewsDetail2(id);
                                               transaction.replace(R.id.parent, newsDetail2);
                                               transaction.commit();
                                           }
                                       }
                                   }
        );


    }

    public void onFinishUpdates(OnInterrnetChangeListener onInterrnetChangeListener) {
        this.onInterrnetChangeListener = onInterrnetChangeListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.network_alert, container, false);
    }

    public interface OnInterrnetChangeListener {
        public void onInternetBack();
    }
}
