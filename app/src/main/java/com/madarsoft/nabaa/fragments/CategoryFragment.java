package com.madarsoft.nabaa.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/*import com.madarsoft.firebasedatabasereader.interfaces.AdViewLoadListener;
import com.madarsoft.firebasedatabasereader.objects.RectangleBannerAd;*/
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.adapters.SearchedSourcesAdapter;
import com.madarsoft.nabaa.controls.AdsControlNabaa;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoryFragment extends MadarFragment {

    //    public static RelativeLayout lowerView;
    private static CategoryFragment _instance;
    private final int SUBJECT_PAGE = 0;
    private final int COUNTRY_PAGE = 1;
    private SubjectListFragment subjectListFragment;
    private CountryListFragment countryListFragment;
    private ImageView subjectGreenView;
    private ImageView countryGreenView;
    private ViewPager contentPager;
    private CategoryPagerAdapter categoryPagerAdapter;
    private RelativeLayout countryLayout;
    private RelativeLayout subjectLayout;
    private TextView subjText;
    private TextView countryText;
    private DrawerLayout mDrawerLayout;
    private ImageView menu;
    private TextView categoryHeader;
    private Typeface font;
    private Typeface headerFont;
    private ImageView home;
    private LinearLayout topAdView;
    private LinearLayout bottomAdView;
    private AdsControlNabaa adsControl;
    private boolean isClear = true;
    private AdsControlNabaa adsControl_top;
    private AdsControlNabaa adsControl_bottom;
    private ImageView search;
//    private EditText inputText;
    //    private TextView textHeader;
//    private ImageView exitSearch;
    private boolean isOpened = true;
//    private ImageView clearText;
    private JsonParser jsonParser;

    private void setRedViewInDrawer() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View redView = (View) vx.getChildAt(6).findViewById(R.id.red_view);
        redView.setVisibility(View.VISIBLE);
    }

    private void setRedViewInDrawerforHome() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View newsFragmentRedView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        View categoryRedView = (View) vx.getChildAt(6).findViewById(R.id.red_view);
        newsFragmentRedView.setVisibility(View.VISIBLE);
        categoryRedView.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/arial.ttf");
        headerFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/arial.ttf");

        subjectGreenView = (ImageView) getActivity().findViewById(R.id.subj_green_view);
        countryGreenView = (ImageView) getActivity().findViewById(R.id.country_green_view);
        contentPager = (ViewPager) getActivity().findViewById(R.id.content_pager);
        countryLayout = (RelativeLayout) getActivity().findViewById(R.id.category_country_layout);
        subjectLayout = (RelativeLayout) getActivity().findViewById(R.id.category_subject_layout);
        countryText = (TextView) getActivity().findViewById(R.id.cat_country_text);
        subjText = (TextView) getActivity().findViewById(R.id.cat_subject_text);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        menu = (ImageView) getActivity().findViewById(R.id.category_menu);
        categoryHeader = (TextView) getActivity().findViewById(R.id.category_header);
        home = (ImageView) getActivity().findViewById(R.id.category_home_button);
        search = (ImageView) getActivity().findViewById(R.id.category_header_search);
//        inputText = (EditText) getActivity().findViewById(R.id.search_category);
//        lower = (RelativeLayout) getActivity().findViewById(R.id.lower);
//        exitSearch = (ImageView) getActivity().findViewById(R.id.category_exit_search);
//        clearText = (ImageView) getActivity().findViewById(R.id.clear_text);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        //Banner Ads View
        topAdView = (LinearLayout) getActivity().findViewById(R.id.cat_top_ad_view);
        bottomAdView = (LinearLayout) getActivity().findViewById(R.id.cat_bottom_ad_view);
        adsControl_top = new AdsControlNabaa(getActivity());
        adsControl_bottom = new AdsControlNabaa(getActivity());

        countryText.setTypeface(font);
        subjText.setTypeface(font);
        categoryHeader.setTypeface(headerFont);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.RIGHT);

            }
        });
        try {
            setRedViewInDrawer();
        } catch (Exception e) {
            e.printStackTrace();
        }
        adsControl_top.getBannerAd(topAdView, "category_top");
        adsControl_top.getSplashAd( "category_top");
//
       /* RectangleBannerAd banner = (new RectangleBannerAd.Builder()).adContainer(topAdView)
                .build();
        adsControl_top.getBannerAd(banner, "category_top");
      banner.setAdListener(new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                topAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdError() {
                topAdView.setVisibility(View.GONE);

            }
        });
        */
 adsControl_bottom.getBannerAd(bottomAdView, "category_bottom");
 /*
        RectangleBannerAd banner_bottom = (new RectangleBannerAd.Builder()).adContainer(bottomAdView)
                .build();
        adsControl_bottom.getBannerAd(banner, "category_bottom");
        banner_bottom.setAdListener(new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                bottomAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdError() {
                bottomAdView.setVisibility(View.GONE);

            }
        });*/

        categoryPagerAdapter = new CategoryPagerAdapter(getChildFragmentManager());
//        new SetAdapterTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        contentPager.setAdapter(categoryPagerAdapter);

        contentPager.setCurrentItem(COUNTRY_PAGE);
       /* countryGreenView.setVisibility(View.VISIBLE);
        countryText.setTextColor(Color.parseColor("#ffffff"));
        subjectGreenView.setVisibility(View.INVISIBLE);*/
        countryLayout.setBackgroundColor(Color.parseColor("#AE111C"));

        search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                SearchDialogFragment searchDialogFragment = SearchDialogFragment.newInstance("SearchDialogFragment");
                searchDialogFragment.show(fm, "SearchDialogFragment");
            }
//                if (inputText.getVisibility() == View.INVISIBLE) {
//                    ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation
//                            .RELATIVE_TO_SELF, 0.5f);
//                    anim.setDuration(200);
//                    anim.setFillAfter(true);
//                    inputText.startAnimation(anim);
//                    anim.setAnimationListener(new Animation.AnimationListener() {
//                        @Override
//                        public void onAnimationStart(Animation animation) {
//
//                        }
//
//                        @Override
//                        public void onAnimationEnd(Animation animation) {
////                                setPadding(0);
//                            categoryHeader.setVisibility(View.INVISIBLE);
//                            inputText.setVisibility(View.VISIBLE);
//                            inputText.requestFocus();
//                            inputText.bringToFront();
//                            hideKeyboard(inputText, true, getActivity());
//                            clearText.setVisibility(View.VISIBLE);
//                            clearText.bringToFront();
////                                lower.setVisibility(View.GONE);
////                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////                                    menu.setImageDrawable(getResources().getDrawable(R.drawable.white_x, getActivity()
////                                            .getApplicationContext().getTheme()));
////                                    menu.bringToFront();
//
//                            search.setVisibility(View.GONE);
//                            search.bringToFront();
////                                } else {
////                                    menu.setImageDrawable(getResources().getDrawable(R.drawable.white_x));
////                                    menu.bringToFront();
////
////                                    search.setVisibility(View.GONE);
////                                    search.bringToFront();
////                                }
//                            exitSearch.setVisibility(View.VISIBLE);
//                            exitSearch.bringToFront();
//
//                        }
//
//                        @Override
//                        public void onAnimationRepeat(Animation animation) {
//                        }
//                    });
//                }
//
//            }
        });

//        exitSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            //exit
//            public void onClick(View v) {
//                ScaleAnimation animate = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation
//                        .RELATIVE_TO_SELF, 0.5f);
//                animate.setDuration(150);
//                animate.setFillAfter(true);
//                inputText.startAnimation(animate);
//                animate.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
////                        setPadding(40);
//                        inputText.setVisibility(View.INVISIBLE);
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            menu.setImageDrawable(getResources().getDrawable(R.drawable.menu, getActivity()
//                                    .getApplicationContext().getTheme()));
//                        } else {
//                            menu.setImageDrawable(getResources().getDrawable(R.drawable.menu));
//                        }
//                        categoryHeader.setVisibility(View.VISIBLE);
//                        clearText.setVisibility(View.GONE);
//                        inputText.setText(null);
//                        exitSearch.setVisibility(View.GONE);
////                        lower.setVisibility(View.VISIBLE);
//                        search.setVisibility(View.VISIBLE);
//                        search.bringToFront();
//                        hideKeyboard(inputText, false, getActivity());
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });
//            }
//        });
        countryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contentPager.setCurrentItem(COUNTRY_PAGE);
                //countryGreenView.setVisibility(View.VISIBLE);
                //subjectGreenView.setVisibility(View.INVISIBLE);
                countryLayout.setBackgroundColor(Color.parseColor("#AE111C"));
                subjectLayout.setBackgroundColor(Color.BLACK);
                //countryText.setTextColor(Color.parseColor("#ffffff"));

            }
        });
        subjectLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contentPager.setCurrentItem(SUBJECT_PAGE);
                //countryGreenView.setVisibility(View.INVISIBLE);
                //subjectGreenView.setVisibility(View.VISIBLE);
                subjectLayout.setBackgroundColor(Color.parseColor("#AE111C"));
                countryLayout.setBackgroundColor(Color.BLACK);
                //subjText.setTextColor(Color.parseColor("#ffffff"));
            }
        });
//        clearText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                inputText.setText(null);
//            }
//        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new NewsHolderFragment(), "newsHolder");
                transaction.commit();
                try {
                    setRedViewInDrawerforHome();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        contentPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case SUBJECT_PAGE:
                        //countryGreenView.setVisibility(View.INVISIBLE);
                       // subjectGreenView.setVisibility(View.VISIBLE);
                        //subjText.setTextColor(Color.parseColor("#ffffff"));
                        subjectLayout.setBackgroundColor(Color.parseColor("#AE111C"));
                        countryLayout.setBackgroundColor(Color.BLACK);
//                       SubjectListFragment.listView.smoothScrollToPosition(SubjectListFragment.generalPosition);
                        return;
                    case COUNTRY_PAGE:
                        /*countryGreenView.setVisibility(View.VISIBLE);
                        subjectGreenView.setVisibility(View.INVISIBLE);
                        countryText.setTextColor(Color.parseColor("#ffffff"));*/
                        countryLayout.setBackgroundColor(Color.parseColor("#AE111C"));
                        subjectLayout.setBackgroundColor(Color.BLACK);
                        return;
//                    default:
//                        return;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//        inputText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                ArrayList<Sources> arr = new ArrayList<Sources>();
//                if (s.toString().length() > 3) {
//                    if (s.toString() != null) {
//                        callJson(URLs.getUserID(), getActivity(), s.toString());
////                        inputText.setAdapter(new SearchedSourcesAdapter(getActivity(), R.layout.searched_source_item, arr));
//                    } else if (s.length() == 0 || inputText.getVisibility() == View.GONE) {
////                        sourcesListView.setAdapter(new SourcesAdapterV2(getActivity(), sourcesList, listener));
//                    }
//                }
//            }
//
//
//        });


    }

    private void callJson(int userID, final Context context, String text) {
        MainControl mainControl = new MainControl(context);
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, "أنت غير متصل بالأنترنت", Toast.LENGTH_SHORT).show();
        }
        HashMap<String, String> dataObj = new HashMap<>();


        dataObj.put(URLs.TAG_COUNTRIES_RESOURCES_USERID, userID + "");
        dataObj.put(URLs.TAG_ADD_COMMENT_COUNT, "40");
        try {
            dataObj.put(URLs.TAG_TEXT, URLDecoder.decode(text, "utf-8") + "");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        dataObj.put(URLs.TAG_PAGE, "0");
        jsonParser = new JsonParser(getActivity(), URLs.SOURCES_SEARCH, URLs.SEARCH_IN_SOURCES_WEB, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();

        jsonParser.onWebSourcesRetrieved(new JsonParser.OnWebSourcesRetrieved() {
            @Override
            public void onFinished(List<Sources> sourceList) {
//                SearchedSourcesAdapter mSearch = new SearchedSourcesAdapter(getActivity(), R.layout.searched_source_item, sourceList);

            }

        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.category, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void hideKeyboard(View view, boolean open, Context context) {
        //sonarQube edit
        if (view == null)
            return;

        if (open == false) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            isOpened = false;
        }
        if (open) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            isOpened = true;
        }
    }

//    private void setPadding(int padd) {
//        int paddingPixel = padd;
//        float density = getActivity().getResources().getDisplayMetrics().density;
//        int paddingDp = (int) (paddingPixel * density);
////        sourcesListView.setPadding(0, 0, 0, paddingDp);
//    }

    private class CategoryPagerAdapter extends FragmentPagerAdapter {

        public CategoryPagerAdapter(FragmentManager fm) {

            super(fm);
            countryListFragment = new CountryListFragment();
            subjectListFragment = new SubjectListFragment();

        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case COUNTRY_PAGE:
                    return countryListFragment;
                case SUBJECT_PAGE:
                    return subjectListFragment;
                default:
                    return countryListFragment;
            }
        }

//        @Override
//        public Parcelable saveState() {
//            // Do Nothing
//            return null;
//        }

//        @Override
//        public void restoreState(Parcelable arg0, ClassLoader arg1) {
//            //do nothing here! no call to super.restoreState(arg0, arg1);
//        }


        @Override
        public int getCount() {
            return 2;
        }
    }

//    private class SetAdapterTask extends AsyncTask<Void, Void, Void> {
//        protected Void doInBackground(Void... params) {
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//
//            if (categoryPagerAdapter != null) contentPager.setAdapter(categoryPagerAdapter);
//    contentPager.setCurrentItem(COUNTRY_PAGE);
//    countryGreenView.setVisibility(View.VISIBLE);
//    countryText.setTextColor(Color.parseColor("#ffffff"));
//    subjectGreenView.setVisibility(View.INVISIBLE);
//        }
//    }
}