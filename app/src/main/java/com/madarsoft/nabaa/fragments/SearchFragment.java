package com.madarsoft.nabaa.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.adapters.NewsSearchAdapter;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.URLs;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Colossus on 21-Jun-17.
 */

public class SearchFragment extends MadarFragment {


    private RelativeLayout inAllSourcesLayout;
    private EditText editText;
    private RelativeLayout inMySourcesLayout;
    private ImageView allSourcesMarker;
    private ImageView mySourcesMarker;
    private ListView searchList;
    private ImageView clearText;
    private ImageView back;
    private LinearLayout container;
    private GifMovieView gif;
    private LinearLayout gifHolder;
    private JsonParser jsonParser;
    private int currentScrollState;
    private int currentFirstVisibleItem;
    private int currentVisibleItemCount;
    private int totalItemCounts;
    private int mLastFirstVisibleItem;
    private boolean lastIndexReached;
    private boolean isLoading;
    private List<News> mList;
    private HashSet<Integer> newsSet;
    private NewsSearchAdapter mSearch;
    private String mtimeStamp;
    private boolean isMySource;

    public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_NULL
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            //match this behavior to your 'Send' (or Confirm) button
        }
        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inAllSourcesLayout = (RelativeLayout) getActivity().findViewById(R.id.search_in_all_sources);
        inMySourcesLayout = (RelativeLayout) getActivity().findViewById(R.id.search_in_my_sources);
        allSourcesMarker = (ImageView) getActivity().findViewById(R.id.search_white_marker_in_all_sources);
        mySourcesMarker = (ImageView) getActivity().findViewById(R.id.search_white_marker_in_my_sources);
        searchList = (ListView) getActivity().findViewById(R.id.search_list);
        clearText = (ImageView) getActivity().findViewById(R.id.clear_search);
        back = (ImageView) getActivity().findViewById(R.id.back_search);
        editText = (EditText) getActivity().findViewById(R.id.search_edit_text);
        container = (LinearLayout) getActivity().findViewById(R.id.search_cont);
        gif = (GifMovieView) getActivity().findViewById(R.id.search_gif);
        gifHolder = (LinearLayout) getActivity().findViewById(R.id.search_gif_holder);
        gif.setMovieResource(R.drawable.loading);
        gif.setVisibility(View.GONE);
        mList = new ArrayList<>();
        newsSet = new HashSet<>();
        inAllSourcesLayout.setBackgroundColor(Color.parseColor("#AE111C"));
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//        editText.requestFocus();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                getFragmentManager().popBackStack();
            }
        });
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchAction(editText.getText().toString());

                    return true;
                }
                return false;
            }
        });

        searchList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                currentScrollState = scrollState;
                isScrollCompleted();
            }


            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                currentFirstVisibleItem = firstVisibleItem;
                currentVisibleItemCount = visibleItemCount;
                totalItemCounts = totalItemCount;
                if (mLastFirstVisibleItem < firstVisibleItem) {
                    Log.i("SCROLLING DOWN", "TRUE");
                }
                mLastFirstVisibleItem = firstVisibleItem;
            }
        });
        clearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText(null);
                try {
                    newsSet.clear();
                    mList.clear();
                    mSearch.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    mSearch.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        inAllSourcesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*allSourcesMarker.setVisibility(View.VISIBLE);
                mySourcesMarker.setVisibility(View.GONE);*/
                inAllSourcesLayout.setBackgroundColor(Color.parseColor("#AE111C"));
                inMySourcesLayout.setBackgroundColor(Color.BLACK);

                editText.setText("");
                try {
                    newsSet.clear();
                    mList.clear();
                    mSearch.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    mSearch.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        inMySourcesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*mySourcesMarker.setVisibility(View.VISIBLE);
                allSourcesMarker.setVisibility(View.GONE);*/
                inMySourcesLayout.setBackgroundColor(Color.parseColor("#AE111C"));
                inAllSourcesLayout.setBackgroundColor(Color.BLACK);
                editText.setText("");
                try {
                    newsSet.clear();
                    mList.clear();
                    mSearch.clear();
                    mSearch.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void searchAction(String s) {
        hideKeyboard(getActivity());

        MainControl mainControl = new MainControl(getActivity());
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(getActivity(), R.string.network_error, Toast.LENGTH_SHORT).show();
            return;
        }
        if (s.toString().length() >= 3) {
            if (s.toString() != null) {
                searchList.setVisibility(View.GONE);
                gifHolder.setVisibility(View.VISIBLE);
                gif.setVisibility(View.VISIBLE);
                gif.setPaused(false);
                try {
                    mList.clear();
                    newsSet.clear();
                    mSearch.clear();
                    mSearch.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (allSourcesMarker.getVisibility() == View.VISIBLE)
                    callJson(URLs.getUserID(), getActivity(), s.toString(), "0", isMySource);
                else
                    callJson(URLs.getUserID(), getActivity(), s.toString(), "0", !isMySource);
            }
        } else {
            try {
                newsSet.clear();
                mList.clear();
                mSearch.clear();
                mSearch.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == 0 &&
                totalItemCounts == (currentFirstVisibleItem + currentVisibleItemCount)) {
            MainControl mainControl = new MainControl(getContext());
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(getContext(), "أنت غير متصل بالأنترنت", Toast.LENGTH_SHORT).show();
            } else {
                lastIndexReached = true;
                if (!isLoading) {
                    if (allSourcesMarker.getVisibility() == View.VISIBLE)
                        callJson(URLs.getUserID(), getActivity(), editText.getText() + "", mtimeStamp, isMySource);
                    else
                        callJson(URLs.getUserID(), getActivity(), editText.getText() + "", mtimeStamp, !isMySource);
                    isLoading = true;
                }

            }
        }
    }

    private void callJson(int userID, final Context context, String text, String timeStamp, boolean isMySource) {
        MainControl mainControl = new MainControl(context);
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, "أنت غير متصل بالأنترنت", Toast.LENGTH_SHORT).show();
        }
        if (!lastIndexReached) {
            try {
                newsSet.clear();
                mList.clear();
                mSearch.clear();
                mSearch.notifyDataSetChanged();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*
        * userId:'20082',
text:'مصر',
count:'10',
timestamp:'0'
        * */

        HashMap<String, String> dataObj = new HashMap<>();
        dataObj.put(URLs.TAG_COUNTRIES_RESOURCES_USERID, userID + "");
        dataObj.put(URLs.TAG_ADD_COMMENT_COUNT, "10");

        try {
            dataObj.put(URLs.TAG_TEXT, URLDecoder.decode(text, "utf-8") + "");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        dataObj.put(URLs.SEARCH_TIME_STAMP, timeStamp + "");
        if (!isMySource)
            jsonParser = new JsonParser(getActivity(), URLs.SEARCH_BY_SOURCES, URLs.SEARCH_BY_SOURCES_TYPE, dataObj);
        else
            jsonParser = new JsonParser(getActivity(), URLs.SEARCH_BY_USER_SOURCES, URLs.SEARCH_BY_USER_SOURCES_TYPE, dataObj);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();

        jsonParser.onSearchedNewsRetrieved(new JsonParser.OnSearchedNewsRetrieved() {
            @Override
            public void onFinished(List<News> searchedNewsList, String timeStamp) {
                mtimeStamp = timeStamp;
                isLoading = false;
                if (lastIndexReached) {
//                    sourcesSet.addAll(sourceList);
//                    mList.addAll(sourceList);
//                    mList.addAll(sourcesSet);
                    for (News s : searchedNewsList) {
                        if (newsSet.add(s.getID()))
                            mList.add(s);
                    }
                    mSearch.notifyDataSetChanged();
                } else {
                    for (News s : searchedNewsList) {
                        if (newsSet.add(s.getID()))
                            mList.add(s);
                    }
//                    mList.addAll(sourceList);
//                    mList.addAll(sourcesSet);
                    mSearch = new NewsSearchAdapter(getActivity(), R.layout.searched_source_item, mList);
                    searchList.setAdapter(mSearch);
                }
                searchList.setVisibility(View.VISIBLE);
                gifHolder.setVisibility(View.GONE);
                gif.setPaused(true);
                gif.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.search_fragment, container, false);
    }


}
