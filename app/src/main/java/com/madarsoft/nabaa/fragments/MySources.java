package com.madarsoft.nabaa.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.firebase.messaging.FirebaseMessaging;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.adapters.MyResourcesAdapter;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.History;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MySources extends MadarFragment {

    MyResourcesAdapter myResourcesAdapter;
    RelativeLayout noSourcesSelected;
    ListView sublistView;
    ListView geolistView;
    Context context;
    private ArrayList<Sources> geoSelectedItems;
    private ArrayList<Sources> subSelectedItems;
    private boolean isGeo;
    private ImageView menu;
    private DrawerLayout mDrawerLayout;
    private TextView header;
    private Typeface headerFont;
    private MyResourcesAdapter mSub;
    private MyResourcesAdapter mGeo;
    private GifMovieView gifMovieView;
    private DataBaseAdapter db;
    private com.google.android.gms.analytics.Tracker mTracker;
    private ImageView home;
    private RelativeLayout chooseSources;
    private ArrayList<History> articleIds;
    private Sources source;
    private ArrayList<Sources> rankedList;
    private int rankMax;
    private LinearLayout deleteAll;
    private JsonParser jsonParser;
    private PleaseWaitFragment pleaseWaitFragment;
    private List<Sources> selectedSources;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName(getString(R.string.user_sources));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        context = getActivity();
        headerFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Hacen_Liner_Screen_Bd.ttf");
        geolistView = (ListView) getActivity().findViewById(R.id.my_resources_list_view_geo);
        menu = (ImageView) getActivity().findViewById(R.id.my_sources_menu1);
        header = (TextView) getActivity().findViewById(R.id.my_resources_header);
        noSourcesSelected = (RelativeLayout) getActivity().findViewById(R.id.webview_error);
        db = new DataBaseAdapter(getActivity().getApplicationContext());
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        chooseSources = (RelativeLayout) getActivity().findViewById(R.id.reload_webivew);
        gifMovieView = (GifMovieView) getActivity().findViewById(R.id.my_sources_gifa);
        deleteAll = (LinearLayout) getActivity().findViewById(R.id.my_sources_delete_all_view);
        home = (ImageView) getActivity().findViewById(R.id.my_sources_news_home_button);
        gifMovieView.bringToFront();
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        gifMovieView.setMovieResource(R.drawable.loading);
        gifMovieView.setVisibility(View.VISIBLE);
        try {
            setRedViewInDrawer();
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu.bringToFront();
        header.setTypeface(headerFont);

        subSelectedItems = new ArrayList<>();
        subSelectedItems = db.getSelectedBySub();

        geoSelectedItems = new ArrayList<>();
        geoSelectedItems = db.getSelectedByGeo();
        int maxSize = geoSelectedItems.size();
        geoSelectedItems.addAll(subSelectedItems);
//Popular...
        try {
            rankedList = db.getSourcesByRank();
            rankMax = rankedList.size();
            maxSize += rankMax;
//        rankedList.subList(rankedList.size(), 10);
            rankedList.addAll(geoSelectedItems);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        articleIds = db.getArticlesId();
        if (rankedList != null) {
            mGeo = new MyResourcesAdapter(getActivity(), rankedList, isGeo, maxSize, false, rankMax);
        } else {
            mGeo = new MyResourcesAdapter(getActivity(), geoSelectedItems, isGeo, maxSize, true, 0);
        }
        selectedSources = db.getSelectedSources();
        if(selectedSources == null || selectedSources.isEmpty()){
            deleteAll.setEnabled(false);
        }


        deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmationDialoge();
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.RIGHT);

            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new NewsHolderFragment(), "newsHolder");
                transaction.commit();
                try {
                    setRedViewInDrawer(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        chooseSources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R
                        .anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new CategoryFragment(), "category");
                transaction.commit();
                try {
                    setRedViewInDrawer(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        mGeo.setOnRowSelectedListener(new MyResourcesAdapter.OnRowSelectedListener() {
            @Override
            public void onSelected(boolean isGeo, int position) {
                if (isGeo) {
//                    geoSelectedItems.remove(position);
                    geolistView.removeViewAt(position);
                    mGeo.notifyDataSetChanged();

                }
//                mSub.notifyDataSetChanged();
            }
        });
//        geolistView.setAdapter(mGeo);

        new LongOperation().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


//        if (mGeo != null) {
//            mGeo.setOnRowSelectedListener(new MyResourcesAdapter.OnRowSelectedListener() {
//                @Override
//                public void onSelected(boolean isGeo) {
//                    mGeo.notifyDataSetChanged();
//                }
//            });
//        }


    }

    AlertDialogFrag commentFrag;
    private void showConfirmationDialoge() {
        FragmentManager fm = ((FragmentActivity)context).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        commentFrag = new AlertDialogFrag(context.getString(R.string.sources_delete_confirmation), context.getString(R.string.yes),
                context.getString(R.string.cancel), AlertDialogFrag.DIALOG_TWO_BUTTONS);
        commentFrag.show(ft, "dialog");
        commentFrag.setCancelable(false);
        commentFrag.OnDialogClickListener(new AlertDialogFrag.OnDialogListener() {
            @Override
            public void onPositiveClickListener() {
                callJson(URLs.DELETE_USER_SOURCES_URL, URLs.DELETE_USER_SOURCES_REQUEST_TYPE, URLs.getUserID());
            }

            @Override
            public void onNegativeClickListener() {
                commentFrag.dismiss();
            }
        });
    }

    private void callJson(String url, final int requestType, int userID) {
        pleaseWaitDialog();
        HashMap<String, String> dataObj = new HashMap<>();
        //if (requestType == URLs.GET_USER_SOURCES_REQUEST_TYPE || requestType == URLs.DELETE_USER_SOURCES_REQUEST_TYPE)
        dataObj.put(URLs.TAG_USER_SOURCES_USER_ID, userID + "");
        jsonParser = new JsonParser(getActivity(), url, requestType, dataObj);
        jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnUserDataDeletionListener() {
            @Override
            public void onFinished(boolean isDeleted) {
                commentFrag.dismiss();
                if (isDeleted) {
                    Toast.makeText(getApplicationContext(), "ØªÙ… Ù…Ø³Ø­ Ø§Ù„Ù…ØµØ§Ø¯Ø± Ø¨Ù†Ø¬Ø§Ø­", Toast.LENGTH_SHORT).show();
                    if (selectedSources != null && !selectedSources.isEmpty())
                        for (int i = 0; i < selectedSources.size(); i++) {
                            FirebaseMessaging.getInstance().unsubscribeFromTopic(selectedSources.get(i).getSource_id() + "");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic(selectedSources.get(i).getSource_id() + "U");

                        }
                    //FirebaseMessaging.getInstance().unsubscribeFromTopic("True");
                    //FirebaseMessaging.getInstance().unsubscribeFromTopic("Urgent");
                    geolistView.setVisibility(View.GONE);
                    noSourcesSelected.setVisibility(View.VISIBLE);
                    try {
                        db.clearMySources(rankedList);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        db.clearMySources(geoSelectedItems);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        rankedList.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        geoSelectedItems.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        mGeo.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "recentNews");
                        MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "hotNews");
                        MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "urgentNews");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Ø­Ø¯Ø« Ø®Ø·Ø£ ÙÙŠ Ø¹Ù…Ù„ÙŠØ© Ø§Ù„Ù…Ø³Ø­", Toast.LENGTH_SHORT).show();
                }
                /*final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pleaseWaitFragment.dismiss();
                    }
                }, 2000);*/
            }
        });
    }

    private void setRedViewInDrawer() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View redView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        View second = (View) vx.getChildAt(5).findViewById(R.id.red_view);
        View third = (View) vx.getChildAt(4).findViewById(R.id.red_view);
        View fourth = (View) vx.getChildAt(3).findViewById(R.id.red_view);
        View fifth = (View) vx.getChildAt(7).findViewById(R.id.red_view);

        redView.setVisibility(View.GONE);
        second.setVisibility(View.GONE);
        third.setVisibility(View.GONE);
        fourth.setVisibility(View.VISIBLE);
        fifth.setVisibility(View.GONE);
    }

    private void setRedViewInDrawer(boolean isHome) {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View newsFragmentRedView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        View categoryRedView = (View) vx.getChildAt(5).findViewById(R.id.red_view);
        View mySourcesRedView = (View) vx.getChildAt(3).findViewById(R.id.red_view);
        if (isHome) {
            newsFragmentRedView.setVisibility(View.VISIBLE);
            categoryRedView.setVisibility(View.GONE);
            mySourcesRedView.setVisibility(View.GONE);
        } else {
            newsFragmentRedView.setVisibility(View.GONE);
            categoryRedView.setVisibility(View.VISIBLE);
            mySourcesRedView.setVisibility(View.GONE);
        }
    }

    private void pleaseWaitDialog() {
        FragmentManager fm = MainActivity.mainActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog2");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        pleaseWaitFragment = new PleaseWaitFragment(getString(R.string.loading));
        try {
            pleaseWaitFragment.show(ft, "dialog2");
        } catch (Exception e) {
            e.printStackTrace();
        }
        pleaseWaitFragment.setCancelable(false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_sources, container, false);
    }

    private class LongOperation extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            for (int i = 0; i < 1; i++) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    Thread.interrupted();
                }
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            gifMovieView.setVisibility(View.GONE);
            gifMovieView.setPaused(true);

            if (result.equals("Executed")) {
                if (geoSelectedItems.isEmpty() && geoSelectedItems.isEmpty()) {
                    noSourcesSelected.setVisibility(View.VISIBLE);
                    chooseSources.setVisibility(View.VISIBLE);
                    chooseSources.bringToFront();
                    menu.bringToFront();
                    geolistView.setVisibility(View.GONE);
                    return;
                }
                geolistView.setAdapter(mGeo);
            }
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is up to you
        }

        @Override
        protected void onPreExecute() {
            gifMovieView.setVisibility(View.VISIBLE);
            gifMovieView.setPaused(false);
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

}


