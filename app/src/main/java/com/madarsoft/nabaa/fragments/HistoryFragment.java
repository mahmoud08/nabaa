package com.madarsoft.nabaa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.adapters.NewsAdapter;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.URLs;

import java.util.ArrayList;

import static com.madarsoft.nabaa.fragments.HotNewsFragment.listener;


/**
 * Created by Colossus on 09-May-17.
 */

public class HistoryFragment extends MadarFragment {

    private GifMovieView gif;
    private ListView newsList;
    private RelativeLayout noHistoryView;
    private LinearLayout deleteAll;
    private ImageView menu;
    private DrawerLayout mDrawerLayout;
    private DataBaseAdapter dataBaseAdapter;
    private ArrayList<News> newsArrayList;
    private NewsAdapter newsAdapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        gif = (GifMovieView) getActivity().findViewById(R.id.history_loading);
        gif.bringToFront();
        gif.setMovieResource(R.drawable.loading);
        gif.setVisibility(View.VISIBLE);
        gif.setPaused(false);
        newsList = (ListView) getActivity().findViewById(R.id.history_list);
        noHistoryView = (RelativeLayout) getActivity().findViewById(R.id.no_History_selected_layout);
        deleteAll = (LinearLayout) getActivity().findViewById(R.id.delete_all_view);
        menu = (ImageView) getActivity().findViewById(R.id.history_menu);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        newsList.setVisibility(View.VISIBLE);
        gif.setVisibility(View.GONE);
        noHistoryView.setVisibility(View.GONE);
        dataBaseAdapter = new DataBaseAdapter(getActivity());
        newsArrayList = dataBaseAdapter.getAllNews();
        newsAdapter = new NewsAdapter(MainActivity.mainActivity, getActivity(), newsArrayList, listener, false, true , null);
        if (newsArrayList.size() == 0) {
            newsList.setVisibility(View.GONE);
            noHistoryView.setVisibility(View.VISIBLE);
            deleteAll.setEnabled(false);
        } else
            newsList.setAdapter(newsAdapter);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    setRedViewInDrawer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mDrawerLayout.openDrawer(Gravity.RIGHT);
            }
        });

        deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmationDialoge();
            }
        });

        newsAdapter.setOnListIsEmpty(new NewsAdapter.OnListIsEmpty() {
            @Override
            public void isEmpty() {
                newsArrayList.clear();
                newsAdapter.notifyDataSetChanged();
                dataBaseAdapter.clearHistory();
                newsList.setVisibility(View.GONE);
                noHistoryView.setVisibility(View.VISIBLE);
            }
        });

    }

    AlertDialogFrag commentFrag;
    private void showConfirmationDialoge() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        commentFrag = new AlertDialogFrag(getActivity().getString(R.string.news_delete_confirmation), getActivity().getString(R.string.yes),
                getActivity().getString(R.string.cancel), AlertDialogFrag.DIALOG_TWO_BUTTONS);
        commentFrag.show(ft, "dialog");
        commentFrag.setCancelable(false);
        commentFrag.OnDialogClickListener(new AlertDialogFrag.OnDialogListener() {
            @Override
            public void onPositiveClickListener() {
                newsArrayList.clear();
                newsAdapter.notifyDataSetChanged();
                dataBaseAdapter.clearHistory();
                newsList.setVisibility(View.GONE);
                noHistoryView.setVisibility(View.VISIBLE);
                commentFrag.dismiss();
            }

            @Override
            public void onNegativeClickListener() {
                commentFrag.dismiss();
            }
        });
    }

    private void setRedViewInDrawer() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View redView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        View second = (View) vx.getChildAt(5).findViewById(R.id.red_view);
        View third = (View) vx.getChildAt(4).findViewById(R.id.red_view);
        View fourth = (View) vx.getChildAt(6).findViewById(R.id.red_view);
        View fifth = (View) vx.getChildAt(7).findViewById(R.id.red_view);

        redView.setVisibility(View.GONE);
        fifth.setVisibility(View.GONE);
        third.setVisibility(View.GONE);
        fourth.setVisibility(View.GONE);
        second.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.history, container, false);
    }
}
