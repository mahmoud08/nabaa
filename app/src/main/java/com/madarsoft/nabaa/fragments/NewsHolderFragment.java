package com.madarsoft.nabaa.fragments;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.NativeAd;
import com.appodeal.ads.NativeCallbacks;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.controls.AdsControlNabaa;
import com.madarsoft.nabaa.controls.ConnectionMonitor;
import com.madarsoft.nabaa.controls.CustomViewPager;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.controls.UpdateService2;
import com.madarsoft.nabaa.entities.URLs;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Colossus on 4/3/2016.
 */
public class NewsHolderFragment extends MadarFragment implements  NativeCallbacks {

    private static LinearLayout parent;
    private static boolean isEnabled;
    private static LinearLayout header;
    //   private RelativeLayout mostReadedLayout;
    //   private TextView mostReadedText;
    //  private ImageView mostReadedGreenView;
    private static CustomViewPager newsPager;
    private static NewsFragment newsFragment;
    private static NewsFragment urgentNews;
    private static HotNewsFragment hotNewsFragment;
    private ImageView menu;
    //    private ImageView newsHeader;
    private RelativeLayout latestLayout;
    private TextView latestText;
    private ImageView latestGreenView;
    private RelativeLayout hotLayout;
    private TextView hotText;
    public static boolean isUseAdMobAds=true;

    private ImageView hotGreenView;
    private android.support.v4.widget.DrawerLayout mDrawerLayout;
    private PagerAdapter mPagerAdapter;
    private RelativeLayout breakingLayout;
    private ImageView breakingView;
    private RelativeLayout breakingHeader;
    private LinearLayout topAdView;
    private LinearLayout bottomAdView;
    private AdsControlNabaa adsControl_bottom;
    private AdsControlNabaa adsControl_top;
    private SharedPreferences myPrefs;
    private SharedPreferences.Editor myPrefsEditor;
    private RecommendPopUp recommendPopUp;
    private ImageView history;
    private ImageView mySource;
    private ImageView addSource;
    private ImageView search;


    public static void setDimValue(boolean flag) {

        if (flag) {
            header.setAlpha(0.2f);
            parent.setAlpha(0.2f);
            isEnabled = true;
            newsPager.setPagingEnabled(false);

        } else {
            header.setAlpha(1f);
            parent.setAlpha(1f);
            isEnabled = false;
            newsPager.setPagingEnabled(true);
        }
    }

    private static String makeFragmentName(int viewPagerId, int index) {
        return "android:switcher:" + viewPagerId + ":" + index;
    }

    private void showRecommendedMsg() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("recommend_dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        recommendPopUp = new RecommendPopUp();

        try {
            recommendPopUp.show(ft, "recommend_dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
        recommendPopUp.setCancelable(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        URLs.setRecentFirstTimeStamp("0");
        URLs.setRecentLastTimeStamp("0");
        URLs.setUrgentFirstTimeStamp("0");
        URLs.setUrgentLastTimeStamp("0");

        URLs.setFirstDateTime("1970-01-01 16:48:03.947");
        Appodeal.setNativeCallbacks(this);
       // Appodeal.getNativeAds(10);
        Appodeal.cache(getActivity(), Appodeal.NATIVE, 1);

       // NativeAdBox nativeAdBox = Appodeal.getNativeAdBox();
        //nativeAdBox.setSize(5);
        NewsFragment.AllNativeAds=new ArrayList<>();
      //  nativeAdBox.setListener(this);
     //   nativeAdBox.load();
        parent = (LinearLayout) getActivity().findViewById(R.id.header);
        menu = (ImageView) getActivity().findViewById(R.id.new_holder_menu);
//        newsHeader = (ImageView) getActivity().findViewById(R.id.holder_header);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        header = (LinearLayout) getActivity().findViewById(R.id.relativeLayout);
        latestLayout = (RelativeLayout) getActivity().findViewById(R.id.latest_news_layout);
        latestText = (TextView) getActivity().findViewById(R.id.latest_news_text);
        latestGreenView = (ImageView) getActivity().findViewById(R.id.latest_news_green_view);

        hotLayout = (RelativeLayout) getActivity().findViewById(R.id.urgent_layout);
        hotText = (TextView) getActivity().findViewById(R.id.urgent_text);
        hotGreenView = (ImageView) getActivity().findViewById(R.id.urgent_green_view);
        newsPager = (CustomViewPager) getActivity().findViewById(R.id.news_pager);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        //Banner Ads View
        topAdView = (LinearLayout) getActivity().findViewById(R.id.news_holder_top_ad_view);
        bottomAdView = (LinearLayout) getActivity().findViewById(R.id.news_holder_bottom_ad_view);
        history = (ImageView) getActivity().findViewById(R.id.new_holder_history);
        mySource = (ImageView) getActivity().findViewById(R.id.new_holder_my_sources);
        addSource = (ImageView) getActivity().findViewById(R.id.new_holder_add_source);
        breakingLayout = (RelativeLayout) getActivity().findViewById(R.id.breaking_news_layout);
        breakingView = (ImageView) getActivity().findViewById(R.id.breaking_news_green_view);
        search = (ImageView) getActivity().findViewById(R.id.new_holder_search);
        myPrefs = getActivity().getSharedPreferences(MainActivity.PREFS_NAME, getActivity().MODE_PRIVATE);
        myPrefsEditor = getActivity().getSharedPreferences(MainActivity.PREFS_NAME, getActivity().MODE_PRIVATE).edit();

        if (myPrefs.getBoolean("recommend", true)) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        showRecommendedMsg();
                        myPrefsEditor.putBoolean("recommend", false).apply();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 30000);
        }
        newsFragment = NewsFragment.newInstance(2, "recentNews");
        urgentNews = NewsFragment.newInstance(1, "urgentNews");
        hotNewsFragment = HotNewsFragment.newInstance(0, "hotNews");
        adsControl_bottom = new AdsControlNabaa(getActivity());
        adsControl_top = new AdsControlNabaa(getActivity());
        try {
            setRedViewInDrawer();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //latestGreenView.setVisibility(View.VISIBLE);
        latestLayout.setBackgroundColor(Color.parseColor("#AE111C"));
        mPagerAdapter = new NewsPageAdapter(getChildFragmentManager());
        newsPager.setAdapter(mPagerAdapter);
//        newsPager.setOffscreenPageLimit(0);

        try {
            newsPager.setOffscreenPageLimit(3);
        } catch (IllegalStateException e) {
        }
        newsPager.setCurrentItem(newsPager.getAdapter().getCount() - 1, false);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEnabled)
                    return;
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                try {
                    setRedViewInDrawer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.replace(R.id.parent, new HistoryFragment(), "history");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        addSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.replace(R.id.parent, new CategoryFragment(), "category");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        mySource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.replace(R.id.parent, new MySources(), "myResources");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.add(R.id.parent, new SearchFragment(), "search");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        adsControl_top.getBannerAd(topAdView, "newsholder");
        adsControl_top.getSplashAd( "newsholder");
    /*    RectangleBannerAd banner = (new RectangleBannerAd.Builder()).adContainer(topAdView)
                .build();
        adsControl_top.getBannerAd(banner, "newsholder");
       banner.setAdListener(new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                topAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdError() {
                topAdView.setVisibility(View.GONE);
            }
        });*/
//        newsHeader.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), NativeAdActivity.class);
//                getActivity().startActivity(intent);
//                FragmentManager manager = getActivity().getSupportFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
//                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
//                transaction.replace(R.id.parent, new DefaultSourcesFragment(), "");
//                transaction.addToBackStack(null);
//                transaction.commit();
//            }
//        });
        adsControl_bottom.getBannerAd(bottomAdView, "newsholder");
      /*  RectangleBannerAd banner_bottom = (new RectangleBannerAd.Builder()).adContainer(bottomAdView)
                .build();
        adsControl_bottom.getBannerAd(banner, "newsholder");
        banner_bottom.setAdListener(new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                bottomAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdError() {
                bottomAdView.setVisibility(View.GONE);

            }
        });*/
        latestLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEnabled)
                    return;
                if (newsPager.getCurrentItem() == 2) {
                    try {
//                        NewsFragment.waterDropListView.smoothScrollToPosition(NewsFragment.waterDropListView.getTop());
//                        NewsFragment.waterDropListView.setSelectionAfterHeaderView();
//                        NewsFragment.waterDropListView.setSelection(0);
//                        MainControl.smoothScrollToPosition(NewsFragment.waterDropListView, 0);
//                        NewsFragment fragment = (NewsFragment) getFragmentManager().findFragmentByTag(makeFragmentName(newsPager.getId(),
//                                2));
//                        fragment.scrollToTop();
                        newsFragment.scrollToTop();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    newsPager.setCurrentItem(2, false);
                    setLatestPageSelected();
                }
            }
        });
        hotLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEnabled)
                    return;
                if (newsPager.getCurrentItem() == 0) {
                    MainControl.smoothScrollToPosition(HotNewsFragment.waterDropListView, 0);
                } else {
                    newsPager.setCurrentItem(0, false);
                    setHotPageSelected();
                }
            }
        });
        //breaking News Layout...
        breakingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEnabled)
                    return;
                if (newsPager.getCurrentItem() == 1) {
                    //change me ...
                    try {
                        urgentNews.scrollToTop();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    MainControl.smoothScrollToPosition(HotNewsFragment.waterDropListView, 0);
                } else {
                    newsPager.setCurrentItem(1, false);
                    setBreakingNewsPage();
                }
            }
        });
//        mostReadedLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                newsPager.setCurrentItem(MOST_READED_PAGE, false);
//                setBreakingNewsPage();
//            }
//        });
        newsPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        setHotPageSelected();
                        hotNewsFragment = HotNewsFragment.newInstance(0, "hotNews");
                        break;
                    case 1:
                        setBreakingNewsPage();
                        break;
                    case 2:
                        setLatestPageSelected();
                        break;
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
//                Toast.makeText(getActivity(), "me " + position, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }

        });

        MainControl mm = new MainControl(getContext());
        if (!mm.checkInternetConnection()) {
            IntentFilter mIntentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
            ConnectionMonitor conn = new ConnectionMonitor();
            getActivity().registerReceiver(conn, mIntentFilter);
        } else
            getActivity().startService(new Intent(getActivity().getBaseContext(), UpdateService2.class));
//        NewsFragment.task.interrupt();
    }

    private void setRedViewInDrawer() {
        /*ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View redView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        View x = (View) vx.getChildAt(3).findViewById(R.id.red_view);
        View second = (View) vx.getChildAt(5).findViewById(R.id.red_view);
        View third = (View) vx.getChildAt(4).findViewById(R.id.red_view);
        View fourth = (View) vx.getChildAt(6).findViewById(R.id.red_view);
        View fifth = (View) vx.getChildAt(7).findViewById(R.id.red_view);
        View eighth = (View) vx.getChildAt(8).findViewById(R.id.red_view);

        redView.setVisibility(View.VISIBLE);
        second.setVisibility(View.GONE);
        third.setVisibility(View.GONE);
        fourth.setVisibility(View.GONE);
        x.setVisibility(View.GONE);
        fifth.setVisibility(View.GONE);
        eighth.setVisibility(View.GONE);*/

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
//        MainActivity.newsHolder = null;
        //URLs.setFirstNewsID(0);
//        NewsFragment.task.interrupt();
        try {
            getActivity().stopService(new Intent(getActivity().getBaseContext(), UpdateService2.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MainControl.writeCacheFile(getActivity(), new ArrayList(NewsFragment.adapter.newsList.subList(0, 25)), "recentNews");
                    if (HotNewsFragment.adapter.newsList.size() > 25)
                        MainControl.writeCacheFile(getActivity(), new ArrayList(HotNewsFragment.adapter.newsList.subList(0, 25)),
                        "hotNews");
                    else
                        MainControl.writeCacheFile(getActivity(),
                                new ArrayList(HotNewsFragment.adapter.newsList.subList(0, HotNewsFragment.adapter.newsList.size())),
                                "hotNews");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();*/

    }

    @Override
    public void onResume() {
        super.onResume();

//        ViewGroup.LayoutParams params = recommendPopUp.getDialog().getWindow().getAttributes();
//        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
//        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
//        recommendPopUp.getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void setLatestPageSelected() {
        /*latestGreenView.setVisibility(View.VISIBLE);
        hotGreenView.setVisibility(View.INVISIBLE);
        breakingView.setVisibility(View.INVISIBLE);*/
        latestLayout.setBackgroundColor(Color.parseColor("#AE111C"));
        hotLayout.setBackgroundColor(Color.BLACK);
        breakingLayout.setBackgroundColor(Color.BLACK);
        //mostReadedGreenView.setVisibility(View.INVISIBLE);
        Log.i("TAG", "Setting screen name: " + "RECENT NEWS");
        //mostReadedText.setTextColor(Color.parseColor("#5F8598"));
    }

    private void setHotPageSelected() {
        /*latestGreenView.setVisibility(View.INVISIBLE);
        breakingView.setVisibility(View.INVISIBLE);
        hotGreenView.setVisibility(View.VISIBLE);*/
        latestLayout.setBackgroundColor(Color.BLACK);
        hotLayout.setBackgroundColor(Color.parseColor("#AE111C"));
        breakingLayout.setBackgroundColor(Color.BLACK);
        Log.i("TAG", "Setting screen name: " + "HOT NEWS");
    }

    //
    private void setBreakingNewsPage() {
        /*latestGreenView.setVisibility(View.INVISIBLE);
        hotGreenView.setVisibility(View.INVISIBLE);
        breakingView.setVisibility(View.VISIBLE);*/
        latestLayout.setBackgroundColor(Color.BLACK);
        hotLayout.setBackgroundColor(Color.BLACK);
        breakingLayout.setBackgroundColor(Color.parseColor("#AE111C"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.news_holder, container, false);
    }

    public class NewsPageAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 3;


        public NewsPageAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
//            Log.d("Test Karim", "boom");
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
//            Log.d("Karim", "getItem");
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return hotNewsFragment;
                case 1: // Fragment # 0 - This will show FirstFragment different title

                    return urgentNews;
                case 2: // Fragment # 1 - This will show SecondFragment

                    return newsFragment;
                default:
                    return null;
            }
        }

        @Override
        public Parcelable saveState() {
            // Do Nothing
            return null;
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
            //do nothing here! no call to super.restoreState(arg0, arg1);
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }
    }



    @Override
    public void onNativeLoaded() {


            if (NewsFragment.AllNativeAds.size()<10)
            {
              isUseAdMobAds = false;
                NewsFragment.AllNativeAds.addAll(Appodeal.getNativeAds(1));
                Appodeal.cache(getActivity(), Appodeal.NATIVE, 1);

                //adapter.notifyDataSetChanged();
            }


    }

    @Override
    public void onNativeFailedToLoad() {
        try
        {
      //  Toast.makeText(getContext(), "failed", Toast.LENGTH_SHORT).show();
        Log.v("newsHolderFragment","native ads failure");

          //  if(newsFragment.adapter!=null ) {
          isUseAdMobAds = true;
      //  }


        }catch (NullPointerException  e)
        {

        }

    }



    @Override
    public void onNativeShown(NativeAd nativeAd) {

    }

    @Override
    public void onNativeClicked(NativeAd nativeAd) {

    }
}