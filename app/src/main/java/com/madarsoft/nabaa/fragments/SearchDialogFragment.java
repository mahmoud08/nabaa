package com.madarsoft.nabaa.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.adapters.SearchedSourcesAdapter;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static com.madarsoft.nabaa.fragments.MyFavFragment.gifMovieView;

/**
 * Created by Colossus on 02-May-17.
 */

public class SearchDialogFragment extends DialogFragment {
    public SearchedSourcesAdapter mSearch;
    int currentFirstVisibleItem = 0;
    int currentVisibleItemCount = 0;
    int totalItemCounts = 0;
    int currentScrollState = 0;
    boolean isLoading;
    private int mLastFirstVisibleItem;
    private EditText mEditText;
    private ImageView exitSearch;
    private ImageView clearText;
    private boolean isOpened = true;
    private ListView searchList;
    private JsonParser jsonParser;
    private boolean isScrolled;
    private int pageNos = 0;
    private List<Sources> myList;
    private boolean lastIndexReached;
    private List<Sources> mList;
    private HashSet<Integer> sourcesSet;

    public static SearchDialogFragment newInstance(String title) {
        SearchDialogFragment frag = new SearchDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.search_dialog, container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
//        WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
//        p.width = ViewGroup.LayoutParams.MATCH_PARENT;

        mEditText = (EditText) view.findViewById(R.id.search_dialog_edit_text);
        exitSearch = (ImageView) view.findViewById(R.id.search_dialog_category_exit_search);
        clearText = (ImageView) view.findViewById(R.id.search_dialog_clear_text);
        searchList = (ListView) view.findViewById(R.id.search_dialog_list);
        gifMovieView = (GifMovieView) view.findViewById(R.id.search_dialog_loading);
        gifMovieView.setMovieResource(R.drawable.gif_reload);
        gifMovieView.bringToFront();
        gifMovieView.setVisibility(View.GONE);
        sourcesSet = new HashSet<>();
        mList = new ArrayList<>();
        // Show soft keyboard automatically and request focus to field
        searchList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                currentScrollState = scrollState;
                isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                currentFirstVisibleItem = firstVisibleItem;
                currentVisibleItemCount = visibleItemCount;
                totalItemCounts = totalItemCount;
                if (mLastFirstVisibleItem < firstVisibleItem) {
                    Log.i("SCROLLING DOWN", "TRUE");
                }
                mLastFirstVisibleItem = firstVisibleItem;
            }
        });
        mEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        if (mEditText.getVisibility() == View.INVISIBLE) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation
                    .RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(200);
            anim.setFillAfter(true);
            mEditText.startAnimation(anim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
//                                setPadding(0);
                    mEditText.setVisibility(View.VISIBLE);
                    mEditText.requestFocus();
                    mEditText.bringToFront();

                    clearText.setVisibility(View.VISIBLE);
                    clearText.bringToFront();
                    exitSearch.setVisibility(View.VISIBLE);
                    exitSearch.bringToFront();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        }
        clearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditText.setText(null);
            }
        });

        exitSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            //exit
            public void onClick(View v) {
                hideKeyboard(mEditText, false, getActivity());
                getDialog().dismiss();
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });
            }
        });
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                MainControl mainControl = new MainControl(getActivity());
                if (!mainControl.checkInternetConnection()) {
                    Toast.makeText(getActivity(), R.string.network_error, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (s.toString().length() >= 3) {
                    if (s.toString() != null) {
                        gifMovieView.setVisibility(View.VISIBLE);
                        gifMovieView.setPaused(false);
                        pageNos = 0;
                        try {
                            mList.clear();
                            sourcesSet.clear();
                            mSearch.clear();
                            mSearch.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        callJson(URLs.getUserID(), getActivity(), s.toString(), 0);
                    }
                } else {
                    try {
                        mSearch.clear();
                        mSearch.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private void callJson(int userID, final Context context, String text, int pageNo) {
        MainControl mainControl = new MainControl(context);
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, "أنت غير متصل بالأنترنت", Toast.LENGTH_SHORT).show();
        }
        if (!lastIndexReached) {
            try {
                mSearch.clear();
                mSearch.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        HashMap<String, String> dataObj = new HashMap<>();


        dataObj.put(URLs.TAG_COUNTRIES_RESOURCES_USERID, userID + "");
        dataObj.put(URLs.TAG_ADD_COMMENT_COUNT, "10");
        try {
            dataObj.put(URLs.TAG_TEXT, URLDecoder.decode(text, "utf-8") + "");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        dataObj.put(URLs.TAG_PAGE, pageNo + "");
        jsonParser = new JsonParser(getActivity(), URLs.SOURCES_SEARCH, URLs.SEARCH_IN_SOURCES_WEB, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();

        jsonParser.onWebSourcesRetrieved(new JsonParser.OnWebSourcesRetrieved() {
            @Override
            public void onFinished(List<Sources> sourceList) {
//                if (sourceList.size() > 9)
                pageNos++;
                isLoading = false;
                if (lastIndexReached) {
//                    sourcesSet.addAll(sourceList);
//                    mList.addAll(sourceList);
//                    mList.addAll(sourcesSet);


                    for (Sources s : sourceList) {
                        if (sourcesSet.add(s.getSource_id()))
                            mList.add(s);
                    }

                    mSearch.notifyDataSetChanged();
                } else {
                    for (Sources s : sourceList) {
                        if (sourcesSet.add(s.getSource_id()))
                            mList.add(s);
                    }
//                    mList.addAll(sourceList);
//                    mList.addAll(sourcesSet);
                    mSearch = new SearchedSourcesAdapter(getActivity(), R.layout.searched_source_item, mList);
                    searchList.setAdapter(mSearch);
                }
                gifMovieView.setPaused(true);
                gifMovieView.setVisibility(View.GONE);
            }

        });
    }

    private void hideKeyboard(View view, boolean open, Context context) {
        if (view == null)
            return;
        if (open == false) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            isOpened = false;
        }
        if (open) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            isOpened = true;
        }
    }

    //
    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == 0 &&
                totalItemCounts == (currentFirstVisibleItem + currentVisibleItemCount)) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            MainControl mainControl = new MainControl(getContext());
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(getContext(), "أنت غير متصل بالأنترنت", Toast.LENGTH_SHORT).show();
            } else {
                lastIndexReached = true;
                if (!isLoading) {
                    callJson(URLs.getUserID(), getContext(), mEditText.getText().toString(), pageNos);
                    isLoading = true;
                }

            }
        }
//        if (isScrolled && currentFirstVisibleItem == 0 && newPostsLayout.getVisibility() == View
//                .VISIBLE) {
//            isScrolled = false;
//            if (!adapter.setviceNewsList.isEmpty()) {
//                if (!cachedNews.isEmpty()) {
//                    adapter.newsList = new ArrayList<>();
//                } else {
//                }
//                adapter.setviceNewsList.clear();
//                cachedNews.clear();
//                adapter.notifyDataSetChanged();
//            }
//        }
    }
}
