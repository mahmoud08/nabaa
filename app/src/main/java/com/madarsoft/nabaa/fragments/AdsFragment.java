//package com.madarsoft.nabaa.fragments;
//
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.facebook.ads.Ad;
//import com.facebook.ads.AdChoicesView;
//import com.facebook.ads.AdError;
//import com.facebook.ads.AdListener;
//import com.facebook.ads.AdSettings;
//import com.facebook.ads.MediaView;
//import com.facebook.ads.NativeAd;
//import com.madarsoft.firebasedatabasereader.objects.RectangleBannerAd;
//import com.madarsoft.nabaa.R;
//import com.madarsoft.nabaa.controls.AdsControlNabaa;
//
///**
// * Created by Colossus on 06-Apr-16.
// */
//public class AdsFragment extends Fragment {
//    View v;
//    private LinearLayout nativeAdContainer;
//    private LinearLayout adView;
//    private NativeAd nativeAd;
//    private AdChoicesView adChoicesView;
//    private ImageView nativeAdIcon;
//    private TextView nativeAdTitle;
//    private TextView nativeAdBody;
//    private TextView nativeAdSocialContext;
//    private Button nativeAdCallToAction;
//    private MediaView nativeAdMedia;
//    private AdsControlNabaa adsControl;
//    private ImageView adCover;
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        adsControl = new AdsControlNabaa(getActivity());
//
//        // Create native UI using the ad metadata.
//        nativeAdIcon = (ImageView) getActivity().findViewById(R.id.native_ad_icon);
//        nativeAdTitle = (TextView) getActivity().findViewById(R.id.native_ad_title);
//        nativeAdBody = (TextView) getActivity().findViewById(R.id.native_ad_body);
//        nativeAdMedia = (MediaView) getActivity().findViewById(R.id.native_ad_media);
//        nativeAdSocialContext = (TextView) getActivity().findViewById(R.id.native_ad_social_context);
//        nativeAdCallToAction = (Button) getActivity().findViewById(R.id.native_ad_call_to_action);
//        adCover = (ImageView) getActivity().findViewById(R.id.ad_cover);
//        adView = (LinearLayout) getActivity().findViewById(R.id.ad_view);
//        nativeAdCallToAction.setBackgroundColor(Color.parseColor("#a4c639"));
//
//        RectangleBannerAd banner = (new RectangleBannerAd.Builder()).adContainer(adView).adBodyTextView(nativeAdBody)
//                .adCallToActionButton(nativeAdCallToAction).adIconImageView(nativeAdIcon).adTitleTextView(nativeAdTitle)
//                .adContainerImageView(adCover)
//                .adMediaMediaView(nativeAdMedia).adSocialContextTextView(nativeAdSocialContext)
//                .build();
//
//        adsControl.getNativeAd(banner, 10);//        showNativeAd();
////        showNativeAd();
//    }
//
//    private void showNativeAd() {
//////        AdSettings.addTestDevice("9b53b12f60e2e305749557d39d2ac555");
//        AdSettings.addTestDevice("966e63cdb73df5523e545f4ed9621312");
////        1203178556381997_1250645504968635
//        nativeAd = new NativeAd(getActivity(), "1203178556381997_1250645504968635");
//
////        nativeAd = new NativeAd(getActivity(), "464488780344323_710228929103639");
////        nativeAd = new NativeAd(getActivity(), getString(R.string.PLACEMENT_ID));
//        nativeAd.setAdListener(new AdListener() {
//            @Override
//            public void onAdClicked(Ad ad) {
//            }
//
//            @Override
//            public void onLoggingImpression(Ad ad) {
//
//            }
//
//            @Override
//            public void onError(Ad ad, AdError error) {
//                Log.d("FaceBook Error ", error.getErrorMessage());
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
//                if (ad != nativeAd) {
//                    return;
//                }
//                // Setting the Text.
//                nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
//                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//                nativeAdTitle.setText(nativeAd.getAdTitle());
//                nativeAdBody.setText(nativeAd.getAdBody());
//
////                 Downloading and setting the ad icon.
//                NativeAd.Image adIcon = nativeAd.getAdIcon();
//                NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);
//                // Download and setting the cover image.
//                NativeAd.Image adCoverImage = nativeAd.getAdCoverImage();
//                nativeAdMedia.setNativeAd(nativeAd);
//                // Add adChoices icon
//                if (adChoicesView == null) {
//                    adChoicesView = new AdChoicesView(getActivity(), nativeAd, true);
//                    adView.addView(adChoicesView, 0);
//                }
//
//                nativeAd.registerViewForInteraction(adView);
//
//            }
//        });
//
//        nativeAd.loadAd();
//    }
//
//    //
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        v = inflater.inflate(R.layout.ad_sample, container, false);
//        return v;
//    }
//
//}
//
//
//
