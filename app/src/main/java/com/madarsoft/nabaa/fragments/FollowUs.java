package com.madarsoft.nabaa.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.CustomWebView;
import com.madarsoft.nabaa.controls.MainControl;

/**
 * Created by Colossus on 09-Oct-16.
 */
public class FollowUs extends MadarFragment {

    private RelativeLayout fb;
    private RelativeLayout twitter;
    private RelativeLayout googlePlus;
    private RelativeLayout instagram;
    private MainControl mainControl;
    private ImageView menu;
    private DrawerLayout mDrawerLayout;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainControl = new MainControl(getActivity());
        fb = (RelativeLayout) getActivity().findViewById(R.id.follow_us_fb);
        twitter = (RelativeLayout) getActivity().findViewById(R.id.settings_twitter);
        googlePlus = (RelativeLayout) getActivity().findViewById(R.id.settings_g_plus);
        instagram = (RelativeLayout) getActivity().findViewById(R.id.settings_instagram);
        menu = (ImageView) getActivity().findViewById(R.id.follow_us_menu);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        try {
            setRedViewInDrawer();
        } catch (Exception e) {
            e.printStackTrace();
        }

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                try {
                    setRedViewInDrawer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/1598373943801358"));
                    startActivity(intent);
                } catch (Exception e) {
                    openUrl("https://www.facebook.com/nabaaapp", getString(R.string.fb_screen), getString(R.string.facebook));
                }
            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                openUrl("http://api.madarsoft.com/mobileads_NFcqDqu/v2/pages/page.aspx?programid=5&name=wemosaly", getString(R.string
// .twitter_screen), getString(R.string.twitter));
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("twitter://user?screen_name=nabaaapp"));
                    startActivity(intent);

                } catch (Exception e) {
                    openUrl("https://twitter.com/nabaaapp", getString(R.string.twitter_screen), getString(R.string.twitter));
                }
            }
        });

        googlePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                openUrl("http://api.madarsoft.com/mobileads_NFcqDqu/v2/pages/page.aspx?programid=5&name=wemosaly", getString(R.string
// .twitter_screen), getString(R.string.twitter));
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://plus.google.com/113987741247628781824"));
                    intent.setPackage("com.google.android.apps.plus");
                    startActivity(intent);
                } catch (Exception e) {
                    openUrl("https://google.com/+Nabaapp", getString(R.string.google_plus), getString(R.string.google_plus));
                }
            }
        });
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                openUrl("http://api.madarsoft.com/mobileads_NFcqDqu/v2/pages/page.aspx?programid=5&name=wemosaly", getString(R.string
// .twitter_screen), getString(R.string.twitter));
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://instagram.com/_u/nabaaapp"));
                    intent.setPackage("com.instagram.android");
                    startActivity(intent);

                } catch (Exception e) {
                    openUrl("https://www.instagram.com/nabaaapp/", getString(R.string.instagram), getString(R.string.instagram));
                }
            }
        });

    }

    private void setRedViewInDrawer() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View redView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        View second = (View) vx.getChildAt(5).findViewById(R.id.red_view);
        View third = (View) vx.getChildAt(4).findViewById(R.id.red_view);
        View fourth = (View) vx.getChildAt(6).findViewById(R.id.red_view);
        View fifth = (View) vx.getChildAt(7).findViewById(R.id.red_view);
        View eighth = (View) vx.getChildAt(12).findViewById(R.id.red_view);
        View ninth = (View) vx.getChildAt(9).findViewById(R.id.red_view);

        redView.setVisibility(View.GONE);
        second.setVisibility(View.GONE);
        third.setVisibility(View.GONE);
        fourth.setVisibility(View.GONE);
        fifth.setVisibility(View.GONE);
        eighth.setVisibility(View.VISIBLE);
        ninth.setVisibility(View.GONE);
    }

    private void openUrl(String url, String screenName, String arabicName) {
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }
//        FragmentManager manager = getActivity().getSupportFragmentManager();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
//        transaction.addToBackStack(null);
//        CustomWebView webview = new CustomWebView();
        Intent intent = new Intent(getActivity(), CustomWebView.class);
        Bundle b = new Bundle();
        b.putString("aboutUs", url);
        b.putString("screenName", screenName);
        b.putString("arabic", arabicName);
        intent.putExtras(b);
        startActivity(intent);
//        webview.setArguments(b);
//        transaction.replace(R.id.parent, webview);
//        transaction.commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.follow_us, container, false);
    }
}
