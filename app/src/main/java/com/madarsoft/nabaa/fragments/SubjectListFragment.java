package com.madarsoft.nabaa.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.analytics.HitBuilders;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.adapters.CategoryAdapter;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.URLs;

import java.util.ArrayList;
import java.util.HashMap;

public class SubjectListFragment extends MadarFragment {
    //    public static int generalPosition;
    public static ListView listView;
    private static GifMovieView gifMovieView;
    private static RelativeLayout notNetwork;
    MainControl mainControl;
    private DataBaseAdapter db;
    private RelativeLayout refresh;
    private com.google.android.gms.analytics.Tracker mTracker;

    private static void initializeListView(FragmentActivity activity) {
        listView = (ListView) activity.findViewById(R.id.subject_list_list_view);
    }

    public static void showData(Context context, DataBaseAdapter db) {
        gifMovieView.setVisibility(View.GONE);
        gifMovieView.setPaused(true);
        ArrayList<Category> categoryArrayList = db.getCategoryByCategoryType(2);

        CategoryAdapter myAdapter2 = new CategoryAdapter(context, R.layout.category_list_item, categoryArrayList, 2, false);
        listView.setAdapter(myAdapter2);
        myAdapter2.notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName(getString(R.string.category_list));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        initializeListView(getActivity());
        db = new DataBaseAdapter(getActivity().getApplicationContext());
        mainControl = new MainControl(getActivity());

        notNetwork = (RelativeLayout) getActivity().findViewById(R.id.subj_no_network);
        refresh = (RelativeLayout) getActivity().findViewById(R.id.subj_no_network_button);

        gifMovieView = (GifMovieView) getActivity().findViewById(R.id.subject_loading);
        gifMovieView.bringToFront();
        gifMovieView.setMovieResource(R.drawable.loading);

        String[] categoryList = new String[10];
        for (int i = 0; i < categoryList.length; i++) {
            categoryList[i] = "Subject Number " + i;
        }
        displayData();

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainControl.checkInternetConnection()) {
                    notNetwork.setVisibility(View.GONE);
                    displayData();

                } else {
                    Toast.makeText(getActivity(), "No Network ", Toast.LENGTH_SHORT).show();

                }
            }
        });


    }

    private void displayData() {

        ArrayList<Category> countryArrayList = db.getCategoryByCategoryType(2);
        if (countryArrayList.size() == 0) {

            callJson(0, getActivity(), 0);
        } else {
            showData(getActivity(), db);
            long highest = countryArrayList.get(0).getTime_stamp();
            int highestIndex = 0;

            for (int s = 1; s < countryArrayList.size(); s++) {
                long curValue = countryArrayList.get(s).getTime_stamp();
                if (curValue > highest) {
                    highest = curValue;
                    highestIndex = s;
                }
            }
            callJson(highest, getActivity(), countryArrayList.size());
        }
    }

    private void callJson(long timeStamp, Context context, int size) {

        MainControl mainControl = new MainControl(context);
        if (!mainControl.checkInternetConnection()) {
            //Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
            if (size == 0)
                notNetwork.setVisibility(View.VISIBLE);
            return;
        }
        if (size == 0) {
            gifMovieView.setVisibility(View.VISIBLE);
            gifMovieView.setPaused(false);
        }

        HashMap<String, String> dataObj = new HashMap<String, String>();
        dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, timeStamp + "");
        new JsonParser(getActivity(), URLs.GET_CATEGORIES_URL, URLs.GET_CATEGORIES_REQUEST_TYPE, dataObj).execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.subject_list, container, false);
    }

}
