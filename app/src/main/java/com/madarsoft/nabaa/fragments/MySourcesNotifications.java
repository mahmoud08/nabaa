package com.madarsoft.nabaa.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.firebase.messaging.FirebaseMessaging;
import com.madarsoft.nabaa.GCM.RegisterApp;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.OnOffSwitch;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.adapters.NotificationsAdapter;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Profile;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Colossus on 07-Sep-16.
 */
@SuppressLint("ValidFragment")
public class MySourcesNotifications extends MadarFragment {
    private final int type;
    private ListView sourcesList;
    private DrawerLayout mDrawerLayout;
    private DataBaseAdapter dp;
    private NotificationsAdapter adapter;
    private TextView dynamicText;
    private boolean isGeneralNotification;
    private ImageView home;
    private ArrayList<Profile> profile;
    private RelativeLayout noSourcesInCountry;
    private RelativeLayout chooseSources;
    private ArrayList<Sources> subSelectedItems;
    private ArrayList<Sources> geoSelectedItems;
    private View greyView;
    private OnOffSwitch generalNotify;
    private MainControl ma;
    private Profile objProfile;

    @SuppressLint("ValidFragment")
    public MySourcesNotifications(int type) {
        this.type = type;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sourcesList = (ListView) getActivity().findViewById(R.id.my_sources_notification_list);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        home = (ImageView) getActivity().findViewById(R.id.my_sources_news_home_button);

        dynamicText = (TextView) getActivity().findViewById(R.id.my_sources_notification_dynamic_text);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        noSourcesInCountry = (RelativeLayout) getActivity().findViewById(R.id.webview_error);
        chooseSources = (RelativeLayout) getActivity().findViewById(R.id.reload_webivew);
        greyView = (View) getActivity().findViewById(R.id.notification_view);
        generalNotify = (OnOffSwitch) getActivity().findViewById(R.id.notify_on_off_general);
        ma = new MainControl(getActivity());


        dp = new DataBaseAdapter(getActivity());

        profile = new ArrayList<>();
        profile = dp.getAllProfiles();

        objProfile = profile.get(0);
        if (type == 1) {// التنبيهات العادية = 1
            dynamicText.setText(getResources().getString(R.string.enable_notification));
            isGeneralNotification = true;
            if (objProfile.getSoundType() > 0) {
                generalNotify.setSwitch(true);
//            isGeneralClosed = false;
            } else {
                generalNotify.setSwitch(false);
                greyView.setVisibility(View.VISIBLE);
                greyView.bringToFront();
//            generalNotify.setSwitch(false);
//            isGeneralClosed = true;
            }

        } else {// التنبيهات العاجلة
            dynamicText.setText(getResources().getString(R.string.notify_urgent));
            isGeneralNotification = false;
            if (objProfile.getUrgentFlag() > 0) {
                generalNotify.setSwitch(true);
//            isUrgentClosed = false;
            } else {
                generalNotify.setSwitch(false);
                greyView.setVisibility(View.VISIBLE);
                greyView.bringToFront();
//            isUrgentClosed = true;
            }
        }
        subSelectedItems = new ArrayList<>();
        subSelectedItems = dp.getSelectedBySub();
        int maxSize = dp.getSelectedByGeo().size();
        geoSelectedItems = new ArrayList<>();
        geoSelectedItems = dp.getSelectedByGeo();
        geoSelectedItems.addAll(subSelectedItems);
        if (geoSelectedItems.isEmpty()) {
            sourcesList.setVisibility(View.GONE);
            noSourcesInCountry.setVisibility(View.VISIBLE);
        } else {
            sourcesList.setVisibility(View.VISIBLE);
            noSourcesInCountry.setVisibility(View.GONE);
        }

        adapter = new NotificationsAdapter(getActivity(), geoSelectedItems, isGeneralNotification, maxSize);
        sourcesList.setAdapter(adapter);

        generalNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ma.checkInternetConnection()) {
                    Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                    return;
                }
                if (type == 1) {
                    if (objProfile.getSoundType() > 0) {
                        generalNotify.setSwitch(false);
                        greyView.setVisibility(View.VISIBLE);
                        greyView.bringToFront();
                        objProfile.setSoundType(-1);
                        dp.updateProfiles(objProfile);
                        adapter.notifyDataSetChanged();
                        FirebaseMessaging.getInstance().unsubscribeFromTopic("True");
                        //FirebaseMessaging.getInstance().subscribeToTopic("true");
                        new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "True");
                        addOrRemoveNotification();
                    } else {
                        FirebaseMessaging.getInstance().subscribeToTopic("True");
                        generalNotify.setSwitch(true);
                        greyView.setVisibility(View.GONE);
                        objProfile.setSoundType(1);
                        dp.updateProfiles(objProfile);
                        adapter.notifyDataSetChanged();
                        addOrRemoveNotification();
                    }
                } else {
                    if (objProfile.getUrgentFlag() > 0) {
                        generalNotify.setSwitch(false);
                        greyView.setVisibility(View.VISIBLE);
                        greyView.bringToFront();
                        objProfile.setUrgentFlag(-1);
                        dp.updateProfiles(objProfile);
                        adapter.notifyDataSetChanged();
                        FirebaseMessaging.getInstance().unsubscribeFromTopic("Urgent");
                        new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "Urgent");
                        addOrRemoveNotification();
                    } else {
                        FirebaseMessaging.getInstance().subscribeToTopic("Urgent");
                        generalNotify.setSwitch(true);
                        greyView.setVisibility(View.GONE);
                        objProfile.setUrgentFlag(1);
                        dp.updateProfiles(objProfile);
                        adapter.notifyDataSetChanged();
                        addOrRemoveNotification();
                    }
                }
            }
        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new NewsHolderFragment(), "newsHolder");
                transaction.commit();
                try {
                    setRedViewInDrawer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        chooseSources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new CategoryFragment());
                transaction.commit();

            }
        });

        greyView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void addOrRemoveNotification() {
        if (!ma.checkInternetConnection()) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        } else {

            HashMap<String, String> dataObj = new HashMap<>();

            dataObj.put(URLs.TAG_USERID, URLs.getUserID() + "");
            if (type == 1) {
                if (objProfile.getSoundType() > 0) {
                    dataObj.put(URLs.TAG_REQUEST_NORMAL, "TRUE");
                    if (objProfile.getUrgentFlag() > 0)
                        dataObj.put(URLs.TAG_REQUEST_URGENT, "TRUE");
                    else
                        dataObj.put(URLs.TAG_REQUEST_URGENT, "FALSE");

                } else {
                    dataObj.put(URLs.TAG_REQUEST_NORMAL, "FALSE");
                    if (objProfile.getUrgentFlag() > 0)
                        dataObj.put(URLs.TAG_REQUEST_URGENT, "TRUE");
                    else
                        dataObj.put(URLs.TAG_REQUEST_URGENT, "FALSE");
                }
            } else {
                if (objProfile.getUrgentFlag() > 0) {
                    dataObj.put(URLs.TAG_REQUEST_URGENT, "TRUE");
                    if (objProfile.getSoundType() > 0)
                        dataObj.put(URLs.TAG_REQUEST_NORMAL, "TRUE");
                    else
                        dataObj.put(URLs.TAG_REQUEST_NORMAL, "FALSE");
                } else {
                    dataObj.put(URLs.TAG_REQUEST_URGENT, "FALSE");
                    if (objProfile.getSoundType() > 0)
                        dataObj.put(URLs.TAG_REQUEST_NORMAL, "TRUE");
                    else
                        dataObj.put(URLs.TAG_REQUEST_NORMAL, "FALSE");
                }
            }
            JsonParser jsonParser = new JsonParser(getActivity(), URLs.NOTIFICATION_HOLDER, URLs.HOLDER_NOTIFICATION_TYPE, dataObj);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                @Override
                public void onFinished() {

                }

                @Override
                public void onFinished(List<Integer> likeResponse) {

                }

                @Override
                public void onFinished(News newsObj) {

                }

                @Override
                public void onFinished(ArrayList<Sources> sourceList) {

                }

                @Override
                public void onFinished(int id) {

                }

                @Override
                public void onFinished(boolean success) {
//                    Toast.makeText(getActivity(), success + "", Toast.LENGTH_LONG).show();
                }
            });
        }
    }


    private void setRedViewInDrawer() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View newsFragmentRedView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        View NotificationRed = (View) vx.getChildAt(7).findViewById(R.id.red_view);
        newsFragmentRedView.setVisibility(View.VISIBLE);
        NotificationRed.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_sources_notification, container, false);
    }

    class NotificationSubscription extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final GcmPubSub pubSub = GcmPubSub.getInstance(getActivity().getApplicationContext());
            final SharedPreferences prefs = getActivity().getSharedPreferences(MainActivity.MyPREFERENCES,
                    Context.MODE_PRIVATE);
            String token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
            try {
                pubSub.unsubscribe(token, "/topics/" + params[0]);
                Log.d("topicGCM ", "Subscribed");
            } catch (IOException e) {

            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals("Executed")) {

            }
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }
    }

}
