package com.madarsoft.nabaa.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.analytics.HitBuilders;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.adapters.CategoryAdapter;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.URLs;

import java.util.ArrayList;
import java.util.HashMap;

public class CountryListFragment extends MadarFragment {

    public static int generalPosition;
    private static ListView listView;
    private static GifMovieView gifMovieView;
    private static RelativeLayout notNetwork;
    private static CategoryAdapter myAdapter2;
    private static int myPosition;
    private static SharedPreferences sharedpreferences;
    private static ArrayList<Category> myList;
    MainControl mainControl;
    private DataBaseAdapter db;
    private RelativeLayout refresh;
    private com.google.android.gms.analytics.Tracker mTracker;

    private static void initializeListView(FragmentActivity activity) {
        listView = (ListView) activity.findViewById(R.id.country_list_view);
    }

    public static void showData(Context context, DataBaseAdapter db) {
        gifMovieView.setVisibility(View.GONE);
        gifMovieView.setPaused(true);
        ArrayList<Category> countryArrayList = db.getCategoryByCategoryType(1);
        myList = new ArrayList<Category>();
        String userIso = sharedpreferences.getString("ISO", "");
        try {
            for (int i = 0; i < countryArrayList.size(); i++) {
                if (countryArrayList.get(i).getIso().equalsIgnoreCase(userIso)) {
                    myList.add(countryArrayList.get(i));
                    countryArrayList.remove(i);
                    //                countryArrayList.add(0, countryArrayList.get(i));
                }
            }
//            countryArrayList.addAll(myList);
            myList.addAll(countryArrayList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        myAdapter2 = new CategoryAdapter(context, R.layout.category_list_item, myList, 1, false);
        listView.setAdapter(myAdapter2);
        if (listView.getCount() == 0) {
            callJson(0, context, 0);
        }
//        myAdapter2.setOnSourceUpdated(new CategoryAdapter.OnSourcesSelected() {
//            @Override
//            public void onFinished(int index) {
//                myPosition = index;
//                listView.setSelection(index);
//                myAdapter2.notifyDataSetChanged();
//            }
//        });
    }

    private static void callJson(long timeStamp, Context context, int size) {

        MainControl mainControl = new MainControl(context);
        if (!mainControl.checkInternetConnection()) {
            //Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
            if (size == 0)
                notNetwork.setVisibility(View.VISIBLE);
            return;
        }
        if (size == 0) {
            gifMovieView.setVisibility(View.VISIBLE);
            gifMovieView.setPaused(false);
        }

        HashMap<String, String> dataObj = new HashMap<String, String>();
        dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, timeStamp + "");
        new JsonParser(context, URLs.GET_COUNTRIES_URL, URLs.GET_COUNTRIES_REQUEST_TYPE, dataObj).execute();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName(getString(R.string.country_list));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        db = new DataBaseAdapter(getActivity().getApplicationContext());
        initializeListView(getActivity());
        mainControl = new MainControl(getActivity());
        sharedpreferences = getActivity().getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        notNetwork = (RelativeLayout) getActivity().findViewById(R.id.country_no_network);
        refresh = (RelativeLayout) getActivity().findViewById(R.id.no_network_button);

        gifMovieView = (GifMovieView) getActivity().findViewById(R.id.country_loading);
        gifMovieView.bringToFront();
        gifMovieView.setMovieResource(R.drawable.loading);

        String[] arr = new String[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = "Country Number " + i;
        }
        displayData();
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainControl.checkInternetConnection()) {
                    notNetwork.setVisibility(View.GONE);
                    displayData();
                } else {
                    Toast.makeText(getActivity(), "No Network ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void displayData() {
        ArrayList<Category> countryArrayList = db.getCategoryByCategoryType(1);
        if (countryArrayList.size() == 0) {
            callJson(0, getActivity(), 0);

        } else {
            showData(getActivity(), db);
            long highest = countryArrayList.get(0).getTime_stamp();
            int highestIndex = 0;

            for (int s = 1; s < countryArrayList.size(); s++) {
                long curValue = countryArrayList.get(s).getTime_stamp();
                if (curValue > highest) {
                    highest = curValue;
                    highestIndex = s;
                }
            }
            callJson(highest, getActivity(), countryArrayList.size());
            System.out.println("highest fitness = " + highest + " indoexOf = " + highestIndex);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        Toast.makeText(getActivity(), "Country on Resume", Toast.LENGTH_SHORT).show();
//        listView.setSelection(generalPosition);
//        listView.smoothScrollToPosition(generalPosition);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.country_list, container, false);
    }

}
