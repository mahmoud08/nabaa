package com.madarsoft.nabaa.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.madarsoft.nabaa.R;

@SuppressLint("ValidFragment")
public class AlertDialogFrag extends DialogFragment {

    public static int DIALOG_ONE_BUTTON = 1;
    public static int DIALOG_TWO_BUTTONS = 2;
    String title;
    String okey;
    String cancel;
    int type;
    private OnDialogListener onDialogListener;

    @SuppressLint("ValidFragment")
    public AlertDialogFrag(String title, String okey, String cancel, int type) {
        this.title = title;
        this.okey = okey;
        this.cancel = cancel;
        this.type = type;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation2;
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.retrieve_dialog, container, false);
        TextView warningText = (TextView) v.findViewById(R.id.warning_text);
        TextView warningOK = (TextView) v.findViewById(R.id.warning_ok);
        TextView warningCancel = (TextView) v.findViewById(R.id.warning_cancel);
        warningText.setText(title);
        warningCancel.setText(cancel);
        warningOK.setText(okey);
        if (type == DIALOG_ONE_BUTTON)
            warningOK.setVisibility(View.GONE);
        else
            warningOK.setVisibility(View.VISIBLE);

        warningOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogListener.onPositiveClickListener();
            }
        });
        warningCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogListener.onNegativeClickListener();
            }
        });

        return v;
    }

    public void OnDialogClickListener(OnDialogListener onDialogListener) {
        this.onDialogListener = onDialogListener;
    }

    public interface OnDialogListener {
        public void onPositiveClickListener();

        public void onNegativeClickListener();
    }
}