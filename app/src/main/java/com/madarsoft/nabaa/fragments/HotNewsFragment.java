package com.madarsoft.nabaa.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.analytics.HitBuilders;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.Views.WaterDropListView;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.adapters.NewsAdapter;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.controls.SpeedScrollListener;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.interfaces.OnListIsFull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by Colossus on 4/3/2016.
 */
public class HotNewsFragment extends MadarFragment implements WaterDropListView.IWaterDropListViewListener, AbsListView.OnScrollListener {

    public static RelativeLayout newPostsLayout;
    public static List<News> tempList;
    public static WaterDropListView waterDropListView;
    public static SpeedScrollListener listener;
    // private static Context context;
    public static NewsAdapter adapter = null;
    public static int pageNo;
    private static Handler handler = new Handler();
    private static Thread thread;
    private static boolean isRecent;
    private static RelativeLayout noSourcesSelectedLayout;
    private static boolean noSources;
    private static boolean firstTimeFlag;
    private static List<News> newsList;
    private static List<News> permNewsList;
    private static RelativeLayout chooseSources;
    private static RelativeLayout newsContainer;
    private static RelativeLayout refresh;
    private static OnListIsFull onListIsFull;
    private static GifMovieView gifMovieView;
    private static Context fragContext;
    private static Animation animShow;
    private static Animation animHide;
    private static boolean isScrolled;
    private static FontTextView noSourcesText;
    private static boolean isProcessingJson;
    private static RelativeLayout noNetwork;
    private static int tempID;
    private static boolean isRefresh;
    private static Toast noNews;
    private static int blocked;
    private static DataBaseAdapter dp;
    //private static List<News> cachedNews = new ArrayList<>();
    int currentFirstVisibleItem = 0;
    int currentVisibleItemCount = 0;
    int totalItemCount = 0;
    int currentScrollState = 0;
    Toast noConnToast;
    private Handler mHandler;
    private ScheduledExecutorService worker;
    private BroadcastReceiver receiver;
    private boolean flag;
    private PackageInfo pInfo;
    private JsonParser jsonParesr;
    private int mLastFirstVisibleItem;
    private CategoryFragment categoryFragment;

    private int lastID;
    private com.google.android.gms.analytics.Tracker mTracker;
    private DrawerLayout mDrawerLayout;

    private static void blockImage(List<News> list, Context context) {
        try {
            blocked = dp.getAllProfiles().get(0).getBlockImg();
//        NativeAd nativeAd = new NativeAd(getActivity(), "464488780344323_710228929103639");

            if (blocked > 0) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setIsBlocked(1);
                }
            } else {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setIsBlocked(-1);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void initializWaterDropListView(HotNewsFragment newsFrag, Activity activity) {


        waterDropListView = (WaterDropListView) activity.findViewById(R.id.urgent_news_list_view);
        newPostsLayout = (RelativeLayout) activity.findViewById(R.id.urgent_news_holder_new_posts);
        noSourcesSelectedLayout = (RelativeLayout) activity.findViewById(R.id.urgent_no_sources_selected_layout);
        noSourcesText = (FontTextView) activity.findViewById(R.id.urgent_no_sources_text);
        waterDropListView.setWaterDropListViewListener(newsFrag);
        waterDropListView.setPullLoadEnable(true);
        chooseSources = (RelativeLayout) activity.findViewById(R.id.urgent_news_choose_sources);
        newsContainer = (RelativeLayout) activity.findViewById(R.id.urgent_news_container);
        refresh = (RelativeLayout) activity.findViewById(R.id.urgent_no_network_button);

        try {
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case 1:
                            waterDropListView.stopRefresh();
                            break;
                        case 2:
                            waterDropListView.stopLoadMore();
                            break;
                    }
                }
            };
        } catch (IllegalStateException e) {
        }
        waterDropListView.setOnScrollListener(newsFrag);
    }

    public static void showData(final Context context, final List<News> newsLists) {

        permNewsList = newsLists;
        if (newsLists != null && noNetwork.getVisibility() == View.VISIBLE) {
            noNetwork.setVisibility(View.GONE);
            gifMovieView.setVisibility(View.VISIBLE);
            gifMovieView.setPaused(false);
        }
        blockImage(newsLists, context);
        tempList = new ArrayList<>();
        tempList = newsLists;

        try {
            FragmentActivity activity = (FragmentActivity) context;
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (newsLists == null || newsLists.isEmpty()) {
                        showNoSourcesView(context);
                        gifMovieView.setEnabled(false);
                        gifMovieView.setVisibility(View.GONE);
                        return;
                    }
                    noSourcesSelectedLayout.setVisibility(View.GONE);
                    adapter = new NewsAdapter(MainActivity.mainActivity, context, newsLists, listener, false, false , null);
                    //URLs.setFirsttUrgentNewsID(adapter.newsList.get(0).getID());
                    //URLs.setFirstDateTime(adapter.newsList.get(0).getDateTime());
                    //URLs.setLastUrgentNewsID(adapter.newsList.get(adapter.newsList.size() - 1).getID());
                    URLs.setLastDateTime(adapter.newsList.get(adapter.newsList.size() - 1).getDateTime());
                    waterDropListView.setAdapter(adapter);
                    waterDropListView.setVisibility(View.VISIBLE);
                    gifMovieView.setVisibility(View.GONE);
                    gifMovieView.setPaused(true);
                    /*try {
                        MainControl.writeCacheFile(context, newsLists, "hotNews");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void showNoSourcesView(Context context) {

        if (URLs.isTimoutExc())
            return;

        /*if (URLs.isTimoutExc())
            return;*/
        MainControl contol = new MainControl(context);
        /*if (!cachedNews.isEmpty())
            return;*/
        if (!contol.checkInternetConnection()) {
            gifMovieView.setVisibility(View.GONE);
            gifMovieView.setPaused(true);
            noNetwork.setVisibility(View.VISIBLE);
            return;
        }
        if (!dp.getSelectedSources().isEmpty()) {
            noSourcesText.setText(context.getString(R.string.no_news));
            chooseSources.setVisibility(View.INVISIBLE);
        }
        noSourcesSelectedLayout.setVisibility(View.VISIBLE);
    }

    public static void showData2(final Context context, final List<News> newsLists) {

        if (newsLists == null || newsLists.isEmpty()) {
            noNews.show();
            return;
        }
        blockImage(newsLists, context);
        FragmentActivity activity = (FragmentActivity) context;
        if (activity == null)
            return;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                isProcessingJson = false;
                if (!newsLists.isEmpty() && newsLists.get(newsLists.size() - 1).getID() != adapter.newsList.get(adapter.newsList.size() -
                        1).getID()) {//20848//21097
                    adapter.newsList.addAll(newsLists);
                    adapter.notifyDataSetChanged();
                    //URLs.setLastUrgentNewsID(adapter.newsList.get(adapter.newsList.size() - 1).getID());
                    URLs.setLastDateTime(adapter.newsList.get(adapter.newsList.size() - 1).getDateTime());
                }
            }
        });
    }

    public static HotNewsFragment newInstance(int i, String hotNews) {
        HotNewsFragment fragmentFirst = new HotNewsFragment();
//        Bundle args = new Bundle();
//        args.putInt("page", page);
//        args.putString("title", title);
//        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fragContext = getActivity();

        dp = new DataBaseAdapter(getActivity());
        try {
            blocked = dp.getAllProfiles().get(0).getBlockImg();
            pageNo = 0;
        } catch (Exception e) {
            e.printStackTrace();
        }

        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        mTracker.setScreenName(getString(R.string.hot_news_screen));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        flag = true;
        listener = new SpeedScrollListener();
        noNews = Toast.makeText(getActivity(), getString(R.string.no_other_news), Toast.LENGTH_SHORT);
        noConnToast = Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT);
        initAnimation();
        gifMovieView = (GifMovieView) getActivity().findViewById(R.id.urgent_news_feed_loading);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        gifMovieView.bringToFront();
        gifMovieView.setMovieResource(R.drawable.loading);

        noNetwork = (RelativeLayout) getActivity().findViewById(R.id.urgent_newslist_no_network);

        permNewsList = new ArrayList<>();
        newsList = new ArrayList<>();
        //callJson(URLs.getUserID(), 0, 25, true, getActivity());
        //getActivity().startService(new Intent(getContext(), UpdateService2.class));

        initializWaterDropListView(this, getActivity());

        chooseSources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new CategoryFragment());
                transaction.commit();
                try {
                    setRedViewInDrawer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        newsContainer.setVisibility(View.VISIBLE);
        //noSourcesSelectedLayout.setVisibility(View.GONE);
        waterDropListView.setVisibility(View.VISIBLE);
        noSources = false;

        final MainControl ma = new MainControl(getActivity());
        if (!ma.checkInternetConnection()) {

            //noNetwork.setVisibility(View.VISIBLE);
            refresh.bringToFront();
            waterDropListView.setVisibility(View.GONE);
            //noSourcesSelectedLayout.setVisibility(View.GONE);
        }

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ma.checkInternetConnection()) {
                    noNetwork.setVisibility(View.GONE);
                    newsContainer.setVisibility(View.VISIBLE);
                    waterDropListView.setVisibility(View.VISIBLE);
                    checkConnection();
                    pageNo = 0;
                    callJson(URLs.getUserID(), 25, true, getActivity());

                } else {
                    noConnToast.show();
                }
            }
        });

        newPostsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //adapter.addData(waterDropListView, newsList);
                //newPostsLayout.setVisibility(View.GONE);
                newPostsLayout.startAnimation(animHide);
                newPostsLayout.setVisibility(View.GONE);
                isScrolled = false;
                adapter.newsList.addAll(0, adapter.setviceNewsList);
                adapter.setviceNewsList.clear();
                if (waterDropListView.getFirstVisiblePosition() == 0) {
                    adapter.notifyDataSetChanged();
                } else {
                    MainControl.smoothScrollToPosition(waterDropListView, 0);
                    adapter.notifyDataSetChanged();
                }
            }

        });

        /*try {
            cachedNews = MainControl.readCachedFile(new File(getActivity().getCacheDir().getAbsoluteFile() + File.separator + "hotNews"));
            if (!cachedNews.isEmpty()) {
                blocked = dp.getAllProfiles().get(0).getBlockImg();
                if (blocked > 0) {
                    for (int i = 0; i < cachedNews.size(); i++) {
                        cachedNews.get(i).setIsBlocked(1);
                    }
                } else {
                    for (int i = 0; i < cachedNews.size(); i++) {
                        cachedNews.get(i).setIsBlocked(-1);
                    }
                }
                adapter = new NewsAdapter(MainActivity.mainActivity, getContext(), cachedNews, listener, false, false , null);
                //URLs.setFirstNewsID(adapter.newsList.get(0).getID());
                waterDropListView.setAdapter(adapter);
                waterDropListView.setVisibility(View.VISIBLE);
                flag = false;
            } else
                adapter.newsList.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        checkConnection();
    }

    private void callJson(int userID, final int countArticle, final boolean isNew, Context context) {

        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_USERID, userID + "");
        //dataObj.put(URLs.TAG_DATE_TIME, dateTime + "");
        dataObj.put(URLs.TAG_COUNT_ARTICLE, countArticle + "");
        dataObj.put(URLs.TAG_PAGE_NUM, pageNo + "");
        new JsonParser(getActivity(), URLs.GET_HOT_NEWS_URL, URLs.GET_HOT_NEWS_REQUEST_TYPE, dataObj).execute();
    }

    @Override
    public void onStart() {
        MainActivity.isDetail = true;
        super.onStart();
        /*LocalBroadcastManager.getInstance(getActivity()).registerReceiver((receiver),
                new IntentFilter(UpdateService.COPA_RESULT)
        );*/
    }

    private void setRedViewInDrawer() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View firstRedView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        firstRedView.setVisibility(View.GONE);
        View redView = (View) vx.getChildAt(5).findViewById(R.id.red_view);
        redView.setVisibility(View.VISIBLE);
    }

    private void checkConnection() {
        MainControl mainControl = new MainControl(fragContext);
        try {
            if (!mainControl.checkInternetConnection()) {
                noConnToast.show();
                gifMovieView.setVisibility(View.GONE);
                gifMovieView.setPaused(true);
                if (permNewsList.isEmpty())
                    noNetwork.setVisibility(View.VISIBLE);
                return;
            }
        } catch (NullPointerException e) {
        }
        // show loading on first screen opening only
        if (flag) {
            flag = false;
            gifMovieView.setVisibility(View.VISIBLE);
            gifMovieView.setPaused(false);
        }
    }

    @Override
    public void onStop() {
        //LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
        super.onStop();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (HotNewsFragment.adapter.newsList.size() > 25)
                        MainControl.writeCacheFile(getActivity(), new ArrayList(HotNewsFragment.adapter.newsList.subList(0, 25)),
                                "hotNews");
                    else
                        MainControl.writeCacheFile(getActivity(),
                                new ArrayList(HotNewsFragment.adapter.newsList.subList(0, HotNewsFragment.adapter.newsList.size())),
                                "hotNews");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onBackButtonPressed() {
        super.onBackButtonPressed();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.urgent_news_list_layout, container, false);
    }

    @Override
    public void onRefresh() {
        if (newPostsLayout.getVisibility() == View.VISIBLE) {
            newPostsLayout.startAnimation(animHide);
            newPostsLayout.setVisibility(View.GONE);
        }
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    //firstTimeFlag = true;
                    isRefresh = true;
                    checkConnection();
                    pageNo = 0;
                    callJson(URLs.getUserID(), 25, true, getActivity());

                    handler.sendEmptyMessage(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onLoadMore() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    checkConnection();
                    if (!isProcessingJson) {
                        pageNo++;
                        callJson(URLs.getUserID(), 25, false, getActivity());
                    }
                    handler.sendEmptyMessage(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItemCount = totalItemCount;

        if (mLastFirstVisibleItem < firstVisibleItem) {
            Log.i("SCROLLING DOWN", "TRUE");
            if (newPostsLayout.getVisibility() == View.VISIBLE) {
                newPostsLayout.startAnimation(animHide);
                newPostsLayout.setVisibility(View.GONE);
            }
        }
        if (mLastFirstVisibleItem > firstVisibleItem) {
            Log.i("SCROLLING UP", "TRUE");
            if (newPostsLayout.getVisibility() == View.GONE && isScrolled) {
                newPostsLayout.setVisibility(View.VISIBLE);
                newPostsLayout.startAnimation(animShow);
            }
        }
        mLastFirstVisibleItem = firstVisibleItem;
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE && this.totalItemCount ==
                (currentFirstVisibleItem + currentVisibleItemCount)) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            checkConnection();

            //callJson(URLs.getUserID(), 25, false, getActivity());
            if (!isProcessingJson) {
                isProcessingJson = true;
                pageNo++;
                //Toast.makeText(getActivity(), pageNo + "", Toast.LENGTH_SHORT).show();
                callJson(URLs.getUserID(), 25, false, getActivity());
            }
        }
        if (isScrolled && currentFirstVisibleItem == 0 && newPostsLayout.getVisibility() == View.VISIBLE) {
            newPostsLayout.startAnimation(animHide);
            adapter.notifyDataSetChanged();
            newPostsLayout.setVisibility(View.GONE);
            isScrolled = false;
        }
    }

    public OnListIsFull getOnListIsFull() {
        return onListIsFull;
    }

    public void setOnListIsFull(OnListIsFull onListIsFull) {
        this.onListIsFull = onListIsFull;
    }

    private void initAnimation() {
        animShow = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        animHide = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
    }

    public static class UrgentNewsReceiver extends BroadcastReceiver {
        List<News> list = new ArrayList<>();


        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                list = intent.getParcelableArrayListExtra("urgentNews");
                //String isNew = intent.getStringExtra("isNew");
                //String dateTime = intent.getStringExtra("dateTime");
                /*if (pageNo > 0) {
                    if (!list.isEmpty()) {
                        showData2(fragContext, list);
                        //tempID = articleID;
                        list.clear();
                    }
                    return;
                }*/
                if (blocked > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).setIsBlocked(1);
                    }
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).setIsBlocked(-1);
                    }
                }
                showData(fragContext, list);
            } catch (Exception e) {
            }
        }
    }
}

