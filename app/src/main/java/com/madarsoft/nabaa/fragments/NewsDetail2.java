package com.madarsoft.nabaa.fragments;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.analytics.HitBuilders;/*
import com.madarsoft.firebasedatabasereader.interfaces.AdViewLoadListener;
import com.madarsoft.firebasedatabasereader.objects.RectangleBannerAd;*/

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.firebase.messaging.FirebaseMessaging;
import com.madarsoft.nabaa.GCM.RegisterApp;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.ExpandableView;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.Views.OnOffSwitch;
import com.madarsoft.nabaa.Views.VideoEnabledWebChromeClient;
import com.madarsoft.nabaa.Views.VideoEnabledWebView;
import com.madarsoft.nabaa.activities.CustomWebView;
import com.madarsoft.nabaa.activities.LoginPopup;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.controls.AdsControlNabaa;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.History;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Profile;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * vc * Created by Colossus on 13-Apr-16.
 */
@SuppressLint("ValidFragment")
public class NewsDetail2 extends MadarFragment {

    public static final String MY_PREFS = "myPrefs";
    public final static String Azkar_FontSize = "font_size";
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
//    private static final String NEWS_URL = "http://nabaapp.com/innerNewsDetails/newsDetails.html?id='+ +'&hideImg=true";

    static int font_min = 20;
    static int font_max = 40;
    static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private static Animator mCurrentAnimator;
    private static int mShortAnimationDuration;
    private static ImageView expandedImageView;
    private static Rect startBounds;
    private static float startScaleFinal;
    //        private static ImageButton newsLogo;
    private static RelativeLayout header;
    private static FontTextView saveImg;
    private static PhotoViewAttacher mAttacher;
    private static News news = new News();
    private static ImageView sourceImg;
    private static LinearLayout sourceHeader;
    private final String defaultServerImg = "saveimagehttp://nabaapp.com/imagesinnerDetails/demoImage.png";
    SharedPreferences prefs;
    ImageView addToFav;
    ImageView report;
    ImageView openNewsUrl;
    OnDetailsUpdate onDetailsUpdate;
    MainControl mainControl;
    SimpleDateFormat print;
    String notificationArticleID = "";
    private Locale locale;
    //    private boolean fromLinks;
//    private String id;
    private ExpandableView actionsView;
    private LinearLayout likesView;
    private LinearLayout commentsView;
    private LinearLayout sharesView;
    private LinearLayout likeBtn;
    private TextView sourceTitle;
    private LinearLayout commentBtn;
    private LinearLayout shareBtn;
    private TextView sharingNumbers;
    private TextView newsTime;
    private ImageView commentImg;
    private TextView commentsNumber;
    private ImageView shareImg;
    private ImageView likeImg;
    private TextView shareTitle;
    private TextView commentTitle;
    private TextView likeTitle;
    private Animation pop;
    private TextView likesNumber;
    private ImageView increaseText;
    private ImageView decreaseText;
    //    private TextView newsDetails;
    private SharedPreferences myPrefs;
    private ArrayList<News> newsList;
    private ImageView back;
    //    private RelativeLayout moveToSourceView;
    private RelativeLayout bottomLayout;
    //    private TextView newsTitle;
    private JsonParser jsonParser;
    private int index;
    //    private GifMovieView gifMovieView;
    private SharedPreferences.Editor prefsEditor;
    private ProgressBar progBar;
    //    private FontTextView copyRight;
    private Toast errotToast;
    private Toast successToast;
    private ProgressBar progressBar;
    private FontTextView urgentLabel;
    //    private ImageView reload;
    private Rect finalBounds;
    private Point globalOffset;
    //    private GifMovieView loading;
    private NewsDetail2 newsDetail;
    private com.google.android.gms.analytics.Tracker mTracker;
    //    private View greyText;
    private Bitmap bitmap;
    private SharedPreferences settings;
    private DrawerLayout mDrawerLayout;
    private VideoEnabledWebView webView;
    private boolean isBlocked;
    //    private GifMovieView loading;
    private GifMovieView gifMovieView;
    private BitmapDrawable drawable;
    private VideoEnabledWebChromeClient webChromeClient;
    private View shadow;
    private LinearLayout topAdView;
    private LinearLayout bottomAdView;
    private AdsControlNabaa adsControl_top;
    private AdsControlNabaa adsControl_bottom;
    private SharedPreferences sharedpreferences;
    private String userServerId;
    private History history;
    private DataBaseAdapter dataBaseAdapter;
    private ArrayList<History> articleIds;
    private ImageView selectSource;
    private ProgressBar selectionProgress;
    private boolean isServerResponsed;
    //private ImageView enableBreakingNotification;
    private OnOffSwitch enableBreakingNotification;
    private ProgressBar followProgress;
    //    private RelativeLayout noNetwork;
//    private RelativeLayout refreshView;
    private Profile objProfile;
    private RelativeLayout sourceNotificationHolder;
    private View seperator;
    private RelativeLayout loadErrorView ,reloadDetails;

    @SuppressLint("ValidFragment")
    public NewsDetail2(News news, int index) {
        this.news = news;
        this.index = index;
        locale = new Locale("ar", "SA");
        print = new SimpleDateFormat("hh:mm a dd-MMMM-yyyy ", locale);
    }

    public NewsDetail2() {
    }

    @SuppressLint("ValidFragment")
    public NewsDetail2(String notificationArticleID) {
        this.notificationArticleID = notificationArticleID;
        locale = new Locale("ar", "SA");
        print = new SimpleDateFormat("hh:mm a dd-MMMM-yyyy ", locale);
    }

    public static void closeAnimation(Context context) {
        MainActivity.zoomEnabled = false;
//        if (mCurrentAnimator != null) {
//            mCurrentAnimator.cancel();
//        }

        // Animate the four positioning/sizing properties in parallel, back to
        // their
        // original values.
//        AnimatorSet set = new AnimatorSet();
//        set.play(
//                ObjectAnimator.ofFloat(expandedImageView, View.X,
//                        startBounds.left))
//                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
//                        startBounds.top))
//                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
//                        startScaleFinal))
//                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_Y,
//                        startScaleFinal));
//        set.setDuration(mShortAnimationDuration);
//        set.setInterpolator(new DecelerateInterpolator());
//        set.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                sourceImg.setAlpha(1f);
        expandedImageView.setVisibility(View.GONE);
//                mCurrentAnimator = null;
//            }

//            @Override
//            public void onAnimationCancel(Animator animation) {
        sourceImg.setAlpha(1f);
        expandedImageView.setVisibility(View.GONE);
//                mCurrentAnimator = null;
//            }
//        });
//        set.start();
//        mCurrentAnimator = set;
        header.setVisibility(View.VISIBLE);
        saveImg.setVisibility(View.GONE);
        sourceHeader.setVisibility(View.VISIBLE);
//        sourceImg.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams
//                .MATCH_PARENT));
//        FragmentActivity activity = (FragmentActivity) sourceImg.getContext();
//        FragmentManager manager = activity.getSupportFragmentManager();
//        try {
//            Picasso.with(context).load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
//                    .error(R.drawable.default_img)
//                    .into(sourceImg);
//        } catch (Exception e) {
//            sourceImg.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.default_img, null));
//        }

    }

    void showDialog(News newsObj) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                addToFav.setEnabled(true);
                return;
            }
        } catch (NullPointerException e) {

        }

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        AddCommentFragment commentFrag = new AddCommentFragment(newsObj, index);

        commentFrag.show(ft, "dialog");
        commentFrag.onFinishUpdates(new AddCommentFragment.OnCommentsUpdate() {
            @Override
            public void onFinished(News newsObj, int index) {
                news = newsObj;
                commentsNumber.setText(newsObj.getCommentsNumber() + "");
                setViewGone(newsObj);
                setViewsVisible(newsObj);
                if (notificationArticleID.length() == 0)
                    onDetailsUpdate.onFinished(index, news);
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName(getString(R.string.news_details));
//        mTracker.setPage(news.getID() + "");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        errotToast = Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT);
        successToast = Toast.makeText(getActivity(), "ØªÙ… Ø§Ù„Ø¥Ø¨Ù„Ø§Øº Ø¹Ù† Ø§Ù„Ù…ØµØ¯Ø± Ø¨Ù†Ø¬Ø§Ø­", Toast.LENGTH_SHORT);
        settings = getActivity().getSharedPreferences(LoginPopup.MyPREFERENCES, 0);

        prefs = getActivity().getSharedPreferences(URLs.MY_PREFS_NAME, getActivity().MODE_PRIVATE);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mainControl = new MainControl(getActivity());
        sharedpreferences = getActivity().getSharedPreferences(LoginPopup.MyPREFERENCES, Context.MODE_PRIVATE);

        userServerId = sharedpreferences.getString(LoginPopup.userServerId, "");

//        noNetwork = (RelativeLayout) getActivity().findViewById(R.id.sources_no_network_me);
//        refreshView = (RelativeLayout) getActivity().findViewById(R.id.refresh_me);
        header = (RelativeLayout) getActivity().findViewById(R.id.header_details);
//        newsTitle = (TextView) getActiviy().findViewById(R.id.news_title);

//        }
//        if (!mainControl.checkInternetConnection()) {
//            Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
//            webView.setVisibility(View.GONE);
//            noNetwork.setVisibility(View.GONE);
//            noNetwork.requestLayout();
//            refreshView.requestLayout();
//            refreshView.bringToFront();
//            noNetwork.setVisibility(View.VISIBLE);
//            noNetwork.bringToFront();
//            refreshView.setFocusable(true);
//            noNetwork.setFocusable(true);
//            refreshView.setClickable(true);
        gifMovieView.setVisibility(View.VISIBLE);
        gifMovieView.setPaused(false);

//        } else {
//            noNetwork.setVisibility(View.GONE);
//            webView.setVisibility(View.VISIBLE);


        List<Profile> profile = dataBaseAdapter.getAllProfiles();
        objProfile = profile.get(0);

        if (notificationArticleID.length() == 0) {
            showData(getActivity());

            if (!checkOnNewsSource()) {
                selectSource.setVisibility(View.VISIBLE);
            } else {
                selectSource.setVisibility(View.GONE);
                if (!checkOnNewsNotification()) {
                    seperator.setVisibility(View.VISIBLE);
                    sourceNotificationHolder.setVisibility(View.VISIBLE);
                } else {
                    seperator.setVisibility(View.GONE);
                    sourceNotificationHolder.setVisibility(View.GONE);
                }

                animateSwitch();
            }

            setViewGone(news);
            setViewsVisible(news);
            callJson(URLs.GET_NEWS_DETAILS_URL, URLs.GET_NEWS_DETAILS_REQUEST_TYPE, URLs.getUserID(), news.getID() + "", getActivity());

        } else {
            callJson(URLs.GET_NOTIFICATION_NEWS_DETAILS_URL, URLs.GET_NOTIFICATION_NEWS_DETAILS__TYPE, URLs.getUserID(),
                    notificationArticleID + "", getActivity());
        }

//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().popBackStack();
//            }
//        });

//        webView.post(new Runnable() {
//            @Override
//            public void run() {
//                webView.loadUrl("http://nabaapp.com/innerNewsDetails/newsDetails.html?id=" + news.getID() + "&hideImg=" + isBlocked + "");
//            }
//        });
//        try {
//            noNetwork.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    errotToast.show();
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        try {
//            refreshView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (!mainControl.checkInternetConnection()) {
//                        Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
//                        noNetwork.setVisibility(View.VISIBLE);
//                        webView.setVisibility(View.GONE);
//
//                        return;
//                    } else {
//                        noNetwork.setVisibility(View.GONE);
//                        webView.setVisibility(View.VISIBLE);
//                        if (notificationArticleID.length() == 0) {
//                            showData(getActivity());
//                            setViewGone(news);
//                            setViewsVisible(news);
//                            callJson(URLs.GET_NEWS_DETAILS_URL, URLs.GET_NEWS_DETAILS_REQUEST_TYPE, URLs.getUserID(), news.getID() + "",
//                                    getActivity());
//                        } else {
//                            callJson(URLs.GET_NOTIFICATION_NEWS_DETAILS_URL, URLs.GET_NOTIFICATION_NEWS_DETAILS__TYPE, URLs.getUserID(),
//                                    notificationArticleID + "", getActivity());
//                        }
//                    }
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        /*setViewGone(news);
        setViewsVisible(news);*/


//        webView.setListener(this, this);
//        try {
////            webView.loadUrl("about:blank");
////            webView.clearView();
//            webView.clearCache(true);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

//        gifMovieView.setVisibility(View.VISIBLE);
//        gifMovieView.setPaused(false);

    }

    private boolean checkOnNewsNotification() {
        Sources sourceObj = dataBaseAdapter.getSourcesBySourceId(news.getSourceID());
        if (sourceObj.getUrgent_notify() > 0)
            return true;
        else
            return false;
    }

    private boolean checkOnNewsSource() {
        List<Sources> sourcesList = dataBaseAdapter.getSelectedSources();
        if (sourcesList != null && !sourcesList.isEmpty()) {
            for (Sources source : sourcesList) {
                if (source.getSource_id() == news.getSourceID())
                    return true;
            }
        }
        return false;
    }

    private void openUrl() {
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }
//        FragmentManager manager = getActivity().getSupportFragmentManager();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
//        transaction.addToBackStack(null);
        Intent intent = new Intent(getActivity(), CustomWebView.class);

//        CustomWebView webview = new CustomWebView();
        Bundle b = new Bundle();
        b.putString("newsURL", news.getNewsUrl());
        intent.putExtras(b);
        startActivity(intent);
//        webview.setArguments(b);

//        transaction.add(R.id.parent, webview);
//        transaction.commit();
    }

    private void callJson(String url, int requestType, int userID, String id, final Context context) {

        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                addToFav.setEnabled(true);
                report.setEnabled(true);
                return;
            }
        } catch (NullPointerException e) {
        }
        if (requestType == URLs.GET_NEWS_DETAILS_REQUEST_TYPE) {
//
//            gifMovieView.setVisibility(View.VISIBLE);
//            gifMovieView.setPaused(false);
        }

        HashMap<String, String> dataObj = new HashMap<>();

        if (requestType == URLs.REPORT_SOURCE_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_REPORT_SOURCE_USER_ID, userID + "");
            dataObj.put(URLs.TAG_REPORT_SOURCE_SOURCE_ID, id);
        } else if (requestType == URLs.ADD_FAVOURITE_REQUEST_TYPE) {
            progBar.setVisibility(View.VISIBLE);
            addToFav.setVisibility(View.GONE);
            dataObj.put(URLs.TAG_ADD_FAVOURITE_USERID, userID + "");
            dataObj.put(URLs.TAG_ADD_FAVOURITE_ARTICLE_ID, id);
        } else if (requestType == URLs.GET_NEWS_DETAILS_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_USERID, userID + "");
            dataObj.put(URLs.TAG_REQUEST_ARTICLE_ID, id);
            dataObj.put(URLs.TAG_REQUEST_URGENT, news.isUrgent() + "");
        } else if (requestType == URLs.GET_NOTIFICATION_NEWS_DETAILS__TYPE) {
            dataObj.put(URLs.TAG_USERID, userID + "");
            dataObj.put(URLs.TAG_REQUEST_ARTICLE_ID, id);
        } else {
            dataObj.put(URLs.TAG_USERID, userID + "");
            dataObj.put(URLs.TAG_REQUEST_ARTICLE_ID, id);
        }

        jsonParser = new JsonParser(getActivity(), url, requestType, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                                       @Override
                                       public void onFinished() {

                                       }

                                       @Override
                                       public void onFinished(List<Integer> likeResponse) {
                                           likeBtn.setEnabled(true);
                                           progressBar.setVisibility(View.GONE);
                                           likeImg.setVisibility(View.VISIBLE);
                                           try {
                                               updateLikeState(likeResponse);
                                               if (notificationArticleID.length() == 0)
                                                   if (onDetailsUpdate != null)
                                                       onDetailsUpdate.onFinished(index, news);
                                           } catch (Exception e) {
                                               errotToast.show();
                                           }
                                       }

                                       @Override
                                       public void onFinished(News newsObj) {
                                           try {
                                               if (!newsObj.getNewsDetails().isEmpty() || !newsObj.getNewsDetails().equals("null")) {
                                                   updateNewsObj(newsObj);
//                                                   newsDetails.setText(news.getNewsDetails());
                                                   if (news.getIsRTL().equalsIgnoreCase("false"))
//                                                       newsDetails.setGravity(Gravity.LEFT);
                                                       if (notificationArticleID.length() == 0)
                                                           if (onDetailsUpdate != null)
                                                               onDetailsUpdate.onFinished(index, news);
//                                                   moveToSourceView.setVisibility(View.VISIBLE);
//                                                   copyRight.setVisibility(View.VISIBLE);
                                               } else {
                                                   Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_LONG).show();
                                                   try {
                                                       updateNewsObj(newsObj);
                                                   } catch (Exception e) {
                                                       e.printStackTrace();
                                                   }
                                               }

                                               if (newsObj.getFavouriteID() > 0) {
                                                   addToFav.setImageResource(R.drawable.favorite_selected);
                                               }
//
//                                               gifMovieView.setVisibility(View.GONE);
//                                               gifMovieView.setPaused(true);
                                           } catch (NullPointerException e) {
//                                               gifMovieView.setVisibility(View.GONE);
//                                               gifMovieView.setPaused(true);
//                                               newsDetails.setText("");
                                               errotToast.show();
                                           }
                                       }

                                       @Override
                                       public void onFinished(ArrayList<Sources> sourceList) {

                                       }

                                       @Override
                                       public void onFinished(int id) {
                                           //add favourite
                                           addToFav.setEnabled(true);
                                           progBar.setVisibility(View.GONE);
                                           addToFav.setVisibility(View.VISIBLE);
                                           if (id > 0) {
                                               addToFav.setImageResource(R.drawable.favorite_selected);
                                               news.setFavouriteID(id);
                                               try {
                                                   onDetailsUpdate.onFinished(index, news);
                                               } catch (Exception e) {
                                                   e.printStackTrace();
                                               }
                                           } else {
                                               errotToast.show();
                                           }

                                       }

                                       @Override
                                       public void onFinished(boolean success) {
                                           report.setEnabled(true);
                                           if (success)
                                               successToast.show();
                                           else {
                                               errotToast.show();
                                           }
                                       }
                                   }

        );
    }

    private void callJson(String url, int requestType, int id, final Context context, String tag) {

        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                addToFav.setEnabled(true);
                return;
            }
        } catch (NullPointerException e) {

        }

        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(tag, id + "");

        if (requestType == URLs.DELETE_FAVOURITE_REQUEST_TYPE) {
            progBar.setVisibility(View.VISIBLE);
            addToFav.setVisibility(View.GONE);
        }

        jsonParser = new JsonParser(getActivity(), url, requestType, dataObj);
        jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {

            }

            @Override
            public void onFinished(List<Integer> likeResponse) {
                likeBtn.setEnabled(true);
                progressBar.setVisibility(View.GONE);
                likeImg.setVisibility(View.VISIBLE);
                try {
                    updateUNLikeState(likeResponse);
                    if (notificationArticleID.length() == 0)
                        onDetailsUpdate.onFinished(index, news);
                } catch (Exception e) {
                    errotToast.show();
                }
            }

            @Override
            public void onFinished(News newsObj) {
                if (notificationArticleID.length() == 0)
                    try {
                        onDetailsUpdate.onFinished(index, newsObj);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int id) {
                //delete favourite
                addToFav.setEnabled(true);
                progBar.setVisibility(View.GONE);
                addToFav.setVisibility(View.VISIBLE);
                if (id == -1)
                    errotToast.show();
                else {
                    addToFav.setImageResource(R.drawable.favorite);
                    news.setFavouriteID(0);
                    try {
                        onDetailsUpdate.onFinished(index, news);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFinished(boolean success) {

            }
        });
    }

    @Override
    public void onResume() {
//        webView.loadUrl("about:blank");
//        webView.loadUrl("http://nabaapp.com/innerNewsDetails/newsDetails.html?id=" + news.getID() + "&hideImg=" + isBlocked + "");
        super.onResume();

    }

    private void setViewGone(News newsObj) {
        if (newsObj.getLikesNumber() == 0)
            likesView.setVisibility(View.GONE);
        if (newsObj.getCommentsNumber() == 0)
            commentsView.setVisibility(View.GONE);
        if (newsObj.getSharesNumber() == 0)
            sharesView.setVisibility(View.GONE);

        if (newsObj.getLikesNumber() == 0 && newsObj.getCommentsNumber() == 0 && newsObj.getSharesNumber() == 0) {
            actionsView.setVisibility(View.GONE);
            bottomLayout.getLayoutParams().height = (int) getActivity().getResources().getDimension(R.dimen.collapsed_height);
        }
    }

    private void setViewsVisible(News newsObj) {

        if (newsObj.getLikesNumber() > 0 || newsObj.getCommentsNumber() > 0 || newsObj.getSharesNumber() > 0) {
            actionsView.setVisibility(View.VISIBLE);
            bottomLayout.getLayoutParams().height = (int) getActivity().getResources().getDimension(R.dimen.expanded_height);
        }

        if (newsObj.isUrgent())
            urgentLabel.setVisibility(View.GONE);

        if (newsObj.getLikesNumber() > 0)
            likesView.setVisibility(View.VISIBLE);
        else
            likesView.setVisibility(View.GONE);
        if (newsObj.getCommentsNumber() > 0)
            commentsView.setVisibility(View.VISIBLE);
        else
            commentsView.setVisibility(View.GONE);
        if (newsObj.getSharesNumber() > 0)
            sharesView.setVisibility(View.VISIBLE);
        else
            sharesView.setVisibility(View.GONE);

        if (news.getLikeID() > 0) {
            likeImg.setImageResource(R.drawable.like);
            likeTitle.setTextColor(Color.parseColor("#CC0000"));
        } else {
            likeImg.setImageResource(R.drawable.like_unactive);
            likeTitle.setTextColor(Color.parseColor("#9EA1A8"));
        }

        if (news.getFavouriteID() > 0) {
            addToFav.setImageResource(R.drawable.favorite_selected);
        }

        if (news.getSharesNumber() < 1000)
            sharingNumbers.setText(news.getSharesNumber() + " ");
        else
            sharingNumbers.setText(news.getSharesNumber() / 1000 + "K ");

        if (news.getCommentsNumber() < 1000)
            commentsNumber.setText(news.getCommentsNumber() + " ");
        else
            commentsNumber.setText(news.getCommentsNumber() / 1000 + "K ");

        if (news.getLikesNumber() < 1000)
            likesNumber.setText(news.getLikesNumber() + " ");
        else
            likesNumber.setText(news.getLikesNumber() / 1000 + "K ");
    }

    private void shareEvent(final News newsObj) {

        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(getContext(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        } else {
//            news.setSharesNumber(news.getSharesNumber()+1);
////
//            setViewsVisible(news);
////
//            sharingNumbers.setText(newsObj.getSharesNumber() + "");
//
//            Intent i = new Intent(Intent.ACTION_SEND);
//            i.setType("text/plain");
//            i.putExtra(Intent.EXTRA_SUBJECT, newsObj.getNewsTitle());
//            i.putExtra(Intent.EXTRA_TEXT, "http://nabaapp.com/" + news.getID() + "" + " \n " + "Ø­Ù…Ù„ ØªØ·Ø¨ÙŠÙ‚ Ù†Ø¨Ø£
// Ø§Ù„Ø§Ø®Ø¨Ø§Ø±ÙŠ Ù…Ø¬Ø§Ù†Ø§Ù‹ ÙˆØ§Ø³ØªÙ…ØªØ¹
// Ø¨Ù…ØªØ§Ø¨Ø¹Ø© Ø§Ù„Ø¹Ø§Ù„Ù… Ù„Ø­Ø¸Ø© Ø¨Ù„Ø­Ø¸Ø©" +
//                    "\n" + getActivity().getString(R
//                    .string.app_domain));
//                startActivity(Intent.createChooser(i, getString(R.string.share)));

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, newsObj.getNewsTitle());
            i.putExtra(Intent.EXTRA_TEXT, "http://nabaapp.com/" + news.getID() + "" + " \n " + getString(R.string.share_text) +
                    "\n" + getActivity().getString(R
                    .string.app_domain));
            startActivity(Intent.createChooser(i, getString(R.string.share)));

            HashMap<String, String> dataObj = new HashMap<>();

            dataObj.put(URLs.TAG_SHARE_ARTICLE_ID, newsObj.getID() + "");
            dataObj.put(URLs.TAG_SHARE_ARTICLE_URL, newsObj.getNewsUrl() + "");

            jsonParser = new JsonParser(getContext(), URLs.SHARE_URL, URLs.SHARE_REQUEST_TYPE, dataObj);
            jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnSharesUpdatedListener() {
                @Override
                public void onFinished(int sharesCount, String shortLink) {
                    if (sharesCount == -1) {
                        Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_LONG).show();
                        return;
                    }
                    news.setSharesNumber(sharesCount);
                    setViewsVisible(news);
                    sharingNumbers.setText(newsObj.getSharesNumber() + "");
                    if (notificationArticleID.length() == 0)
                        try {
                            onDetailsUpdate.onFinished(index, news);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    for (int i = 0; i < articleIds.size(); i++) {
                        if (articleIds.get(i).getArticleId() == news.getID()) {
                            history.setLike_Id(news.getLikeID());
                            history.setLikesCount(news.getLikesNumber());
                            history.setFavouriteId(news.getFavouriteID());
                            history.setShareCount(news.getSharesNumber());
                            dataBaseAdapter.updateArticleById(news.getID(), history);
                        }
                        break;
                    }
//                /*Intent i = new Intent(Intent.ACTION_SEND);
//                i.setType("text/plain");s
//                i.putExtra(Intent.EXTRA_SUBJECT, news.getNewsTitle());
//                i.putExtra(Intent.EXTRA_TEXT, Uri.encode(newsObj.getNewsUrl(), ALLOWED_URI_CHARS));
//                startActivity(Intent.createChooser(i, getString(R.string.share)));*/


                }
            });
        }
    }


    private void onSelectSource(final Sources source) {

        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }

        isServerResponsed = true;
        selectionProgress.setVisibility(View.VISIBLE);
        selectSource.setVisibility(View.GONE);

        if (source.getSelected_or_not() == 1) {
            //change it on the server
            HashMap<String, String> dataObj = new HashMap<>();
            dataObj.put(URLs.TAG_ADD_SOURCE_USERID, URLs.getUserID() + "");
            dataObj.put(URLs.TAG_SOURCE_SOURCEID, news.getSourceID() + "");
            jsonParser = new JsonParser(getActivity(), URLs.DELETE_SOURCE_URL, URLs.SOURCE_DELETE_REQUEST_TYPE, dataObj);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                @Override
                public void onFinished() {
                }

                @Override
                public void onFinished(List<Integer> likeResponse) {

                }

                @Override
                public void onFinished(News newsObj) {

                }

                @Override
                public void onFinished(ArrayList<Sources> sourceList) {

                }

                @Override
                public void onFinished(int userID) {

                }

                @Override
                public void onFinished(boolean success) {
                    isServerResponsed = false;
                    selectSource.setVisibility(View.VISIBLE);
                    selectionProgress.setVisibility(View.GONE);
                    if (success) {
                        selectSource.setImageResource(R.drawable.add);
                        source.setSelected_or_not(0);
                        source.setNumber_followers(source.getNumber_followers() - 1);
                        followProgress.setEnabled(false);
                        new Subscription().execute(source.getSource_id() + "");
                        dataBaseAdapter.updateSources(source);
                        try {
                            // sourceLit = dataBaseAdapter.getSourcesByCategoryId(catObj.getCategory_id(), catObj.getCategory_type());
                            // SourcesFragmentV2.updateSourceList(sourceLit);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "recentNews");
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "urgentNews");
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "hotNews");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        /////
                        Toast.makeText(getActivity(), R.string.error, Toast.LENGTH_LONG).show();
                        selectSource.setImageResource(R.drawable.source_news_check);
                    }
                }
            });

        } else {
            //change it on the server
            HashMap<String, String> dataObj = new HashMap<>();
            dataObj.put(URLs.TAG_ADD_SOURCE_USERID, URLs.getUserID() + "");
            dataObj.put(URLs.TAG_SOURCE_SOURCEID, news.getSourceID() + "");
            jsonParser = new JsonParser(getActivity(), URLs.ADD_SOURCE_URL, URLs.SOURCE_ADD_REQUEST_TYPE, dataObj);
            //change it on locale
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                @Override
                public void onFinished() {
                }

                @Override
                public void onFinished(List<Integer> likeResponse) {

                }

                @Override
                public void onFinished(News newsObj) {

                }

                @Override
                public void onFinished(ArrayList<Sources> sourceList) {

                }

                @Override
                public void onFinished(int userID) {
                }

                @Override
                public void onFinished(boolean success) {
                    isServerResponsed = false;
                    selectSource.setVisibility(View.VISIBLE);
                    selectionProgress.setVisibility(View.GONE);
                    if (success) {
                        selectSource.setImageResource(R.drawable.source_news_check);
                        source.setSelected_or_not(1);
                        source.setUrgent_notify(1);
                        source.setNumber_followers(source.getNumber_followers() + 1);
                        FirebaseMessaging.getInstance().subscribeToTopic(source.getSource_id() + "U");
                        animateSwitch();

                        selectSource.setEnabled(false);
                        dataBaseAdapter.updateSources(source);
                        if (onDetailsUpdate != null)
                            onDetailsUpdate.onFinished(index, news);
                        //if (dataBaseAdapter.getSourcesBySourceId(news.getSourceID()).getUrgent_notify() <= 0) {
                        //seperator.setVisibility(View.VISIBLE);
                        //sourceNotificationHolder.setVisibility(View.VISIBLE);
                        //}
                        //followersNumb.setText(source.getNumber_followers() + " " + getString(R.string.followers));
                        try {
                            //sourceLit = dataBaseAdapter.getSourcesByCategoryId(catObj.getCategory_id(), catObj.getCategory_type());
                            //SourcesFragmentV2.updateSourceList(sourceLit);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "recentNews");
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "hotNews");
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "urgentNews");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.error, Toast.LENGTH_LONG).show();
                        selectSource.setImageResource(R.drawable.add);
                    }
                }
            });
        }
    }

    private void animateSwitch() {
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (dataBaseAdapter.getSourcesBySourceId(news.getSourceID()) != null && dataBaseAdapter.getSourcesBySourceId(news
                        .getSourceID()).getUrgent_notify() > 0)
                    enableBreakingNotification.setSwitch(true);
                else
                    enableBreakingNotification.setSwitch(false);
            }
        }, 50);
    }

    private void updateNewsObj(News obj) {
        if (obj.getID() > 0 || news.getID() != obj.getID()){
            news.setSourceID(obj.getSourceID());
            news.setID(obj.getID());
        }
        news.setNewsDetails(obj.getNewsDetails());
        news.setFavouriteID(obj.getFavouriteID());
        news.setNewsUrl(obj.getNewsUrl());
        sourceHeader.setEnabled(true);
        news.setLogo_url(Uri.encode(obj.getLogo_url(), ALLOWED_URI_CHARS));
        if (dataBaseAdapter.getSourcesBySourceId(news.getSourceID()) == null) {
            Sources mSource = new Sources();
            mSource.setGeo_id(obj.getCountryID());
            mSource.setSub_id(obj.getCategoryID());
            mSource.setSource_id(obj.getSourceID());
            mSource.setSource_name(obj.getSourceName());
            mSource.setDetails(obj.getDetails());
            mSource.setLogo_url(obj.getSourceImage());
            mSource.setChange_type(obj.getChangeType());
            mSource.setNumber_followers(obj.getSourceFollowers());
            mSource.setTimeStamp(0);
            mSource.setNotifiable(0);
            mSource.setSelected_or_not(0);

            dataBaseAdapter.insertInSources(mSource);
        }

        if (notificationArticleID.length() != 0) {
            news.setSourceID(obj.getSourceID());
            if (!checkOnNewsSource()) {
                selectSource.setVisibility(View.VISIBLE);
            } else {
                selectSource.setVisibility(View.GONE);
                if (!checkOnNewsNotification()) {
                    seperator.setVisibility(View.VISIBLE);
                    sourceNotificationHolder.setVisibility(View.VISIBLE);
                } else {
                    seperator.setVisibility(View.GONE);
                    sourceNotificationHolder.setVisibility(View.GONE);
                }

                animateSwitch();
            }

            news.setSourceLogoUrl(obj.getSourceLogoUrl());
            //news.setSourceID(obj.getSourceID());
            news.setNewsTitle(obj.getNewsTitle());
            news.setSourceTitle(obj.getSourceTitle());
            //news.setLogo_url(obj.getLogo_url());
            news.setSourceImage(obj.getSourceImage());
            news.setSourceLogoUrl(obj.getSourceImage());
            news.setID(obj.getID());
            news.setNewsTitle(obj.getNewsTitle());
            news.setArticleDate(obj.getArticleDate());
            news.setLogo_url(Uri.encode(obj.getLogo_url(), ALLOWED_URI_CHARS));
            news.setLikesNumber(obj.getLikesNumber());
            news.setCommentsNumber(obj.getCommentsNumber());
            news.setSharesNumber(obj.getSharesNumber());
            news.setLikeID(obj.getLikeID());
            //news.setNewsUrl(obj.getNewsUrl());
            news.setTimeOffset(obj.getTimeOffset());
            try {
                news.setIsRTL(obj.getIsRTL());
            } catch (Exception e) {
                e.printStackTrace();
            }
            showData(getActivity());
        }
    }

    public void showData(Context context) {
        sourceTitle.setText(news.getSourceTitle());
        sourceTitle.setGravity(Gravity.RIGHT);
        if (news.getSourceLogoUrl() != null)
            Picasso.with(getActivity())
                    .load(news.getSourceLogoUrl())
                    .placeholder(R.anim.progress_animation)
                    .error(R.drawable.default_img)
                    .into(sourceImg);
        else
            Picasso.with(getActivity())
                    .load(news.getSourceImage())
                    .placeholder(R.anim.progress_animation)
                    .error(R.drawable.default_img)
                    .into(sourceImg);

//        try {
//            if (news.getIsBlocked() > 0) {
//                loading.setVisibility(View.GONE);
//                reload.setVisibility(View.VISIBLE);
//                newsLogo.setImageResource(R.drawable.default_img);
//            } else {
//                Picasso.with(getActivity())
//                        .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
//                        .error(R.drawable.default_img)
//                        .into(newsLogo);
//
//                loading.setVisibility(View.GONE);
//                reload.setVisibility(View.GONE);
//                //holder.isVisible = true;
//            }
//        } catch (Exception ee) {
//            newsLogo.setImageResource(R.drawable.default_img);
//            loading.setVisibility(View.GONE);
//        }

        try {
            Calendar cal = mainControl.setTimeZone(Long.parseLong(news.getArticleDate()), news.getTimeOffset());
            newsTime.setText(context.getString(R.string.updated) + " " + print.format(cal.getTime()));
        } catch (Exception e) {
        }

        /*newsTitle.setMaxLines(2);
        newsTitle.setEllipsize(TextUtils.TruncateAt.END);*/
//        newsTitle.setText(news.getNewsTitle());
//        if (news.getIsRTL().equalsIgnoreCase("false"))
//            newsTitle.setGravity(Gravity.LEFT);
//            if (prefs.getInt(Azkar_FontSize, 0) != 0)
//            newsDetails.setTextSize(prefs.getInt(Azkar_FontSize, 0));
        setViewGone(news);
        setViewsVisible(news);
        webView.loadUrl("http://nabaapp.com/home/newsdetails?id=" + news.getID() + "&hideImg=" + isBlocked + "&version=3");
//        webView.loadUrl("http://api.madarsoft.com/madarWebsites/hiComment/newsDetails.html?id=" + news.getID() + "&hideImg=" + isBlocked
//                + "");
        try {
            Picasso.with(getActivity()).load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                    .error(R.drawable.default_img)
                    .into(expandedImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        history = new History();
        try {
            Sources source = dataBaseAdapter.getSourcesBySourceId(news.getSourceID());
            int rank = source.getRank();
            rank++;
            source.setRank(rank);
            dataBaseAdapter.updateSourceById(news.getSourceID(), source);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        loading = (GifMovieView) getActiviy().findViewById(R.id.loading_spinner);
//        loading.setMovieResource(R.drawable.gif_reload);
        newsList = new ArrayList<>();
        try {
            articleIds = dataBaseAdapter.getArticlesId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (articleIds.size() == 0) {
            addArticle();
        }
        try {
            for (int i = 0; i < articleIds.size(); i++) {
                if (articleIds.get(i).getArticleId() == news.getID()) {
                    break;
                } else {
                    addArticle();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void onFinishUpdates(OnDetailsUpdate onDetailsUpdate) {
        this.onDetailsUpdate = onDetailsUpdate;
    }

    public void updateLikeState(List<Integer> updatedResult) {
        try {
            if (updatedResult.get(0) > 0) {
                news.setLikeID(updatedResult.get(0));
                news.setLikesNumber(updatedResult.get(1));
                likesNumber.setText(news.getLikesNumber() + " ");
                news.setLikesNumber(news.getLikesNumber());
                setViewsVisible(news);
                for (int i = 0; i < articleIds.size(); i++) {
                    if (articleIds.get(i).getArticleId() == news.getID()) {
                        history.setLike_Id(news.getLikeID());
                        history.setLikesCount(news.getLikesNumber());
                        history.setFavouriteId(news.getFavouriteID());
                        history.setShareCount(news.getSharesNumber());
                        history.setCommentsCount(news.getCommentsNumber());
                        dataBaseAdapter.updateArticleById(news.getID(), history);
                        break;
                    }

                }


            }
        } catch (IndexOutOfBoundsException e) {
        }
    }

    public void updateUNLikeState(List<Integer> updatedResult) {
        try {
            if (updatedResult.get(0) > -1) {
                news.setLikesNumber(updatedResult.get(0));
                likesNumber.setText(news.getLikesNumber() + " ");
                news.setLikesNumber(news.getLikesNumber());
                news.setLikeID(0);
                setViewGone(news);
                setViewsVisible(news);
                for (int i = 0; i < articleIds.size(); i++) {
                    if (articleIds.get(i).getArticleId() == news.getID()) {
                        history.setLike_Id(news.getLikeID());
                        history.setLikesCount(news.getLikesNumber());
                        history.setFavouriteId(news.getFavouriteID());
                        history.setShareCount(news.getSharesNumber());
                        history.setCommentsCount(news.getCommentsNumber());
                        dataBaseAdapter.updateArticleById(news.getID(), history);
                    }
                    break;
                }
            }
        } catch (IndexOutOfBoundsException e) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.news_detail2, container, false);

        actionsView = (ExpandableView) root.findViewById(R.id.actions_expanded_view);

        loadErrorView = (RelativeLayout) root.findViewById(R.id.webview_error);

        reloadDetails = (RelativeLayout) root.findViewById(R.id.reload_webivew);

        progBar = (ProgressBar) root.findViewById(R.id.favourite_loading);
        // actionsView.setTag(position);
        likesView = (LinearLayout) root.findViewById(R.id.likes_view);
        // likesView.setTag(position);
        commentsView = (LinearLayout) root.findViewById(R.id.comments_view);
        // commentsView.setTag(position);
        sharesView = (LinearLayout) root.findViewById(R.id.shares_view);
        // sharesView.setTag(position);

        likeBtn = (LinearLayout) root.findViewById(R.id.like_btn);
        // likeBtn.setTag(position);
        commentBtn = (LinearLayout) root.findViewById(R.id.comment_btn);
        // commentBtn.setTag(position);
        shareBtn = (LinearLayout) root.findViewById(R.id.share_btn);
        // shareBtn.setTag(position);
        myPrefs = getActivity().getSharedPreferences(MY_PREFS, getActivity().MODE_PRIVATE);
        //fontSize = myPrefs.getInt(Azkar_FontSize, font_min);

        sourceHeader = (LinearLayout) root.findViewById(R.id.news_detail_source_header);
        sourceHeader.setEnabled(false);
//        moveToSourceView = (RelativeLayout) root.findViewById(R.id.read_from_source_button);
        bottomLayout = (RelativeLayout) root.findViewById(R.id.bottom_layout);
        sourceTitle = (TextView) root.findViewById(R.id.source_title);
        newsTime = (TextView) root.findViewById(R.id.news_time);
        sharingNumbers = (TextView) root.findViewById(R.id.sharing_number);
        commentsNumber = (TextView) root.findViewById(R.id.comments_number);
        likesNumber = (TextView) root.findViewById(R.id.likes_number);
        likeTitle = (TextView) root.findViewById(R.id.like_txt);
        commentTitle = (TextView) root.findViewById(R.id.comment_txt);
        shareTitle = (TextView) root.findViewById(R.id.share_txt);
        selectionProgress = (ProgressBar) root.findViewById(R.id.select_progress);
        sourceImg = (ImageView) root.findViewById(R.id.source_image);
        likeImg = (ImageView) root.findViewById(R.id.like_img);
        commentImg = (ImageView) root.findViewById(R.id.comment_img);
        shareImg = (ImageView) root.findViewById(R.id.share_img);
        shadow = (View) root.findViewById(R.id.custom_bottom_shadow);
        gifMovieView = (GifMovieView) root.findViewById(R.id.loading_spinner_me);
        gifMovieView.setMovieResource(R.drawable.loading);
        gifMovieView.bringToFront();
        dataBaseAdapter = new DataBaseAdapter(getActivity());
        sourceNotificationHolder = (RelativeLayout) root.findViewById(R.id.source_notification_holder_urgent);
        seperator = root.findViewById(R.id.custom_bottom_shadow2);
//        newsLogo = (ImageButton) root.findViewById(R.id.news_logo);
//        greyText = (View) root.findViewById(R.id.grey_txt);
//        newsLogo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (reload.getVisibility() == View.VISIBLE)
//                    return;
//                try {
//                    BitmapDrawable drawable = (BitmapDrawable) newsLogo.getDrawable();
//                    bitmap = drawable.getBitmap();
//                    zoomImageFromThumb(newsLogo, bitmap);
//                } catch (Exception e) {
//                    Toast.makeText(getActivity(), getString(R.string.image_open_excep),
//                            Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//        newsDetails = (TextView) getActiviy().findViewById(R.id.news_detail);
        increaseText = (ImageView) root.findViewById(R.id.news_increase_text);
        decreaseText = (ImageView) root.findViewById(R.id.news_decrease_text);
        back = (ImageView) root.findViewById(R.id.news_detail_back);
        addToFav = (ImageView) root.findViewById(R.id.add_to_fav);
        report = (ImageView) root.findViewById(R.id.news_report);
        //enableBreakingNotification = (ImageView) root.findViewById(R.id.enable_breaking_news);
        enableBreakingNotification = (OnOffSwitch) root.findViewById(R.id.source_enable_breaking_news);
        followProgress = (ProgressBar) root.findViewById(R.id.selection_progress);
        openNewsUrl = (ImageView) root.findViewById(R.id.news_from_source);
//        copyRight = (FontTextView) rogetActiviy()ot.findViewById(R.id.news_copy_right);
        progressBar = (ProgressBar) root.findViewById(R.id.like_loading);
        urgentLabel = (FontTextView) root.findViewById(R.id.urgent_label);
//        reload = (ImageView) getActiviy().findViewById(R.id.reload);
        webView = (VideoEnabledWebView) root.findViewById(R.id.details_web_view);
        expandedImageView = (ImageView) root.findViewById(R.id.expanded_image);
        selectSource = (ImageView) root.findViewById(R.id.select_source_img);

        //Banner Ads View
        topAdView = (LinearLayout) getActivity().findViewById(R.id.news_details_top_ad_view);
        bottomAdView = (LinearLayout) getActivity().findViewById(R.id.news_detail_bottom_ad_view);
        adsControl_top = new AdsControlNabaa(getActivity());
        adsControl_bottom = new AdsControlNabaa(getActivity());


//        history.setShareId(news.getS);
        // Initialize the VideoEnabledWebChromeClient and set event handlers
        View nonVideoLayout = root.findViewById(R.id.nonVideoLayout); // Your own view, read class comments
        ViewGroup videoLayout = (ViewGroup) root.findViewById(R.id.videoLayout); // Your own view, read class comments
        View loadingView = getActivity().getLayoutInflater().inflate(R.layout.loading_layout, null); // Your own view, read class comments
        webChromeClient = new VideoEnabledWebChromeClient(nonVideoLayout, videoLayout, loadingView, webView) // See all available
                // constructors...
        {
            // Subscribe to standard events, such as onProgressChanged()...
            @Override
            public void onProgressChanged(WebView view, int progress) {
                // Your code...
            }
        };
        webChromeClient.setOnToggledFullscreen(new VideoEnabledWebChromeClient.ToggledFullscreenCallback() {
            @Override
            public void toggledFullscreen(boolean fullscreen) {
                // Your code to handle the full-screen change, for example showing and hiding the title bar. Example:
                if (fullscreen) {
                    WindowManager.LayoutParams attrs = getActivity().getWindow().getAttributes();
                    attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
                    attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    getActivity().getWindow().setAttributes(attrs);
                    sourceHeader.setVisibility(View.GONE);
                    header.setVisibility(View.GONE);
                    bottomLayout.setVisibility(View.GONE);
                    shadow.setVisibility(View.GONE);


                } else {
                    WindowManager.LayoutParams attrs = getActivity().getWindow().getAttributes();
                    attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
                    attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    getActivity().getWindow().setAttributes(attrs);
                    sourceHeader.setVisibility(View.VISIBLE);
                    header.setVisibility(View.VISIBLE);
                    bottomLayout.setVisibility(View.VISIBLE);
                    shadow.setVisibility(View.VISIBLE);
                }

            }
        });

        reloadDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gifMovieView.setVisibility(View.VISIBLE);
                gifMovieView.setPaused(false);
                webView.setVisibility(View.VISIBLE);
                loadErrorView.setVisibility(View.GONE);
                webView.loadUrl("http://nabaapp.com/home/newsdetails?id=" + news.getID() + "&hideImg=" + isBlocked + "&version=3");
            }
        });

        webView.setWebChromeClient(webChromeClient);
        webView.setWebViewClient(new WebViewClient() {
            // shouldOverrideUrlLoading makes this `WebView` the default handler for URLs inside the app, so that links are not kicked
            // out to other apps.

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // Use an external email program if the link begins with "mailto:".
                try {
                    try {
                        System.runFinalization();
                        Runtime.getRuntime().gc();
                        System.gc();
                        webView.freeMemory();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (URLDecoder.decode(url, "UTF-8").contains("loadnow://")) {
                        if (sharedpreferences.getString(LoginPopup.userServerId, "").isEmpty())
                            webView.loadUrl("javascript:window.loadSocialData('',0, '')");
                        else
                            webView.loadUrl("javascript:window.loadSocialData('" + sharedpreferences.getString(LoginPopup.userName, "") +
                                    "', " +
                                    sharedpreferences.getString(LoginPopup
                                            .userServerId, "") + ",'" + sharedpreferences.getString(LoginPopup
                                            .imgUrl,
                                    "") + "')");

                    } else if (URLDecoder.decode(url, "UTF-8").toLowerCase().contains("loadrelatednewsdetails://")) {
                        try {
                            String aa[] = url.split("://");
                            //webView.loadUrl(url);
                            FragmentManager manager = getActivity().getSupportFragmentManager();
                            FragmentTransaction transaction = manager.beginTransaction();
                            transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                            transaction.addToBackStack(null);
                            NewsDetail2 nesDetails = new NewsDetail2(aa[1]);
                            //NewsDetail2 nesDetails = new NewsDetail2("-8505763082654550151");
                            transaction.add(R.id.parent, nesDetails, "newsDetail");
                            transaction.commit();

                        } catch (Exception e) {
                            Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (URLDecoder.decode(url, "UTF-8").contains("increasecomments://")) {
                            news.setCommentsNumber(news.getCommentsNumber() + 1);
                            setViewsVisible(news);
                            setViewGone(news);
                            try {
                                onDetailsUpdate.onFinished(index, news);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return true;

                        }                                           //decreasecomments://
                        if (URLDecoder.decode(url, "UTF-8").contains("decreasecomments://")) {
                            news.setCommentsNumber(news.getCommentsNumber() - 1);
                            setViewsVisible(news);
                            setViewGone(news);
                            try {
                                onDetailsUpdate.onFinished(index, news);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return true;
                        }
                        if (URLDecoder.decode(url, "UTF-8").contains("loginaction://")) {
                            if (sharedpreferences.getString(LoginPopup.userServerId, "").isEmpty()) {
                                Intent intent = new Intent(getActivity(), LoginPopup.class);
                                intent.putExtra("commentSend", "commentSend");
                                startActivityForResult(intent, 2);
                            } else {
                                sharedpreferences = getActivity().getSharedPreferences(LoginPopup.MyPREFERENCES, Context.MODE_PRIVATE);
                                String myProfile = sharedpreferences.getString(LoginPopup.imgUrl, "");
                                String mUserName = sharedpreferences.getString(LoginPopup.userName, "");
                                updateServerComments(mUserName, myProfile);
                            }

                        } else if (URLDecoder.decode(url, "UTF-8").contains("saveimage")) {
//                            downloadRequest(url.substring(9));
                            BitmapDrawable drawable = (BitmapDrawable) sourceImg.getDrawable();
                            bitmap = drawable.getBitmap();
                            try {
                                zoomImageFromThumb(sourceImg, bitmap);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            gifMovieView.setVisibility(View.GONE);
                            gifMovieView.setPaused(true);

                          /*  بايظة الاتنينين مش شبه بعض*/
                        }
                        else if(new URL(url).getHost().equalsIgnoreCase(new URL(news.getNewsUrl()).getHost())){
                            if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setPackage("com.android.chrome");
                                try {
                                    getActivity().startActivity(intent);
                                } catch (ActivityNotFoundException ex) {
                                    // Chrome browser presumably not installed so allow user to choose instead
                                    intent.setPackage(null);
                                    getActivity().startActivity(intent);
                                }
                                return true;
                            } else {
                                try {
                                    Intent intent = new Intent(getActivity(), CustomWebView.class);
                                    Bundle b = new Bundle();
                                    b.putString("newsURL", news.getNewsUrl());
                                    intent.putExtras(b);
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return true;
                            }// Handle By application itself
                        }else {
                            webView.goBack();
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        }
//                        view.loadUrl(url);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return true;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                gifMovieView.setVisibility(View.GONE);
                gifMovieView.setPaused(true);

            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                webView.setVisibility(View.GONE);
                loadErrorView.setVisibility(View.VISIBLE);
                webView.loadUrl("about:blank");
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                webView.stopLoading();
                webView.setVisibility(View.GONE);
                loadErrorView.setVisibility(View.VISIBLE);
                webView.loadUrl("about:blank");
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                // Handle the error
                webView.stopLoading();
                webView.setVisibility(View.GONE);
                loadErrorView.setVisibility(View.VISIBLE);
                webView.loadUrl("about:blank");
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                webView.stopLoading();
                webView.setVisibility(View.GONE);
                loadErrorView.setVisibility(View.VISIBLE);
                webView.loadUrl("about:blank");
            }
        });

//        ActivityCompat.requestPermissions(getActivity(),
//                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        increaseText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (newsDetails.length() == 0) {
//                    return;
//                }
//                if (!mainControl.checkInternetConnection()) {
//                    Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
//                    noNetwork.setVisibility(View.VISIBLE);
//                    webView.setVisibility(View.GONE);
//                    gifMovieView.setVisibility(View.GONE);
//                    gifMovieView.setPaused(true);
//                    return;
////                } else {
//                    noNetwork.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
                int fontSize = prefs.getInt(Azkar_FontSize, font_min);
                if (fontSize < font_max) {
                    fontSize++;
                    prefsEditor = getActivity().getSharedPreferences(URLs.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
                    prefsEditor.putInt(Azkar_FontSize, fontSize);
                    prefsEditor.apply();
                    webView.loadUrl("javascript:increaseFont()");
                }
                //SharedPreferences.Editor prefsEditor = myPrefs.edit();
//                    newsDetails.setTextSize(fontSize);
//                }
            }
        });

        decreaseText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (newsDetails.length() == 0) {
//                    return;
//                }
//                if (!mainControl.checkInternetConnection()) {
//                    Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
//                    noNetwork.setVisibility(View.VISIBLE);
//                    webView.setVisibility(View.GONE);
////                    return;
//                } else {
//                    noNetwork.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
                int fontSize = prefs.getInt(Azkar_FontSize, font_min);
                if (fontSize > font_min) {
                    fontSize--;
                    prefsEditor = getActivity().getSharedPreferences(URLs.MY_PREFS_NAME, getActivity().MODE_PRIVATE).edit();
                    prefsEditor.putInt(Azkar_FontSize, fontSize);
                    prefsEditor.apply();
                    webView.loadUrl("javascript:decreaseFont()");
                }
//                    newsDetails.setTextSize(fontSize);
//                }
            }
        });
        adsControl_top.getBannerAd(topAdView, "details_top");
        adsControl_top.getSplashAd("details_top");
      /*  RectangleBannerAd banner = (new RectangleBannerAd.Builder()).adContainer(topAdView)
                .build();
        adsControl_top.getBannerAd(banner, "details_top");
        banner.setAdListener(new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                topAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdError() {
                topAdView.setVisibility(View.GONE);

            }
        });*/
        adsControl_bottom.getBannerAd(bottomAdView, "details_bottom");
    /*    RectangleBannerAd banner_bottom = (new RectangleBannerAd.Builder()).adContainer(bottomAdView)
                .build();
        adsControl_bottom.getBannerAd(banner, "details_bottom");
       banner_bottom.setAdListener(new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                bottomAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdError() {
                bottomAdView.setVisibility(View.GONE);

            }
        });*/
//        webView.setDownloadListener(new DownloadListener() {
//
//            public void onDownloadStart(String url, String userAgent,
//                                        String contentDisposition, String mimetype,
//                                        long contentLength) {
////                ActivityCompat.requestPermissions(getActivity(),
////                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
////                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
//
//
//                requestPermission();
//
//                if (settings.getBoolean("allow", true)) {
//                    OutputStream fOut = null;
//                    Uri outputFileUri;
//                    try {
//                        downloadRequest(url);
//            /*File getActiviy() = new File(Environment.getExternalStorageDirectory()
//                    + File.separator + "hinews_images" + File.separator);*/
//                        //File getActiviy() = new File("/sdcard/hinews_images");
////                        String s = Environment.getExternalStorageDirectory().toString() + "/";
////                        File root = new File(s + "Nabaa_images");
////                        if (!root.exists())
////                            root.mkdirs();
////                        //fOut = new FileOutputStream("/sdcard/myPaintings/image.jpg");
////
////                        String name = getImageName(news.getLogo_url());
////                        long time = System.currentTimeMillis();
////                        File sdImageMainDirectory = new File(root, time + ".jpg");
////
////                        fOut = new FileOutputStream(sdImageMainDirectory);
////                        bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
////                        fOut.flush();
////                        fOut.close();
////                        Toast.makeText(getActivity(), getString(R.string.success),
////                                Toast.LENGTH_SHORT).show();
////                        MediaScannerConnection.scanFile(getActivity(), new String[]{sdImageMainDirectory.getPath()}, null,
////                                new MediaScannerConnection.OnScanCompletedListener() {
////                                    @Override
////                                    public void onScanCompleted(String path, Uri uri) {
////                                        Log.i("", "Scanned " + path);
////                                    }
////                                });
//                    } catch (Exception e) {
//                        e.printStackTrace();
////                Toast.makeText(getActivity(), getString(R.string.error),
////                        Toast.LENGTH_SHORT).show();
//                    }
//                } else if (!settings.getBoolean("allow", true)) {
//                    Toast.makeText(getActivity(), getString(R.string.storagePermession),
//                            Toast.LENGTH_SHORT).show();
//                }
////                dm.enqueue(request);
//            }
//        });
        addToFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToFav.setEnabled(false);
                if (news.getFavouriteID() == 0)
                    callJson(URLs.ADD_FAVOURITE_URL, URLs.ADD_FAVOURITE_REQUEST_TYPE, URLs.getUserID(), news.getID() + "", getActivity());
                else
                    callJson(URLs.DELETE_FAVOURITE_URL, URLs.DELETE_FAVOURITE_REQUEST_TYPE, news.getFavouriteID(), getActivity(), URLs
                            .TAG_DELETE_FAVOURITE_FAVOURITE_ARTICLE_ID);
            }
        });

        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                report.setEnabled(false);
                callJson(URLs.REPORT_SOURCE_URL, URLs.REPORT_SOURCE_REQUEST_TYPE, URLs.getUserID(), news.getSourceID() + "", getActivity());
            }
        });

        enableBreakingNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sources sourceObj = dataBaseAdapter.getSourcesBySourceId(news.getSourceID());
                if (sourceObj.getUrgent_notify() > 0) {
                    //followProgress.setVisibility(View.VISIBLE);
                    //enableBreakingNotification.setVisibility(View.GONE);
                    callJson(URLs.REMOVE_URGENT_URL, URLs.REMOVE_URGENT_REQUEST_TYPE, URLs.getUserID(), true, sourceObj);
                } else {
                    //enableBreakingNotification.setVisibility(View.GONE);
                    //followProgress.setVisibility(View.VISIBLE);
                    callJson(URLs.ADD_URGENT_URL, URLs.ADD_URGENT_REQUEST_TYPE, URLs.getUserID(), false, sourceObj);
                }
            }
        });

        selectSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isServerResponsed)
                    onSelectSource(dataBaseAdapter.getSourcesBySourceId(news.getSourceID()));
            }
        });

        openNewsUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.getNewsUrl()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setPackage("com.android.chrome");
                    try {
                        getActivity().startActivity(intent);
                    } catch (ActivityNotFoundException ex) {
                        // Chrome browser presumably not installed so allow user to choose instead
                        intent.setPackage(null);
                        getActivity().startActivity(intent);
                    }
                } else {
                    openUrl();
                }
            }
        });


        likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mainControl.checkInternetConnection()) {
                    Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }

                likeBtn.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
                likeImg.setVisibility(View.GONE);
                if (news.getLikeID() == 0)
                    callJson(URLs.ADD_LIKE_URL, URLs.ADD_LIKE_REQUEST_TYPE, URLs.getUserID(), news.getID() + "", getActivity());
                else
                    callJson(URLs.DELETE_LIKE_URL, URLs.DELETE_LIKE_REQUEST_TYPE, news.getLikeID(), getActivity(), URLs
                            .TAG_ADD_LIKE_LIKE_ID);
            }
        });

        commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //commentImg.setImageResource(R.drawable.comments);
//                pop = AnimationUtils.loadAnimation(getActivity(), R.anim.pop);
//                commentImg.startAnimation(pop);
//                showDialog(news);
                webView.loadUrl("javascript:window.scrollToComments()");

            }
        });

        sourceHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = ((FragmentActivity) getActivity()).getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new SourceNews(news.getSourceID(), true, false, "", true));
                //transaction.add(R.id.parent, new SourceNews(news.getSourceID(), true, false, ""));
                transaction.commit();
            }
        });
        commentsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showDialog(news);
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //shareImg.setImageResource(R.drawable.share);
                pop = AnimationUtils.loadAnimation(getActivity(), R.anim.pop);
                shareImg.startAnimation(pop);
                shareEvent(news);
            }
        });

//        reload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loading.setVisibility(View.VISIBLE);
//                try {
//                    Picasso.with(getActivity())
//                            .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
//                            .error(R.drawable.default_img)
//                            .into(newsLogo, new Callback() {
//                                @Override
//                                public void onSuccess() {
//                                    loading.setVisibility(View.GONE);
//                                }
//
//                                @Override
//                                public void onError() {
//                                    loading.setVisibility(View.GONE);
//                                }
//                            });
//                    //holder.isVisible = true;
//
//                } catch (IllegalArgumentException e) {
//                    newsLogo.setImageResource(R.drawable.default_img);
//                    loading.setVisibility(View.GONE);
//                }
//                reload.setVisibility(View.GONE);
//                news.setIsBlocked(-1);
//                onDetailsUpdate.onFinished(index, news);
//            }
//        });

        if (news.getIsBlocked() == 1)
            isBlocked = true;
        else
            isBlocked = false;
        return root;
    }

    private void addArticle() {
        history.setUrgent(news.isUrgent ? 1 : 0);
        history.setSourceName(news.getSourceTitle());
        history.setApprev(news.getTimeOffset());
        history.setShareId(0);
        history.setSourceLogo(news.getSourceLogoUrl());
        history.setSourceId(news.getSourceID());
        history.setArticleDate(news.getArticleDate());
        history.setArticleId(news.getID());
        history.setShareLink(news.getShareUrl());
        history.setArticleImage(news.getLogo_url());
        history.setArticleTitle(news.getNewsTitle());
        history.setIsRtl(news.getIsRTL());
        history.setLike_Id(news.getLikeID());
        history.setLikesCount(news.getLikesNumber());
        history.setShareCount(news.getSharesNumber());
        history.setCommentsCount(news.getCommentsNumber());
        history.setFavouriteId(news.getFavouriteID());
        dataBaseAdapter.insertInHistory(history);
        articleIds = dataBaseAdapter.getArticlesId();
    }

    public void updateServerComments(String userName, String userImg) {
        webView.loadUrl("javascript:window.loadSocialData('" + userName + "', " + sharedpreferences.getString(LoginPopup.userServerId,
                "") + ", '" + userImg + "')");
    }

    private void downloadRequest(String url) {
        String s = Environment.getExternalStorageDirectory().toString() + "/";
        File root = new File(s + "Nabaa_images");
        if (!root.exists())
            root.mkdirs();
//                        fOut = new FileOutputStream("/sdcard/myPaintings/image.jpg");
//
//                        String name = getImageName(news.getLogo_url());
        long time = System.currentTimeMillis();
        File sdImageMainDirectory = new File(root, time + ".jpg");

//                        fOut = new FileOutputStream(sdImageMainDirectory);
//                        bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
//                        fOut.flush();
//                        fOut.close();
        DownloadManager mdDownloadManager = (DownloadManager) getActivity()
                .getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(
                Uri.parse(url));
        File destinationFile = new File(
                Environment.getExternalStorageDirectory(),
                getFileName(url));
        request.setDescription("Downloading ...");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationUri(Uri.fromFile(sdImageMainDirectory));
        mdDownloadManager.enqueue(request);
    }

    public String getFileName(String url) {
        String filenameWithoutExtension = "";
        filenameWithoutExtension = String.valueOf(System.currentTimeMillis()
                + ".jpg");
        return filenameWithoutExtension;
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || !settings.getBoolean("allow", false)) {

            // Should we show an explanation?
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }

    public void saveImgFromAct() {
        saveImg(drawable.getBitmap());
    }

    private void saveImg(Bitmap bm) {
        requestPermission();

        if (settings.getBoolean("allow", true)) {
            OutputStream fOut = null;
            Uri outputFileUri;
            try {
            /*File root = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "hinews_images" + File.separator);*/
                //File root = new File("/sdcard/hinews_images");
                String s = Environment.getExternalStorageDirectory().toString() + "/";
                File root = new File(s + "Nabaa_images");
                if (!root.exists())
                    root.mkdirs();
                //fOut = new FileOutputStream("/sdcard/myPaintings/image.jpg");

                String name = getImageName(news.getLogo_url());
                long time = System.currentTimeMillis();
                File sdImageMainDirectory = new File(root, time + ".jpg");

                fOut = new FileOutputStream(sdImageMainDirectory);
                bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
                Toast.makeText(getActivity(), getString(R.string.success),
                        Toast.LENGTH_SHORT).show();
                MediaScannerConnection.scanFile(getActivity(), new String[]{sdImageMainDirectory.getPath()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            @Override
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i("", "Scanned " + path);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
//                Toast.makeText(getActivity(), getString(R.string.error),
//                        Toast.LENGTH_SHORT).show();
            }
        } else if (!settings.getBoolean("allow", true)) {
            Toast.makeText(getActivity(), getString(R.string.storagePermession),
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void fullScreen() {

        // BEGIN_INCLUDE (get_current_ui_flags)
        // The UI options currently enabled are represented by a bitfield.
        // getSystemUiVisibility() gives us that bitfield.
        int uiOptions = getActivity().getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        // END_INCLUDE (get_current_ui_flags)
        // BEGIN_INCLUDE (toggle_ui_flags)
        boolean isImmersiveModeEnabled =
                ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
        if (isImmersiveModeEnabled) {
            Log.i("", "Turning immersive mode mode off. ");
        } else {
            Log.i("", "Turning immersive mode mode on.");
        }

        // Navigation bar hiding:  Backwards compatible to ICS.
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        // Immersive mode: Backward compatible to KitKat.
        // Note that this flag doesn't do anything by itself, it only augments the behavior
        // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
        // all three flags are being toggled together.
        // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
        // Sticky immersive mode differs in that it makes the navigation and status bars
        // semi-transparent, and the UI flag does not get cleared when the user interacts with
        // the screen.
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        getActivity().getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
        //END_INCLUDE (set_ui_flags)
    }

    private String getImageName(String url) {

        String strArr[] = url.split("/");
        return strArr[strArr.length - 1];
    }

    @Override
    public void onStop() {
//        Toast.makeText(getActivity(), "onStop", Toast.LENGTH_LONG).show();
//        webView.loadUrl("about:blank");
        MainActivity.isDetail = true;
        super.onStop();
        try {
            webView.loadUrl("javascript:closeVideo()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void zoomImageFromThumb(final View thumbView, final Bitmap imageBitmap) {
        // If there's an animation in progress, cancel it immediately and
        // proceed with this one.
        try {
            Picasso.with(getActivity()).load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                    .error(R.drawable.default_img)
                    .into(expandedImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

        MainActivity.zoomEnabled = true;
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }
        // Load the high-resolution "zoomed-in" image.
        try {
            saveImg = (FontTextView) getActivity().findViewById(R.id.save_img);
            header.setVisibility(View.INVISIBLE);
            saveImg.setVisibility(View.VISIBLE);
            sourceHeader.setVisibility(View.INVISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        expandedImageView.setDefaultScale(1);
//        expandedImageView.setImageBitmap(imageBitmap);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step
        // involves lots of math. Yay, math.
        startBounds = new Rect();
        finalBounds = new Rect();
        globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the
        // final bounds are the global visible rectangle of the container view.
        // Also
        // set the container view's offset as the origin for the bounds, since
        // that's
        // the origin for the positioning animation properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        getActivity().findViewById(R.id.container).getGlobalVisibleRect(finalBounds,
                globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the
        // "center crop" technique. This prevents undesirable stretching during
        // the animation.
        // Also calculate the start scaling factor (the end scaling factor is
        // always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height() > (float) startBounds
                .width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;

        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins,
        // it will position the zoomed-in view in the place of the thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);
        drawable = (BitmapDrawable) expandedImageView.getDrawable();
        expandedImageView.setImageDrawable(drawable);
        mAttacher = new PhotoViewAttacher(expandedImageView);
//        mAttacher.update();
        // Set the pivot point for SCALE_X and SCALE_Y transformations to the
        // top-left corner of
        // the zoomed-in view (the default is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties
        // (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set.play(
                ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_Y,
                        startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;
        // Upon clicking the zoomed-in image, it should zoom back down to the
        // original bounds
        // and show the thumbnail instead of the expanded image.
        startScaleFinal = startScale;
        saveImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });
        saveImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveImg(drawable.getBitmap());
            }
        });

    }


    @Override
    public void onBackButtonPressed() {

    }

    public interface OnDetailsUpdate {

        void onFinished(int index, News obj);

    }

    class Subscription extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            final GcmPubSub pubSub = GcmPubSub.getInstance(getActivity().getApplicationContext());
            final SharedPreferences prefs = getActivity().getSharedPreferences(MainActivity.MyPREFERENCES,
                    Context.MODE_PRIVATE);
            String token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
            try {
                pubSub.unsubscribe(token, "/topics/" + params[0] + "U");
            } catch (IOException e) {
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

            if (result.equals("Executed")) {


            }
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }

    }

    private void callJson(String url, int requestType, int id, final boolean isRemove, final Sources sourceObj) {
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(getActivity(), getActivity().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }
        HashMap<String, String> dataObj = new HashMap<>();
//        dataObj.put(tag, id + "");
        if (requestType == URLs.ADD_NOTIFICATION_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_SOURCE_ID, sourceObj.getSource_id() + "");
        } else if (requestType == URLs.REMOVE_NOTIFICATION_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_SOURCE_ID, sourceObj.getSource_id() + "");
        }
        if (requestType == URLs.ADD_URGENT_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_SOURCE_ID, sourceObj.getSource_id() + "");
        } else if (requestType == URLs.REMOVE_URGENT_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_SOURCE_ID, sourceObj.getSource_id() + "");
        }
        jsonParser = new JsonParser(getActivity(), url, requestType, dataObj);
        jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {

            }

            @Override
            public void onFinished(List<Integer> likeResponse) {

            }

            @Override
            public void onFinished(News newsObj) {
            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int id) {
            }

            @Override
            public void onFinished(boolean success) {
                if (success) {
                    if (isRemove) {
                        followProgress.setVisibility(View.GONE);
                        //enableBreakingNotification.setImageResource(R.drawable._on);
                        enableBreakingNotification.setSwitch(false);
                        //FirebaseMessaging.getInstance().unsubscribeFromTopic("Urgent");
                        //new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "Urgent");
                        sourceObj.setUrgent_notify(-1);
                        enableBreakingNotification.setVisibility(View.VISIBLE);
                        //isRemoveFromSub = true;
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(sourceObj
                                .getSource_id() + "U");
                        new Subscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, sourceObj
                                .getSource_id()
                                + "U");
                        dataBaseAdapter.updateSources(sourceObj);
                        if (onDetailsUpdate != null)
                            onDetailsUpdate.onFinished(index, news);
                    } else {
                        followProgress.setVisibility(View.GONE);
                        //enableBreakingNotification.setImageResource(R.drawable._off);
                        enableBreakingNotification.setSwitch(true);
                        //enableBreakingNotification.setEnabled(false);
                        sourceObj.setUrgent_notify(1);
                        //objProfile.setUrgentFlag(1);
                        //dataBaseAdapter.updateProfiles(objProfile);
                        enableBreakingNotification.setVisibility(View.VISIBLE);
                        //isRemoveFromSub = false;
                        FirebaseMessaging.getInstance().subscribeToTopic(sourceObj.getSource_id() + "U");
                        dataBaseAdapter.updateSources(sourceObj);
                        if (onDetailsUpdate != null)
                            onDetailsUpdate.onFinished(index, news);
                    }
//                    Toast.makeText(context, success + "", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    if (isRemove) {
                        followProgress.setVisibility(View.GONE);
                        enableBreakingNotification.setVisibility(View.VISIBLE);
                        //enableBreakingNotification.setImageResource(R.drawable._off);
                        enableBreakingNotification.setSwitch(true);
                        //enableBreakingNotification.setEnabled(false);
                        //FirebaseMessaging.getInstance().subscribeToTopic("Urgent");
                        //objProfile.setUrgentFlag(1);
                        //dataBaseAdapter.updateProfiles(objProfile);
                        sourceObj.setUrgent_notify(1);
                        //isRemoveFromSub = false;
                        FirebaseMessaging.getInstance().subscribeToTopic(sourceObj
                                .getSource_id() + "U");
//                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
//                                    .getSource_id() + "");
                        dataBaseAdapter.updateSources(sourceObj);
                        if (onDetailsUpdate != null)
                            onDetailsUpdate.onFinished(index, news);
                    } else {
                        followProgress.setVisibility(View.GONE);
                        enableBreakingNotification.setVisibility(View.VISIBLE);
                        //enableBreakingNotification.setImageResource(R.drawable._on);
                        enableBreakingNotification.setSwitch(false);
                        sourceObj.setUrgent_notify(-1);
                        //isRemoveFromSub = true;
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(sourceObj
                                .getSource_id() + "U");
                        new Subscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, sourceObj
                                .getSource_id()
                                + "U");
                        dataBaseAdapter.updateSources(sourceObj);
                        if (onDetailsUpdate != null)
                            onDetailsUpdate.onFinished(index, news);
                    }
                }
            }
        });
    }
}