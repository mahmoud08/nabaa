package com.madarsoft.nabaa.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.madarsoft.nabaa.R;

/**
 * Created by Colossus on 09-Jun-16.
 */
@SuppressLint("ValidFragment")
public class PleaseWaitFragment extends DialogFragment {
    String title;


    public PleaseWaitFragment(String title) {
        this.title = title;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation2;
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.please_wait, container, false);
        TextView warningText = (TextView) v.findViewById(R.id.warning_text);
        warningText.setText(title);

        return v;
    }

}
