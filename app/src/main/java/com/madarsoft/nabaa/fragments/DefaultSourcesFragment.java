//package com.madarsoft.nabaa.fragments;
//
//import android.content.Context;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.telephony.TelephonyManager;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//
//import com.basv.gifmoviewview.widget.GifMovieView;
//import com.madarsoft.nabaa.R;
//import com.madarsoft.nabaa.activities.MainActivity;
//import com.madarsoft.nabaa.adapters.DefaultSourcesAdapter;
//import com.madarsoft.nabaa.controls.JsonParser;
//import com.madarsoft.nabaa.controls.MainControl;
//import com.madarsoft.nabaa.entities.News;
//import com.madarsoft.nabaa.entities.Sources;
//import com.madarsoft.nabaa.entities.URLs;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//
///**
// * Created by Colossus on 12-Jun-17.
// */
//
//public class DefaultSourcesFragment extends MadarFragment {
//
//    private ListView defaultList;
//    private GifMovieView gifMovieView;
//    private JsonParser jsonParser;
//    private JsonParser jsonParesr;
//    private PleaseWaitFragment pleaseWaitFragment;
//    private LinearLayout goToMain;
//    private LinearLayout goToSources;
//    private NewsHolderFragment newsHolder;
//    private MainControl mainControl;
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        defaultList = (ListView) getActivity().findViewById(R.id.default_sources_list);
//        gifMovieView = (GifMovieView) getActivity().findViewById(R.id.default_gif);
//        gifMovieView.bringToFront();
//        gifMovieView.setMovieResource(R.drawable.loading);
//        goToMain = (LinearLayout) getActivity().findViewById(R.id.default_got_to_main);
//        goToSources = (LinearLayout) getActivity().findViewById(R.id.default_got_to_sources);
//        callJson(0);
//
//        goToMain.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                FragmentManager manager = ((FragmentActivity) getActivity()).getSupportFragmentManager();
////                FragmentTransaction transaction = manager.beginTransaction();
////                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
////                transaction.addToBackStack(null);
////                newsHolder = new NewsHolderFragment();
////                transaction.replace(R.id.parent, newsHolder);
////                //transaction.add(R.id.parent, new SourceNews(news.getSourceID(), true, false, ""));
////                transaction.commit();
//            }
//        });
//
//        goToSources.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                myPrefsEditor.putBoolean("recommend", false).apply();
//
//                FragmentManager manager = ((FragmentActivity) getActivity()).getSupportFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
//                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
//                transaction.addToBackStack(null);
//                CategoryFragment category = new CategoryFragment();
//                transaction.add(R.id.parent, category);
//                //transaction.add(R.id.parent, new SourceNews(news.getSourceID(), true, false, ""));
//                transaction.commit();
//            }
//        });
//    }
//
//    private void callJson(long timeStamp) {
////        pleaseWaitDialog();
//        HashMap<String, String> dataObj = new HashMap<String, String>();
//        dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, timeStamp + "");
//        new JsonParser(getActivity(), URLs.GET_COUNTRIES_URL, URLs.GET_COUNTRIES_REQUEST_TYPE, dataObj).execute();
//        jsonParesr = new JsonParser(getActivity(), URLs.GET_CATEGORIES_URL, URLs.GET_CATEGORIES_REQUEST_TYPE, dataObj);
//        jsonParesr.execute();
//        jsonParesr.onFinishUpdates(new JsonParser.OnCategoriesAddedListener() {
//            @Override
//            public void onFinished() {
////                callJson(URLs.GET_USER_SOURCES_URL, URLs.GET_USER_SOURCES_REQUEST_TYPE, URLs.getUserID());
//                callJson(URLs.getUserID(), getActivity());
//
//            }
//        });
//    }
//
//    private void pleaseWaitDialog() {
//        FragmentManager fm = MainActivity.mainActivity.getSupportFragmentManager();
//        FragmentTransaction ft = fm.beginTransaction();
//        Fragment prev = fm.findFragmentByTag("dialog2");
//        if (prev != null) {
//            ft.remove(prev);
//        }
//        ft.addToBackStack(null);
//        pleaseWaitFragment = new PleaseWaitFragment(getString(R.string.loading));
//        try {
//            pleaseWaitFragment.show(ft, "dialog2");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        pleaseWaitFragment.setCancelable(false);
//
//    }
//
//    private void callJson(int userID, final Context context) {
//        mainControl = new MainControl(context);
//        if (!mainControl.checkInternetConnection()) {
//            //Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        gifMovieView.setVisibility(View.VISIBLE);
//        gifMovieView.setPaused(false);
//        HashMap<String, String> dataObj = new HashMap<>();
//
//        dataObj.put(URLs.TAG_COUNTRIES_RESOURCES_USERID, userID + "");
//        dataObj.put(URLs.TAG_MCC, +getMcc() + "");
//        jsonParser = new JsonParser(getActivity(), URLs.DEFAULT_SOURCES, URLs.DEFAULT_SOURCES_TYPE, dataObj);
//        jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//
//
//        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
//            @Override
//            public void onFinished() {
//
//            }
//
//            @Override
//            public void onFinished(List<Integer> likeResponse) {
//
//            }
//
//            @Override
//            public void onFinished(News newsObj) {
//
//            }
//
//            @Override
//            public void onFinished(ArrayList<Sources> sourceList) {
//                defaultList.setAdapter(new DefaultSourcesAdapter(context, sourceList));
//                gifMovieView.setVisibility(View.GONE);
//                gifMovieView.setPaused(false);
//                PleaseWaitFragment pleaseWaitFragment = (PleaseWaitFragment) getActivity().getSupportFragmentManager().findFragmentByTag
//                        ("dialog2");
//                pleaseWaitFragment.dismiss();
//
//            }
//
//            @Override
//            public void onFinished(int id) {
//
//            }
//
//            @Override
//            public void onFinished(boolean success) {
//
//            }
//        });
//    }
//
//    private int getMcc() {
//        int mcc = 0;
//        TelephonyManager tel = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
//        String networkOperator = tel.getNetworkOperator();
//
//        if (!TextUtils.isEmpty(networkOperator)) {
//            mcc = Integer.parseInt(networkOperator.substring(0, 3));
//            int mnc = Integer.parseInt(networkOperator.substring(3));
//        }
//        return mcc;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.default_sources, container, false);
//    }
//
//
//}
