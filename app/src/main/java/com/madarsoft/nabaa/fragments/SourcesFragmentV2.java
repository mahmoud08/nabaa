package com.madarsoft.nabaa.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.analytics.HitBuilders;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.adapters.SourcesAdapterV2;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.controls.SpeedScrollListener;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ZProf on 06/03/2016.
 */
@SuppressLint("ValidFragment")
public class SourcesFragmentV2 extends MadarFragment {

    static ArrayList<Sources> sourcesList;
    static SourcesAdapterV2 resAdap;
    private static Category catObj;
    MainControl mainControl;
    private int type;
    private ListView sourcesListView;
    private SpeedScrollListener listener;
    private DataBaseAdapter dp;
    private ImageView search;
    private RelativeLayout back;
    private Animation animHide;
    private Animation animShow;
    private EditText inputText;
    private TextView textHeader;
    //    private RelativeLayout lower;
    private ImageView exitSearch;
    private Typeface headerFont;
    private boolean isClear = true;
    private boolean isOpened = true;
    private String mySource;
    private ImageView homeButton;
    private RelativeLayout noSourcesInCountry;
    private RelativeLayout noNetwork;
    private GifMovieView gifMovieView;
    private RelativeLayout refresh;
    private JsonParser jsonParser;
    private LinearLayout dimmedView;
    private boolean isFirstTime;
    private com.google.android.gms.analytics.Tracker mTracker;
    private DrawerLayout mDrawerLayout;

    @SuppressLint("ValidFragment")
    public SourcesFragmentV2(Category catObj, int type) {
        setCatObj(catObj);
        this.type = type;
    }

    public SourcesFragmentV2() {
    }

    public static Category getCatObj() {
        return catObj;
    }

    public static void setCatObj(Category catObject) {
        catObj = catObject;
    }

    public static void updateSourceList(List<Sources> newlist) {
        sourcesList.clear();
        sourcesList.addAll(newlist);
        resAdap.notifyDataSetChanged();
    }

    public void showData(Context context, List<Sources> sources) {
        gifMovieView.setVisibility(View.GONE);
        gifMovieView.setPaused(true);
        listener = new SpeedScrollListener();
//        Collections.sort(sourcesList, new Comparator<Sources>() {
//            @Override
//            public int compare(Sources lhs, Sources rhs) {
//                return Integer.valueOf(rhs.getNumber_followers()).compareTo(lhs.getNumber_followers());
//            }
//        });
//if (resAdap == null || resAdap.sourcesList.isEmpty()) {
        resAdap = new SourcesAdapterV2(context, sourcesList, listener, catObj, type);
        //resAdap.areAllItemsEnabled(true);
        sourcesListView.setAdapter(resAdap);
        /*} else {
            for (Sources sor : sources) {
                if (!resAdap.containsObject(sor.getSource_id())) {
                    resAdap.sourcesList.add(sor);
                }
            }
        }
        gifMovieView.setVisibility(View.GONE);
        gifMovieView.setPaused(true);*/
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MainActivity.newsHolder = null;
        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        mTracker.setScreenName(getString(R.string.sources_screen));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        //clear adapter if exist
        try {
            resAdap.sourcesList.clear();
            resAdap = null;
        } catch (NullPointerException e) {
        }
        dp = new DataBaseAdapter(getActivity());
        animHide = AnimationUtils.loadAnimation(getActivity(), R.anim.hide);
        animShow = AnimationUtils.loadAnimation(getActivity(), R.anim.show);
        sourcesListView = (ListView) getActivity().findViewById(R.id.sources_list_view);
        mainControl = new MainControl(getActivity());
        search = (ImageView) getActivity().findViewById(R.id.header_search);
        gifMovieView = (GifMovieView) getActivity().findViewById(R.id.gifa);
        gifMovieView.bringToFront();
        gifMovieView.setMovieResource(R.drawable.loading);

        noSourcesInCountry = (RelativeLayout) getActivity().findViewById(R.id.webview_error);
        noNetwork = (RelativeLayout) getActivity().findViewById(R.id.no_network);
        refresh = (RelativeLayout) getActivity().findViewById(R.id.no_network_button);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        textHeader = (TextView) getActivity().findViewById(R.id.resources_header);
        inputText = (EditText) getActivity().findViewById(R.id.search_edit_text);
//        lower = (RelativeLayout) getActivity().findViewById(R.id.lower);
        exitSearch = (ImageView) getActivity().findViewById(R.id.exit_search);
        homeButton = (ImageView) getActivity().findViewById(R.id.sources_home_button);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //noSourcesInCountry = (RelativeLayout) getActivity().findViewById(R.id.no_sources_selected_layout);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FragmentManager manager = ((FragmentActivity) getActivity()).getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    transaction.addToBackStack(null);
                    transaction.replace(R.id.parent, new NewsHolderFragment(), "newsHolder");
                    transaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            headerFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/arial.ttf");
            textHeader.setTypeface(headerFont);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            textHeader.setText(getString(R.string.sources_of) + " " + catObj.getCategory_name().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (isClear) {
                    if (inputText.getVisibility() == View.INVISIBLE) {
                        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation
                                .RELATIVE_TO_SELF, 0.5f);
                        anim.setDuration(200);
                        anim.setFillAfter(true);
                        inputText.startAnimation(anim);
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                setPadding(0);
                                textHeader.setVisibility(View.INVISIBLE);
                                inputText.setVisibility(View.VISIBLE);
                                inputText.requestFocus();
                                inputText.bringToFront();
                                hideKeyboard(inputText, true, getActivity());
//                                lower.setVisibility(View.GONE);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    search.setImageDrawable(getResources().getDrawable(R.drawable.white_x, getActivity()
                                            .getApplicationContext().getTheme()));
                                    search.setVisibility(View.VISIBLE);
                                    search.bringToFront();

                                } else {
                                    search.setImageDrawable(getResources().getDrawable(R.drawable.white_x));
                                    search.setVisibility(View.VISIBLE);
                                    search.bringToFront();
                                }
                                exitSearch.setVisibility(View.VISIBLE);
                                exitSearch.bringToFront();
                                isClear = false;
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                    }
                } else {
                    inputText.setText(null);
                }
            }
        });

        exitSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            //exit
            public void onClick(View v) {
                ScaleAnimation animate = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation
                        .RELATIVE_TO_SELF, 0.5f);
                animate.setDuration(150);
                animate.setFillAfter(true);
                inputText.startAnimation(animate);
                animate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        setPadding(40);
                        inputText.setVisibility(View.INVISIBLE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            search.setImageDrawable(getResources().getDrawable(R.drawable.search_icon, getActivity()
                                    .getApplicationContext().getTheme()));
                        } else {
                            search.setImageDrawable(getResources().getDrawable(R.drawable.search_icon));
                        }
                        isClear = true;
                        textHeader.setVisibility(View.VISIBLE);
                        inputText.setText(null);
                        exitSearch.setVisibility(View.GONE);
//                        lower.setVisibility(View.VISIBLE);
                        search.setVisibility(View.VISIBLE);
                        search.bringToFront();
                        hideKeyboard(inputText, false, getActivity());
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });

        sourcesList = dp.getSourcesByCategoryId(catObj.getCategory_id(), catObj.getCategory_type());
        showData(getActivity(), sourcesList);
        callJson(catObj, URLs.getUserID(), getActivity());

        inputText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ArrayList<Sources> arr = new ArrayList<Sources>();
                if (s.toString() != null) {
                    for (int i = 0; i < sourcesList.size(); i++) {
                        if ((sourcesList.get(i).getSource_name().toLowerCase()).contains(s.toString())) {
                            arr.add(sourcesList.get(i));
                        }
                    }
                    sourcesListView.setAdapter(new SourcesAdapterV2(getActivity(), arr, listener, catObj, type));
                } else if (s.length() == 0 || inputText.getVisibility() == View.GONE) {
                    sourcesListView.setAdapter(new SourcesAdapterV2(getActivity(), sourcesList, listener, catObj, type));
                }
            }
        });
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainControl.checkInternetConnection()) {
                    callJson(catObj, URLs.getUserID(), getActivity());
                } else {
                    Toast.makeText(getActivity(), "No Network ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setPadding(int padd) {
        int paddingPixel = padd;
        float density = getActivity().getResources().getDisplayMetrics().density;
        int paddingDp = (int) (paddingPixel * density);
        sourcesListView.setPadding(0, 0, 0, paddingDp);
    }

    private void callJson(final Category catObj, int userID, final Context context) {
        MainControl mainControl = new MainControl(context);
        if (!mainControl.checkInternetConnection()) {
            //Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
            if (sourcesList.isEmpty()) {
                noNetwork.setVisibility(View.VISIBLE);
                sourcesListView.setVisibility(View.GONE);
            }
            return;
        }
        noNetwork.setVisibility(View.GONE);
        sourcesListView.setVisibility(View.VISIBLE);
        if (sourcesList.isEmpty()) {
            gifMovieView.setVisibility(View.VISIBLE);
            gifMovieView.setPaused(false);
        }
        HashMap<String, String> dataObj = new HashMap<>();
        if (catObj.getCategory_type() == 1) {
            dataObj.put(URLs.TAG_COUNTRID, catObj.getCategory_id() + "");
            dataObj.put(URLs.TAG_COUNTRIES_RESOURCES_USERID, userID + "");
            dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, catObj.getSources_time_stamp() + "");
            jsonParser = new JsonParser(getActivity(), URLs.GET_COUNTRIES_RESOURCES_URL, URLs.GET_COUNTRIES_RESOURCES_TYPE, dataObj);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
        } else {
            dataObj.put(URLs.TAG_CATEGORYID, catObj.getCategory_id() + "");
            dataObj.put(URLs.TAG_COUNTRIES_RESOURCES_USERID, userID + "");
            if (sourcesList.size() < 1)
                dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, 0 + "");
            else
                dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, catObj.getSources_time_stamp() + "");

            jsonParser = new JsonParser(getActivity(), URLs.GET_CATEGORIES_RESOURCES_URL, URLs.GET_COUNTRIES_RESOURCES_TYPE, dataObj);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
        }

        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {

            }

            @Override
            public void onFinished(List<Integer> likeResponse) {

            }

            @Override
            public void onFinished(News newsObj) {

            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {
                sourcesList = dp.getSourcesByCategoryId(catObj.getCategory_id(), catObj.getCategory_type());
                try {
                    if (sourcesList.isEmpty() && sourceList.isEmpty()) {
                        sourcesListView.setVisibility(View.GONE);
                        noSourcesInCountry.setVisibility(View.VISIBLE);
                    } else {
                        sourcesListView.setVisibility(View.VISIBLE);
                        noSourcesInCountry.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                showData(context, sourceList);
            }

            @Override
            public void onFinished(int id) {

            }

            @Override
            public void onFinished(boolean success) {

            }
        });
    }

    private void hideKeyboard(View view, boolean open, Context context) {
        //sonarQube edit
        if (view == null)
            return;

        if (open == false) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            isOpened = false;
        }
        if (open) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            isOpened = true;
        }
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sources_v2, container, false);
    }

    @Override
    public void onBackButtonPressed() {
        super.onBackButtonPressed();
    }


}