package com.madarsoft.nabaa.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.adapters.CategoryAdapter;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;

import java.util.List;

/**
 * Created by Colossus on 03-Jul-16.
 */
@SuppressLint("ValidFragment")
public class MenuCountry extends MadarFragment {
    private List<Category> category;
    private ListView countryList;
    private DataBaseAdapter db;
    private int type;
    private com.google.android.gms.analytics.Tracker mTracker;
    private RelativeLayout menuHolder;

    @SuppressLint("ValidFragment")
    public MenuCountry(List<Category> category, int type) {
        this.category = category;
        this.type = type;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        countryList = (ListView) getActivity().findViewById(R.id.menu_country_list);
        db = new DataBaseAdapter(getActivity());

        CategoryAdapter categoryAdapter = new CategoryAdapter(getActivity(), R.layout.category_list_item, category, 1, true);
        countryList.setAdapter(categoryAdapter);
        FontTextView title = (FontTextView) getActivity().findViewById(R.id.menu_country_header);
        menuHolder = (RelativeLayout) getActivity().findViewById(R.id.menu_country_holder);
        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();

        mTracker = application.getDefaultTracker();
        if (type == 1) {
            title.setText(getString(R.string.countries));
            mTracker.setScreenName(getString(R.string.user_selected_counrties));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        } else {
            title.setText(getString(R.string.categories));
            mTracker.setScreenName(getString(R.string.user_selected_categories));
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
        countryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // To do ...
               /* FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new SourceNews()); //change me
                transaction.commit();*/
            }
        });
        menuHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.menu_country, container, false);
    }
}
