package com.madarsoft.nabaa.fragments;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.OnOffSwitch;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Profile;

import java.util.ArrayList;

public class Settings extends MadarFragment {
    final String PREFS_NAME = "FIRSTENTRY";
    private final String RATE_US = "rateUs";
    private final String SUPPORT = "support";
    private final String FB = "facebook";
    private final String ABOUT_US = "aboutUs";
    private final String TWITTER = "twitter";
    private OnOffSwitch blockImg;
    private DataBaseAdapter dataBaseAdapter;
    private ArrayList<Profile> profile;
    //    private RelativeLayout notification;
    private RelativeLayout support;
    private RelativeLayout fb;
    private RelativeLayout twitter;
    private ImageView menu;
    private DrawerLayout mDrawerLayout;
    private MainControl mainControl;
    private RelativeLayout aboutUs;
    private com.google.android.gms.analytics.Tracker mTracker;
    private RelativeLayout rateUs;
//    private RelativeLayout question;


//    public static Intent getOpenFacebookIntent(Context context, String url) {
//
//        try {
//            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
//            return new Intent(Intent.ACTION_VIEW, Uri.parse("hinewsapp"));
//        } catch (Exception e) {
//            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/<user_name_here>"));
//            openUrl("https://www.facebook.com/hinewsapp/", getString(R.string.fb_screen), getString(R.string.facebook));
//        }
//    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        mTracker.setScreenName(getString(R.string.settings_screen));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        mainControl = new MainControl(getActivity());
        dataBaseAdapter = new DataBaseAdapter(getActivity());
        blockImg = (OnOffSwitch) getActivity().findViewById(R.id.setting_hide_img);
//        notification = (RelativeLayout) getActivity().findViewById(R.id.settings_notification);
//        support = (RelativeLayout) getActivity().findViewById(R.id.settings_support);
//        fb = (RelativeLayout) getActivity().findViewById(R.id.settings_fb);
//        twitter = (RelativeLayout) getActivity().findViewById(R.id.settings_twitter);
//        aboutUs = (RelativeLayout) getActivity().findViewById(R.id.settings_about_us);
//        rateUs = (RelativeLayout) getActivity().findViewById(R.id.settings_rate_us);
//        question = (RelativeLayout) getActivity().findViewById(R.id.setting_questions);
//        notification = (RelativeLayout) getActivity().findViewById(R.id.setting_notification);

        menu = (ImageView) getActivity().findViewById(R.id.settings_menu);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);


        try {
            setRedViewInDrawer();
        } catch (Exception e) {
            e.printStackTrace();
        }
        profile = new ArrayList<>();
        profile = dataBaseAdapter.getAllProfiles();
        if (profile.get(0).getBlockImg() > 0)
            blockImg.setSwitch(true);
        else
            blockImg.setSwitch(false);

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.RIGHT);
            }
        });
        blockImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (profile.get(0).getBlockImg() > 0) {
                    profile.get(0).setBlockImg(-1);
                    dataBaseAdapter.updateProfiles(profile.get(0));
                    blockImg.setSwitch(false);
                } else {
                    profile.get(0).setBlockImg(1);
                    dataBaseAdapter.updateProfiles(profile.get(0));
                    blockImg.setSwitch(true);
                }

            }
        });
//        notification.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager manager = getActivity().getSupportFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
//                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
//                transaction.replace(R.id.parent, new NotificationHolder(), "notification");
//                transaction.addToBackStack(null);
//                transaction.commit();
//            }
//        });

//        fb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/308932912789390"));
//                    startActivity(intent);
//                } catch (Exception e) {
//                    openUrl("https://www.facebook.com/hinewsapp/", getString(R.string.fb_screen), getString(R.string.facebook));
//                }
//            }
//        });
//        twitter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                openUrl("http://api.madarsoft.com/mobileads_NFcqDqu/v2/pages/page.aspx?programid=5&name=wemosaly", getString(R.string
//// .twitter_screen), getString(R.string.twitter));
//                try {
//                    Intent intent = new Intent(Intent.ACTION_VIEW,
//                            Uri.parse("twitter://user?screen_name=hinewsapp"));
//                    startActivity(intent);
//
//                } catch (Exception e) {
//                    openUrl("https://twitter.com/hinewsapp", getString(R.string.twitter_screen), getString(R.string.twitter));
//                }
//            }
//        });

//        question.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openUrl("http://nabaapp.com/nabaaFAQ/m.nabaa.FAQ.html", getString(R.string.FAQ), getString(R.string.questions));
//            }
//        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.settings, container, false);
    }

//    private void openUrl(String url, String screenName, String arabicName) {
//        if (!mainControl.checkInternetConnection()) {
//            Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
//            return;
//        }
////        FragmentManager manager = getActivity().getSupportFragmentManager();
////        FragmentTransaction transaction = manager.beginTransaction();
////        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
////        transaction.addToBackStack(null);
////        CustomWebView webview = new CustomWebView();
//        Intent intent = new Intent(getActivity(), CustomWebView.class);
//        Bundle b = new Bundle();
//        b.putString("aboutUs", url);
//        b.putString("screenName", screenName);
//        b.putString("arabic", arabicName);
//        intent.putExtras(b);
//        startActivity(intent);
////        webview.setArguments(b);
////        transaction.replace(R.id.parent, webview);
////        transaction.commit();
//    }

    private void setRedViewInDrawer() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View redView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        View second = (View) vx.getChildAt(5).findViewById(R.id.red_view);
        View third = (View) vx.getChildAt(4).findViewById(R.id.red_view);
        View fourth = (View) vx.getChildAt(6).findViewById(R.id.red_view);
        View fifth = (View) vx.getChildAt(9).findViewById(R.id.red_view);

        redView.setVisibility(View.GONE);
        second.setVisibility(View.GONE);
        third.setVisibility(View.GONE);
        fourth.setVisibility(View.GONE);
        fifth.setVisibility(View.VISIBLE);
    }

}
