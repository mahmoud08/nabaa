package com.madarsoft.nabaa.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;

/**
 * Created by Colossus on 24-May-16.
 */
public class PromoHolder extends MadarFragment {

    private static final int PROMO_CHOOSE_SOURCES = 0;
    private static final int PROMO_NOTIFICATIONS = 1;
    private static final int PROMO_FILTER = 2;
    private static final int PROMO_LIKE = 3;
    private static final int PROMO_HOT_NEWS = 4;
    private static final int HIDE_IMG = 5;


    private ViewPager promoPager;
    private PromoAdapter promoAdapter;
    //    private FontTextView skip;
//    private FontTextView next;
//    private ImageView quickImg, geoSubImg, descImg, addFavImg;
    private DrawerLayout mDrawerLayout;
    private Button skip;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        promoPager = (ViewPager) getActivity().findViewById(R.id.promo_pager);
//        skip = (FontTextView) getActivity().findViewById(R.id.promo_skip);
//        next = (FontTextView) getActivity().findViewById(R.id.promo_next);
        skip = (Button) getActivity().findViewById(R.id.promo_skip);
        skip.bringToFront();
//        geoSubImg = (ImageView) getActivity().findViewById(R.id.promo_sub_geo_img);
//        descImg = (ImageView) getActivity().findViewById(R.id.promo_desc_img);
//        addFavImg = (ImageView) getActivity().findViewById(R.id.promo_fav_img);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        promoAdapter = new PromoAdapter(getChildFragmentManager());
        promoPager.setAdapter(promoAdapter);
        promoPager.setCurrentItem(PROMO_CHOOSE_SOURCES);

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                FragmentManager manager = getActivity().getSupportFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
//                transaction.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit, R.anim.enter, R.anim.exit);
//                transaction.replace(R.id.parent, new NewsHolderFragment(), "newsHolder");
//                transaction.addToBackStack(null);
//                transaction.commit();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                getActivity().finish();
                startActivity(intent);
//                getActivity().finish();

            }
        });
//

//        next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int cuurentPos = promoPager.getCurrentItem();
//                switch (cuurentPos) {
//                    case PROMO_QUICK:
//                        setGeoSubImg();
//                        next.setClickable(true);
//                        promoPager.setCurrentItem(PROMO_GEO_SUB);
//                        next.setClickable(true);
//                        next.setVisibility(View.VISIBLE);
//                        skip.setText(R.string.skip);
//                        return;
//                    case PROMO_GEO_SUB:
//                        setDesImg();
//                        promoPager.setCurrentItem(PROMO_DESCRIPTION);
//                        next.setClickable(true);
//                        next.setVisibility(View.VISIBLE);
//                        skip.setText(R.string.skip);
//                        return;
//                    case PROMO_DESCRIPTION:
//                        setFavImg();
//                        promoPager.setCurrentItem(PROMO_ADD_FAV);
//                        next.setVisibility(View.INVISIBLE);
//                        next.setClickable(false);
//                        skip.setText(R.string.done);
//                        return;
//                    default:
//                        return;
//
//                }
//
//
//            }
//        });
//        promoPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                switch (position) {
//                    case PROMO_CHOOSE_SOURCES:
//                        setQuickimg();
//                        next.setClickable(true);
//                        next.setVisibility(View.VISIBLE);
//                        return;
//                    case PROMO_GEO_SUB:
//                        setGeoSubImg();
//                        next.setClickable(true);
//                        next.setVisibility(View.VISIBLE);
//                        return;
//                    case PROMO_DESCRIPTION:
//                        setDesImg();
//                        next.setClickable(true);
//                        next.setVisibility(View.VISIBLE);
//                        return;
//                    case PROMO_ADD_FAV:
//                        setFavImg();
//                        next.setVisibility(View.INVISIBLE);
//                        next.setClickable(false);
//                        skip.setText(R.string.done);
//
//
//                        return;
//                    default:
//                        return;
//
//                }
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//
//
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.promo_holder2, container, false);
    }

//    void setQuickimg() {
//        skip.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
//        next.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
//        quickImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.white_circle, null));
//        geoSubImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        descImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        addFavImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        skip.setText(R.string.skip);
//    }
//
//    void setGeoSubImg() {
//        skip.setTextColor(ContextCompat.getColor(getActivity(), R.color.red));
//        next.setTextColor(ContextCompat.getColor(getActivity(), R.color.red));
//        quickImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        geoSubImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.red_circle, null));
//        descImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        addFavImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        skip.setText(R.string.skip);
//    }
//
//    void setDesImg() {
//        skip.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
//        next.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
//        quickImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        geoSubImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        descImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.white_circle, null));
//        addFavImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        skip.setText(R.string.skip);
//    }
//
//    void setFavImg() {
//        skip.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
//        next.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
//        quickImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        geoSubImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        descImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.grey_circle, null));
//        addFavImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.white_circle, null));
//        skip.setText(R.string.done);
//
//
//    }

    private class PromoAdapter extends FragmentStatePagerAdapter {

        public PromoAdapter(FragmentManager fm) {

            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case PROMO_CHOOSE_SOURCES:
                    return new PromoChooseSources();
                case PROMO_NOTIFICATIONS:
                    return new PromoNotifications();
                case PROMO_FILTER:
                    return new PromoFilter();
                case PROMO_LIKE:
                    return new PromoLike();
                case PROMO_HOT_NEWS:
                    return new PromoHotNews();
                case HIDE_IMG:
                    return new PromoHideImg();
                default:
                    return new PromoChooseSources();
            }
        }

        @Override
        public int getCount() {
            return 6;
        }
    }

}
