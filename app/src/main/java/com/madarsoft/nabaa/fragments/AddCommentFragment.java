package com.madarsoft.nabaa.fragments;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.ExpandableView;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.activities.CustomWebView;
import com.madarsoft.nabaa.activities.LoginPopup;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.adapters.CommentsAdapter;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.entities.Comments;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.URLs;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import im.delight.android.webview.AdvancedWebView;

/**
 * Created by Colossus on 06-Apr-16.
 */
@SuppressLint("ValidFragment")
public class AddCommentFragment extends DialogFragment{

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    EditText commentEditTxt;
    CommentsAdapter myAdapter2;
    InputMethodManager lManager;
    FontTextView commentsNumText;
    ImageView commentsIcon;
    LinearLayout.LayoutParams params;
    LinearLayout commentView;
    int index;
    ProgressBar loading;
    private News articleObj;
    private JsonParser jsonParser;
    private Context context;
    private ListView commentListView;
    private RelativeLayout noCommentsView;
    private GifMovieView gifMovieView;
    private OnCommentsUpdate onCommentsUpdated;
    private SharedPreferences sharedpreferences;
    private String userServerId;
    private FontTextView edit;
    private FontTextView delete;
    private ExpandableView editDelHolder;
    private View backGround;
    private int currentRowPosition;
    private List<Comments> commentList;
    private RelativeLayout editLayout;
    private FontTextView applyEdit;
    private FontTextView cancel;
    private EditText editText;
    private ImageView userPic;
    private String newCommentText;
    private RelativeLayout commentSend;
    private ImageView commentImg;
    private RelativeLayout bottomLayout;
    private PleaseWaitFragment pleaseWaitFragment;
    private boolean isFinished;


    public AddCommentFragment() {
    }

    @SuppressLint("ValidFragment")
    public AddCommentFragment(News articleObj, int index) {
        this.articleObj = articleObj;
        this.index = index;
    }

    public static void setLogin() {

    }

    void showDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        /*DialogFragment newFragment = new AlertDialogFrag();
        newFragment.show(ft, "dialog");*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    public void showData(Context context, List<Comments> commentList) {

        myAdapter2 = new CommentsAdapter(context, R.layout.comment_list_item, commentList);
        lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        gifMovieView.setVisibility(View.GONE);
        gifMovieView.setPaused(true);

        if (commentList == null || commentList.isEmpty()) {
            noCommentsView.setVisibility(View.VISIBLE);
            commentListView.setVisibility(View.GONE);
            return;
        }
        this.commentList = commentList;

        commentListView.setAdapter(myAdapter2);

        commentEditTxt.post(new Runnable() {
            public void run() {
                commentEditTxt.requestFocusFromTouch();
                lManager.showSoftInput(commentEditTxt, 0);
            }
        });
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_dialog);
        dialog.getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return dialog;
    }

    private void callJson(String url, final int requestType, String userID, int id, final Context context, final String commentText) {
        MainControl mainControl = new MainControl(context);
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            gifMovieView.setVisibility(View.GONE);
            gifMovieView.setPaused(true);
            return;
        }

        HashMap<String, String> dataObj = new HashMap<>();
        if (requestType == URLs.RETRIEVE_COMMENT_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_RETRIEVE_COMMENT_ID, 0 + "");
            dataObj.put(URLs.TAG_RETRIEVE_ARTICEL_ID, id + "");
            dataObj.put(URLs.TAG_RETRIEVE_COMMENT_COUNT, articleObj.getCommentsNumber() + "");
        } else if (requestType == URLs.ADD_COMMENT_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_ADD_COMMENT_USER_ACCOUNT_ID, userID + "");
            dataObj.put(URLs.TAG_ADD_COMMENT_ARTICLE_ID, id + "");
            dataObj.put(URLs.TAG_ADD_COMMENT_COUNT, (articleObj.getCommentsNumber() + 1) + "");
            dataObj.put(URLs.TAG_ADD_COMMENT_TEXT, commentText);
            loading.setVisibility(View.VISIBLE);
            commentImg.setVisibility(View.GONE);
        } else if (requestType == URLs.EDIT_COMMENT_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_EDIT_COMMENT_ID, id + "");
            dataObj.put(URLs.TAG_EDIT_COMMENT_COUNT, articleObj.getCommentsNumber() + "");
            dataObj.put(URLs.TAG_EDIT_COMMENT_TEXT, commentText);
        } else if (requestType == URLs.DELETE_COMMENT_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_DELETE_COMMENT_ID, id + "");
            dataObj.put(URLs.TAG_DELETE_COMMENT_COUNT, articleObj.getCommentsNumber() + "");
        }
        /*dataObj.put(URLs.TAG_ADD_COMMENT_USER_ID, userID + "");
        dataObj.put(URLs.TAG_ADD_COMMENT_ARTICLE_ID, id + "");
        dataObj.put(URLs.TAG_ADD_COMMENT_TEXT, userID + " ");
        dataObj.put(URLs.TAG_ADD_COMMENT_COUNT, id + "");*/

        jsonParser = new JsonParser(getActivity(), url, requestType, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnCommentsUpdatedListener() {
            @Override
            public void onFinished(List<Comments> commentsList) {
                if (requestType == URLs.RETRIEVE_COMMENT_REQUEST_TYPE) {
                    showData(context, commentsList);
                    isFinished = true;
                } else if (requestType == URLs.ADD_COMMENT_REQUEST_TYPE) {
                    loading.setVisibility(View.GONE);
                    commentImg.setVisibility(View.VISIBLE);
                    if (commentsList == null) {
                        Toast.makeText(context, getString(R.string.error), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    commentListView.setVisibility(View.VISIBLE);
                    if (noCommentsView.getVisibility() == View.VISIBLE)
                        noCommentsView.setVisibility(View.GONE);
                    if (articleObj.getLikesNumber() == 0)
                        commentView.setLayoutParams(params);
                    AddCommentFragment.this.commentList = commentsList;
                    try {
                        myAdapter2.comments = commentsList;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    articleObj.setCommentsNumber(commentsList.size());
                    if (commentListView.getAdapter() == null)
                        commentListView.setAdapter(myAdapter2);
                    else
                        myAdapter2.notifyDataSetChanged();

                    if (commentView.getVisibility() == View.GONE) {
                        commentsNumText.setVisibility(View.VISIBLE);
                        commentsIcon.setVisibility(View.VISIBLE);
                        commentView.setVisibility(View.VISIBLE);
                    }

                    commentsNumText.setText(articleObj.getCommentsNumber() + "");
                    onCommentsUpdated.onFinished(articleObj, index);

                    lManager.hideSoftInputFromWindow(commentEditTxt.getWindowToken(), 0);
                    commentEditTxt.setText("");
                }

                if (requestType == URLs.DELETE_COMMENT_REQUEST_TYPE) {
                    showData(context, commentsList);
                    editDelHolder.setVisibility(View.GONE);
                    backGround.setVisibility(View.GONE);
                    articleObj.setCommentsNumber(commentsList.size());

                    myAdapter2.notifyDataSetChanged();

                    commentsNumText.setText(articleObj.getCommentsNumber() + "");
                    if (commentsList.isEmpty())
                        commentView.setVisibility(View.GONE);
                    onCommentsUpdated.onFinished(articleObj, index);
                    pleaseWaitFragment.dismiss();
                }
            }

            @Override
            public void onFinished(boolean result) {
                editLayout.setVisibility(View.GONE);
                commentListView.setVisibility(View.VISIBLE);
                if (result) {
                    commentList.get(currentRowPosition).setCommentText(commentText);
                    myAdapter2.notifyDataSetChanged();
//                    isFinished = true;
                } else {
                    //Toast.makeText(context, getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_comment, container, false);

        LinearLayout likeView = (LinearLayout) v.findViewById(R.id.like_view);
        commentView = (LinearLayout) v.findViewById(R.id.coment_view);
        LinearLayout shareView = (LinearLayout) v.findViewById(R.id.share_view);
        editLayout = (RelativeLayout) v.findViewById(R.id.add_comment_edit_layout);
        applyEdit = (FontTextView) v.findViewById(R.id.edit_comment_edit);
        cancel = (FontTextView) v.findViewById(R.id.edit_comment_cancel);
        editText = (EditText) v.findViewById(R.id.edit_comment_edit_text);
        userPic = (ImageView) v.findViewById(R.id.comment_user_pic);

        commentEditTxt = (EditText) v.findViewById(R.id.comment_edittext);
        noCommentsView = (RelativeLayout) v.findViewById(R.id.no_comments);
        commentListView = (ListView) v.findViewById(R.id.comments_listview);
        ImageView likesIcon = (ImageView) v.findViewById(R.id.likes_icon);
        commentsIcon = (ImageView) v.findViewById(R.id.comments_icon);
        ImageView sharesIcon = (ImageView) v.findViewById(R.id.shares_icon);

        FontTextView likesNumText = (FontTextView) v.findViewById(R.id.likes_num);
        commentsNumText = (FontTextView) v.findViewById(R.id.comments_num);
        FontTextView sharesNumTxt = (FontTextView) v.findViewById(R.id.shares_num);

        commentSend = (RelativeLayout) v.findViewById(R.id.send_btn);
        commentImg = (ImageView) v.findViewById(R.id.send_img);
        loading = (ProgressBar) v.findViewById(R.id.loading_comment);
        edit = (FontTextView) v.findViewById(R.id.comment_edit);
        delete = (FontTextView) v.findViewById(R.id.comment_delete);
        editDelHolder = (ExpandableView) v.findViewById(R.id.comment_edit_del_holder);
        backGround = (View) v.findViewById(R.id.add_comment_background);
        bottomLayout = (RelativeLayout) v.findViewById(R.id.bottom_layout);

        sharedpreferences = getActivity().getSharedPreferences(LoginPopup.MyPREFERENCES, Context.MODE_PRIVATE);
        userServerId = sharedpreferences.getString(LoginPopup.userServerId, "");
        params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.gravity = Gravity.CENTER_VERTICAL;
        params.setMargins(0, 0, 15, 0);

        if (articleObj.getLikesNumber() == 0) {
            likeView.setVisibility(View.GONE);
            if (articleObj.getCommentsNumber() > 0)
                commentView.setLayoutParams(params);
            else if (articleObj.getSharesNumber() > 0)
                shareView.setLayoutParams(params);
            //likesNumText.setVisibility(View.GONE);
        }
        if (articleObj.getCommentsNumber() == 0) {
            commentView.setVisibility(View.GONE);
            //commentsNumText.setVisibility(View.GONE);
        }
        if (articleObj.getSharesNumber() == 0) {
            shareView.setVisibility(View.GONE);
            //sharesNumTxt.setVisibility(View.GONE);
        }

        likesNumText.setText(articleObj.getLikesNumber() + "");
        commentsNumText.setText(articleObj.getCommentsNumber() + "");
        sharesNumTxt.setText(articleObj.getSharesNumber() + "");

        gifMovieView = (GifMovieView) v.findViewById(R.id.comments_loading);
        gifMovieView.setMovieResource(R.drawable.loading);
        gifMovieView.setVisibility(View.VISIBLE);
        gifMovieView.setPaused(false);
        gifMovieView.bringToFront();
        callJson(URLs.RETRIEVE_COMMENT_URL, URLs.RETRIEVE_COMMENT_REQUEST_TYPE, "", articleObj.getID(), getActivity(), "");

        commentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!String.valueOf(commentList.get(position).getAccountID()).equals(sharedpreferences.getString(LoginPopup.userServerId,
                        ""))) {
                    return;
                }
                View c = getActivity().getCurrentFocus();
                if (c != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                float x = commentListView.getChildAt(position).getX();
                float y = commentListView.getChildAt(position).getY();
                String color = parent.getAdapter().getItem(position).toString();
                currentRowPosition = position;
                if (editDelHolder.getVisibility() == View.VISIBLE) {
                    editDelHolder.setVisibility(View.GONE);
                    backGround.setVisibility(View.GONE);
                } else {

                    animateHolder(editDelHolder, x, y);
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editLayout.setVisibility(View.GONE);
                commentListView.setVisibility(View.VISIBLE);
                bottomLayout.setVisibility(View.VISIBLE);
                View c = getActivity().getCurrentFocus();
                if (c != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(c.getWindowToken(), 0);
                }
            }
        });

        //edit and commit changes and return to comments frag also
        applyEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editLayout.setVisibility(View.GONE);
                commentListView.setVisibility(View.VISIBLE);
                bottomLayout.setVisibility(View.VISIBLE);
                View c = getActivity().getCurrentFocus();
                if (c != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(c.getWindowToken(), 0);
                }
                /*callJson(URLs.EDIT_COMMENT_URL, URLs.EDIT_COMMENT_REQUEST_TYPE, sharedpreferences.getString(LoginPopup.userServerId, ""),
                        commentList.get(currentRowPosition).getCommentID(), getActivity(), editText.getText().toString().trim());*/
                newCommentText = editText.getText().toString().trim();
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation myFadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fast_fade);
                edit.startAnimation(myFadeInAnimation);
                myFadeInAnimation.setDuration(250);
                myFadeInAnimation.setAnimationListener(new Animation.AnimationListener() {

                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        if (editLayout.getVisibility() == View.VISIBLE) {
                            editLayout.setVisibility(View.GONE);
                            commentListView.setVisibility(View.VISIBLE);
                            bottomLayout.setVisibility(View.VISIBLE);
                            View c = getActivity().getCurrentFocus();
                            if (c != null) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(c.getWindowToken(), 0);
                            }

                        } else {
                            editLayout.setVisibility(View.VISIBLE);
                            editDelHolder.setVisibility(View.GONE);
                            commentListView.setVisibility(View.GONE);
                            bottomLayout.setVisibility(View.GONE);
                            backGround.setVisibility(View.GONE);
                            editText.setText(commentList.get(currentRowPosition).getCommentText());

                            try {
                                Picasso.with(context)
                                        .load(Uri.encode(sharedpreferences.getString(LoginPopup.imgUrl, ""), ALLOWED_URI_CHARS))
                                        .error(R.drawable.profile)
                                        .into(userPic);
                            } catch (IllegalArgumentException e) {
                                userPic.setImageResource(R.drawable.profile);
                            }
                            View c = getActivity().getCurrentFocus();
                            if (c != null) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(c.getWindowToken(), 0);
                            }
                        }
                    }
                });

//                showEditDialog();
            }
        });
        //delete the selected Row
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation myFadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fast_fade);
                delete.startAnimation(myFadeInAnimation);
                myFadeInAnimation.setDuration(250);
                myFadeInAnimation.setAnimationListener(new Animation.AnimationListener() {

                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        pleaseWaitDialog();
                        callJson(URLs.DELETE_COMMENT_URL, URLs.DELETE_COMMENT_REQUEST_TYPE, sharedpreferences.getString(LoginPopup
                                .userServerId, ""), commentList.get(currentRowPosition).getCommentID(), getActivity(), commentEditTxt
                                .getText().toString().trim());


                    }
                });


            }
        });

        commentSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (commentEditTxt.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), getString(R.string.no_comment), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (sharedpreferences.getString(LoginPopup.userServerId, "").isEmpty()) {
                    Intent intent = new Intent(getActivity(), LoginPopup.class);
                    intent.putExtra("commentSend", "commentSend");
                    startActivityForResult(intent, 2);
                    return;
                }
                if (isFinished) {
                    callJson(URLs.ADD_COMMENT_URL, URLs.ADD_COMMENT_REQUEST_TYPE, sharedpreferences.getString(LoginPopup.userServerId,
                            ""), articleObj.getID(), getActivity(),
                            commentEditTxt.getText().toString().trim());
                } else {
                    Toast.makeText(getActivity(), "فضلا انتظر  ", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }



    private void pleaseWaitDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog2");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        pleaseWaitFragment = new PleaseWaitFragment(getString(R.string.loading));
        try {
            pleaseWaitFragment.show(ft, "dialog2");
        } catch (Exception e) {
            e.printStackTrace();
        }
        pleaseWaitFragment.setCancelable(false);

    }

    public void updateComment(String serverId) {/*
        callJson(URLs.ADD_COMMENT_URL, URLs.ADD_COMMENT_REQUEST_TYPE, serverId, articleObj.getID(), getActivity(), commentEditTxt.getText
                ().toString().trim());*/
//        Toast.makeText(getActivity(), userServerId + "", Toast.LENGTH_SHORT).show();
    }

    public void animateHolder(LinearLayout editHolder, float x, float y) {
        editHolder.setScaleX(1.4f);
        editHolder.setScaleY(1.4f);
        PropertyValuesHolder pVh = PropertyValuesHolder.ofFloat(editHolder.SCALE_X, 1.0f);
        PropertyValuesHolder pVh2 = PropertyValuesHolder.ofFloat(editHolder.SCALE_Y, 1.0f);
        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(editHolder, pVh, pVh2);
        objectAnimator.setRepeatMode(ValueAnimator.INFINITE);
        objectAnimator.setDuration(130);
        objectAnimator.start();
        editHolder.bringToFront();
        edit.bringToFront();
        delete.bringToFront();
        editDelHolder.setVisibility(View.VISIBLE);
        editDelHolder.setTranslationX(x);
        editDelHolder.setTranslationY(y + 150f);
        backGround.setVisibility(View.VISIBLE);
        editDelHolder.expand(editDelHolder);
    }

    public void onFinishUpdates(OnCommentsUpdate onCommentsUpdated) {
        this.onCommentsUpdated = onCommentsUpdated;
    }


    public interface OnCommentsUpdate {
        void onFinished(News newsObj, int index);
    }
}
