/*
package com.madarsoft.nabaa.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.appodeal.ads.NativeAd;
import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.analytics.HitBuilders;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.Views.WaterDropListView;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.adapters.NewsAdapter;
import com.madarsoft.nabaa.animation.ExpandableSelector;
import com.madarsoft.nabaa.animation.ExpandableSelectorListener;
import com.madarsoft.nabaa.animation.OnExpandableItemClickListener;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.controls.SpeedScrollListener;
import com.madarsoft.nabaa.controls.UpdateService2;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.interfaces.OnListIsFull;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

*/
/**
 * Created by basma on 8/7/2017.
 *//*


public class BasmaNewsFragment extends MadarFragment implements WaterDropListView
        .IWaterDropListViewListener, AbsListView.OnScrollListener{

    public RelativeLayout newPostsLayout;
    public List<News> tempList;
    public RecyclerView waterDropListView;
    public SpeedScrollListener listener;
    // private  Context context;
    NewsAdapter adapter = null;
    int currentFirstVisibleItem = 0;
    int currentVisibleItemCount = 0;
    int totalItemCount = 0;
    int currentScrollState = 0;
    BasmaNewsFragment.RecentNewsReceiver recentNewsReceiver = new BasmaNewsFragment.RecentNewsReceiver();
    List<Category> categories;
    Category category;
    boolean ishaveMore = false;
    boolean isHaveCountries = false;
    private Handler handler = new Handler();
    private Thread thread;
    private boolean isRecent;
    private RelativeLayout noSourcesSelectedLayout;
    private boolean noSources;
    private boolean firstTimeFlag;
    private List<News> newsList;
    private List<News> permNewsList;
    private RelativeLayout chooseSources;
    private RelativeLayout newsContainer;
    private RelativeLayout refresh;
    private OnListIsFull onListIsFull;
    private GifMovieView gifMovieView;
    private Context fragContext;
    private Animation animShow;
    private Animation animHide;
    private boolean isScrolled;
    private FontTextView noSourcesText;
    private RelativeLayout noNetwork;
    //private CategoryFragment categoryFragment;
    private int tempID;
    private boolean isRefresh;
    private Toast noRecentNews;
    private Toast noOldNews;
    private boolean isProcessingJson;
    private int blocked;
    private Toast noConnToast;
    private LinearLayout dimmedLayout;
    private DataBaseAdapter dp;
    private List<News> cachedNews = new ArrayList<>();
    //private static List<News> cachedNews = new LinkedList(Arrays.asList(new News[25]));
    private ExpandableSelector sizesExpandableSelector;
    private List<Category> countries;
    private List<Category> moreExpandableItems;
    private Handler mHandler;
    private ScheduledExecutorService worker;
    private BroadcastReceiver receiver;
    private boolean flag;
    private com.google.android.gms.analytics.Tracker mTracker;
    private PackageInfo pInfo;
    private int mLastFirstVisibleItem;
    private String title;
    private DrawerLayout mDrawerLayout;
    //    private NativeAd nativeAd;
    private List<News> virtualList;
    private List<News> tempCahsNews;
    private boolean isCashed;
    public   static   List<NativeAd> AllNativeAds = new ArrayList<>();
    private ArrayList<News> diffrentItems;

    // newInstance constructor for creating fragment with arguments
    public static NewsFragment newInstance(int page, String title) {
        NewsFragment fragmentFirst = new NewsFragment();
        Bundle args = new Bundle();
        args.putInt("page", page);
        args.putString("title", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    private void initializWaterDropListView(NewsFragment newsFrag, View view) {

        waterDropListView = (RecyclerView) view.findViewById(R.id.news_list_view);
        newPostsLayout = (RelativeLayout) view.findViewById(R.id.news_holder_new_posts);
        noSourcesSelectedLayout = (RelativeLayout) view.findViewById(R.id
                .no_sources_selected_layout);
        noSourcesText = (FontTextView) view.findViewById(R.id.no_sources_text);
        //waterDropListView.setWaterDropListViewListener(newsFrag);
       // waterDropListView.setPullLoadEnable(true);
        chooseSources = (RelativeLayout) view.findViewById(R.id.recent_news_choose_sources);
        newsContainer = (RelativeLayout) view.findViewById(R.id.news_container);
        refresh = (RelativeLayout) view.findViewById(R.id.no_network_button);

        try {
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case 1:
                            waterDropListView.stopRefresh();
                            break;
                        case 2:
                            waterDropListView.stopLoadMore();
                            break;
                    }
                }
            };
        } catch (IllegalStateException e) {
        }
        //waterDropListView.setOnScrollListener(newsFrag);
    }

    private void initializeSizesExpandableSelector() {
        final List<Category> expandableItems = new ArrayList<>();
        moreExpandableItems = new ArrayList<>();

        //countries = dp.getCountriesDesc();
        //List<Category> categories = dp.getSubjectDesc();
        //if (countries.size() == 0)
        final Handler handler = new Handler();

        new Thread(new Runnable() {
            @Override
            public void run() {


                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        countries = dp.getCategoryByGeoSelected();
                        //if (categories.size() == 0)
                        categories = dp.getCategoryBySubSelected();
                        Collections.sort(countries, new Comparator<Category>() {
                            @Override
                            public int compare(Category lhs, Category rhs) {
                                return Integer.valueOf(rhs.getCount()).compareTo(lhs.getCount());
                            }
                        });

                        Collections.sort(categories, new Comparator<Category>() {
                            @Override
                            public int compare(Category lhs, Category rhs) {
                                return Integer.valueOf(rhs.getCount()).compareTo(lhs.getCount());
                            }
                        });

                        if (countries.size() > 0) {
                            isHaveCountries = true;
                            for (int i = 0; i < categories.size(); i++) {
                                if (i < 3)
                                    expandableItems.add(categories.get(i));
                                else
                                    moreExpandableItems.add(categories.get(i));
                            }
                            Collections.sort(expandableItems, new Comparator<Category>() {
                                @Override
                                public int compare(Category lhs, Category rhs) {
                                    return Integer.valueOf(lhs.getCount()).compareTo(rhs.getCount());
                                }
                            });

                            category = new Category();
                            category.setID(0);
                            expandableItems.add(0, category);

                            if (moreExpandableItems.size() > 0) {
                                expandableItems.removeAll(moreExpandableItems);
                                category = new Category();
                                ishaveMore = true;
                                category.setCategory_name(fragContext.getString(R.string.more));
                                category.setID(URLs.MORE_ITEMS);
                                expandableItems.add(1, category);
                            }

                            category = new Category();
                            category.setCategory_name(fragContext.getString(R.string.countries));
                            category.setID(URLs.COUNTRIES_ITEMS);
                            expandableItems.add(expandableItems.size(), category);
                        } else {
                            for (int i = 0; i < categories.size(); i++) {
                                if (i < 4)
                                    expandableItems.add(categories.get(i));
                                else
                                    moreExpandableItems.add(categories.get(i));
                            }
                            Collections.sort(expandableItems, new Comparator<Category>() {
                                @Override
                                public int compare(Category lhs, Category rhs) {
                                    return Integer.valueOf(lhs.getCount()).compareTo(rhs.getCount());
                                }
                            });

                            category = new Category();
                            category.setID(0);
                            expandableItems.add(0, category);

                            if (moreExpandableItems.size() > 0) {
                                expandableItems.removeAll(moreExpandableItems);
                                category = new Category();
                                category.setCategory_name(fragContext.getString(R.string.more));
                                ishaveMore = true;
                                category.setID(URLs.MORE_ITEMS);
                                expandableItems.add(1, category);
                            }
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                            sizesExpandableSelector.showExpandableItems(expandableItems, isHaveCountries,
                                    ishaveMore, R.layout.expandable_selector_lollipop);
                        else
                            sizesExpandableSelector.showExpandableItems(expandableItems, isHaveCountries,
                                    ishaveMore, R.layout.expandable_selector);
                        dp.closeDB();
                        configureExpandableSelectorListener();
                        configureExpandableSelectorClickListener(getActivity());
                    }

                });
            }
        }).start();
    }

    private void configureExpandableSelectorListener() {

        sizesExpandableSelector.setExpandableSelectorListener(new ExpandableSelectorListener() {
            @Override
            public void onCollapse() {
            }

            @Override
            public void onExpand() {
                sizesExpandableSelector.bringToFront();
                sizesExpandableSelector.setAlpha(1);
                waterDropListView.setAlpha(0.2f);
                newPostsLayout.setAlpha(0.2f);
                dimmedLayout.setVisibility(View.VISIBLE);
                NewsHolderFragment.setDimValue(true);
            }

            @Override
            public void onCollapsed() {

            }

            @Override
            public void onExpanded() {
            }
        });
    }

    private void configureExpandableSelectorClickListener(final Context context) {

        sizesExpandableSelector.setOnExpandableItemClickListener(new OnExpandableItemClickListener() {
            @Override
            public void onExpandableItemClickListener(int index, View view) {
                if (sizesExpandableSelector.isCollapsed()) {
                    if (!(new MainControl(context)).checkInternetConnection()) {
                        noConnToast.show();
                        return;
                    }
                }

                if (view.getId() > 0) {
                    dp.increment(view.getId());
                }

                if (view.getId() == URLs.COUNTRIES_ITEMS) {
                    if (countries != null && !countries.isEmpty())
                        openFragment(countries, context, 1);
                }
                if (view.getId() == URLs.MORE_ITEMS) {
                    if (moreExpandableItems != null && !moreExpandableItems.isEmpty())
                        openFragment(moreExpandableItems, context, 2);
                }
                //Toast.makeText(getActivity(), view.getId() + "", Toast.LENGTH_SHORT).show();
                sizesExpandableSelector.collapse();
                waterDropListView.setAlpha(1f);
                newPostsLayout.setAlpha(1f);
                dimmedLayout.setVisibility(View.GONE);
                NewsHolderFragment.setDimValue(false);
            }
        });
    }

    private void openFragment(List<Category> list, Context context, int type) {
        FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim
                .pop_exit);
        transaction.addToBackStack(null);
        MenuCountry menuCountry = new MenuCountry(list, type);
        //NewsDetail2 nesDetails = new NewsDetail2("-8505763082654550151");
        transaction.add(R.id.parent, menuCountry);
        transaction.commit();
    }

    public void showData(final Context context, final List<News> newsLists, final String isNew) {
        permNewsList = newsLists;
        blocked = dp.getAllProfiles().get(0).getBlockImg();
        if (newsLists != null && noNetwork.getVisibility() == View.VISIBLE) {
            noNetwork.setVisibility(View.GONE);
//            gifMovieView.setVisibility(View.VISIBLE);
            gifMovieView.setPaused(false);
        }
        tempList = new ArrayList<>();
        tempList = newsLists;

        FragmentActivity activity = (FragmentActivity) context;
        try {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (isNew.equalsIgnoreCase("true")) {
                        if (newsLists == null || newsLists.isEmpty()) {
                            showNoSourcesView(context);
                            return;
                        }
                        initializeSizesExpandableSelector();
                        configureExpandableSelectorListener();
                        configureExpandableSelectorClickListener(context);
                        noSourcesSelectedLayout.setVisibility(View.GONE);
                        firstTimeFlag = true;
                        if (blocked > 0) {
                            for (int i = 0; i < newsLists.size(); i++) {
                                newsLists.get(i).setIsBlocked(1);
//                                if (i % 4 == 0 && i != 0) {
                                //                                newsLists.get(i).setNativeAd(new NativeAd(context,//
                                // "464488780344323_710228929103639"));
//                                }
                            }
                        } else {
                            for (int i = 0; i < newsLists.size(); i++) {
                                newsLists.get(i).setIsBlocked(-1);
//                                if (i % 4 == 0 && i != 0) {
                                //                                newsLists.get(i).setNativeAd(new NativeAd(context,//
                                // "464488780344323_710228929103639"));
//                                }

                            }
                        }
                        adapter = new NewsAdapter(MainActivity.mainActivity, context, newsLists, listener, false, false , null);
//adapter.setNativeAds(AllNativeAds);
                        //adapter.notifyDataSetChanged();

                        //URLs.setFirstNewsID(adapter.newsList.get(0).getID());
                        try {
                            if (title.equalsIgnoreCase(getString(R.string.recent_news_tag)))
                                URLs.setRecentFirstTimeStamp(adapter.newsList.get(0).getTime_stamp());
                                //URLs.setLastNewsID(adapter.newsList.get(newsLists.size() - 1).getID());
                            else
                                URLs.setUrgentFirstTimeStamp(adapter.newsList.get(0).getTime_stamp());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            if (title.equalsIgnoreCase(getString(R.string.recent_news_tag)))
                                URLs.setRecentLastTimeStamp(adapter.newsList.get(newsLists.size() - 1)
                                        .getTime_stamp());
                            else
                                URLs.setUrgentLastTimeStamp(adapter.newsList.get(newsLists.size() - 1)
                                        .getTime_stamp());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        waterDropListView.setAdapter(adapter);
                        waterDropListView.setVisibility(View.VISIBLE);
                        gifMovieView.setVisibility(View.GONE);
                        gifMovieView.setPaused(true);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNoSourcesView(Context context) {

        if (URLs.isTimoutExc())
            return;

        gifMovieView.setVisibility(View.GONE);
        gifMovieView.setPaused(true);

        if (cachedNews != null && !cachedNews.isEmpty()) {
            noRecentNews.show();
            return;
        }

        MainControl contol = new MainControl(context);
        if (!contol.checkInternetConnection()) {
            noNetwork.setVisibility(View.VISIBLE);
            return;
        }
        if (!dp.getSelectedSources().isEmpty()) {

            noSourcesText.setText(context.getString(R.string.no_news));
            chooseSources.setVisibility(View.INVISIBLE);
        }
        noSourcesSelectedLayout.setVisibility(View.VISIBLE);
        noSourcesSelectedLayout.bringToFront();
    }


    private void compareList(final List<News> serviceNews) {
        // Create a couple ArrayList objects and populate them
        // with some delicious fruits.
        diffrentItems = new ArrayList<>();
        // Remove all elements in firstList from secondList
        for (int i = 0; i < serviceNews.size(); i++) {
            if (serviceNews.get(i).getID() != cachedNews.get(0).getID()) {
                diffrentItems.add(serviceNews.get(i));
            } else
                break;
        }
    }

    public void showData2(final Context context, final List<News> serviceNews, final String
            isNew) throws NullPointerException {
        virtualList = new ArrayList<>();
        tempCahsNews = new ArrayList<>();

        FragmentActivity activity = (FragmentActivity) context;
        if (activity == null)
            return;
        try {
            activity.runOnUiThread(new Runnable() {

                public void run() {
                    gifMovieView.setVisibility(View.GONE);
                    gifMovieView.setPaused(true);
                    if (isNew.equalsIgnoreCase("true")) {
                        if (serviceNews == null || serviceNews.isEmpty()) {
                            gifMovieView.setVisibility(View.GONE);
                            gifMovieView.setPaused(true);
                            if (isRefresh) {
                                noRecentNews.show();
                                isRefresh = false;
                            }

                            return;
                        }
                        //getActivity().stopService(new Intent(getActivity().getBaseContext(), UpdateService2.class));
                        if (isRefresh) {
                            newPostsLayout.setVisibility(View.GONE);
                            newPostsLayout.startAnimation(animHide);
                        } else {
                            newPostsLayout.setVisibility(View.VISIBLE);
                            newPostsLayout.startAnimation(animShow);
                        }
                        try {
                            blockImage(serviceNews);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //URLs.setFirstNewsID(adapter.newsList.get(0).getID());
                        try {
                            if (title.equalsIgnoreCase(getString(R.string.recent_news_tag)))
                                URLs.setRecentFirstTimeStamp(serviceNews.get(0).getTime_stamp());
                            else
                                URLs.setUrgentFirstTimeStamp(serviceNews.get(0).getTime_stamp());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //                    boolean isEqual = serviceNews.containsAll(cachedNews) && cachedNews
                        //                            .containsAll(serviceNews);
                        boolean isEqual = false;
                        try {
                            isEqual = serviceNews.get(0).getNewsTitle().equalsIgnoreCase(cachedNews.get(0).getNewsTitle().toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (!cachedNews.isEmpty()) {
                            compareList(serviceNews);
                            cachedNews = MainControl.addToStack(cachedNews, serviceNews);
                            if (title.equalsIgnoreCase(getString(R.string.recent_news_tag)))
                                URLs.setRecentLastTimeStamp(cachedNews.get(cachedNews.size() - 1)
                                        .getTime_stamp());
                            else
                                URLs.setUrgentLastTimeStamp(cachedNews.get(cachedNews.size() - 1)
                                        .getTime_stamp());
                        }
                        if (isRefresh) {
                            newPostsLayout.setVisibility(View.GONE);
                            //                        newPostsLayout.startAnimation(animHide);
                            if (!cachedNews.isEmpty()) {
                                adapter.newsList = new ArrayList<>();
                                adapter.newsList.addAll(0, cachedNews);
                                //adapter.addList(cachedNews);
                            } else {
                                if (virtualList.size() > 0) {
                                    try {
                                        adapter.newsList.addAll(0, virtualList);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        adapter.newsList.addAll(0, serviceNews);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            //adapter.addList(serviceNews);
                            adapter.notifyDataSetChanged();
                            cachedNews = new ArrayList<>();
                            isRefresh = false;
                            gifMovieView.setVisibility(View.GONE);
                            gifMovieView.setPaused(true);
                            try {
                                getActivity().startService(new Intent(getContext(), UpdateService2.class));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //                        Looper.prepare();
                            //                        final Handler handler = new Handler();
                            //                        handler.postDelayed(new Runnable() {
                            //                            @Override
                            //                            public void run() {
                            //                                try {
                            //                                    getActivity().startService(new Intent(getContext(),
                            // UpdateService2.class));
                            //                                } catch (Exception e) {
                            //                                    e.printStackTrace();
                            //                                }
                            //                            }
                            //                        }, 120000);
                            return;
                        }
                        // updates occurs here if first item equal 0
                        //                    if (waterDropListView.getFirstVisiblePosition() == 0) {
                        if (!cachedNews.isEmpty()) {
                            gifMovieView.setVisibility(View.GONE);
                            gifMovieView.setPaused(true);
                            ////                            adapter.newsList = new ArrayList<>();
                            ////                            adapter.newsList.addAll(0, cachedNews);
                            ////                            adapter.notifyDataSetChanged();
                            if (isEqual) {
                                newPostsLayout.setVisibility(View.GONE);
                                newPostsLayout.startAnimation(animHide);
                                gifMovieView.setVisibility(View.GONE);
                                gifMovieView.setPaused(true);
                            } else {
                                isCashed = true;
                                virtualList.addAll(serviceNews);
                                ////                            tempCahsNews.addAll(cachedNews);
                                newPostsLayout.setVisibility(View.VISIBLE);
                                newPostsLayout.startAnimation(animShow);
                                getActivity().stopService(new Intent(getActivity().getBaseContext(), UpdateService2.class));
                            }
                            //adapter.addList(cachedNews);
                        } else {
                            gifMovieView.setVisibility(View.GONE);
                            gifMovieView.setPaused(true);
                            newPostsLayout.setVisibility(View.VISIBLE);
                            newPostsLayout.startAnimation(animShow);
                            //
                            virtualList.addAll(serviceNews);
                            getActivity().stopService(new Intent(getActivity().getBaseContext(), UpdateService2.class));
                            //                            adapter.newsList.addAll(0, serviceNews);
                        }
                        //                        if (isEqual&&cachedNews.isEmpty()) {
                        //                            newPostsLayout.setVisibility(View.GONE);
                        //                            newPostsLayout.startAnimation(animHide);
                        //                        } else {
                        //                            newPostsLayout.setVisibility(View.VISIBLE);
                        //                            newPostsLayout.startAnimation(animShow);
                        //                        }

                        // //    adapter.addList(serviceNews);
                        //                        adapter.notifyDataSetChanged();
                        cachedNews = new ArrayList<>();
                        if (serviceNews.size() == 0) {
                            newPostsLayout.setVisibility(View.GONE);
                            newPostsLayout.startAnimation(animHide);
                            gifMovieView.setVisibility(View.GONE);
                            gifMovieView.setPaused(true);
                        }
                        //                    }
                        //                     else
                        //                        adapter.setviceNewsList.clear();
                        //                        if (!cachedNews.isEmpty())
                        //                            adapter.setviceNewsList.addAll(cachedNews);
                        //                        else
                        //                            adapter.setviceNewsList.addAll(serviceNews);

                        //                    if (newPostsLayout.getVisibility() == View.GONE && !isEqual &&
                        //                            waterDropListView.getFirstVisiblePosition() != 0) {
                        //                        newPostsLayout.setVisibility(View.VISIBLE);
                        //                        newPostsLayout.startAnimation(animShow);
                        //                        isScrolled = true;
                        //                    }
                        //isScrolled = true;
                        gifMovieView.setVisibility(View.GONE);
                        gifMovieView.setPaused(true);

                    } else {
                        isProcessingJson = false;
                        if (serviceNews == null || serviceNews.isEmpty()) {
                            gifMovieView.setVisibility(View.GONE);
                            gifMovieView.setPaused(true);
                            noOldNews.show();
                            return;
                        }
                        try {
                            blockImage(serviceNews);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (serviceNews.get(serviceNews.size() - 1).getID() != adapter.newsList.get
                                (adapter.newsList.size() - 1).getID()) {//20848//21097
                            adapter.newsList.addAll(serviceNews);
                            gifMovieView.setVisibility(View.GONE);
                            gifMovieView.setPaused(true);
                            //adapter.appendList(serviceNews);
                            adapter.notifyDataSetChanged();
                            //URLs.setLastNewsID(adapter.newsList.get(adapter.newsList.size() - 1)
                            // .getID());
                            if (title.equalsIgnoreCase(getString(R.string.recent_news_tag)))
                                URLs.setRecentLastTimeStamp(adapter.newsList.get(adapter.newsList
                                        .size() - 1).getTime_stamp());
                            else
                                URLs.setUrgentLastTimeStamp(adapter.newsList.get(adapter.newsList
                                        .size() - 1).getTime_stamp());
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //UrgentNewsReceiver urgentNewsReceiver = new UrgentNewsReceiver();

    private void blockImage(List<News> list) {
        blocked = dp.getAllProfiles().get(0).getBlockImg();
        //NativeAd nativeAd = new NativeAd(getActivity(), "464488780344323_710228929103639");

        if (blocked > 0) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(1);
                if (i % 4 == 0 && i != 0) {
// list.get(i).setNativeAd(new NativeAd(getActivity(),
// "464488780344323_710228929103639"));

                }
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(-1);
                if (i % 4 == 0 && i != 0) {
//                    list.get(i).setNativeAd(new NativeAd(getActivity(),
// "464488780344323_710228929103639"));

                }

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!dp.getSelectedSources().isEmpty()) {

            chooseSources.setVisibility(View.INVISIBLE);
        }
        try {
            if (adapter.newsList.size() > 0) {
                gifMovieView.setVisibility(View.GONE);
                gifMovieView.setPaused(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //        try {
//            getActivity().startService(new Intent(getActivity().getBaseContext(), UpdateService2.class));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragContext = getActivity();
        dp = new DataBaseAdapter(getActivity());

        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        initAnimation();
        mTracker.setScreenName(getString(R.string.latest_news_screen));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        title = getArguments().getString("title");
        flag = true;
        firstTimeFlag = false;
        listener = new SpeedScrollListener();
        noRecentNews = Toast.makeText(getActivity(), getString(R.string.no_recent_news), Toast
                .LENGTH_SHORT);
        noOldNews = Toast.makeText(getActivity(), getString(R.string.no_other_news), Toast
                .LENGTH_SHORT);
        noConnToast = Toast.makeText(getActivity(), getString(R.string.no_internet), Toast
                .LENGTH_SHORT);

        permNewsList = new ArrayList<>();
        newsList = new ArrayList<>();
    }

    private boolean checkConnection() {
        MainControl mainControl = new MainControl(fragContext);
        try {
            if (!mainControl.checkInternetConnection()) {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        noConnToast.show();
                        gifMovieView.setVisibility(View.GONE);
                        gifMovieView.setPaused(true);
                        if (permNewsList.isEmpty())
                            noNetwork.setVisibility(View.VISIBLE);
                    }
                });
                return false;
            }
        } catch (NullPointerException e) {
        }
        // show loading on first screen opening only
        if (flag) {
            flag = false;
//            gifMovieView.setVisibility(View.VISIBLE);
//            gifMovieView.setPaused(false);
        }
        return true;
    }

    private void callJson(int userID, final String timeStamp, final int countArticle, final
    boolean isNew) {

        JsonParser jsonParser = null;

        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_USERID, userID + "");
        //dataObj.put(URLs.TAG_REQUEST_ARTICLE_ID, articleID + "");
        dataObj.put(URLs.TAG_COUNT_ARTICLE, countArticle + "");
        dataObj.put(URLs.TAG_IS_NEW, isNew + "");
        dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, timeStamp);

        if (title.equalsIgnoreCase("recentNews"))
            jsonParser = new JsonParser(getActivity(), URLs.GET_RECENT_NEWS_URL, URLs
                    .GET_RECENT_NEWS_REQUEST_TYPE, dataObj);
        else
            jsonParser = new JsonParser(getActivity(), URLs.GET_URGENT_NEWS_URL, URLs
                    .GET_URGENT_NEWS_REQUEST_TYPE, dataObj);

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonParser.onFinishUpdates(new JsonParser.OnNewsRetrieveListner() {
            @Override
            public void onFinished(List<News> newsList, String timeStamp, String isNew) {
                try {
                    if (timeStamp.equalsIgnoreCase("0"))
                        showData(fragContext, newsList, isNew);
                    else
                        showData2(fragContext, newsList, isNew);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        MainActivity.isDetail = true;
        if (title.equalsIgnoreCase("recentNews"))
            getActivity().registerReceiver(recentNewsReceiver, new IntentFilter("com.madarsoft" +
                    ".displayRecentNews"));
        else
            getActivity().registerReceiver(recentNewsReceiver, new IntentFilter("com.madarsoft" +
                    ".displayUrgNews"));
    }

    @Override
    public void onStop() {
        //LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
        super.onStop();
        firstTimeFlag = false;
        try {
            getActivity().unregisterReceiver(recentNewsReceiver);
        } catch (IllegalArgumentException e) {
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MainControl.writeCacheFile(getActivity(), new ArrayList(adapter.newsList
                            .subList(0, 25)), title);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
//        try {
//            getActivity().stopService(new Intent(getActivity().getBaseContext(), UpdateService2.class));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public void onBackButtonPressed() {
        super.onBackButtonPressed();
    }

    @Override
    public void onPause() {
        super.onPause();
        firstTimeFlag = false;
//        try {
//            getActivity().stopService(new Intent(getActivity().getBaseContext(), UpdateService2.class));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    private void setRedViewInDrawer() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View firstRedView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        firstRedView.setVisibility(View.GONE);
        View redView = (View) vx.getChildAt(5).findViewById(R.id.red_view);
        redView.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.news_list_layout, container, false);
        sizesExpandableSelector = (ExpandableSelector) view.findViewById(R.id.es_sizes);
        gifMovieView = (GifMovieView) view.findViewById(R.id.news_feed_loading);
        dimmedLayout = (LinearLayout) view.findViewById(R.id.dimmed_layout);
        noNetwork = (RelativeLayout) view.findViewById(R.id.newslist_no_network);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        initializWaterDropListView(this, view);
        getActivity().startService(new Intent(getActivity().getBaseContext(), UpdateService2.class));
        gifMovieView.bringToFront();
        gifMovieView.setMovieResource(R.drawable.loading);
        chooseSources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R
                        .anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new CategoryFragment());
                transaction.commit();
                try {
                    setRedViewInDrawer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        newsContainer.setVisibility(View.VISIBLE);
        //noSourcesSelectedLayout.setVisibility(View.GONE);
        waterDropListView.setVisibility(View.VISIBLE);
        noSources = false;
        //}

        final MainControl ma = new MainControl(getActivity());
        if (!ma.checkInternetConnection()) {
            //noNetwork.setVisibility(View.VISIBLE);
            refresh.bringToFront();
            waterDropListView.setVisibility(View.GONE);
            //noSourcesSelectedLayout.setVisibility(View.GONE);
        }

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ma.checkInternetConnection()) {
                    noNetwork.setVisibility(View.GONE);
                    newsContainer.setVisibility(View.VISIBLE);
                    waterDropListView.setVisibility(View.VISIBLE);
                    checkConnection();
                    callJson(URLs.getUserID(), "0", 25, true);

                } else {
                    //noConnToast.show();
                }
            }
        });
        newPostsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    newPostsLayout.startAnimation(animHide);
                    newPostsLayout.setVisibility(View.GONE);
                    isScrolled = false;
                    final MainControl ma = new MainControl(getActivity());
                    if (!ma.checkInternetConnection()) {
                        noConnToast.show();
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                if (waterDropListView.getFirstVisiblePosition() == 0) {
//                    adapter.notifyDataSetChanged();
//                } else {
                try {
                    if (blocked > 0) {
                        for (int i = 0; i < adapter.setviceNewsList.size(); i++) {
                            adapter.setviceNewsList.get(i).setIsBlocked(1);
                        }
                    } else {
                        for (int i = 0; i < adapter.setviceNewsList.size(); i++) {
                            adapter.setviceNewsList.get(i).setIsBlocked(-1);
                        }
                    }
                    if (virtualList.size() == 0) {
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
//                        MainActivity.myArray.get(0).toString();
                    if (isCashed) {
                        adapter.newsList = new ArrayList<>();
                        adapter.newsList.addAll(0, virtualList);
                        isCashed = false;
                        MainControl.smoothScrollToPosition(waterDropListView, 0);
                   */
/* waterDropListView.setSelectionAfterHeaderView();
                    waterDropListView.setSelection(0);*//*

                        adapter.notifyDataSetChanged();
                    } else {
                        adapter.newsList.addAll(0, virtualList);
                        diffrentItems.clear();
                        MainControl.smoothScrollToPosition(waterDropListView, 0);
                   */
/* waterDropListView.setSelectionAfterHeaderView();
                    waterDropListView.setSelection(0);*//*

                        adapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                    if (!cachedNews.isEmpty()) {
//                        adapter.newsList = new ArrayList<>();
//                        adapter.newsList.addAll(0, adapter.setviceNewsList);
//                    } else {
//                        adapter.newsList.addAll(0, adapter.setviceNewsList);
//                    }
//                    adapter.setviceNewsList.clear();
//                    cachedNews.clear();

//                } else {
//                    if (blocked > 0) {
//                        for (int i = 0; i < adapter.setviceNewsList.size(); i++) {
//                            adapter.setviceNewsList.get(i).setIsBlocked(1);
//
//                        }
//                    } else {
//                        for (int i = 0; i < adapter.setviceNewsList.size(); i++) {
//                            adapter.setviceNewsList.get(i).setIsBlocked(-1);
//
//                        }
//                    }
//                    if (!cachedNews.isEmpty()) {
//                        adapter.newsList = new ArrayList<>();
//                        adapter.newsList.addAll(0, adapter.setviceNewsList);
//                    } else {
//                        adapter.newsList.addAll(0, adapter.setviceNewsList);
//                    }
//                    adapter.setviceNewsList.clear();
//                    cachedNews.clear();
                MainControl.smoothScrollToPosition(waterDropListView, 0);
//                   */
/* waterDropListView.setSelectionAfterHeaderView();
//                    waterDropListView.setSelection(0);*//*

//                    adapter.notifyDataSetChanged();
//                }
                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                getActivity().startService(new Intent(getContext(), UpdateService2.class));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 120000);
                    virtualList.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        try {
            cachedNews = MainControl.readCachedFile(new File(getActivity().getCacheDir()
                    .getAbsoluteFile() + File.separator + title));
            if (!cachedNews.isEmpty()) {
                //URLs.setLastTimeStamp(cachedNews.get(cachedNews.size() - 1).getTime_stamp());
                blocked = dp.getAllProfiles().get(0).getBlockImg();
                if (blocked > 0) {
                    for (int i = 0; i < cachedNews.size(); i++) {
                        cachedNews.get(i).setIsBlocked(1);

                    }
                } else {
                    for (int i = 0; i < cachedNews.size(); i++) {
                        cachedNews.get(i).setIsBlocked(-1);
                    }
                }
//                if()
                adapter = new NewsAdapter(MainActivity.mainActivity, getContext(), cachedNews, listener, false, false , null);
                // adapter.setNativeAds(AllNativeAds);
                // adapter.notifyDataSetChanged();
                if (title.equalsIgnoreCase(getString(R.string.recent_news_tag))) {
                    URLs.setRecentLastTimeStamp(cachedNews.get(cachedNews.size() - 1)
                            .getTime_stamp());

                } else {
                    URLs.setUrgentLastTimeStamp(cachedNews.get(cachedNews.size() - 1)
                            .getTime_stamp());
                }
                waterDropListView.setAdapter(adapter);
                waterDropListView.setVisibility(View.VISIBLE);
                permNewsList = cachedNews;
                initializeSizesExpandableSelector();
                flag = false;
            } else
                adapter.newsList.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        checkConnection();

        return view;
    }

    @Override
    public void onRefresh() {
        */
/*if (!diffrentItems.isEmpty()) {
            adapter.newsList.addAll(0, diffrentItems);

            diffrentItems.clear();
        }*//*

//        try {
//            getActivity().stopService(new Intent(getContext(), UpdateService2.class));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        newPostsLayout.setVisibility(View.GONE);
        try {
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (newPostsLayout.getVisibility() == View.VISIBLE) {
//                newPostsLayout.startAnimation(animHide);
                newPostsLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //firstTimeFlag = true;
                isRefresh = true;
                if (checkConnection()) {
                    try {
                        callJson(URLs.getUserID(), (title.equalsIgnoreCase(getString(R.string
                                .recent_news_tag)) ? URLs.getRecentFirstTimeStamp() : URLs
                                .getUrgentFirstTimeStamp()), 25, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                handler.sendEmptyMessage(1);
//                try {
////                    newPostsLayout.startAnimation(animHide);
//                    newPostsLayout.setVisibility(View.GONE);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                Looper.prepare();
//                final Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
                try {
                    getActivity().startService(new Intent(getContext(), UpdateService2.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                    }
//                }, 120000);
            }


        });
        try {
            getActivity().startService(new Intent(getContext(), UpdateService2.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        newPostsLayout.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getActivity().stopService(new Intent(getContext(), UpdateService2.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLoadMore() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    if (checkConnection()) {
                        if (!isProcessingJson)
                            //callJson(URLs.getUserID(), URLs.getRecnetLastTimeStamp(), 25, false);
                            callJson(URLs.getUserID(), (title.equalsIgnoreCase(getString(R.string
                                    .recent_news_tag)) ? URLs.getRecnetLastTimeStamp() : URLs
                                    .getUrgentLastTimeStamp()), 25, false);
                    }
                    //checkConnection();
                    handler.sendEmptyMessage(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

        this.currentScrollState = scrollState;
        this.isScrollCompleted();

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItemCount = totalItemCount;
        if (mLastFirstVisibleItem < firstVisibleItem) {
            Log.i("SCROLLING DOWN", "TRUE");
            sizesExpandableSelector.setAlpha(0.2f);
//            if (newPostsLayout.getVisibility() == View.VISIBLE) {
//                newPostsLayout.startAnimation(animHide);
//                newPostsLayout.setVisibility(View.GONE);
//            }
        }
        if (mLastFirstVisibleItem > firstVisibleItem) {
            Log.i("SCROLLING UP", "TRUE");
            sizesExpandableSelector.setAlpha(0.2f);
            if (newPostsLayout.getVisibility() == View.GONE && isScrolled) {
                newPostsLayout.setVisibility(View.VISIBLE);
                newPostsLayout.startAnimation(animShow);
            }
        }
        mLastFirstVisibleItem = firstVisibleItem;
    }

    private void isScrollCompleted() {
        sizesExpandableSelector.setAlpha(1.0f);
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE &&
                this.totalItemCount == (currentFirstVisibleItem + currentVisibleItemCount)) {
            */
/*** In this way I detect if there's been a scroll which has completed ***//*

            */
/*** do the work for load more date! ***//*

            if (checkConnection()) {
                isProcessingJson = true;
                //callJson(URLs.getUserID(), URLs.getRecnetLastTimeStamp(), 25, false);
                callJson(URLs.getUserID(), (title.equalsIgnoreCase(getString(R.string
                        .recent_news_tag)) ? URLs.getRecnetLastTimeStamp() : URLs
                        .getUrgentLastTimeStamp()), 25, false);
            }
        }
        if (isScrolled && currentFirstVisibleItem == 0 && newPostsLayout.getVisibility() == View
                .VISIBLE) {
//            newPostsLayout.startAnimation(animHide);
//            adapter.notifyDataSetChanged();
//            newPostsLayout.setVisibility(View.GONE);
            isScrolled = false;
            // && !adapter.newsList.containsAll(adapter.setviceNewsList)
            if (!adapter.setviceNewsList.isEmpty()) {
                if (!cachedNews.isEmpty()) {
                    adapter.newsList = new ArrayList<>();
//                    adapter.newsList.addAll(0, adapter.setviceNewsList);
                } else {
//                    adapter.newsList.addAll(0, adapter.setviceNewsList);
                }
                adapter.setviceNewsList.clear();
                cachedNews.clear();
                adapter.notifyDataSetChanged();
            }
        }
    }

    public OnListIsFull getOnListIsFull() {
        return onListIsFull;
    }

    public void setOnListIsFull(OnListIsFull onListIsFull) {
        this.onListIsFull = onListIsFull;
    }

    private void initAnimation() {
        animShow = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        animHide = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
    }

    public void scrollToTop() throws NullPointerException {
        MainControl.smoothScrollToPosition(waterDropListView, 0);
//        if (checkConnection()) {
//            callJson(URLs.getUserID(), (title.equalsIgnoreCase(getString(R.string
//                    .recent_news_tag)) ? URLs.getRecentFirstTimeStamp() : URLs
//                    .getUrgentFirstTimeStamp()), 25, true);
//        }
//        adapter.getCount();
//        waterDropListView.setSelection(0);
    }




    public class RecentNewsReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                //if (!URLs.serviceFlag) {
                URLs.serviceFlag = true;
                ArrayList<News> myList = intent.getParcelableArrayListExtra("recentNews");
                virtualList = myList;
                String isNew = intent.getStringExtra("isNew");
                String timeStamp = intent.getStringExtra("timeStamp");
                if (!timeStamp.equals("0") || !cachedNews.isEmpty()) {
                    //if (!list.isEmpty()) {
                    showData2(fragContext, myList, isNew);
                    //tempID = articleID;
                    myList.clear();
                    //}
                    return;
                }
                try {
                    blockImage(myList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                showData(fragContext, myList, isNew);
                //}
            } catch (NullPointerException e) {
            }
            //Toast.makeText(context,aa.size()+"",Toast.LENGTH_SHORT).show();
        }
    }
}
*/
