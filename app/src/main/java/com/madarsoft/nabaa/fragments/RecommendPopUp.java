package com.madarsoft.nabaa.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.activities.MainActivity;

import static com.madarsoft.nabaa.R.id.parent;

/**
 * Created by Colossus on 20-Jun-17.
 */

public class RecommendPopUp extends DialogFragment {


    private SharedPreferences settingsM;
    private SharedPreferences.Editor settingEditor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation2;
        dialog.setCancelable(false);
        settingsM = getActivity().getSharedPreferences(MainActivity.PREFS_NAME, Context.MODE_PRIVATE);
        settingEditor = getActivity().getSharedPreferences(MainActivity.PREFS_NAME, Context.MODE_PRIVATE).edit();

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.recommended_sources_text, container, false);
        ImageView cancel = (ImageView) v.findViewById(R.id.cancel);
        FontTextView chooseSources = (FontTextView) v.findViewById(R.id.choose);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingEditor.putBoolean("recommend", false).apply();
                getDialog().dismiss();
            }
        });

        chooseSources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingEditor.putBoolean("recommend", false).apply();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                CategoryFragment categoryFragment = new CategoryFragment();
                transaction.replace(parent, categoryFragment, "category");
                transaction.commit();
                getDialog().dismiss();
            }
        });

        return v;
    }

}
