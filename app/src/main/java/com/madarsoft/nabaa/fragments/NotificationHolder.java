package com.madarsoft.nabaa.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmPubSub;
import com.madarsoft.nabaa.GCM.RegisterApp;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.OnOffSwitch;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Profile;

import java.util.ArrayList;

/**
 * Created by Colossus on 06-Sep-16.
 */
public class NotificationHolder extends MadarFragment {

    final String PREFS_NAME = "FIRSTENTRY";
    private ImageView menu;
    private DrawerLayout mDrawerLayout;
//    private OnOffSwitch urgentNotify;
//    private OnOffSwitch generalNotify;
//    private DataBaseAdapter dp;
    private ArrayList<Profile> profile;
    private Profile objProfile;
    private boolean isGeneralClosed;
    private RelativeLayout generalHolder;
    private RelativeLayout urgentHolder;
    private boolean isUrgentClosed;
    private ImageView home;
    private boolean isRemove;
    private MainControl ma;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        menu = (ImageView) getActivity().findViewById(R.id.settings_menu);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

//        urgentNotify = (OnOffSwitch) getActivity().findViewById(R.id.notify_on_off_urgent);
//        generalNotify = (OnOffSwitch) getActivity().findViewById(R.id.notify_on_off_general);
        generalHolder = (RelativeLayout) getActivity().findViewById(R.id.notification_holder_general);
        urgentHolder = (RelativeLayout) getActivity().findViewById(R.id.notification_holder_urgent);
        home = (ImageView) getActivity().findViewById(R.id.my_sources_news_home_button);
//        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ma = new MainControl(getActivity());
//        dp = new DataBaseAdapter(getActivity());
//        profile = new ArrayList<>();
//        profile = dp.getAllProfiles();
        try {
            setRedViewInDrawer();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        objProfile = profile.get(0);
//
//        if (objProfile.getSoundType() > 0) {
//            generalNotify.setSwitch(true);
//            isGeneralClosed = false;
//        } else {
//            generalNotify.setSwitch(false);
//            isGeneralClosed = true;
//        }
//
//        if (objProfile.getUrgentFlag() > 0) {
//            urgentNotify.setSwitch(true);
//            isUrgentClosed = false;
//        } else {
//            urgentNotify.setSwitch(false);
//            isUrgentClosed = true;
//        }
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.RIGHT);
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new NewsHolderFragment(), "newsHolder");
                transaction.commit();
                try {
                    setRedViewInDrawer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
//        generalNotify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (!ma.checkInternetConnection()) {
//                    Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_LONG).show();
//                    return;
//                }
//                if (objProfile.getSoundType() > 0) {
//                    generalNotify.setSwitch(false);
//                    objProfile.setSoundType(-1);
//                    dp.updateProfiles(objProfile);
//                    isGeneralClosed = true;
//                    isRemove = true;
//                   new GeneralSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "True");
//                } else {
//                    generalNotify.setSwitch(true);
//                    objProfile.setSoundType(1);
//                    isRemove = false;
////                    new GeneralSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "True");
//                    dp.updateProfiles(objProfile);
//                    isGeneralClosed = false;
////
////                    SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
////                    if (settings.getBoolean("my_first_time_general", true)) {
////                        try {
////                            Thread.sleep(1000);
////                        } catch (InterruptedException e) {
////                            e.printStackTrace();
////                        }
////                        FragmentManager manager = getActivity().getSupportFragmentManager();
////                        FragmentTransaction transaction = manager.beginTransaction();
////                        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
////                        transaction.addToBackStack(null);
////                        transaction.replace(R.id.parent, new MySourcesNotifications(1));
////                        transaction.commit();
////                        // record the fact that the app has been started at least once
////                        settings.edit().putBoolean("my_first_time_general", false).commit();
////                        return;
////                    }
//                }
//            }
//        });
//
//        urgentNotify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!ma.checkInternetConnection()) {
//                    Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_LONG).show();
//                    return;
//                }
//                if (objProfile.getUrgentFlag() > 0) {
//                    urgentNotify.setSwitch(false);
//                    objProfile.setUrgentFlag(-1);
//                    dp.updateProfiles(objProfile);
//                    isUrgentClosed = true;
//                    isRemove = true;
////                    new GeneralSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "Urgent");
//                } else {
//                    urgentNotify.setSwitch(true);
//                    objProfile.setUrgentFlag(1);
//                    dp.updateProfiles(objProfile);
//                    isRemove = false;
////                    new GeneralSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "Urgent");
//                    isUrgentClosed = false;
////
////                    SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
////                    if (settings.getBoolean("my_first_time_urgent", true)) {
////                        try {
////                            Thread.sleep(1000);
////                        } catch (InterruptedException e) {
////                            e.printStackTrace();
////                        }
////                        FragmentManager manager = getActivity().getSupportFragmentManager();
////                        FragmentTransaction transaction = manager.beginTransaction();
////                        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
////                        transaction.addToBackStack(null);
////                        transaction.replace(R.id.parent, new MySourcesNotifications(2));
////                        transaction.commit();
////                        // record the fact that the app has been started at least once
////                        settings.edit().putBoolean("my_first_time_urgent", false).commit();
////                        return;
////                    }
//                }
//            }
//        });
        generalHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ma.checkInternetConnection()) {
                    Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                    return;
                }
                if (isGeneralClosed) {
                    return;
                } else {
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    transaction.addToBackStack(null);
                    transaction.replace(R.id.parent, new MySourcesNotifications(1));
                    transaction.commit();
                }
            }
        });


        urgentHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ma.checkInternetConnection()) {
                    Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                    return;
                }
                if (isUrgentClosed) {
                    return;
                } else {
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    transaction.addToBackStack(null);
                    transaction.replace(R.id.parent, new MySourcesNotifications(2));
                    transaction.commit();
                }
            }
        });
    }

    private void setRedViewInDrawer() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View newsFragmentRedView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        View NotificationRed = (View) vx.getChildAt(8).findViewById(R.id.red_view);
        newsFragmentRedView.setVisibility(View.GONE);
        NotificationRed.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notification_holder, container, false);
    }

//    class GeneralSubscription extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected String doInBackground(String... params) {
//            GcmPubSub pubSub = null;
//            SharedPreferences prefs = null;
//            try {
//                pubSub = GcmPubSub.getInstance(getActivity().getApplicationContext());
//                prefs = getActivity().getSharedPreferences(MainActivity.MyPREFERENCES,
//                        Context.MODE_PRIVATE);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            String token = null;
//            try {
//                token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            try {
//                if (isRemove) {
//                    pubSub.unsubscribe(token, "/topics/" + params[0] + "");
//                } else {
//                    pubSub.subscribe(token, "/topics/" + params[0] + "", null);
//                }
//            } catch (Exception e) {
//
//            }
//            return "";
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            // might want to change "executed" for the returned string passed
//            // into onPostExecute() but that is upto you
//        }
//    }
}