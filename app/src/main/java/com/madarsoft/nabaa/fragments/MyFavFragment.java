package com.madarsoft.nabaa.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.analytics.HitBuilders;/*
import com.madarsoft.firebasedatabasereader.interfaces.AdViewLoadListener;
import com.madarsoft.firebasedatabasereader.objects.RectangleBannerAd;*/
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.adapters.MyFavAdapter;
import com.madarsoft.nabaa.controls.AdsControlNabaa;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.controls.SpeedScrollListener;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.URLs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import com.madarsoft.firebasedatabasereader.interfaces.AdViewListener;

/**
 * Created by Colossus on 15-May-16.
 */

public class MyFavFragment extends MadarFragment implements AbsListView.OnScrollListener {

    public static List<News> tempList;
    public static MyFavAdapter adapter;
    public static ListView favListView;
    public static GifMovieView gifMovieView;
    private static SpeedScrollListener listener;
    private static RelativeLayout noFavSelected;
    private static List<News> news;
    private static boolean firstTimeFlag = false;
    private static Context fragContext;
    private static int blocked;
    private static DataBaseAdapter dataBase;
    int currentFirstVisibleItem = 0;
    int currentVisibleItemCount = 0;
    int totalItemCount = 0;
    int currentScrollState = 0;
    private ImageView menu;
    private DrawerLayout mDrawerLayout;
    private JsonParser jsonParser;
    private boolean flag_loading;
    private RelativeLayout noNetwork;
    private RelativeLayout refresh;
    private boolean flag;
    private boolean isJsonProcessing;
    private com.google.android.gms.analytics.Tracker mTracker;
    private ImageView home;
    private LinearLayout topAdView;
    private LinearLayout bottomAdView;
    private AdsControlNabaa adsControl_bottom;
    private AdsControlNabaa adsControl_top;

    public static void showData(final Context context, final List<News> newsLists) {
        tempList = new ArrayList<>();
        tempList = newsLists;
        listener = new SpeedScrollListener();
        try {
            blockImage(newsLists, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentActivity activity = (FragmentActivity) context;
        try {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if (!firstTimeFlag) {
                        if (newsLists.isEmpty()) {
                            gifMovieView.setEnabled(false);
                            gifMovieView.setVisibility(View.GONE);
                            noFavSelected.setVisibility(View.VISIBLE);
                            return;
                        }
                        adapter = new MyFavAdapter(context, listener, newsLists);
                        favListView.setAdapter(adapter);
                        favListView.setVisibility(View.VISIBLE);
                        gifMovieView.setVisibility(View.GONE);

                    } else {
                        adapter.appendData(newsLists);
                        gifMovieView.setVisibility(View.GONE);
                    }
                }
            });
        } catch (NullPointerException w) {

        }

    }

    private static void blockImage(List<News> list, Context context) {
        blocked = dataBase.getAllProfiles().get(0).getBlockImg();
        //NativeAd nativeAd = new NativeAd(getActivity(), "464488780344323_710228929103639");

        if (blocked > 0) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(1);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(-1);

            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        dataBase = new DataBaseAdapter(getActivity());
        mTracker.setScreenName(getString(R.string.favourite_news));
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        flag = true;
        fragContext = getActivity();
        menu = (ImageView) getActivity().findViewById(R.id.my_fav_menu);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        favListView = (ListView) getActivity().findViewById(R.id.fav_list);
        gifMovieView = (GifMovieView) getActivity().findViewById(R.id.my_fav_loading);
        noFavSelected = (RelativeLayout) getActivity().findViewById(R.id.webview_error);
        noNetwork = (RelativeLayout) getActivity().findViewById(R.id.newslist_no_network);
        refresh = (RelativeLayout) getActivity().findViewById(R.id.no_network_button);
        favListView.setOnScrollListener(MyFavFragment.this);
        home = (ImageView) getActivity().findViewById(R.id.my_fav_news_home_button);
        //Banner Ads View
        topAdView = (LinearLayout) getActivity().findViewById(R.id.fav_top_ad_view);
        bottomAdView = (LinearLayout) getActivity().findViewById(R.id.fav_bottom_ad_view);
        adsControl_top = new AdsControlNabaa(getActivity());
        adsControl_bottom = new AdsControlNabaa(getActivity());
        URLs.setFavNewsID(0);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.RIGHT);
            }
        });
        try {
            setRedViewInDrawer(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        final MainControl ma = new MainControl(getActivity());
        if (!ma.checkInternetConnection()) {
            noNetwork.setVisibility(View.VISIBLE);
            refresh.bringToFront();
            favListView.setVisibility(View.GONE);
            //noSourcesSelectedLayout.setVisibility(View.GONE);
        }
        adsControl_top.getBannerAd(topAdView, "fav_top");
        adsControl_top.getSplashAd( "fav_top");
    /*    RectangleBannerAd banner = (new RectangleBannerAd.Builder()).adContainer(topAdView)
                .build();
        adsControl_top.getBannerAd(banner, "fav_top");
        banner.setAdListener(new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                topAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdError() {
                topAdView.setVisibility(View.GONE);

            }
        });
*/
        adsControl_bottom.getBannerAd(bottomAdView, "fav_bottom");
    /*
        RectangleBannerAd banner_bottom = (new RectangleBannerAd.Builder()).adContainer(bottomAdView)
                .build();
        adsControl_bottom.getBannerAd(banner, "fav_bottom");
       banner_bottom.setAdListener(new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                bottomAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdError() {
                bottomAdView.setVisibility(View.GONE);

            }
        });*/

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new NewsHolderFragment(), "newsHolder");
                transaction.commit();
                try {
                    setRedViewInDrawer(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ma.checkInternetConnection()) {
                    noNetwork.setVisibility(View.GONE);
                    checkConnection();
                    gifMovieView.bringToFront();
                    gifMovieView.setMovieResource(R.drawable.loading);
                    gifMovieView.setVisibility(View.VISIBLE);
                    gifMovieView.setPaused(false);
                    callJson(URLs.FAVOURITE_NEWS_URL, URLs.FAVOURITE_NEWS_REQUEST_TYPE, URLs.getUserID(), 0, getActivity(), 4);
//                    favListView.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getActivity(), "No Network ", Toast.LENGTH_SHORT).show();

                }
            }
        });

        gifMovieView.bringToFront();
        gifMovieView.setMovieResource(R.drawable.loading);
        gifMovieView.setVisibility(View.VISIBLE);
        gifMovieView.setPaused(false);
        callJson(URLs.FAVOURITE_NEWS_URL, URLs.FAVOURITE_NEWS_REQUEST_TYPE, URLs.getUserID(), 0, getActivity(), 4);

    }

    public void keepOrRemove() {
        Fragment frg = null;
        frg = getActivity().getSupportFragmentManager().findFragmentByTag("myFav");
        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.detach(frg);
        ft.attach(frg);
        ft.commit();
    }

    private void setRedViewInDrawer(boolean myFav) {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View firstRedView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        firstRedView.setVisibility(View.VISIBLE);
        View redView = (View) vx.getChildAt(4).findViewById(R.id.red_view);
        redView.setVisibility(View.GONE);
        if (myFav) {
            firstRedView.setVisibility(View.GONE);
            redView.setVisibility(View.VISIBLE);
        } else {
            firstRedView.setVisibility(View.VISIBLE);
            redView.setVisibility(View.GONE);
        }
    }

    private void checkConnection() {
        MainControl mainControl = new MainControl(fragContext);
        try {
            if (!mainControl.checkInternetConnection()) {
                gifMovieView.setVisibility(View.GONE);
                gifMovieView.setPaused(true);
                try {
                    if (tempList.isEmpty())
                        noNetwork.setVisibility(View.VISIBLE);
                    return;
                } catch (NullPointerException e) {
                }

            }
        } catch (NullPointerException e) {
        }
        // show loading on first screen opening only
        if (flag) {
            flag = false;
            gifMovieView.setVisibility(View.VISIBLE);
            gifMovieView.setPaused(false);
        }
    }

    private void callJson(String url, final int requestType, int userID, int id, final Context context, int count) {
        MainControl mainControl = new MainControl(context);
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            //uncomment this lines
            gifMovieView.setVisibility(View.GONE);
            gifMovieView.setPaused(true);
            return;
        }
        isJsonProcessing = true;
        HashMap<String, String> dataObj = new HashMap<>();
        dataObj.put(URLs.TAG_FAVOURITE_NEWS_ARTICLE_ID, id + "");
        dataObj.put(URLs.TAG_FAVOURITE_NEWS_USER_ID, userID + "");
        dataObj.put(URLs.TAG_FAVOURITE_NEWS_COUNT, count + "");

        jsonParser = new JsonParser(getActivity(), url, requestType, dataObj);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();

        jsonParser.onFinishUpdates(new JsonParser.OnRetrieveFavouriteNewsListener() {
            @Override
            public void onFinished(List<News> favouriteNews) {
                isJsonProcessing = false;
                showData(getActivity(), favouriteNews);
                Log.i("ADSAD", "adsasD");

            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItemCount = totalItemCount;
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE && this.totalItemCount ==
                (currentFirstVisibleItem + currentVisibleItemCount)) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
//            callJson(URLs.FAVOURITE_NEWS_URL, URLs.FAVOURITE_NEWS_REQUEST_TYPE, URLs.getUserID(), URLs.getFavNewsID(), getActivity(),
// currentVisibleItemCount);
            if (!isJsonProcessing) {
                callJson(URLs.FAVOURITE_NEWS_URL, URLs.FAVOURITE_NEWS_REQUEST_TYPE, URLs.getUserID(), URLs.getFavNewsID(), getActivity(),
                        4);
                firstTimeFlag = true;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_fav, container, false);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        firstTimeFlag = false;
    }
}
