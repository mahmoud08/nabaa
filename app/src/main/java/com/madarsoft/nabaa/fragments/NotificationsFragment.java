//package com.madarsoft.hinewz.fragments;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.widget.DrawerLayout;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.ProgressBar;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.RelativeLayout;
//
//import com.basv.gifmoviewview.widget.GifMovieView;
//import com.google.android.gms.analytics.HitBuilders;
//import com.madarsoft.hinewz.R;
//import com.madarsoft.hinewz.Views.OnOffSwitch;
////import com.madarsoft.hinewz.adapters.NotificationAdapter;
//import com.madarsoft.hinewz.adapters.NotificationsAdapter;
//import com.madarsoft.hinewz.controls.AnalyticsApplication;
//import com.madarsoft.hinewz.controls.JsonParser;
//import com.madarsoft.hinewz.controls.MainControl;
//import com.madarsoft.hinewz.database.DataBaseAdapter;
//import com.madarsoft.hinewz.entities.Profile;
//import com.madarsoft.hinewz.entities.Sources;
//
//import java.util.ArrayList;
//
///**
// */
//public class NotificationsFragment extends MadarFragment {
//
////    private static final String URGENT_TOPIC = "urgent";
//    public boolean isUtility;
//    ListView notificationsListView;
//    NotificationAdapter adapter;
//    DataBaseAdapter dp;
//    OnOffSwitch allAlerts;
//    boolean test;
//    //    Subscription sub;
//    private android.support.v4.widget.DrawerLayout mDrawerLayout;
//    //    private ImageView menu;
//    private OnOffSwitch generalNotify;
//    private OnOffSwitch urgentNotify;
//    private int urgentNotificationStatus;
//    private View greyView;
//    private int generalNotification;
//    private RadioGroup soundRadioGroup;
//    private RadioButton soundRadio;
//    private RadioButton vibrationRadio;
//    private RadioButton bothRadio;
//    private ArrayList<Profile> profile;
//    private Profile objProfile;
//    private RelativeLayout holder;
//    private MainControl mainControl;
//    private JsonParser jsonParser;
//    private ImageView separator;
//    private boolean isRemove;
//    //    private Subscription asub;
//    private ProgressBar progress;
//    private com.google.android.gms.analytics.Tracker mTracker;
//    private GifMovieView gifMovieView;
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
//        mTracker = application.getDefaultTracker();
//
//        mTracker.setScreenName(getString(R.string.notifications_screen));
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
//
//        notificationsListView = (ListView) getActivity().findViewById(R.id.notifications_list_view);
////        final ScrollView scrollview = ((ScrollView) getActivity().findViewById(R.id.notification_scroll));
//
//        generalNotify = (OnOffSwitch) getActivity().findViewById(R.id.notify_on_off_general);
////        urgentNotify = (OnOffSwitch) getActivity().findViewById(R.id.notify_on_off_urgent);
////        menu = (ImageView) getActivity().findViewById(R.id.my_sources_menu);
//        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
////        soundRadioGroup = (RadioGroup) getActivity().findViewById(R.id.notification_sound_type);
////        soundRadio = (RadioButton) getActivity().findViewById(R.id.notification_sound);
////        vibrationRadio = (RadioButton) getActivity().findViewById(R.id.notification_vibration);
////        bothRadio = (RadioButton) getActivity().findViewById(R.id.notification_both);
////        holder = (RelativeLayout) getActivity().findViewById(R.id.notification_holder);
//        greyView = (View) getActivity().findViewById(R.id.notification_view);
////        progress = (ProgressBar) getActivity().findViewById(R.id.select_progress);
////        gifMovieView = (GifMovieView) getActivity().findViewById(R.id.gifa);
////        gifMovieView.bringToFront();
//
////        gifMovieView.setMovieResource(R.drawable.loading);
////        gifMovieView.setVisibility(View.VISIBLE);
//        dp = new DataBaseAdapter(getActivity());
//        profile = new ArrayList<>();
//        mainControl = new MainControl(getActivity());
//        profile = dp.getAllProfiles();
//        objProfile = profile.get(0);
//
//        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
////        urgentNotificationStatus = dp.getAllProfiles().get(0).getUrgentFlag();
////        generalNotification = dp.getAllProfiles().get(0).getSoundType();
//        Sources source = new Sources();
//
//        ArrayList<Sources> sources = new ArrayList<>();
//        sources.add(source);
//        sources.addAll(dp.getSelectedSources());
////        adapter = new NotificationsAdapter(getActivity(), R.layout.notified_source_item, sources);
//        adapter = new NotificationsAdapter(getActivity(), sources);
//        notificationsListView.setAdapter(adapter);
//
////        notificationsListView.setVisibility(View.GONE);
//
////        notificationsListView.setFocusable(false);
//
////        if (objProfile.getUrgentFlag() > 0) {
////            isRemove = false;
////            try {
////                isUtility = false;
////                new Subscription().execute(URGENT_TOPIC);
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////            urgentNotify.setSwitch(true);
////        } else {
////            isRemove = true;
////            try {
////                isUtility = false;
////                new Subscription().execute(URGENT_TOPIC);
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////            urgentNotify.setSwitch(false);
////        }
//        if (objProfile.getSoundType() > 0) {
//            generalNotify.setSwitch(true);
//            greyView.setVisibility(View.GONE);
//        } else {
//            generalNotify.setSwitch(false);
//            greyView.setVisibility(View.VISIBLE);
//            greyView.bringToFront();
////            holder.setClickable(false);
//        }
////        switch (objProfile.getSoundType()) {
////            case 1:
////                soundRadio.setChecked(true);
////                break;
////            case 2:
////                vibrationRadio.setChecked(true);
////                break;
////            case 3:
////                bothRadio.setChecked(true);
////                break;
////        }
////        isUtility = true;
////        new Subscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
////        menu.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                mDrawerLayout.openDrawer(Gravity.RIGHT);
////            }
////        });
////        greyView.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                Toast.makeText(getActivity(), getString(
////                        R.string.enable_notification_first
////                ), Toast.LENGTH_SHORT).show();
////            }
////        });
//        generalNotify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (objProfile.getSoundType() > 0) {
//                    generalNotify.setSwitch(false);
//                    greyView.setVisibility(View.VISIBLE);
//                    greyView.bringToFront();
////                  holder.setClickable(false);
//                    objProfile.setSoundType(-1);
//                    objProfile.setUrgentFlag(-1);
//                    dp.updateProfiles(objProfile);
//                    adapter.notifyDataSetChanged();
//                } else {
//                    generalNotify.setSwitch(true);
//                    greyView.setVisibility(View.GONE);
//                    objProfile.setSoundType(1);
////                  soundRadioGroup.check(R.id.notification_sound);
//                    objProfile.setUrgentFlag(1);
//                    dp.updateProfiles(objProfile);
//                    adapter.notifyDataSetChanged();
//                }
//            }
//        });
//        greyView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//
////        urgentNotify.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                if (objProfile.getUrgentFlag() > 0) {
//////                    urgentNotify.setSwitch(false);
//////                    profile.get(0).setUrgentFlag(-1);
//////                    dp.updateProfiles(profile.get(0));
//////                    Toast.makeText(getActivity(), getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
////                    isRemove = true;
////                    isUtility = false;
////                    urgentNotify.setVisibility(View.GONE);
////                    progress.setVisibility(View.VISIBLE);
////                    new Subscription().execute(URGENT_TOPIC);
//////                    sub.execute(URGENT_TOPIC);
////                    callJson(URLs.REMOVE_URGENT_URL, URLs.REMOVE_URGENT_REQUEST_TYPE, URLs.getUserID(), getActivity(), URLs
//// .TAG_REMOVE_URGENT_USER_ID, true);
////                }
////                  else {
//////                    urgentNotify.setSwitch(true);
//////                    profile.get(0).setUrgentFlag(1);
//////                    dp.updateProfiles(profile.get(0));
////                    isUtility = false;
////                    urgentNotify.setVisibility(View.GONE);
////                    progress.setVisibility(View.VISIBLE);
//////                    Toast.makeText(getActivity(), getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
////                    isRemove = false;
////                    new Subscription().execute(URGENT_TOPIC);
//////                    subscribeTopics(URGENT_TOPIC);
////                    callJson(URLs.ADD_URGENT_URL, URLs.ADD_URGENT_REQUEST_TYPE, URLs.getUserID(), getActivity(), URLs
//// .TAG_ADD_URGENT_USER_ID, false);
////
////                }
////            }
////        });
////        soundRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
////            @Override
////            public void onCheckedChanged(RadioGroup group, int checkedId) {
////                switch (checkedId) {
////                    case R.id.notification_sound:
////                        soundRadio.setChecked(true);
////                        objProfile.setSoundType(1);
////                        dp.updateProfiles(objProfile);
////                        break;
////                    case R.id.notification_vibration:
////                        vibrationRadio.setChecked(true);
////                        objProfile.setSoundType(2);
////                        dp.updateProfiles(objProfile);
////                        break;
////                    case R.id.notification_both:
////                        bothRadio.setChecked(true);
////                        objProfile.setSoundType(3);
////                        dp.updateProfiles(objProfile);
////                        break;
////                }
////            }
////        });
//    }
//
//
//    @Override
//    @Nullable
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.notifications, container, false);
//    }
//
//    @Override
//    public void onBackButtonPressed() {
//        super.onBackButtonPressed();
//    }
//
//
////    private void callJson(String url, int requestType, int id, final Context context, String tag, final boolean isRemove) {
////
////        try {
////            if (!mainControl.checkInternetConnection()) {
////                Toast.makeText(context, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
////                return;
////            }
////        } catch (NullPointerException e) {
////
////        }
////        HashMap<String, String> dataObj = new HashMap<>();
////        dataObj.put(tag, id + "");
////        if (requestType == URLs.ADD_URGENT_REQUEST_TYPE) {
////            dataObj.put(URLs.TAG_ADD_URGENT_USER_ID, id + "");
////        } else {
////            dataObj.put(URLs.TAG_REMOVE_URGENT_USER_ID, id + "");
////        }
////
////        jsonParser = new JsonParser(getActivity(), url, requestType, dataObj);
////        jsonParser.execute();
////        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
////            @Override
////            public void onFinished() {
////
////            }
////
////            @Override
////            public void onFinished(List<Integer> likeResponse) {
////
////            }
////
////            @Override
////            public void onFinished(News newsObj) {
////            }
////
////            @Override
////            public void onFinished(ArrayList<Sources> sourceList) {
////
////            }
////
////            @Override
////            public void onFinished(int id) {
////            }
////
////            @Override
////            public void onFinished(boolean success) {
////                if (success) {
////                    if (isRemove) {
////                        urgentNotify.setSwitch(false);
////                        objProfile.setUrgentFlag(-1);
////                        urgentNotify.setVisibility(View.VISIBLE);
////                        progress.setVisibility(View.GONE);
////                        dp.updateProfiles(objProfile);
////                    } else {
////                        urgentNotify.setSwitch(true);
////                        objProfile.setUrgentFlag(1);
////                        urgentNotify.setVisibility(View.VISIBLE);
////                        progress.setVisibility(View.GONE);
////                        dp.updateProfiles(objProfile);
////                    }
////                } else {
////                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
////                    if (isRemove) {
////                        urgentNotify.setSwitch(true);
////                        urgentNotify.setVisibility(View.VISIBLE);
////                        progress.setVisibility(View.GONE);
////                        objProfile.setUrgentFlag(1);
////                        dp.updateProfiles(objProfile);
////                    } else {
////                        urgentNotify.setSwitch(false);
////                        urgentNotify.setVisibility(View.VISIBLE);
////                        progress.setVisibility(View.GONE);
////                        objProfile.setUrgentFlag(-1);
////                        dp.updateProfiles(objProfile);
////                    }
////                }
////            }
////        });
////    }
//
////    private void subscribeTopics(String topicName) {
////        GcmPubSub pubSub = GcmPubSub.getInstance(getActivity());
////        final SharedPreferences prefs = getActivity().getSharedPreferences(MainActivity.MyPREFERENCES,
////                Context.MODE_PRIVATE);
////        String token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
////
////        try {
////            pubSub.subscribe(token, "/topics/" + topicName + "", null);
//////            Toast.makeText(getActivity(), topicName + " created Successfully ", Toast.LENGTH_LONG).show();
////
////        } catch (IOException e) {
////            Log.d("TOPIC ERROR", e.toString());
////        }
////    }
////
////    private void unSubscribeTopics(String topicName) {
////        GcmPubSub pubSub = GcmPubSub.getInstance(getActivity().getApplicationContext());
////        final SharedPreferences prefs = getActivity().getSharedPreferences(MainActivity.MyPREFERENCES,
////                Context.MODE_PRIVATE);
////        String token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
////        try {
////            pubSub.unsubscribe(token, "/topics/" + topicName + "");
////
//////                        Toast.makeText(getActivity(), topicName + " unSubscribed Successfully ", Toast.LENGTH_LONG).show();
////
////        } catch (IOException e) {
////            Log.d("TOPIC ERROR", e.toString());
////        }
////    }
//
////    public static class Helper {
////        public static void getListViewSize(ListView myListView) {
////            ListAdapter myListAdapter = myListView.getAdapter();
////            if (myListAdapter == null) {
////                //do nothing return null
////                return;
////            }
////            //set listAdapter in loop for getting final size
////            int totalHeight = 0;
////            for (int size = 0; size < myListAdapter.getCount(); size++) {
////                View listItem = myListAdapter.getView(size, null, myListView);
////                listItem.measure(0, 0);
////                totalHeight += listItem.getMeasuredHeight();
////            }
////            //setting listview item in adapter
////            ViewGroup.LayoutParams params = myListView.getLayoutParams();
////            params.height = totalHeight + (myListView.getDividerHeight() * (myListAdapter.getCount() - 1));
////            myListView.setLayoutParams(params);
////            // print height of adapter on log
////            Log.i("height of listItem:", String.valueOf(totalHeight));
////        }
////    }
////
////    class Subscription extends AsyncTask<String, Void, String> {
////
////        @Override
////        protected String doInBackground(String... params) {
////            if (isUtility) {
////                for (int i = 0; i < 1; i++) {
////                    try {
////                        Thread.sleep(1000);
////                    } catch (InterruptedException e) {
////                        Thread.interrupted();
////                    }
////                }
////
////                return "Executed";
////            }
////            GcmPubSub pubSub = null;
////            SharedPreferences prefs = null;
////            try {
////                pubSub = GcmPubSub.getInstance(getActivity().getApplicationContext());
////                prefs = getActivity().getSharedPreferences(MainActivity.MyPREFERENCES,
////                        Context.MODE_PRIVATE);
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////            String token = null;
////            try {
////                token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////            try {
////                if (isRemove) {
////                    pubSub.unsubscribe(token, "/topics/" + params[0] + "");
////                } else {
////                    pubSub.subscribe(token, "/topics/" + params[0] + "", null);
////                }
////            } catch (Exception e) {
////
////            }
////            return "";
////        }
////
////        @Override
////        protected void onPostExecute(String result) {
////
////            if (result.equals("Executed")) {
////
//////                Utility.setListViewHeightBasedOnChildren(notificationsListView);
////                // Helper.getListViewSize(notificationsListView);
////                gifMovieView.setVisibility(View.GONE);
////                gifMovieView.setPaused(true);
////                notificationsListView.setVisibility(View.VISIBLE);
////                Helper.getListViewSize(notificationsListView);
////
////            }
////            // might want to change "executed" for the returned string passed
////            // into onPostExecute() but that is upto you
////        }
////
////    }
//
//
//}