package com.madarsoft.nabaa.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.firebase.messaging.FirebaseMessaging;/*
import com.madarsoft.firebasedatabasereader.interfaces.AdViewLoadListener;
import com.madarsoft.firebasedatabasereader.objects.RectangleBannerAd;*/
import com.madarsoft.nabaa.GCM.RegisterApp;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.Views.OnOffSwitch;
import com.madarsoft.nabaa.Views.WaterDropListView;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.adapters.NewsAdapter;
import com.madarsoft.nabaa.controls.AdsControlNabaa;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.controls.SpeedScrollListener;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//import com.madarsoft.firebasedatabasereader.interfaces.AdViewListener;

/**
 * Created by Colossus on 21-Jun-16.
 */
@SuppressLint("ValidFragment")
public class SourceNews extends MadarFragment implements WaterDropListView.IWaterDropListViewListener, AbsListView.OnScrollListener {
    public static boolean fromNewsDetails = false;
    public static RelativeLayout newPostsLayout;
    private static RelativeLayout noSourcesText;
    private static boolean isProcessingJson;
    private static Animation animShow;
    private static Animation animHide;
    private static boolean isScrolled;
    DataBaseAdapter dataBaseAdapter;
    Toast error;
    int currentFirstVisibleItem = 0;
    int currentVisibleItemCount = 0;
    int totalItemCount = 0;
    int currentScrollState = 0;
    private Category catObj;
    private String title;
    private int ID;
    private Sources sourceObj;
    private FontTextView sourceDetail;
    private FontTextView sourceTitle;
    private ImageView sourceImg;
    private FontTextView followersNumb;
    private ImageView home;
    private WaterDropListView listView;
    private JsonParser jsonParser;
    private int countArticles;
    private String firsTimeStamp = "0";
    private String lastTimeStamp = "0";
    private GifMovieView gifMovieView;
    private RelativeLayout noNetwork;
    private SpeedScrollListener listener;
    private NewsAdapter adapter;
    private MainControl mainControl;
    private Toast noInternet;
    private int mLastFirstVisibleItem;
    private Handler handler;
    private boolean isRefresh;
    private boolean isFirstTime = true;
    private boolean flag;
    private Toast noNews;
    private boolean isServerResponsed;
    private RelativeLayout selectSource;
    private ImageView selectImg;
    private ProgressBar selectionProgress;
    private boolean isSource;
    private boolean isCountry;

    private RelativeLayout blurredView;
    private Timer timer;
    private RelativeLayout refresh;
    private RelativeLayout sourcesParent;
    private RelativeLayout lowerView;
    private com.google.android.gms.analytics.Tracker mTracker;
    private int blocked;
    private boolean isRemoveFromSub;
    private ArrayList<Sources> sourceLit;
    private DrawerLayout mDrawerLayout;
    private LinearLayout topAdView;
    private LinearLayout bottomAdView;
    private AdsControlNabaa adsControl_top;
    private AdsControlNabaa adsControl_bottom;
    private OnOffSwitch enableBreakingNotification;
    private RelativeLayout sourceNotificationHolder;
    private ProgressBar followProgress;
    private boolean isBreakingNews;
//    private FontTextView sourceTitleSub;

    @SuppressLint("ValidFragment")
    public SourceNews(int ID, boolean isSource, boolean isCountry, String title, boolean fromNewsDetails) {
        this.ID = ID;
        this.isSource = isSource;
        this.fromNewsDetails = fromNewsDetails;
        this.isCountry = isCountry;
        this.title = title;
    }

    @SuppressLint("ValidFragment")
    public SourceNews(int ID, boolean isSource, boolean isCountry, String title, Category catObj, boolean fromNewsDetails) {
        this.catObj = catObj;
        this.ID = ID;
        this.isSource = isSource;
        this.fromNewsDetails = fromNewsDetails;
        this.isCountry = isCountry;
        this.title = title;
    }

    public SourceNews() {
    }

    private void blockImage(List<News> list) {
        blocked = dataBaseAdapter.getAllProfiles().get(0).getBlockImg();
//        NativeAd nativeAd = new NativeAd(getActivity(), "464488780344323_710228929103639");

        if (blocked > 0) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(1);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(-1);

            }
        }
    }

    public void setSelction() {
        selectImg.setImageResource(R.drawable.source_news_check);
        sourceObj.setSelected_or_not(1);
    }

    public void setNotification() {
        sourceObj.setUrgent_notify(1);
        enableBreakingNotification.setSwitch(true);
    }

    private void setRedViewInDrawer() {
        ListView vx = (ListView) mDrawerLayout.getChildAt(1);
        if (vx == null)
            return;
        View newsFragmentRedView = (View) vx.getChildAt(2).findViewById(R.id.red_view);
        View categoryRedView = (View) vx.getChildAt(5).findViewById(R.id.red_view);
        newsFragmentRedView.setVisibility(View.VISIBLE);
        categoryRedView.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        flag = true;
        mainControl = new MainControl(getActivity());
        dataBaseAdapter = new DataBaseAdapter(getActivity());
        firsTimeStamp = "0";
        lastTimeStamp = "0";

        sourcesParent = (RelativeLayout) getActivity().findViewById(R.id.source_news_holder);
        sourceTitle = (FontTextView) getActivity().findViewById(R.id.source_news_name);
//        sourceTitleSub = (FontTextView) getActivity().findViewById(R.id.source_news_name_sub);
        sourceImg = (ImageView) getActivity().findViewById(R.id.source_news_img);
        followersNumb = (FontTextView) getActivity().findViewById(R.id.source_followers);
        sourceDetail = (FontTextView) getActivity().findViewById(R.id.source_detail);
        home = (ImageView) getActivity().findViewById(R.id.sources_news_home_button);
        listView = (WaterDropListView) getActivity().findViewById(R.id.source_news_list);
        selectSource = (RelativeLayout) getActivity().findViewById(R.id.select_item);

        selectImg = (ImageView) getActivity().findViewById(R.id.select_img);
        followProgress = (ProgressBar) getActivity().findViewById(R.id.source_selection_progress);
        selectionProgress = (ProgressBar) getActivity().findViewById(R.id.select_progress);
        listView.setWaterDropListViewListener(this);
        listView.setPullLoadEnable(true);
        refresh = (RelativeLayout) getActivity().findViewById(R.id.refresh);
        noNews = Toast.makeText(getActivity(), getString(R.string.no_other_news), Toast.LENGTH_SHORT);
        noInternet = Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT);
        error = Toast.makeText(getActivity(), R.string.error, Toast.LENGTH_LONG);
        gifMovieView = (GifMovieView) getActivity().findViewById(R.id.source_news_loading);
        noNetwork = (RelativeLayout) getActivity().findViewById(R.id.sources_no_network);
        lowerView = (RelativeLayout) getActivity().findViewById(R.id.lower);
        blurredView = (RelativeLayout) getActivity().findViewById(R.id.blurred);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        enableBreakingNotification = (OnOffSwitch) getActivity().findViewById(R.id.source_enable_breaking_news);
        topAdView = (LinearLayout) getActivity().findViewById(R.id.source_news_top_ad_view);
        bottomAdView = (LinearLayout) getActivity().findViewById(R.id.source_bottom_ad_view);
        adsControl_top = new AdsControlNabaa(getActivity());
        adsControl_bottom = new AdsControlNabaa(getActivity());
        sourceNotificationHolder = (RelativeLayout) getActivity().findViewById(R.id.source_notification_holder_urgent);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        newPostsLayout = (RelativeLayout) getActivity().findViewById(R.id.news_holder_new_posts2);
        newPostsLayout.bringToFront();
        gifMovieView.setMovieResource(R.drawable.loading);
        gifMovieView.bringToFront();

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gifMovieView.setVisibility(View.VISIBLE);
                gifMovieView.setPaused(false);
                callJson(URLs.getUserID(), ID, 25, true, firsTimeStamp, true);
            }
        });
        //as
        AnalyticsApplication application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        if (isSource) {
            sourceObj = new Sources();

            try {
                sourceObj = dataBaseAdapter.getSourcesBySourceId(ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (sourceObj == null) {
                 callJson(URLs.getUserID(), ID, countArticles, true, firsTimeStamp, isFirstTime);

            }


            if (sourceObj == null) {

            } else {
                if (sourceObj.getUrgent_notify() > 0)
                    enableBreakingNotification.setSwitch(true);
                else
                    enableBreakingNotification.setSwitch(false);

                sourceTitle.setText(sourceObj.getSource_name() + "");
                //sourceTitleSub.setText(sourceObj.getSource_name() + "");
                sourceDetail.setText(sourceObj.getDetails() + "");
                followersNumb.setText(sourceObj.getNumber_followers() + " " + getString(R.string.followers));
                Picasso.with(getActivity())
                        .load(sourceObj.getLogo_url())
                        .placeholder(R.anim.progress_animation)
                        .error(R.drawable.item_icon)
                        .into(sourceImg);
                if (sourceObj.getSelected_or_not() == 1)
                    selectImg.setImageResource(R.drawable.source_news_check);
                else
                    selectImg.setImageResource(R.drawable.add);
                mTracker.setScreenName(getString(R.string.source_news));
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        } else {
            blurredView.setVisibility(View.GONE);
            sourceNotificationHolder.setVisibility(View.GONE);
            sourceTitle.setText(title);
            if (isCountry) {
                mTracker.setScreenName(getString(R.string.country_news));
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            } else {
                mTracker.setScreenName(getString(R.string.category_news));
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
            //sourceTitleSub.setText(title);
        }

        countArticles = 25;
        initAnimation();
        // show loading on first screen opening only
        if (flag) {
            flag = false;
            gifMovieView.setVisibility(View.VISIBLE);
            gifMovieView.setPaused(false);
        }

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    callJson(URLs.getUserID(), ID, countArticles, true, firsTimeStamp, isFirstTime);
                } catch (NullPointerException e) {
                }
            }
        }, 0, 18000);
        adsControl_top.getBannerAd(topAdView, "onesource_top");
        adsControl_top.getSplashAd( "onesource_top");
      /*  RectangleBannerAd banner = (new RectangleBannerAd.Builder()).adContainer(topAdView)
                .build();
        adsControl_top.getBannerAd(banner, "onesource_top");
        banner.setAdListener(new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                topAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdError() {
                topAdView.setVisibility(View.GONE);

            }
        });
        */
        adsControl_bottom.getBannerAd(bottomAdView, "onesource_bottom");

        /*

        RectangleBannerAd banner_bottom = (new RectangleBannerAd.Builder()).adContainer(bottomAdView)
                .build();
        adsControl_bottom.getBannerAd(banner, "onesource_bottom");
        banner_bottom.setAdListener(new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                bottomAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdError() {
                bottomAdView.setVisibility(View.GONE);

            }
        });*/

        enableBreakingNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBreakingNews = true;
                if (sourceObj.getUrgent_notify() > 0) {
                    followProgress.setVisibility(View.VISIBLE);
                    enableBreakingNotification.setVisibility(View.GONE);
                    callJson(URLs.REMOVE_URGENT_URL, URLs.REMOVE_URGENT_REQUEST_TYPE, URLs.getUserID(), true);
                } else {
                    enableBreakingNotification.setVisibility(View.GONE);
                    followProgress.setVisibility(View.VISIBLE);
                    callJson(URLs.ADD_URGENT_URL, URLs.ADD_URGENT_REQUEST_TYPE, URLs.getUserID(), false);
                }


            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.replace(R.id.parent, new NewsHolderFragment(), "newsHolder");
                transaction.commit();
                try {
                    setRedViewInDrawer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case 1:
                            listView.stopRefresh();
                            break;
                        case 2:
                            listView.stopLoadMore();
                            break;
                    }
                }
            };
        } catch (IllegalStateException e) {
        }
        listView.setOnScrollListener(this);

        newPostsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPostsLayout.startAnimation(animHide);
                newPostsLayout.setVisibility(View.GONE);
                isScrolled = false;
                blockImage(adapter.newsList);
                adapter.newsList.addAll(0, adapter.setviceNewsList);
                adapter.setviceNewsList.clear();
                if (listView.getFirstVisiblePosition() == 0) {
                    adapter.notifyDataSetChanged();
                } else {
                    MainControl.smoothScrollToPosition(listView, 0);
                    adapter.notifyDataSetChanged();
                }
            }

        });

        selectSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBreakingNews = false;
                if (!isServerResponsed)
                    onSelectSource(sourceObj);
            }
        });
    }

    private void callJson(String url, int requestType, int id, final boolean isRemove) {
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(getActivity(), getActivity().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }
        HashMap<String, String> dataObj = new HashMap<>();
//        dataObj.put(tag, id + "");
        if (requestType == URLs.ADD_NOTIFICATION_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_SOURCE_ID, sourceObj.getSource_id() + "");
        } else if (requestType == URLs.REMOVE_NOTIFICATION_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_SOURCE_ID, sourceObj.getSource_id() + "");
        }
        if (requestType == URLs.ADD_URGENT_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_SOURCE_ID, sourceObj.getSource_id() + "");
        } else if (requestType == URLs.REMOVE_URGENT_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_SOURCE_ID, sourceObj.getSource_id() + "");
        }
        jsonParser = new JsonParser(getActivity(), url, requestType, dataObj);
        jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {

            }

            @Override
            public void onFinished(List<Integer> likeResponse) {

            }

            @Override
            public void onFinished(News newsObj) {
            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int id) {
            }

            @Override
            public void onFinished(boolean success) {
                if (success) {
                    if (isRemove) {
                        followProgress.setVisibility(View.GONE);
                        enableBreakingNotification.setSwitch(false);
                        sourceObj.setUrgent_notify(-1);
                        enableBreakingNotification.setVisibility(View.VISIBLE);
                        isRemoveFromSub = true;
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(sourceObj
                                .getSource_id() + "U");
                        new Subscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, sourceObj
                                .getSource_id() + "");
                        dataBaseAdapter.updateSources(sourceObj);
                    } else {
                        followProgress.setVisibility(View.GONE);
                        enableBreakingNotification.setSwitch(true);
                        sourceObj.setUrgent_notify(1);
                        enableBreakingNotification.setVisibility(View.VISIBLE);
                        isRemoveFromSub = false;
                        FirebaseMessaging.getInstance().subscribeToTopic(sourceObj.getSource_id() + "U");
//                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
//                                    .getSource_id() + "");
                        dataBaseAdapter.updateSources(sourceObj);
                    }
//                    Toast.makeText(context, success + "", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    if (isRemove) {
                        followProgress.setVisibility(View.GONE);
                        enableBreakingNotification.setVisibility(View.VISIBLE);
                        enableBreakingNotification.setSwitch(true);
                        sourceObj.setUrgent_notify(1);
                        isRemoveFromSub = false;
                        FirebaseMessaging.getInstance().subscribeToTopic(sourceObj
                                .getSource_id() + "U");
//                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
//                                    .getSource_id() + "");
                        dataBaseAdapter.updateSources(sourceObj);
                    } else {
                        followProgress.setVisibility(View.GONE);
                        enableBreakingNotification.setVisibility(View.VISIBLE);
                        enableBreakingNotification.setSwitch(false);
                        sourceObj.setUrgent_notify(-1);
                        isRemoveFromSub = true;
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(sourceObj
                                .getSource_id() + "U");
                        new Subscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, sourceObj
                                .getSource_id()
                                + "U");
                        dataBaseAdapter.updateSources(sourceObj);
                    }
                }
            }


        });
    }

    private void callJson(int userID, int sourceId, final int countArticle, final boolean isNew, String timeStamp, final boolean
            isFirstLoading) throws NullPointerException {

        if (checkConnection() == false)
            return;

        if (getActivity() == null)
            return;

        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                noNetwork.setVisibility(View.GONE);
                lowerView.setBackgroundColor(Color.TRANSPARENT);
                sourcesParent.setBackgroundColor(Color.parseColor("#D7D7D7"));
            }
        });

        HashMap<String, String> dataObj = new HashMap<>();
        if (isSource) {
            dataObj.put(URLs.TAG_USERID, userID + "");
            dataObj.put(URLs.TAG_COUNT_ARTICLE, countArticle + "");
            dataObj.put(URLs.TAG_IS_NEW, isNew + "");
            dataObj.put(URLs.TAG_SOURCE_SOURCEID, sourceId + "");
            dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, timeStamp);
            jsonParser = new JsonParser(getActivity(), URLs.GET_SOURCE_NEWS_URL, URLs.GET_SOURCE_NEWS_REQUEST_TYPE, dataObj);
        } else {
            dataObj.put(URLs.TAG_USERID, userID + "");
            dataObj.put(URLs.TAG_COUNT_ARTICLE, countArticle + "");
            dataObj.put(URLs.TAG_IS_NEW, isNew + "");
            dataObj.put(URLs.TAG_CATEGORYID, sourceId + "");
            dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, timeStamp);
            dataObj.put(URLs.TAG_REQUEST_IS_COUNTRY, isCountry + "");
            jsonParser = new JsonParser(getActivity(), URLs.GET_CATEGORY_NEWS_URL, URLs.GET_CATEGORY_NEWS_TYPE, dataObj);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();

        jsonParser.onFinishUpdates(new JsonParser.OnSourceNewsListner() {
            @Override
            public void onFinished(List<News> newsList) {
                try {
                    if (ID > 0)
                        sourceNewsDisplay(getActivity(), newsList, isNew, isFirstLoading);
                } catch (NullPointerException e) {
                }
                try {
                    if (sourceObj.getUrgent_notify() > 0)
                        enableBreakingNotification.setSwitch(true);
                    else
                        enableBreakingNotification.setSwitch(false);

                    sourceObj = dataBaseAdapter.getSourcesBySourceId(ID);
                    sourceTitle.setText(sourceObj.getSource_name() + "");
                    //sourceTitleSub.setText(sourceObj.getSource_name() + "");
                    sourceDetail.setText(sourceObj.getDetails() + "");
                    followersNumb.setText(sourceObj.getNumber_followers() + " " + getString(R.string.followers));
                    Picasso.with(getActivity())
                            .load(sourceObj.getLogo_url())
                            .placeholder(R.anim.progress_animation)
                            .error(R.drawable.item_icon)
                            .into(sourceImg);
                    if (sourceObj.getSelected_or_not() == 1)
                        selectImg.setImageResource(R.drawable.source_news_check);
                    else
                        selectImg.setImageResource(R.drawable.add);
                    mTracker.setScreenName(getString(R.string.source_news));
                    mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void sourceNewsDisplay(final Context context, final List<News> newsLists, final boolean isNew, final boolean firstLoading)
            throws NullPointerException {
        blockImage(newsLists);
        listener = new SpeedScrollListener();

        MainActivity activity = (MainActivity) context;
        if (activity == null)
            return;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                if (firstLoading) {
                    if (newsLists == null || newsLists.isEmpty()) {
                        error.show();
                        gifMovieView.setEnabled(false);
                        gifMovieView.setVisibility(View.GONE);
                        return;
                    }
                    countArticles = 4;
                    isFirstTime = false;
                    adapter = new NewsAdapter(MainActivity.mainActivity, context, newsLists, listener, isSource, false, SourceNews.this);
                    firsTimeStamp = adapter.newsList.get(0).getTime_stamp();
                    lastTimeStamp = adapter.newsList.get(adapter.newsList.size() - 1).getTime_stamp();
                    listView.setAdapter(adapter);
                    gifMovieView.setVisibility(View.GONE);
                    gifMovieView.setPaused(true);

                } else {
                    isProcessingJson = false;
                    if (isNew) {
                        if (isRefresh) {
                            isRefresh = false;
                            if (newsLists == null || newsLists.isEmpty()) {
                                noNews.show();
                                return;
                            }
                            adapter.newsList.addAll(0, newsLists);
                            adapter.notifyDataSetChanged();
                            return;
                        }
                        if (newsLists == null || newsLists.isEmpty())
                            return;
                        firsTimeStamp = newsLists.get(0).getTime_stamp();
                        adapter.setviceNewsList.addAll(newsLists);
                        newPostsLayout.setVisibility(View.VISIBLE);
                        newPostsLayout.startAnimation(animShow);
                        isScrolled = true;
                    } else {
                        if (newsLists == null || newsLists.isEmpty()) {
                            noNews.show();
                            return;
                        }
                        if (newsLists.get(newsLists.size() - 1).getID() != adapter.newsList.get(adapter.newsList.size() - 1).getID()) {//20848//21097
                            adapter.newsList.addAll(newsLists);
                            adapter.notifyDataSetChanged();
                            //URLs.setLastNewsID(adapter.newsList.get(adapter.newsList.size() - 1).getID());
                            lastTimeStamp = adapter.newsList.get(adapter.newsList.size() - 1).getTime_stamp();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
        this.totalItemCount = totalItemCount;

        if (mLastFirstVisibleItem < firstVisibleItem) {
            Log.i("SCROLLING DOWN", "TRUE");
            if (newPostsLayout.getVisibility() == View.VISIBLE) {
                newPostsLayout.startAnimation(animHide);
                newPostsLayout.setVisibility(View.GONE);
            }
        }
        if (mLastFirstVisibleItem > firstVisibleItem) {
            Log.i("SCROLLING UP", "TRUE");
            if (newPostsLayout.getVisibility() == View.GONE && isScrolled) {
                newPostsLayout.setVisibility(View.VISIBLE);
                newPostsLayout.startAnimation(animShow);
            }
        }
        mLastFirstVisibleItem = firstVisibleItem;
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE && this.totalItemCount ==
                (currentFirstVisibleItem + currentVisibleItemCount)) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            //checkConnection();
            if (!isProcessingJson) {
                isProcessingJson = true;
                callJson(URLs.getUserID(), ID, 25, false, lastTimeStamp, false);
            }
        }
        if (isScrolled && currentFirstVisibleItem == 0 && newPostsLayout.getVisibility() == View.VISIBLE) {
            newPostsLayout.startAnimation(animHide);
            adapter.notifyDataSetChanged();
            newPostsLayout.setVisibility(View.GONE);
            isScrolled = false;
        }
    }

    @Override
    public void onLoadMore() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    //checkConnection();
                    if (!isProcessingJson) {
                        callJson(URLs.getUserID(), ID, 25, false, lastTimeStamp, false);
                    }
                    handler.sendEmptyMessage(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean checkConnection() {
        try {
            if (!mainControl.checkInternetConnection()) {
                MainActivity activity = (MainActivity) getActivity();
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        noInternet.show();
                        if (adapter == null || adapter.newsList.isEmpty()) {
                            noNetwork.setVisibility(View.VISIBLE);
                            noNetwork.bringToFront();
                            sourcesParent.setBackgroundColor(Color.parseColor("#FFFFFF"));
                            lowerView.setBackgroundColor(Color.parseColor("#DFDEDE"));

                        }
                        gifMovieView.setVisibility(View.GONE);
                        gifMovieView.setPaused(true);

                    }
                });
                return false;
            }
        } catch (NullPointerException e) {
        }
        return true;
    }

    @Override
    public void onRefresh() {
        if (newPostsLayout.getVisibility() == View.VISIBLE) {
            newPostsLayout.startAnimation(animHide);
            newPostsLayout.setVisibility(View.GONE);
        }
        /*try {
            if (!adapter.setviceNewsList.isEmpty()) {
                adapter.newsList.addAll(0, adapter.setviceNewsList);
                adapter.setviceNewsList.clear();
                if (listView.getFirstVisiblePosition() == 0) {
                    adapter.notifyDataSetChanged();
                } else {
                    MainControl.smoothScrollToPosition(listView, 0);
                    adapter.notifyDataSetChanged();
                }
            }
        } catch (NullPointerException e) {
        }*/

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    //firstTimeFlag = true;
                    isRefresh = true;
                    //checkConnection();
                    callJson(URLs.getUserID(), ID, 4, true, firsTimeStamp, false);

                    handler.sendEmptyMessage(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void onSelectSource(final Sources source) {

        if (!mainControl.checkInternetConnection()) {
            noInternet.show();
            return;
        }

        isServerResponsed = true;
        selectionProgress.setVisibility(View.VISIBLE);
        selectImg.setVisibility(View.GONE);

        if (source.getSelected_or_not() == 1) {
            //change it on the server
            HashMap<String, String> dataObj = new HashMap<>();
            dataObj.put(URLs.TAG_ADD_SOURCE_USERID, URLs.getUserID() + "");
            dataObj.put(URLs.TAG_SOURCE_SOURCEID, ID + "");
            jsonParser = new JsonParser(getActivity(), URLs.DELETE_SOURCE_URL, URLs.SOURCE_DELETE_REQUEST_TYPE, dataObj);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                @Override
                public void onFinished() {
                }

                @Override
                public void onFinished(List<Integer> likeResponse) {

                }

                @Override
                public void onFinished(News newsObj) {

                }

                @Override
                public void onFinished(ArrayList<Sources> sourceList) {

                }

                @Override
                public void onFinished(int userID) {

                }

                @Override
                public void onFinished(boolean success) {
                    isServerResponsed = false;
                    selectImg.setVisibility(View.VISIBLE);
                    selectionProgress.setVisibility(View.GONE);
                    if (success) {
                        selectImg.setImageResource(R.drawable.add);
                        source.setSelected_or_not(0);
                        source.setNumber_followers(source.getNumber_followers() - 1);

                        new Subscription().execute(source.getSource_id() + "");
                        dataBaseAdapter.updateSources(source);
                        followersNumb.setText(source.getNumber_followers() + " " + getString(R.string.followers));
                        try {
                            sourceLit = dataBaseAdapter.getSourcesByCategoryId(catObj.getCategory_id(), catObj.getCategory_type());
                            SourcesFragmentV2.updateSourceList(sourceLit);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "recentNews");
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "urgentNews");
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "hotNews");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        /////
                        error.show();
                        selectImg.setImageResource(R.drawable.source_news_check);
                    }
                }
            });

        } else {
            //change it on the server
            HashMap<String, String> dataObj = new HashMap<>();
            dataObj.put(URLs.TAG_ADD_SOURCE_USERID, URLs.getUserID() + "");
            dataObj.put(URLs.TAG_SOURCE_SOURCEID, ID + "");
            jsonParser = new JsonParser(getActivity(), URLs.ADD_SOURCE_URL, URLs.SOURCE_ADD_REQUEST_TYPE, dataObj);
            //change it on locale
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                @Override
                public void onFinished() {
                }

                @Override
                public void onFinished(List<Integer> likeResponse) {

                }

                @Override
                public void onFinished(News newsObj) {

                }

                @Override
                public void onFinished(ArrayList<Sources> sourceList) {

                }

                @Override
                public void onFinished(int userID) {

                }

                @Override
                public void onFinished(boolean success) {
                    isServerResponsed = false;
                    selectImg.setVisibility(View.VISIBLE);
                    selectionProgress.setVisibility(View.GONE);
                    if (success) {
                        selectImg.setImageResource(R.drawable.source_news_check);
                        source.setSelected_or_not(1);
                        source.setNumber_followers(source.getNumber_followers() + 1);
                        dataBaseAdapter.updateSources(source);

                        followersNumb.setText(source.getNumber_followers() + " " + getString(R.string.followers));
                        try {
                            sourceLit = dataBaseAdapter.getSourcesByCategoryId(catObj.getCategory_id(), catObj.getCategory_type());
                            SourcesFragmentV2.updateSourceList(sourceLit);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "recentNews");
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "hotNews");
                            MainControl.writeCacheFile(getActivity(), new ArrayList<News>(), "urgentNews");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        error.show();
                        selectImg.setImageResource(R.drawable.add);
                    }

                }
            });
        }
    }

    private void initAnimation() {
        animShow = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        animHide = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.source_news, container, false);

        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
        isFirstTime = true;
        countArticles = 25;
    }


    class Subscription extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            final GcmPubSub pubSub = GcmPubSub.getInstance(getActivity().getApplicationContext());
            final SharedPreferences prefs = getActivity().getSharedPreferences(MainActivity.MyPREFERENCES,
                    Context.MODE_PRIVATE);
            String token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
            try {
                if (isBreakingNews) {
                    pubSub.unsubscribe(token, "/topics/" + params[0] + "U");
                } else {
                    pubSub.unsubscribe(token, "/topics/" + params[0] + "");

                }


            } catch (IOException e) {

            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

            if (result.equals("Executed")) {


            }
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }

    }
}
