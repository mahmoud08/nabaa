package com.madarsoft.nabaa.activities;


import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.controls.FirebaseStorageControl;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Profile;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.firebase.MyFirebaseMessagingService;

import java.util.ArrayList;
import java.util.List;
//import com.twitter.sdk.android.Twitter;
//import com.twitter.sdk.android.core.TwitterAuthConfig;
//
//import io.fabric.sdk.android.Fabric;


public class Splash extends FragmentActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.


    public static int SPLASH_TIME_OUT = 1000;
    public static Splash splash;
    public static Toast toast;
    MainControl mainControl;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    private GoogleApiClient google_api_client;
    private GoogleApiClient mGoogleApiClient;
    private JsonParser jsonParser;
    private ProgressBar progBar;
    private List<News> cachedRecentNews = new ArrayList<>();
    private List<News> cachedHotNews = new ArrayList<>();
    private SharedPreferences userDataPref;
    private SharedPreferences settings;

    public void getColorWrapper(Context context) {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //noinspection deprecation
            window.setStatusBarColor(context.getResources().getColor(R.color.red));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getColorWrapper(getApplicationContext());
        super.onCreate(savedInstanceState);
//        try {
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancelAll();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        FacebookSdk.sdkInitialize(getApplicationContext());
        FirebaseStorageControl.downloadAdsData(this);
        MainActivity.mainActivity = null;
        setContentView(R.layout.splash_screen);
        splash = this;
        toast = Toast.makeText(this, "الإتصال يأخذ وقتا اكثر من اللازم", Toast.LENGTH_LONG);
        mainControl = new MainControl(this);
        DataBaseAdapter db = new DataBaseAdapter(this);
        try {
            db.expiredNews();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Error In Deletion ", "Error Anwar");
        }
        db.changeName();
        prefs = getSharedPreferences("MY_PREFS", MODE_PRIVATE);
        editor = getSharedPreferences("MY_PREFS", MODE_PRIVATE).edit();
        userDataPref = getSharedPreferences(LoginPopup.MyPREFERENCES, MODE_PRIVATE);
        progBar = (ProgressBar) findViewById(R.id.splash_progressbar);
        settings = getSharedPreferences(MainActivity.PREFS_NAME, MODE_PRIVATE);
        if (settings.getBoolean("my_first_time", true)) {
            userDataPref.edit().putInt("launchDays", 7).apply();
        }

        List<Profile> users = db.getAllProfiles();
        progBar.setVisibility(View.GONE);

        if (users.isEmpty()) {
            mainControl.setUserID("2");
            progBar.setVisibility(View.VISIBLE);
        } else {
            URLs.setUserID(users.get(0).getUserId());
            editor.putBoolean("closed", true);
            editor.commit();
        }
        URLs.setRecentFirstTimeStamp("0");
        URLs.setRecentLastTimeStamp("0");
        URLs.setUrgentFirstTimeStamp("0");
        URLs.setUrgentLastTimeStamp("0");

       /*try {
            cachedRecentNews = MainControl.readCachedFile(new File(getCacheDir().getAbsoluteFile() + File.separator + "recentNews"));
            //cachedHotNews = MainControl.readCachedFile(new File(getCacheDir().getAbsoluteFile() + File.separator + "hotNews"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (!cachedRecentNews.isEmpty()) {
            URLs.setFirstTimeStamp(cachedRecentNews.get(0).getTime_stamp());
            //URLs.setLastNewsID(adapter.newsList.get(newsLists.size() - 1).getID());
            URLs.setLastTimeStamp(cachedRecentNews.get(cachedRecentNews.size() - 1).getTime_stamp());
        } else {
            URLs.setFirstTimeStamp("0");
            URLs.setLastTimeStamp("0");
        }
        if (!cachedHotNews.isEmpty())
            HotNewsFragment.pageNo = 1;*/

        //temp code for testing
        new Handler().postDelayed(new Runnable() {
            public boolean fromPushNotification;
            private String id;

            @Override
            public void run() {

                if (prefs.getBoolean("closed", false)) {
                    try {
                        Intent intent = getIntent();
                        id = intent.getExtras().get("id").toString();
//                        fromPushNotification = getIntent().getBooleanExtra("fromNotification", false);
                    } catch (NullPointerException e) {

                    }
                    try {
                        Intent mIntent = getIntent();
                        if (mIntent.getStringExtra("page").equalsIgnoreCase(MyFirebaseMessagingService.FOLLOW)) {
                            Intent i = new Intent(Splash.splash, MainActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("page", mIntent.getStringExtra("page"));
                            bundle.putString("id", mIntent.getStringExtra("id"));
                            i.putExtras(bundle);
                            startActivity(i);
                            Splash.splash.finish();
                            return;
                        } else if (mIntent.getStringExtra("page").equalsIgnoreCase(MyFirebaseMessagingService.SOURCE)) {
                            Intent i = new Intent(Splash.splash, MainActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("page", mIntent.getStringExtra("page"));
                            bundle.putString("id", mIntent.getStringExtra("id"));
                            i.putExtras(bundle);
                            startActivity(i);
                            Splash.splash.finish();
                            return;
                        } else if (mIntent.getStringExtra("page").equalsIgnoreCase(MyFirebaseMessagingService.COUNTRY) || mIntent
                                .getStringExtra("page").equalsIgnoreCase(MyFirebaseMessagingService.CATEGORY)) {
                            Intent i = new Intent(Splash.splash, MainActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("page", mIntent.getStringExtra("page"));
                            bundle.putString("id", mIntent.getStringExtra("id"));
                            i.putExtras(bundle);
                            startActivity(i);
                            Splash.splash.finish();
                            return;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Intent mainActivityIntent = new Intent(Splash.this, MainActivity.class);
                        Intent intent = getIntent();
                        Uri data = intent.getData();
                        if (data != null) {
                            String data_str = data.toString();
                            String newsId = data_str.substring(data_str.lastIndexOf("id=") + 3);
                            mainActivityIntent.putExtra("newsId", newsId);
                            startActivity(mainActivityIntent);
                            Splash.splash.finish();
                            return;
                        }
//                        fromPushNotification = getIntent().getBooleanExtra("fromNotification", false);
                    } catch (Exception el) {

                    }
                    Intent i = new Intent(Splash.splash, MainActivity.class);
                    try {
                        if (id.length() > 0 && id != null) {
                            Bundle bundle = new Bundle();
                            bundle.putString("id", id);
                            i.putExtras(bundle);
                        }
                    } catch (NullPointerException ee) {

                    }
                    Splash.splash.startActivity(i);
                    Splash.splash.finish();
                }
            }
        }, Splash.SPLASH_TIME_OUT);

        mainControl.onFinishUpdates(new MainControl.OnUserExistListner() {
            @Override
            public void onFinished(boolean flag) {
                if (flag) {
                    Intent i = new Intent(Splash.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });

       /* if (URLs.getUserID() != 0 && !prefs.getBoolean("closed", false)) {
            showDialog();
        }
        mainControl.onFinishUpdates(new MainControl.OnUserExistListner() {
            @Override
            public void onFinished(boolean serviceFlag) {
                if (!serviceFlag) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(Splash.this, MainActivity.class);
                            startActivity(i);
                            finish();

                        }
                    }, SPLASH_TIME_OUT);
                } else
                    showDialog();
            }
        });*/

        getColorWrapper(getApplicationContext());

    }

//    public void setOnAddUserFinished(OnAddUserFinished onAddUserFinished) {
//        this.onAddUserFinished = onAddUserFinished;
//    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }


}