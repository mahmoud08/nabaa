package com.madarsoft.nabaa.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.analytics.HitBuilders;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.controls.AnalyticsApplication;
import com.madarsoft.nabaa.firebase.MyFirebaseMessagingService;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by Colossus on 27-Jul-16.
 */
public class CustomWebView extends FragmentActivity {
    private static final int FILECHOOSER_RESULTCODE = 2888;
    private ValueCallback<Uri> mUploadMessage;
    private Uri mCapturedImageURI = null;
    private WebView myWebView;
    private GifMovieView loading;
    private RelativeLayout header;
    private DrawerLayout mDrawerLayout;
    private boolean isDetails;
    private String newsUrl;
    private com.google.android.gms.analytics.Tracker mTracker;
    private boolean mIsInForegroundMode;
    private SharedPreferences sharedpreferences;
    private TextView headerText;
    private String webViewTitle;
    private RelativeLayout loadErrorView ,reloadDetails;

    public void getColorWrapper(Context context) {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //noinspection deprecation
            window.setStatusBarColor(context.getResources().getColor(R.color.red));
        }
    }

    //
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getColorWrapper(this);
        setContentView(R.layout.custom_web_view);
        sharedpreferences = getSharedPreferences(LoginPopup.MyPREFERENCES, Context.MODE_PRIVATE);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        mTracker.setScreenName(getName());
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        headerText = (TextView) findViewById(R.id.custom_web_header);
        myWebView = (WebView) findViewById(R.id.mywebview);
        loading = (GifMovieView) findViewById(R.id.webview_loading);
        header = (RelativeLayout) findViewById(R.id.web_view_header);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(getActivity(), "ChangeMe", Toast.LENGTH_SHORT).show();
            }
        });
        Bundle extras = getIntent().getExtras();

        loadErrorView = (RelativeLayout) findViewById(R.id.webview_error);

        reloadDetails = (RelativeLayout) findViewById(R.id.reload_webivew);

        if (extras != null) {

            try {
                if (extras.get("newsURL").toString() != null) {
                    isDetails = true;
                    newsUrl = extras.get("newsURL").toString();
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            try {
                if (extras.get("aboutUs").toString() != null) {
                    isDetails = false;
                    newsUrl = extras.get("aboutUs").toString();
                }
            } catch (NullPointerException ee) {

            }
            try {
                if (extras.get(MyFirebaseMessagingService.URL).toString() != null) {
                    isDetails = false;
                    newsUrl = extras.get(MyFirebaseMessagingService.URL).toString();
                    webViewTitle = extras.get(MyFirebaseMessagingService.WEB_VIEW_TITLE).toString();

                }
            } catch (NullPointerException ee) {

            }
        }
        getName();
        if (isDetails) header.setVisibility(View.VISIBLE);
        else header.setVisibility(View.GONE);

        if (getName().equals(getString(R.string.about_us_screen))) {
            header.setVisibility(View.VISIBLE);
            headerText.setText(getString(R.string.about_us));
        } else if (getName().equals(getString(R.string.call_us))) {
            header.setVisibility(View.VISIBLE);
            headerText.setText(getString(R.string.call_us));
        } else if (getName().equals(getString(R.string.FAQ))) {
            header.setVisibility(View.VISIBLE);
            headerText.setText(getString(R.string.questions));
        } else if (getName().equals(getString(R.string.suggest_source))) {
            header.setVisibility(View.VISIBLE);
            headerText.setText(getString(R.string.suggest_source));
        } else {
            try {
                if (!webViewTitle.equals("")) {
                    header.setVisibility(View.VISIBLE);
                    headerText.setText(webViewTitle);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        reloadDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading.setVisibility(View.VISIBLE);
                loading.setPaused(false);
                myWebView.setVisibility(View.VISIBLE);
                loadErrorView.setVisibility(View.GONE);
                try {
                    myWebView.clearCache(true);
                    myWebView.loadUrl(URLDecoder.decode(newsUrl, "UTF-8").toString());
                    // myWebView.loadDataWithBaseURL("", newsUrl, "text/html", "UTF-8", "");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        loading.setMovieResource(R.drawable.loading);
        loading.bringToFront();
        loading.setVisibility(View.VISIBLE);
        loading.setPaused(false);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        myWebView.getSettings().setAppCacheEnabled(false);
        //  myWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        // myWebView.getSettings().setAllowFileAccess(true);
        //myWebView.getSettings().setSupportZoom(true);
//        myWebView.setListener(this, this);
        try {
            myWebView.clearCache(true);
            myWebView.loadUrl(URLDecoder.decode(newsUrl, "UTF-8").toString());
           // myWebView.loadDataWithBaseURL("", newsUrl, "text/html", "UTF-8", "");

        } catch (Exception e) {
            e.printStackTrace();
        }
        myWebView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                loading.setVisibility(View.GONE);
                loading.setPaused(true);
            }
          /*  @Override
            public void onReceivedSslError (WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
                Toast.makeText(CustomWebView.this, getString(R.string.no_internet) , Toast.LENGTH_LONG).show();
            }*/

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCod,String description, String failingUrl) {
                Toast.makeText(CustomWebView.this, getString(R.string.no_internet) , Toast.LENGTH_LONG).show();
                myWebView.stopLoading();
                myWebView.setVisibility(View.GONE);
                loadErrorView.setVisibility(View.VISIBLE);
                myWebView.loadUrl("about:blank");
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                Toast.makeText(CustomWebView.this, getString(R.string.no_internet) , Toast.LENGTH_LONG).show();
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                myWebView.stopLoading();
                myWebView.setVisibility(View.GONE);
                loadErrorView.setVisibility(View.VISIBLE);
                myWebView.loadUrl("about:blank");
            }

        });


//        myWebView.setWebChromeClient(new WebChromeClient() {
//
//            // openFileChooser for Android 3.0+
//            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
//
//                // Update message
//                mUploadMessage = uploadMsg;
//
//                try {
//
//                    // Create AndroidExampleFolder at sdcard
//
//                    File imageStorageDir = new File(
//                            Environment.getExternalStoragePublicDirectory(
//                                    Environment.DIRECTORY_PICTURES)
//                            , "AndroidExampleFolder");
//
//                    if (!imageStorageDir.exists()) {
//                        // Create AndroidExampleFolder at sdcard
//                        imageStorageDir.mkdirs();
//                    }
//
//                    // Create camera captured image file path and name
//                    File file = new File(
//                            imageStorageDir + File.separator + "IMG_"
//                                    + String.valueOf(System.currentTimeMillis())
//                                    + ".jpg");
//
//                    mCapturedImageURI = Uri.fromFile(file);
//
//                    // Camera capture image intent
//                    final Intent captureIntent = new Intent(
//                            android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//
//                    captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
//
//                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//                    i.addCategory(Intent.CATEGORY_OPENABLE);
//                    i.setType("image/*");
//
//                    // Create file chooser intent
//                    Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
//
//                    // Set camera intent to file chooser
//                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
//                            , new Parcelable[]{captureIntent});
//
//                    // On select image call onActivityResult method of activity
//                    startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
//
//                } catch (Exception e) {
//                    Toast.makeText(getBaseContext(), "Exception:" + e,
//                            Toast.LENGTH_LONG).show();
//                }
//
//            }
//
//            // openFileChooser for Android < 3.0
//            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
//                openFileChooser(uploadMsg, "");
//            }
//
//            //openFileChooser for other Android versions
//            public void openFileChooser(ValueCallback<Uri> uploadMsg,
//                                        String acceptType,
//                                        String capture) {
//
//                openFileChooser(uploadMsg, acceptType);
//            }
//
//
//            // The webPage has 2 filechoosers and will send a
//            // console message informing what action to perform,
//            // taking a photo or updating the file
//
//            public boolean onConsoleMessage(ConsoleMessage cm) {
//
//                onConsoleMessage(cm.message(), cm.lineNumber(), cm.sourceId());
//                return true;
//            }
//
//            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
//                //Log.d("androidruntime", "Show console messages, Used for debugging: " + message);
//
//            }
//
//            // For Android 3.0+
////            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
////                mUploadMessage = uploadMsg;
////
////                try {
////                    File imageStorageDir = new File(
////                            Environment.getExternalStoragePublicDirectory(
////                                    Environment.DIRECTORY_PICTURES)
////                            , "AndroidExampleFolder");
////
////                    if (!imageStorageDir.exists()) {
////                        // Create AndroidExampleFolder at sdcard
////                        imageStorageDir.mkdirs();
////                    }
////
////                    // Create camera captured image file path and name
////                    File file = new File(
////                            imageStorageDir + File.separator + "IMG_"
////                                    + String.valueOf(System.currentTimeMillis())
////                                    + ".jpg");
////
////                    mCapturedImageURI = Uri.fromFile(file);
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
////
////                // Camera capture image intent
////                final Intent captureIntent = new Intent(
////                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
////
////                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
////
////                Toast.makeText(getApplicationContext(), "From web Chrome client", Toast.LENGTH_LONG).show();
////                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
////                i.addCategory(Intent.CATEGORY_OPENABLE);
////                i.setType("image/*");
////                startActivityForResult(i, FILECHOOSER_RESULTCODE);
////
////            }
////
////            // For Android > 4.1
////            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
////                openFileChooser(uploadMsg, "");
////            }
//
//        });
    }

    private String getName() {
        try {
            //Toast.makeText(getApplicationContext(), getIntent().getExtras().get("screenName").toString(), Toast.LENGTH_LONG).show();
            return getIntent().getExtras().get("screenName").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsInForegroundMode = true;
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("mIsInForegroundMode", mIsInForegroundMode);
        editor.apply();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsInForegroundMode = false;
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("mIsInForegroundMode", mIsInForegroundMode);
        editor.apply();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage) {
                return;
            }
            Uri result = null;
            try {
                if (resultCode != RESULT_OK) {
                    result = null;
                } else {
                    // retrieve from the private variable if the intent is null
                    result = data == null ? mCapturedImageURI : data.getData();
                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "activity :" + e,
                        Toast.LENGTH_LONG).show();
            }
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
            return;
        }
    }
//
//    @Override
//    public void onPageStarted(String url, Bitmap favicon) {
//
//    }
//
//    @Override
//    public void onPageFinished(String url) {
//        loading.setVisibility(View.GONE);
//        loading.setPaused(true);
//
//    }
//
//    @Override
//    public void onPageError(int errorCode, String description, String failingUrl) {
//
//    }
//
//    @Override
//    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition,
//                                    String userAgent) {
//
//    }
//
//    @Override
//    public void onExternalPageRequest(String url) {
//
//    }
}

