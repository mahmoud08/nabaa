package com.madarsoft.nabaa.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.firebase.iid.FirebaseInstanceId;
import com.madarsoft.nabaa.GCM.RegisterApp;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.CircleTransform;
import com.madarsoft.nabaa.adapters.DrawerAdapter;
import com.madarsoft.nabaa.controls.AdsControlNabaa;
import com.madarsoft.nabaa.controls.AutoUpdateControl;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.controls.RateItDialogFragment;
import com.madarsoft.nabaa.controls.UpdateService2;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Profile;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.firebase.MyFirebaseMessagingService;
import com.madarsoft.nabaa.firebase.RegisterAppFirebase;
import com.madarsoft.nabaa.fragments.AddCommentFragment;
import com.madarsoft.nabaa.fragments.CategoryFragment;
import com.madarsoft.nabaa.fragments.FollowUs;
import com.madarsoft.nabaa.fragments.HistoryFragment;
import com.madarsoft.nabaa.fragments.MadarFragment;
import com.madarsoft.nabaa.fragments.MyFavFragment;
import com.madarsoft.nabaa.fragments.MySources;
import com.madarsoft.nabaa.fragments.NewsDetail2;
import com.madarsoft.nabaa.fragments.NewsHolderFragment;
import com.madarsoft.nabaa.fragments.NoNetwork;
import com.madarsoft.nabaa.fragments.NotificationHolder;
import com.madarsoft.nabaa.fragments.PromoHolder;
import com.madarsoft.nabaa.fragments.Settings;
import com.madarsoft.nabaa.fragments.SourceNews;
import com.madarsoft.nabaa.fragments.SourcesFragmentV2;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;
import io.presage.Presage;

import static com.madarsoft.nabaa.R.id.parent;

//import com.madarsoft.nabaa.controls.RateThisApp;

//import com.madarsoft.nabaa.GCM.GCMIntentService;
//import com.madarsoft.nabaa.GCM.RegisterApp;
//import com.madarsoft.nabaa.controls.AutoUpdateControl;

//import com.facebook.ads.NativeAd;

public class MainActivity extends FragmentActivity {

    public static final String MyPREFERENCES = "UserDataPrefs";
    public final static String PREFS_NAME = "MyPrefsFile";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String GCM_RESPONSE = "serverResponse";
    public static Typeface HacenLiner, HacenLinerPrintOut, faceRobotoLight, faceRoboto;
    public static NewsHolderFragment newsHolder;
    public static MainActivity mainActivity;
    public static boolean isDetail;
    public static int testID;
    public static Intent intent;
    public static Intent loginIntent;
    public static ArrayList<News> myArray;
    public static boolean active;
    public static boolean zoomEnabled;
    static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private static int lunchDays = 0;
    //    RateThisApp.Config config;
    //    private String newsId;
    AdsControlNabaa adsControl;
    private CategoryFragment category;
    private TypedArray typedArray;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private String[] vals;
    private MySources myResources;
    private NewsHolderFragment newsHolderFragment;
    private MyFavFragment myFavFragment;
    private DrawerAdapter drawerAdapter;
    private SharedPreferences sharedpreferences;
    private String imgUrl;
    private String userName;
    private Settings settings;
    //    private NativeAd nativeAd;
    private boolean fromPushNotification;
    private String id;
    private String page;
    private boolean mIsInForegroundMode;
    private boolean fromNotification;
    private Uri mCapturedImageURI = null;
    private ActionBarDrawerToggle mDrawerToggle;
    private DataBaseAdapter database;
    private Profile objProfile;
    //    private NotificationHolder notification;
    private MainControl mainControl;
    private FollowUs followUs;
    private boolean fromLinks;
    private String newsId;
    private String appPackage;
    private SharedPreferences gcmPrefs;
    private NotificationHolder notification;
    private LayoutInflater inflater;
    private String userId;
    private HistoryFragment historyFrag;
    private Sources sourceObj;
    private static final String TWITTER_KEY = "9CmLVRbsvyUOt96A3XQld8t2M";
    private static final String TWITTER_SECRET = "5S3eeSf0lE24kl6km75W8Wq0Bq80rZ1rL9DkFwqQFAVMyb6iFx";

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {

            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public void getColorWrapper(Context context) {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //noinspection deprecation
            window.setStatusBarColor(context.getResources().getColor(R.color.red));
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        try {
            if (fragment.getTag().equalsIgnoreCase("newsHolder")) {
                View v = mDrawerList.getChildAt(2 - mDrawerList.getFirstVisiblePosition());
                if (v == null)
                    return;
                View redView = (View) v.findViewById(R.id.red_view);
                redView.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Appodeal.disableLocationPermissionCheck();
        Appodeal.disableNetwork(this, "cheetah");
        Appodeal.setAutoCache(Appodeal.INTERSTITIAL, false);
        Appodeal.setAutoCache(Appodeal.NATIVE, false);
        Appodeal.setAutoCache(Appodeal.BANNER, false);
        Appodeal.setAutoCache(Appodeal.MREC, false);


        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        final TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        //dJ_P4ZlhJk8:APA91bHf8yP_UeHn3D5L2VNxsqQNRsSYLgSotGxoOavWS_SgRHjHXTreOkwil5HzPaYa2P7DxbmCIXsihZ-U-Mv78F
        // -mvbySu9QEHUBmlnkukV8AUJnWCXS7c418r2N0a6b6Wo77Y2Co
        Appodeal.initialize(this, AdsControlNabaa.NABAA_ID, Appodeal.INTERSTITIAL | Appodeal.BANNER | Appodeal.MREC | Appodeal.NATIVE);

        adsControl = new AdsControlNabaa(this);
        //adsControl.getSplashAd();
        if (adsControl.isOguryEnabled())
        {
            Presage.getInstance().setContext(this.getBaseContext());
            Presage.getInstance().start("270567");

        }
// firebase registration
//        try {
//            FirebaseApp.initializeApp(this);
//            String deviceToken = FirebaseInstanceId.getInstance().getToken();
//        FirebaseMessaging.getInstance().subscribeToTopic("anwarM2");
//            FCMToServer(deviceToken);
//            Log.d("Fire base token", deviceToken);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        mainControl = new MainControl(this);

        getColorWrapper(this);
        appPackage = getPackageName();
        //disable RTL directions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        setContentView(R.layout.main);
        if (HacenLiner == null) {
            HacenLiner = Typeface.createFromAsset(getAssets(), "fonts/arialbd.ttf");
        }
        if (HacenLinerPrintOut == null) {
            HacenLinerPrintOut = Typeface.createFromAsset(getAssets(), "fonts/arial.ttf");
        }
        typedArray = getResources().obtainTypedArray(R.array.drawer_imgs);
        vals = getResources().getStringArray(R.array.drawer_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        drawerAdapter = new DrawerAdapter(getApplicationContext(), R.layout.drawer_list_item, typedArray, vals, "", "");
        mDrawerList.setAdapter(drawerAdapter);
        database = new DataBaseAdapter(this);

        sharedpreferences = getSharedPreferences(LoginPopup.MyPREFERENCES, Context.MODE_PRIVATE);
        mainActivity = this;
        DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                mDrawerList.setSelection(0);
                ListView vx = (ListView) mDrawerLayout.getChildAt(1);
                if (vx == null)
                    return;

                try {
                    View xview = (View) vx.getChildAt(9).findViewById(R.id.red_view);
                    xview.setVisibility(View.GONE);
                    View a = (View) vx.getChildAt(10).findViewById(R.id.red_view);
                    a.setVisibility(View.GONE);
                    View b = (View) vx.getChildAt(11).findViewById(R.id.red_view);
                    b.setVisibility(View.GONE);
                    drawerAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                //drawer is open
//                try {
//                    if (findViewById(R.id.settings_instagram) != null) {
//                        ListView v = (ListView) mDrawerLayout.getChildAt(1);
//                        if (v == null)
//                            return;
//                        View view = v.getChildAt(11).findViewById(R.id.red_view);
//                        view.setVisibility(View.VISIBLE);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                try {
                    if (findViewById(R.id.history_menu) != null) {
                        ListView v = (ListView) mDrawerLayout.getChildAt(1);
                        if (v == null)
                            return;
                        View view = v.getChildAt(5).findViewById(R.id.red_view);
                        view.setVisibility(View.VISIBLE);

                    } else {
                        mDrawerList.setSelection(0);
                        if (findViewById(R.id.holder_header) == null)
                            return;
                        ListView v = (ListView) mDrawerLayout.getChildAt(1);
                        if (v == null)
                            return;
                        View view = v.getChildAt(2).findViewById(R.id.red_view);
                        view.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        };
        mDrawerLayout.addDrawerListener(drawerListener);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDrawerList.setSelection(0);
                sliderNavigator(position);
            }
        });
        checkForUpdates();
        try {
//            fromPushNotification= getIntent().getBooleanExtra("fromNotification", false);
            id = getIntent().getExtras().get("id").toString();

        } catch (Exception e) {

        }
        try {
            page = getIntent().getStringExtra("page").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (page != null) {
            switch (page) {
                case MyFirebaseMessagingService.FOLLOW:
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    FollowUs newsDetail2 = new FollowUs();
                    transaction.replace(parent, newsDetail2);
                    transaction.commit();
                    break;
                case MyFirebaseMessagingService.SOURCE:
//    public SourceNews(int ID, boolean isSource, boolean isCountry, String title, boolean fromNewsDetails) {
                    sourceObj = new Sources();
                    try {
                        sourceObj = database.getSourcesBySourceId(Integer.parseInt(id));
                    } catch (Exception e) {

                    }

                    FragmentManager manager1 = getSupportFragmentManager();
                    FragmentTransaction transaction1 = manager1.beginTransaction();
                    SourceNews sourceNews = new SourceNews(Integer.parseInt(id), true, false, sourceObj.getSource_name().toString(),
                            false);
                    transaction1.replace(parent, sourceNews);
                    transaction1.commit();
                    break;

                case MyFirebaseMessagingService.COUNTRY:

                    ArrayList<Category> countryList = database.getCategoryById(Integer.parseInt(id));
                    FragmentManager manager2 = getSupportFragmentManager();
                    FragmentTransaction transaction2 = manager2.beginTransaction();
                    if (Integer.parseInt(id) == 0) {
                        CategoryFragment categoryFragment = new CategoryFragment();
                        transaction2.replace(parent, categoryFragment);
                    } else {
                        SourcesFragmentV2 sourcesFragmentV2 = new SourcesFragmentV2(countryList.get(0), 1);
                        transaction2.replace(parent, sourcesFragmentV2);
                    }
                    transaction2.commit();
                    break;
                case MyFirebaseMessagingService.CATEGORY:

                    ArrayList<Category> catList = database.getCategoryById(Integer.parseInt(id));
                    FragmentManager manager3 = getSupportFragmentManager();
                    FragmentTransaction transaction3 = manager3.beginTransaction();
                    if (Integer.parseInt(id) == 0) {
                        CategoryFragment categoryFragment = new CategoryFragment();
                        transaction3.replace(parent, categoryFragment);
                    } else {
                        SourcesFragmentV2 sourcesFragmentV2 = new SourcesFragmentV2(catList.get(0), 2);
                        transaction3.replace(parent, sourcesFragmentV2);
                    }

                    transaction3.commit();
                    break;
            }
        } else {
            if (id != null) {
                if (id.length() > 0) {
                    if (!mainControl.checkInternetConnection()) {
                        Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                        FragmentManager manager = getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        NoNetwork noNetwork = new NoNetwork(id);
                        transaction.replace(parent, noNetwork);
                        transaction.commit();
//                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                        notificationManager.cancelAll();
                        return;
                    } else {
                        FragmentManager manager = getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        NewsDetail2 newsDetail2 = new NewsDetail2(id);
                        transaction.replace(parent, newsDetail2);
                        transaction.commit();
//                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                        notificationManager.cancelAll();
                        SharedPreferences mysharedPreferences = getSharedPreferences(MyFirebaseMessagingService.COUNT_SHARED, Context
                                .MODE_PRIVATE);
                        SharedPreferences.Editor editor = mysharedPreferences.edit();
                        editor.putInt("Notify_count", 0).apply();

                    }
                    //Toast.makeText(MainActivity.this, "FromNotification", Toast.LENGTH_SHORT).show();
                }
            } else {
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                if (settings.getBoolean("my_first_time", true)) {
                    // first time task
                    // Here, thisActivity is the current activity
//                if (ContextCompat.checkSelfPermission(this,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                        != PackageManager.PERMISSION_GRANTED) {
//
//                    // Should we show an explanation?
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//
//                        // Show an expanation to the user *asynchronously* -- don't block
//                        // this thread waiting for the user's response! After the user
//                        // sees the explanation, try again to request the permission.
//
//                    } else {
//
//                        // No explanation needed, we can request the permission.
//
//                        ActivityCompat.requestPermissions(this,
//                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);
//
//                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                        // app-defined int constant. The callback method gets the
//                        // result of the request.
//                    }
//                }


                    objProfile = database.getAllProfiles().get(0);
                    objProfile.setSoundType(1);
                    objProfile.setUrgentFlag(1);
                    //database.updateProfiles(objProfile);
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    PromoHolder promoHolder = new PromoHolder();
                    transaction.replace(parent, promoHolder);
                    transaction.commit();
                    // record the fact that the app has been started at least once
                    settings.edit().putBoolean("my_first_time", false).apply();
                    return;
                }
//            String newsId = null;
                try {
                    newsId = getIntent().getExtras().get("newsId").toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (newsId != null && newsId.length() > 0) {
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    NewsDetail2 newsDetail2 = new NewsDetail2(newsId);
                    transaction.replace(parent, newsDetail2);
                    transaction.commit();

                } else {
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    newsHolderFragment = new NewsHolderFragment();
                    transaction.replace(parent, newsHolderFragment, "newsHolder");
                    transaction.commit();
                }
            }
        }
        registerApp();
        registerAppFirebase(this);
        AutoUpdateControl.checkForUpdate(MainActivity.this);
    }

    public void registerAppFirebase(Context context) {
        RegisterAppFirebase firebase = new RegisterAppFirebase(context);

//        if (firebase.checkPlayServices(this)) {
        String token = RegisterAppFirebase.getRegistrationToken(context);

        if (token.trim().isEmpty()) {
            firebase.registerApp();
        } else {
            Log.i("FIREBASE", "Registrations id is " + token);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sharedpreferences.edit().putBoolean("allow", true).apply();
                    try {
                        NewsDetail2 fragment = (NewsDetail2) getSupportFragmentManager().findFragmentByTag("newsDetail");
                        fragment.saveImgFromAct();

                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                } else {
                    sharedpreferences.edit().putBoolean("allow", false).apply();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void switchToNewsHolder(int position) {
        if (newsHolder == null) {
            newsHolder = new NewsHolderFragment();
            /*try {
                NewsFragment.tempList.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            switchToFragment(position, newsHolder, "newsHolder");
        } else {
            switchToFragment(position, newsHolder, "newsHolder");
        }
    }

    private void sliderNavigator(int position) {
//        try {
//            for (int i = 0; i <= 9; i++) {
//                View v = mDrawerList.getChildAt(i - mDrawerList.getFirstVisiblePosition());
//                if (v == null)
//                    return;
//                View redView = (View) v.findViewById(R.id.red_view);
//                redView.setVisibility(View.GONE);
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        switch (position) {
            case 0:
                if (sharedpreferences.getString(LoginPopup.userName, "").length() < 1) {
                    if (intent == null) {
                        intent = new Intent(this, LoginPopup.class);
                        startActivityForResult(intent, 1);
                    } else {
                        intent = null;
                        mDrawerLayout.closeDrawers();
                    }
                } else {
                    return;
                }
                break;
            case 1:
                if (loginIntent == null) {
                    loginIntent = new Intent(this, LoginPopup.class);
                    startActivityForResult(loginIntent, 1);
                } else {
                    loginIntent = null;
                    mDrawerLayout.closeDrawers();
                }
                break;
            case 2:
                NewsHolderFragment myFragment = (NewsHolderFragment) getSupportFragmentManager().findFragmentByTag("newsHolder");
                if (myFragment != null && myFragment.isVisible()) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    return;
                } else {
                    //URLs.setFirstNewsID(0);
                    //URLs.setFirstTimeStamp("0");
                    URLs.setFirstDateTime("1970-01-01 16:48:03.947");
                    //URLs.setLastTimeStamp("0");
                    //URLs.setLastNewsID(0);
                    switchToNewsHolder(position);
                    startService(new Intent(getApplicationContext(), UpdateService2.class));

                }
                break;
            case 3:
                if (myResources != null && myResources.isVisible()) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    return;
                } else {
                    switchToMySourcesFrag(position);
                }
                break;
            case 4:
                if (myFavFragment != null && myFavFragment.isVisible()) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    return;
                } else {
                    switchToMyFavFrag(position);
                }

                break;
            case 5:
                if (historyFrag != null && historyFrag.isVisible()) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    return;
                } else {
                    switchToHistoryFrag(position);
                }
                break;
            case 6:
                if (category != null && category.isVisible()) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    return;
                } else {
                    switchToMyCategoryFrag(position);
                }
                break;
            case 7:
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                openUrl("http://nabaapp.com/suggest/index?userId=" + URLs.getUserID() + "", getString(R.string
                        .suggest_source), getString(R.string.suggest_source));
                break;
            case 8:
                if (notification != null && notification.isVisible()) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    return;
                } else {
                    switchToNotificationScreen(position);
                }
                break;
            case 9:
                if (settings != null && settings.isVisible()) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    return;
                } else {
                    switchToSettingsFrag(position);
                }
                break;
            case 10:
                mDrawerLayout.closeDrawer(Gravity.RIGHT);

//                mDrawerList.smoothScrollToPosition(0);
                mDrawerList.setSelection(0);
//rate us
                if (!mainControl.checkInternetConnection()) {
                    Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(this, CustomWebView.class);
                Bundle b = new Bundle();
//                "http://nabaapp.com/nabaaFAQ/m.nabaa.FAQ.html", getString(R.string.FAQ), getString(R.string.questions)
                b.putString("aboutUs", "http://nabaapp.com/nabaaFAQ/m.nabaa.FAQ.html");
                b.putString("screenName", getString(R.string.FAQ));
                b.putString("arabic", getString(R.string.questions));
                intent.putExtras(b);
                startActivity(intent);
                try {
                    View v = mDrawerList.getChildAt(10 - mDrawerList.getFirstVisiblePosition());
                    if (v == null)
                        return;
                    View redView = (View) v.findViewById(R.id.red_view);
                    redView.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 11:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
//                i.putExtra(Intent.EXTRA_SUBJECT, "حمل تطبيق نبأ الاخباري مجاناً واستمتع بمتابعة العالم لحظة بلحظة");
                i.putExtra(Intent.EXTRA_TEXT, "حمل تطبيق نبأ الاخباري مجاناً واستمتع بمتابعة العالم لحظة بلحظة" + "\n" + "http://nabaapp" +
                        ".com");
//                Toast.makeText(context, shortLink, Toast.LENGTH_LONG).show();
//                i.putExtra(Intent.EXTRA_TEXT, "Hi Newz World");
                startActivity(Intent.createChooser(i, getString(R.string.share)));
                try {
                    View v = mDrawerList.getChildAt(11 - mDrawerList.getFirstVisiblePosition());
                    if (v == null)
                        return;
                    View redView = (View) v.findViewById(R.id.red_view);
                    redView.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 12:
//follow us
                if (followUs != null && followUs.isVisible()) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    return;
                } else {
                    switchToFollowUsFrag(position);
                }
                break;
            case 13:
                if (!mainControl.checkInternetConnection()) {
                    Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }
                mDrawerLayout.closeDrawer(Gravity.RIGHT);

//                mDrawerList.smoothScrollToPosition(0);
                mDrawerList.setSelection(0);
//rate us
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackage)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackage)));
                }
                try {
                    View v = mDrawerList.getChildAt(13 - mDrawerList.getFirstVisiblePosition());
                    if (v == null)
                        return;
                    View redView = (View) v.findViewById(R.id.red_view);
                    redView.setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 14:
//call us
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                openUrl("http://api.madarsoft.com/contact_us/v1/contact/report?programID=28&platform=2&developerInfo=", getString(R.string
                        .call_us), getString(R.string.call_us));
                try {
                    View v = mDrawerList.getChildAt(14 - mDrawerList.getFirstVisiblePosition());
                    if (v == null)
                        return;
                    View redView = (View) v.findViewById(R.id.red_view);
                    redView.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 16:
//about us
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                PackageInfo pInfo = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String version = pInfo.versionName;
                int verCode = pInfo.versionCode;
                openUrl("http://nabaapp.com/nabaaaboutUs/m.nabaa.aboutUs.html?version=" + version +
                        "&build=" + verCode + "", getString(R.string
                        .about_us_screen), getString(R.string.about_us));
                try {
                    View v = mDrawerList.getChildAt(16 - mDrawerList.getFirstVisiblePosition());
                    if (v == null)
                        return;
                    View redView = (View) v.findViewById(R.id.red_view);
                    redView.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 15:
//our_apps
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                openUrl("https://play.google.com/store/apps/developer?id=Madar%20Software&hl=en", getString(R.string
                        .our_apps), getString(R.string.our_apps));
                try {
                    View v = mDrawerList.getChildAt(15 - mDrawerList.getFirstVisiblePosition());
                    if (v == null)
                        return;
                    View redView = (View) v.findViewById(R.id.red_view);
                    redView.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
        try {
            for (int i = 0; i <= 15; i++) {
                View v = mDrawerList.getChildAt(i - mDrawerList.getFirstVisiblePosition());
                if (v == null)
                    return;
                View redView = (View) v.findViewById(R.id.red_view);
                redView.setVisibility(View.GONE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openUrl(String url, String screenName, String arabicName) {
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }
//        FragmentManager manager = getActivity().getSupportFragmentManager();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
//        transaction.addToBackStack(null);
//        CustomWebView webview = new CustomWebView();
        Intent intent = new Intent(this, CustomWebView.class);
        Bundle b = new Bundle();
        b.putString("aboutUs", url);
        b.putString("screenName", screenName);
        b.putString("arabic", arabicName);
        intent.putExtras(b);
        startActivity(intent);
//        webview.setArguments(b);
//        transaction.replace(R.id.parent, webview);
//        transaction.commit();
    }

//    @Override
//    public void onTokenRefresh() {
//        // Get updated InstanceID token.
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        Log.d(TAG, "Refreshed token: " + refreshedToken);
//
//        // If you want to send messages to this application instance or
//        // manage this apps subscriptions on the server side, send the
//        // Instance ID token to your app server.
//        sendRegistrationToServer(refreshedToken);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != -1) {
            if (resultCode == RESULT_OK) {
                imgUrl = data.getStringExtra("imgUrl");
                userName = data.getStringExtra("userName");
//                userId = data.getStringExtra(LoginPopup.userId);
//                BigInteger bigUserId = null;
//                try {
//                    bigUserId = new BigInteger(userId);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                if (onAddUserFinished != null)
//                    onAddUserFinished.onFinishAdd(userServerId);
                try {
                    NewsDetail2 newsDetail2 = (NewsDetail2) getSupportFragmentManager().findFragmentByTag("newsDetail");
                    newsDetail2.updateServerComments(userName, imgUrl);
                    /*if (newsDetail2.myType.length() == 0)
                        newsDetail2.refreshWebView();
                    newsDetail2.authenticateTwitter(requestCode, resultCode, data);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    AddCommentFragment mSomeFragment = (AddCommentFragment) getSupportFragmentManager().findFragmentByTag("dialog");
                    //AddCommentFragment frag = ((AddCommentFragment) MainActivity.mainActivity.getSupportFragmentManager()
                    // .findFragmentByTag("dialog"));
                    if (mSomeFragment != null) {
                       /* if (mSomeFragment.myType.length() == 0)
                            mSomeFragment.refreshWebView();*/
                        mSomeFragment.updateComment(testID + "");
                        //mSomeFragment.authenticateTwitter(requestCode, resultCode, data);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    updateViews(userName, imgUrl);
                } catch (Exception e) {
                }
            }
        }
    }

    public void updateViews(String username, String imgurl) {
        updateProfileImage(0, username, imgurl, mDrawerList);
        updateView(1, username, mDrawerList);
        drawerAdapter.notifyDataSetChanged();
    }

    public void updateProfileImage(int i, String userName, String imgUrl, ListView mDrawerList) {
        View v = mDrawerList.getChildAt(i - mDrawerList.getFirstVisiblePosition());
        if (v == null)
            return;
        ImageView headerImg = (ImageView) v.findViewById(R.id.drawer_header_img);
        if (imgUrl.length() < 1 && userName.length() < 1) {
            headerImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.profile, null));

        } else {
            try {
                Picasso.with(getApplicationContext()).load(imgUrl).transform(new CircleTransform()).placeholder(R.anim
                        .progress_animation).into(headerImg);
            } catch (Exception e) {

            }
        }
    }

    private void updateView(int index, String userName, ListView mDrawerList) {
        View v = mDrawerList.getChildAt(index - mDrawerList.getFirstVisiblePosition());
        if (v == null)
            return;
        TextView textView = (TextView) v.findViewById(R.id.header_text_slider);
        TextView headerLogOut = (TextView) v.findViewById(R.id.header_log_out);
//        View line = (View) v.findViewById(R.id.login_line);
        if (userName.length() < 1) {
            headerLogOut.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
            textView.setTextColor(Color.parseColor("#CC0000"));
//            line.setVisibility(View.GONE);
            textView.setText(R.string.log_in);
        } else {
            try {
                textView.setVisibility(View.VISIBLE);
                textView.setText(userName);
                textView.setTextColor(Color.parseColor("#000000"));
//                line.setVisibility(View.VISIBLE);
                headerLogOut.setVisibility(View.VISIBLE);
            } catch (Exception e) {
            }
        }
    }

    private void switchToMyCategoryFrag(int position) {
        if (category == null) {
            category = new CategoryFragment();
            try {
                switchToFragment(position, category, "category");
            } catch (Exception e) {
            }
        } else {
            try {
                switchToFragment(position, category, "category");
            } catch (Exception e) {
            }
        }
    }

    private void switchToSettingsFrag(int position) {
        if (settings == null) {
            settings = new Settings();
            switchToFragment(position, settings, "settings");
        } else {
            switchToFragment(position, settings, "settings");
        }
    }

    private void switchToFollowUsFrag(int position) {
        if (followUs == null) {
            followUs = new FollowUs();
            switchToFragment(position, followUs, "followUs");
        } else {
            switchToFragment(position, followUs, "followUs");
        }
    }

    private void switchToNotificationScreen(int position) {
        if (notification == null) {
            notification = new NotificationHolder();
            switchToFragment(position, notification, "notification");
        } else {
            switchToFragment(position, notification, "notification");
        }
    }


//    private void switchToNotificationFrag(int position) {
//        if (notification == null) {
//            notification = new NotificationHolder();
//            switchToFragment(position, notification, "notificationFrag");
//        } else {
//            switchToFragment(position, notification, "notificationFrag");
//        }
//    }

    private void switchToMyFavFrag(int position) {
        if (myFavFragment == null) {
            myFavFragment = new MyFavFragment();
            switchToFragment(position, myFavFragment, "myFav");
        } else {
            switchToFragment(position, myFavFragment, "myFav");
        }
    }

    private void switchToHistoryFrag(int position) {
        if (historyFrag == null) {
            historyFrag = new HistoryFragment();
            switchToFragment(position, historyFrag, "history");
        } else {
            switchToFragment(position, historyFrag, "history");
        }
    }

    private void switchToMySourcesFrag(int position) {
        if (myResources == null) {
            myResources = new MySources();
            switchToFragment(position, myResources, "myResources");
        } else {
            switchToFragment(position, myResources, "myResources");
        }
    }

    @Override
    public void onBackPressed() {
//        getColorWrapper

       /* NewsDetail2 fragment = (NewsDetail2) getSupportFragmentManager().findFragmentByTag("newsDetail");
        //AddCommentFragment frag = ((AddCommentFragment) MainActivity.mainActivity.getSupportFragmentManager()
        // .findFragmentByTag("dialog"));
        if (fragment != null) {
            fragment.onBackButtonPressed();
            return;
        }*/

        try {
            View v = mDrawerList.getChildAt(3 - mDrawerList.getFirstVisiblePosition());
            if (v == null)
                return;
            View redView = (View) v.findViewById(R.id.red_view);
            redView.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
                return;
            }
        } catch (Exception e) {
        }


        try {
            if (zoomEnabled) {
                NewsDetail2.closeAnimation(getApplicationContext());
                return;
            }
            if (SourceNews.fromNewsDetails) {
                SourceNews.fromNewsDetails = false;
                super.onBackPressed();
                return;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            if (findViewById(R.id.details_web_view) != null && id != null || page != null) {
                page = null;
                id = null;
                navigateToMain();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (findViewById(R.id.details_web_view) != null && newsId != null) {
                navigateToMain();
                newsId = null;
                id = null;
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (findViewById(R.id.details_web_view) != null) {
                try {
                    MyFavFragment mSomeFragment = (MyFavFragment) getSupportFragmentManager().findFragmentByTag("myFav");
                    mSomeFragment.keepOrRemove();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.onBackPressed();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (findViewById(R.id.history_menu) != null) {
            try {
                MainControl.writeCacheFile(this, new ArrayList<News>(), "recentNews");
                MainControl.writeCacheFile(this, new ArrayList<News>(), "hotNews");
                MainControl.writeCacheFile(this, new ArrayList<News>(), "urgentNews");
            } catch (IOException e) {
                e.printStackTrace();
            }
            navigateToMain();
            return;
        }
        try {
            if (findViewById(R.id.select_img) != null) {
//                super.onBackPressed();

                navigateToMain();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (findViewById(R.id.search_white_marker_in_all_sources) != null) {
                super.onBackPressed();

//                navigateToMain();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            if (findViewById(R.id.details_web_view) != null) {
                super.onBackPressed();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (findViewById(R.id.sources_news_home_button) != null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            navigateToMain();
//            super.onBackPressed();
            return;
        }

        if (findViewById(R.id.mywebview) != null || findViewById(R.id.sources_home_button) != null || findViewById(R.id
                .sources_news_home_button) != null ||
                findViewById(R.id.menu_country_list) != null) {

            super.onBackPressed();
            return;
        } else if (findViewById(R.id.details_web_view) != null && id == null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            super.onBackPressed();
            return;
        } else if (findViewById(R.id.details_web_view) != null) {
            navigateToMain();
            id = null;//
            return;
        } else if (findViewById(R.id.new_holder_search) != null) {
            setResult(Activity.RESULT_CANCELED);
            finishAffinity();
        }
        if (findViewById(R.id.promo_pager) != null) {
            return;
        }

        if (findViewById(R.id.notifications_list_view) != null) {
            super.onBackPressed();
            return;
        }
        if (findViewById(R.id.my_sources_notification_dynamic_text) != null) {
            super.onBackPressed();
            return;
        }
        id = getIntent().getStringExtra("id");
        if (id != null) {
            navigateToMain();
            fromNotification = true;
            return;
        }


        if (findViewById(R.id.category_home_button) != null || findViewById(R.id.my_sources_news_home_button) != null || findViewById(R
                .id.my_fav_news_home_button) != null || findViewById(R.id.follow_us_fb) != null) {
            navigateToMain();
            return;
        }
        navigateToMain();

        try {
//            mDrawerList.getChildAt(2).setBackgroundColor(Color.parseColor("#CD443A"));
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            View v = mDrawerList.getChildAt(3 - mDrawerList.getFirstVisiblePosition());
            if (v == null)
                return;
            View redView = (View) v.findViewById(R.id.red_view);
            redView.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i <= 11; i++) {
            try {
                View v = mDrawerList.getChildAt(i - mDrawerList.getFirstVisiblePosition());
                if (v == null)
                    return;
                View redView = (View) v.findViewById(R.id.red_view);
                redView.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    public void navigateToMain() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit, R.anim.enter, R.anim.exit);
        transaction.replace(parent, new NewsHolderFragment(), "newsHolder");
        transaction.addToBackStack(null);
        transaction.commit();
        try {
            startService(new Intent(getApplicationContext(), UpdateService2.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        id = intent.getStringExtra("id");
        if (id != null) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            NewsDetail2 newsDetail2 = new NewsDetail2(id);
            transaction.replace(parent, newsDetail2);
            transaction.commit();
        }
    }

    private void switchToFragment(int position, MadarFragment madarFragment, String tag) {
//        mDrawerList.getChildAt(position).setBackgroundColor(Color.parseColor("#CD443A"));
        View v = mDrawerList.getChildAt(position - mDrawerList.getFirstVisiblePosition());
        if (v == null)
            return;
        View redView = (View) v.findViewById(R.id.red_view);

        if (position == 9 || position == 10 || position == 11) {
            redView.setVisibility(View.GONE);
        } else {
            redView.setVisibility(View.VISIBLE);
        }
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        transaction.replace(parent, madarFragment, tag);
        transaction.addToBackStack(null);
        transaction.commit();
        mDrawerLayout.closeDrawer(Gravity.RIGHT);
        try {
            stopService(new Intent(getApplicationContext(), UpdateService2.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            } else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        try {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onResume();
        checkForCrashes();
        mIsInForegroundMode = true;
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("mIsInForegroundMode", mIsInForegroundMode);
        editor.apply();
        active = true;
        adsControl.registerForAdsListening();


    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterManagers();
        mIsInForegroundMode = false;
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("mIsInForegroundMode", mIsInForegroundMode);
        editor.apply();
        active = false;
        adsControl.unregisterAdListening();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();

    }

    private void checkForCrashes() {
        CrashManager.register(this, getString(R.string.hockyapp_id));
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        CrashManager.register(this, getString(R.string.hockyapp_id));
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    private void registerApp() {
        if (checkPlayServices()) {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
            String regid = getRegistrationId(getApplicationContext());
            gcmPrefs = getGCMPreferences(getApplicationContext());
            if (regid.trim().isEmpty()) {
                new RegisterApp(getApplicationContext(), gcm, getAppVersion(getApplicationContext()))
                        .execute();
//                gcmToServer(regid);
                Log.i("GCM", regid);
            } else {
//                gcmToServer(regid);
                Log.i("GCM", "Registrations id is " + regid);
            }
        } else {
            Log.i("GCM", "No valid Google Play Services APK found.");
        }
    }

    private int getMcc() {
        int mcc = 0;
        TelephonyManager tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String networkOperator = tel.getNetworkOperator();

        if (!TextUtils.isEmpty(networkOperator)) {
            mcc = Integer.parseInt(networkOperator.substring(0, 3));
            int mnc = Integer.parseInt(networkOperator.substring(3));
        }
        return mcc;
    }

    private void FCMToServer(String regId) {
        HashMap<String, String> dataObj = new HashMap<>();
        dataObj.put(URLs.TAG_USERID, URLs.getUserID() + "");
        dataObj.put(URLs.TAG_REG_ID, regId + "");
        dataObj.put(URLs.TAG_MCC, getMcc() + "");
        JsonParser jsonParser = new JsonParser(getApplicationContext(), URLs.GCM_TO_SERVER, URLs.GET_GCM_TO_SERVER_TYPE, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();

        jsonParser.onFinishedGCM(new JsonParser.OnFinishGCM() {
            @Override
            public void onFinished(boolean success) {
                if (success) {
//                    SharedPreferences.Editor editor = gcmPrefs.edit();
//                    editor.putBoolean(GCM_RESPONSE, success).apply();
//                    Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.success), Toast.LENGTH_SHORT)
// .show();
                } else {
//                    SharedPreferences.Editor editor = gcmPrefs.edit();
//                    editor.putBoolean(GCM_RESPONSE, success).apply();
//                    Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //dNSVdfb2vFA:APA91bEdwt8dASV23jcic-LYE8Hb2SVnAwvEKPMtZPj13xSv8UwKD
    // -Aj1Qdb1Lt3SwOI5e17AqXf7MfVHBhD4SQKnEFHv5XVBE8ArV0x17MWjq4WCXGOKN0Re5xlt0UFHw7p7hIZdBcB
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            }
            return false;
        }
        return true;
    }

    private String getRegistrationId(Context context) {
        gcmPrefs = getGCMPreferences(context);
        String registrationId = gcmPrefs.getString(RegisterApp.PROPERTY_REG_ID, "");
        if (registrationId.trim().isEmpty()) {
            Log.i("GCM", "Registration not found.");
            return registrationId;
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = gcmPrefs.getInt(RegisterApp.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context.getApplicationContext());
        if (registeredVersion != currentVersion) {
            Log.i("GCM", "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return context.getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);
    }

    // Some function.
    public boolean isInForeground() {
        return mIsInForegroundMode;
    }

    @Override
    protected void onStart() {
        super.onStart();
        final MainActivity mainActivity = this;
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
        // Monitor launch times and interval from installation
//                RateThisApp.onStart(mainActivity);
//
//                // Custom criteria: 3 days and 5 launches
//                config = new RateThisApp.Config(sharedpreferences.getInt("launchDays", 0), 0);
//                // Custom title ,message and buttons names
//                /*config.setTitle(R.string.my_own_title);
//                config.setMessage(R.string.my_own_message);
//                config.setYesButtonText(R.string.my_own_rate);
//                config.setNoButtonText(R.string.my_own_thanks);
//                config.setCancelButtonText(R.string.my_own_cancel);*/
//                RateThisApp.init(config);
//                // If the criteria is satisfied, "Rate this app" dialog will be shown
//                if (mainActivity.isFinishing()) {
//                    return;
//                }
//                RateThisApp.showRateDialogIfNeeded(mainActivity);
//
//                RateThisApp.setCallback(new RateThisApp.Callback() {
//                    @Override
//                    public void onYesClicked() {
//                        //Toast.makeText(MainActivity.this, "Yes event", Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onNoClicked() {
//                        //config = new RateThisApp.Config(7, 0);
//                        sharedpreferences.edit().putInt("launchDays", 7).apply();
//                        //Toast.makeText(MainActivity.this, "No event", Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onCancelClicked() {
//                        sharedpreferences.edit().putInt("launchDays", 1).apply();
//                        //Toast.makeText(MainActivity.this, "Cancel event", Toast.LENGTH_SHORT).show();
//                    }
//                });
//        RateItDialogFragment.show(this, getSupportFragmentManager());
//        AppRater.app_launched(this);
        RateItDialogFragment.app_launched(this);
        ;

//            }
//        }, 1800);
    }

    private void appOnMarketVersion() {
        String response = SendNetworkUpdateAppRequest(); //  do the network request
        // should send the current version
        // to server
        if (response.equals("YES")) // Start Intent to download the app user has to manually install it by clicking on the notification
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("URL TO LATEST APK")));
    }

    private String SendNetworkUpdateAppRequest() {
        return null;
    }
}