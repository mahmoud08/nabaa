//package com.madarsoft.nabaa.activities;
//
///**
// * Created by Colossus on 10-Apr-17.
// */
//
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.facebook.ads.Ad;
//import com.facebook.ads.AdChoicesView;
//import com.facebook.ads.AdError;
//import com.facebook.ads.AdListener;
//import com.facebook.ads.AdSettings;
//import com.facebook.ads.MediaView;
//import com.facebook.ads.NativeAd;
//import com.madarsoft.nabaa.R;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class NativeAdActivity extends AppCompatActivity {
//    private NativeAd nativeAd;
//    private LinearLayout nativeAdContainer;
//    private LinearLayout adView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_native_ad);
//        showNativeAd();
//    }
//
//    private void showNativeAd() {
////        AdSettings.addTestDevice("966e63cdb73df5523e545f4ed9621312");
//
//        nativeAd = new NativeAd(this, "1203178556381997_1250645504968635");
//        nativeAd.setAdListener(new AdListener() {
//
//            @Override
//            public void onError(Ad ad, AdError error) {
//                // Ad error callback
//                Log.d("Anwar's  ", error.getErrorMessage() + " " + ad.toString());
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
//
//                // Add the Ad view into the ad container.
////                nativeAdContainer = (LinearLayout) findViewById(R.id.native_ad_container);
//                LayoutInflater inflater = LayoutInflater.from(NativeAdActivity.this);
//                adView = (LinearLayout) inflater.inflate(R.layout.ad_sample, nativeAdContainer, false);
//                nativeAdContainer.addView(adView);
//
//                // Create native UI using the ad metadata.
//                ImageView nativeAdIcon = (ImageView) adView.findViewById(R.id.native_ad_icon);
//                TextView nativeAdTitle = (TextView) adView.findViewById(R.id.native_ad_title);
//                MediaView nativeAdMedia = (MediaView) adView.findViewById(R.id.native_ad_media);
//                TextView nativeAdSocialContext = (TextView) adView.findViewById(R.id.native_ad_social_context);
//                TextView nativeAdBody = (TextView) adView.findViewById(R.id.native_ad_body);
//                Button nativeAdCallToAction = (Button) adView.findViewById(R.id.native_ad_call_to_action);
//                // Set the Text.
//                nativeAdTitle.setText(nativeAd.getAdTitle());
//                nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
//                nativeAdBody.setText(nativeAd.getAdBody());
//                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
//                // Download and display the ad icon.
//                NativeAd.Image adIcon = nativeAd.getAdIcon();
//                NativeAd.downloadAndDisplayImage(adIcon, nativeAdIcon);
//                // Download and display the cover image.
//                nativeAdMedia.setNativeAd(nativeAd);
//                // Add the AdChoices icon
//                LinearLayout adChoicesContainer = (LinearLayout) findViewById(R.id.ad_choices_container);
//                AdChoicesView adChoicesView = new AdChoicesView(NativeAdActivity.this, nativeAd, true);
//                adChoicesContainer.addView(adChoicesView);
//
//                // Register the Title and CTA button to listen for clicks.
//                List<View> clickableViews = new ArrayList<>();
//                clickableViews.add(nativeAdTitle);
//                clickableViews.add(nativeAdCallToAction);
//                nativeAd.registerViewForInteraction(nativeAdContainer, clickableViews);
//            }
//
//            @Override
//            public void onAdClicked(Ad ad) {
//                // Ad clicked callback
//            }
//        });
//
//        // Request an ad
//        nativeAd.loadAd();
//    }
//}