package com.madarsoft.nabaa.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.digits.sdk.android.Digits;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.fragments.AlertDialogFrag;
import com.madarsoft.nabaa.fragments.PleaseWaitFragment;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

public class LoginPopup extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient
        .ConnectionCallbacks {

    public static final String MyPREFERENCES = "UserDataPrefs";
    public static final String userName = "userName";
    public static final String userId = "userId";
    public static final String imgUrl = "imgUrl";
    public static final String email = "email";
    public static final String userType = "userType";
    public static final String typeFacebook = "face";
    public static final String typeTwitter = "twitter";
    public static final String typeGoogle = "google";
    public static final String userServerId = "userServerId";


    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "LoginPopup";
    //    private static final String TWITTER_KEY = "5sCGiaSQr05C25OTzssDaLUIT";//anwar's config
//    private static final String TWITTER_SECRET = "KGWO8HZvXgN8f7d6YM0X52oq5dzZvGLkay5G7MCYB7aMCqNjIW";//anwar's config
    private static final String TWITTER_KEY = "9CmLVRbsvyUOt96A3XQld8t2M";
    private static final String TWITTER_SECRET = "5S3eeSf0lE24kl6km75W8Wq0Bq80rZ1rL9DkFwqQFAVMyb6iFx";
    public Dialog d;
    GoogleApiClient google_api_client;
    String myType;
    private PleaseWaitFragment pleaseWaitFragment;
    private SignInButton googleSignIn_btn;
    private Button cancel;
    private ProgressDialog mProgressDialog;
    private GoogleApiClient mGoogleApiClient;
    private TwitterLoginButton twitterLoginButton;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private AccessToken accessToken;
    private ProfileTracker profileTracker;
    private RelativeLayout googleSignOut;
    private RelativeLayout twiiterSignOut;
    private String profileImage;
    private SharedPreferences sharedpreferences;
    private JsonParser jsonParser;
    private boolean isLogginOut = true;
    private LinearLayout loginHolder;
    private LinearLayout loginParent;
    private DataBaseAdapter dataBaseAdapter;
    private int deviceUserId;

    private GifMovieView gifMovieView;
    private boolean mIsInForegroundMode;


    private void callJson(String url, final int requestType, int userID, String userName, String imageURL, final Context context, String
            userType, String publicID) {
        MainControl mainControl = new MainControl(context);
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
            //gifMovieView.setVisibility(View.GONE);
            //gifMovieView.setPaused(true);
            return;
        }

        HashMap<String, String> dataObj = new HashMap<>();
        dataObj.put(URLs.TAG_EDIT_USER_ACCOUT_USER_ID, userID + "");
        dataObj.put(URLs.TAG_EDIT_USER_ACCOUT_USER_NAME, userName + "");
        dataObj.put(URLs.TAG_EDIT_USER_ACCOUT_USER_IMAGE, imageURL + "");
        dataObj.put(URLs.TAG_EDIT_USER_ACCOUT_USER_TYPE, userType + "");
        dataObj.put(URLs.TAG_EDIT_USER_ACCOUT_USER_PUBLIC_ID, publicID + "");

        jsonParser = new JsonParser(context, url, requestType, dataObj);
        jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnUserAccountListener() {
            @Override
            public void onFinished(int userAccountID) {
                if (userAccountID > 0) {
                    //success
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(userServerId, userAccountID + "");
                    MainActivity.testID = userAccountID;
                    try {
                        pleaseWaitFragment.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    editor.commit();
                    finish();
                } else {

                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        final TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        FacebookSdk.sdkInitialize(getApplicationContext());

        dataBaseAdapter = new DataBaseAdapter(getApplicationContext());
        deviceUserId = dataBaseAdapter.getAllProfiles().get(0).getUserId();
        setContentView(R.layout.login_popup);
        googleSignIn_btn = (SignInButton) findViewById(R.id.sign_in_button);
        loginButton = (LoginButton) findViewById(R.id.login_button_facebook);
        cancel = (Button) findViewById(R.id.login_cancel);
        googleSignIn_btn.setSize(googleSignIn_btn.SIZE_STANDARD);
        googleSignIn_btn.setScopes(new Scope[]{Plus.SCOPE_PLUS_LOGIN});
        googleSignOut = (RelativeLayout) findViewById(R.id.google_sign_out_button);
        twiiterSignOut = (RelativeLayout) findViewById(R.id.twitter_sign_out);
        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Signing in....");
        loginHolder = (LinearLayout) findViewById(R.id.login_holder);
        loginParent = (LinearLayout) findViewById(R.id.login_parent);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        if (sharedpreferences.getString(userName, "").length() > 1) {
            String social = sharedpreferences.getString(userType, "").toString();
            loginParent.setVisibility(View.GONE);
            isLogginOut = false;
            showConfirmationDialoge(social);
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.loginIntent = null;
                MainActivity.intent = null;
                finish();
            }
        });

// Twitter
        if (Twitter.getInstance().core.getSessionManager().getActiveSession() != null) {
            twitterLoginButton.setVisibility(View.GONE);
            twiiterSignOut.setVisibility(View.VISIBLE);
        } else {
            twitterLoginButton.setVisibility(View.VISIBLE);
            twiiterSignOut.setVisibility(View.GONE);
        }

        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(final Result<TwitterSession> result) {
                // The TwitterSession is also available through:
                // Twitter.getInstance().core.getSessionManager().getActiveSession()

                Twitter.getApiClient().getAccountService().verifyCredentials(true, false, new Callback<User>() {
                    @Override
                    public void success(Result<User> userResult) {
                        User user = userResult.data;
                        profileImage = user.profileImageUrlHttps;
                        TwitterSession session = result.data;
                        String biggerImg = profileImage.replace("_normal", "");
                        twitterLoginButton.setVisibility(View.GONE);
                        twiiterSignOut.setVisibility(View.VISIBLE);
                        myType = typeTwitter;
                        String msg = "@" + session.getUserName() + " logged in! (#" + session.getUserId() + ")";
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(userName, session.getUserName());
                        editor.putString(userId, session.getUserId() + "");
                        editor.putString(imgUrl, biggerImg);
                        editor.putString(userType, myType);
                        editor.commit();
                        Intent intent = new Intent();
                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                        String myProfile = sharedpreferences.getString(imgUrl, "");
                        callJson(URLs.EDIT_USER_ACCOUNT_URL, URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE, deviceUserId, session.getUserName(),
                                biggerImg, getApplicationContext(), myType, session.getUserId() + "");

                        intent.putExtra(imgUrl, biggerImg);
                        intent.putExtra(userName, sharedpreferences.getString(userName, ""));
                        intent.putExtra(userId, session.getUserId() + "");
                        setResult(RESULT_OK, intent);
                        pleaseWaitDialog();
                    }

                    @Override
                    public void failure(TwitterException e) {
                        Toast.makeText(getApplicationContext(), "حدث خطأ ما", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });

        twiiterSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Twitter.getSessionManager().clearActiveSession();
                Digits.getSessionManager().clearActiveSession();

//                Toast.makeText(getApplicationContext(), "All accounts are cleared",
//                        Toast.LENGTH_SHORT).show();
                twitterLoginButton.setVisibility(View.VISIBLE);
                twiiterSignOut.setVisibility(View.GONE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear().commit();

            }
        });

//twitter End
        //FaceBook
        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions("email");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
//                Toast.makeText(getApplicationContext(), "Success" + loginResult.getAccessToken().getUserId(), Toast.LENGTH_LONG).show();
                loginResult.getAccessToken();
                accessToken = AccessToken.getCurrentAccessToken();
                profileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(
                            Profile oldProfile,
                            Profile currentProfile) {
                        if (currentProfile != null) {
                            profileImage = "https://graph.facebook.com/" + currentProfile.getId() + "/picture?type=large&redirect=true";
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            myType = typeFacebook;

                            editor.putString(userName, currentProfile.getFirstName());
                            editor.putString(userId, currentProfile.getId() + "");
                            editor.putString(imgUrl, profileImage);
                            editor.putString(userType, myType);
                            editor.commit();
                        } else {
                        }
                    }
                };

            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });

        accessToken = AccessToken.getCurrentAccessToken();
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                if (currentProfile != null) {
                    profileImage = "https://graph.facebook.com/" + currentProfile.getId() + "/picture?type=large&redirect=true";
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    myType = typeFacebook;

                    editor.putString(userName, currentProfile.getFirstName() + currentProfile.getLastName());
                    editor.putString(userId, currentProfile.getId() + "");
                    editor.putString(imgUrl, profileImage);
                    editor.putString(userType, myType);
                    editor.apply();
                    Intent intent = new Intent();
//                    PreferenceManager.getDefaultSharedPreferences(this)
                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    String myProfile = sharedpreferences.getString(imgUrl, "");
//                    callJson(URLs.EDIT_USER_ACCOUNT_URL,URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE ,);
                    callJson(URLs.EDIT_USER_ACCOUNT_URL, URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE, deviceUserId, currentProfile.getFirstName()
                            , profileImage, getApplicationContext(), myType, currentProfile.getId());

                    intent.putExtra(imgUrl, myProfile);
                    intent.putExtra(userId, currentProfile.getId());
                    intent.putExtra(userName, sharedpreferences.getString(userName, ""));
                    setResult(RESULT_OK, intent);
                    pleaseWaitDialog();
//                    finish();
                } else {
//                            SharedPreferences.Editor editor = sharedpreferences.edit();
//                            editor.clear().apply();
//                            LoginManager.getInstance().logOut();
                }
            }
        };


// FaceBook end

        googleSignIn_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);


            }
        });


        googleSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                // [START_EXCLUDE]
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.clear().apply();
//                                updateUI(false);
                                // [END_EXCLUDE]
                            }
                        });
            }
        });

    }

    public void logOutFromAll(String social) {
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.clear().commit();

        if (social.equalsIgnoreCase(typeFacebook)) {
//
//            if (AccessToken.getCurrentAccessToken() == null) {
//                return; // already logged out
//            }
            LoginManager.getInstance().logOut();
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.clear().commit();
            Intent intent = new Intent();
//                    PreferenceManager.getDefaultSharedPreferences(this)
            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String myProfile = sharedpreferences.getString(imgUrl, "");
//                    callJson(URLs.EDIT_USER_ACCOUNT_URL,URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE ,);
            intent.putExtra(imgUrl, myProfile);
            intent.putExtra(userName, sharedpreferences.getString(userName, ""));
            setResult(RESULT_OK, intent);
            finish();

        }

        if (social.equalsIgnoreCase(typeGoogle)) {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                mGoogleApiClient.clearDefaultAccountAndReconnect().setResultCallback(new ResultCallback<Status>() {

                    @Override
                    public void onResult(Status status) {

                        mGoogleApiClient.disconnect();

                        Intent intent = new Intent();
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.clear().commit();
//                    PreferenceManager.getDefaultSharedPreferences(this)
                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                        String myProfile = sharedpreferences.getString(imgUrl, "");
//                    callJson(URLs.EDIT_USER_ACCOUNT_URL,URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE ,);

                        intent.putExtra(imgUrl, myProfile);
                        intent.putExtra(userName, sharedpreferences.getString(userName, ""));
                        setResult(RESULT_OK, intent);
                        finish();

                    }
                });

            }

            try {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                // [START_EXCLUDE]
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.clear().commit();
                                Intent intent = new Intent();
                                //                    PreferenceManager.getDefaultSharedPreferences(this)
                                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                String myProfile = sharedpreferences.getString(imgUrl, "");
                                //                    callJson(URLs.EDIT_USER_ACCOUNT_URL,URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE ,);

                                intent.putExtra(imgUrl, myProfile);
                                intent.putExtra(userName, sharedpreferences.getString(userName, ""));
                                setResult(RESULT_OK, intent);
                                finish();
                                //                            updateUI(false);
                                // [END_EXCLUDE]
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (social.equalsIgnoreCase(typeTwitter)) {
            Twitter.getSessionManager().clearActiveSession();
            Digits.getSessionManager().clearActiveSession();

//            Toast.makeText(getApplicationContext(), "All accounts are cleared",
//                    Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.clear().commit();
            Intent intent = new Intent();
//                    PreferenceManager.getDefaultSharedPreferences(this)
            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String myProfile = sharedpreferences.getString(imgUrl, "");
//                    callJson(URLs.EDIT_USER_ACCOUNT_URL,URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE ,);

            intent.putExtra(imgUrl, myProfile);
            intent.putExtra(userName, sharedpreferences.getString(userName, ""));
            setResult(RESULT_OK, intent);
            finish();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        twitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    //Google Authentication.
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        MainControl mainControl = new MainControl(getApplicationContext());
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_SHORT).show();
            //gifMovieView.setVisibility(View.GONE);
            //gifMovieView.setPaused(true);
            return;
        }
       // Toast.makeText(getApplicationContext(), result.isSuccess()+"",Toast.LENGTH_LONG).show();

        if (result.isSuccess() && isLogginOut) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String personName = acct.getDisplayName();
            String personEmail = acct.getEmail();
            String personId = acct.getId();
            try {
                profileImage = acct.getPhotoUrl().toString();
            } catch (NullPointerException e) {
            }


//            Toast.makeText(getApplicationContext(), "Hello " + personName, Toast.LENGTH_LONG).show();

            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(userName, personName);
            editor.putString(userId, acct.getId());
            editor.putString(imgUrl, profileImage);
            editor.putString(email , personEmail);
            myType = typeGoogle;
            editor.putString(userType, myType);
            editor.commit();

//            updateUI(true);
            callJson(URLs.EDIT_USER_ACCOUNT_URL, URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE, deviceUserId, personName, profileImage,
                    getApplicationContext(), myType, personId);
            Intent intent = new Intent();
//                    PreferenceManager.getDefaultSharedPreferences(this)
            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String myProfile = sharedpreferences.getString(imgUrl, "");

//                    callJson(URLs.EDIT_USER_ACCOUNT_URL,URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE ,);

            intent.putExtra(imgUrl, myProfile);
            intent.putExtra(userName, sharedpreferences.getString(userName, ""));
            intent.putExtra(userId, sharedpreferences.getString(userId, ""));

            setResult(RESULT_OK, intent);
            pleaseWaitDialog();
//            finish();

        } else {
            // Signed out, show unauthenticated UI.
//            updateUI(false);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.clear().commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
            if (opr.isDone()) {
                // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
                // and the GoogleSignInResult will be available instantly.
                Log.d(TAG, "Got cached sign-in");
                GoogleSignInResult result = opr.get();
                handleSignInResult(result);
            } else {
                // If the user has not previously signed in on this device or the sign-in has expired,
                // this asynchronous branch will attempt to sign in the user silently.  Cross-device
                // single sign-on will occur in this branch.
                mGoogleApiClient.disconnect();
                opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                    @Override
                    public void onResult(GoogleSignInResult googleSignInResult) {
                        handleSignInResult(googleSignInResult);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //    private void updateUI(boolean signedIn) {
//        if (signedIn) {
//            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
//            findViewById(R.id.google_sign_out_button).setVisibility(View.VISIBLE);
//        } else {
//            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
//            findViewById(R.id.google_sign_out_button).setVisibility(View.GONE);
//        }
//    }
    @Override
    protected void onResume() {
        super.onResume();
        mIsInForegroundMode = true;
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("mIsInForegroundMode", mIsInForegroundMode);
        editor.apply();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsInForegroundMode = false;
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("mIsInForegroundMode", mIsInForegroundMode);
        editor.apply();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        profileTracker.stopTracking();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void showConfirmationDialoge(final String social) {
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        final AlertDialogFrag commentFrag = new AlertDialogFrag(getString(R.string.log_out_confirmation), getString(R.string.yes),
                getString(R.string.cancel), AlertDialogFrag.DIALOG_TWO_BUTTONS);
        commentFrag.show(ft, "dialog");
        commentFrag.setCancelable(false);
        commentFrag.OnDialogClickListener(new AlertDialogFrag.OnDialogListener() {
            @Override
            public void onPositiveClickListener() {
//                Toast.makeText(getApplicationContext(), "Yes Clicked And logged out", Toast.LENGTH_LONG).show();
                logOutFromAll(social);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear().commit();

            }

            @Override
            public void onNegativeClickListener() {
//                Toast.makeText(getApplicationContext(), "no clicked !", Toast.LENGTH_LONG).show();
                commentFrag.dismiss();
                MainActivity.loginIntent = null;
                MainActivity.intent = null;
                finish();
            }
        });
    }


    private void pleaseWaitDialog() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        pleaseWaitFragment = new PleaseWaitFragment(getString(R.string.loading));
        try {
            pleaseWaitFragment.show(ft, "dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
        pleaseWaitFragment.setCancelable(false);
    }

}
