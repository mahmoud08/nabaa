package com.madarsoft.nabaa.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Colossus on 30-Apr-17.
 */

public class SearchedSourcesAdapter extends ArrayAdapter<Sources> {
    private final Context context;
    private List<Sources> sourcesList;

    private JsonParser jsonParser;
    private DataBaseAdapter dp;
    private Sources mSource;

    public SearchedSourcesAdapter(Context context, int resource, List<Sources> sourcesList) {
        super(context, resource, sourcesList);
        this.context = context;
        this.sourcesList = sourcesList;
        dp = new DataBaseAdapter(context);
        mSource = new Sources();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.searched_source_item, null);

        final ImageView follow = (ImageView) convertView.findViewById(R.id.search_my_resource_item_add);
        final TextView sourceTitle = (TextView) convertView.findViewById(R.id.search_source_item_name_tv);
        final ProgressBar selectionProgressBar = (ProgressBar) convertView.findViewById(R.id.search_select_progress);
        final ImageView sourceImage = (ImageView) convertView.findViewById(R.id.search_source_item_img);
        if (sourcesList.get(position).getSelected_or_not() == 1)
            follow.setImageResource(R.drawable.source_news_check);
        else
            follow.setImageResource(R.drawable.add);
        sourceTitle.setText(sourcesList.get(position).getSource_name());
        try {
            Picasso.with(context)
                    .load(sourcesList.get(position).getLogo_url())
                    .placeholder(R.anim.progress_animation)
                    .error(R.drawable.default_img)
                    .into(sourceImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        follow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                //parent.getChildAt(position - ((ListView)parent).getFirstVisiblePosition()).setEnabled(false);
                MainControl mainControl = new MainControl(context);
                if (!mainControl.checkInternetConnection()) {
                    Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }
                follow.setVisibility(View.GONE);
                selectionProgressBar.setVisibility(View.VISIBLE);
                if (sourcesList.get(position).getSelected_or_not() == 1) {
                    //change it on the server
                    HashMap<String, String> dataObj = new HashMap<>();
                    dataObj.put(URLs.TAG_ADD_SOURCE_USERID, URLs.getUserID() + "");
                    dataObj.put(URLs.TAG_SOURCE_SOURCEID, sourcesList.get(position).getSource_id() + "");
                    jsonParser = new JsonParser(context, URLs.DELETE_SOURCE_URL, URLs.SOURCE_DELETE_REQUEST_TYPE, dataObj);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    else
                        jsonParser.execute();
                    jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                        @Override
                        public void onFinished() {


                        }

                        @Override
                        public void onFinished(List<Integer> likeResponse) {
                        }

                        @Override
                        public void onFinished(News newsObj) {
                        }

                        @Override
                        public void onFinished(ArrayList<Sources> sourceList) {
                        }

                        @Override
                        public void onFinished(int userID) {
                        }

                        @Override
                        public void onFinished(final boolean success) {
                            MainActivity activity = (MainActivity) context;
                            activity.runOnUiThread(new Runnable() {
                                public void run() {

                                    if (success) {
                                        selectionProgressBar.setVisibility(View.GONE);
                                        follow.setImageResource(R.drawable.add);
                                        follow.setVisibility(View.VISIBLE);
                                        mSource.setSelected_or_not(0);
//                                        mSource.setNumber_followers(source.getNumber_followers() - 1);
//                                        dp.updateSelected(source.getSource_id(), source.getNumber_followers() - 1, 0);
                                        dp.updateSources(mSource);
                                        sourcesList.get(position).setSelected_or_not(0);
                                        FirebaseMessaging.getInstance().unsubscribeFromTopic(sourcesList.get(position).getId() + "");
                                        try {
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "recentNews");
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "hotNews");
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "urgentNews");
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else
                                        Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                } else {
                    //change it on the server
                    HashMap<String, String> dataObj = new HashMap<String, String>();
                    dataObj.put(URLs.TAG_ADD_SOURCE_USERID, URLs.getUserID() + "");
                    dataObj.put(URLs.TAG_SOURCE_SOURCEID, sourcesList.get(position).getSource_id() + "");
                    jsonParser = new JsonParser(context, URLs.ADD_SOURCE_URL, URLs.SOURCE_ADD_REQUEST_TYPE, dataObj);
                    //change it on locale
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    else
                        jsonParser.execute();
                    jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                        @Override
                        public void onFinished() {
                        }

                        @Override
                        public void onFinished(List<Integer> likeResponse) {
                        }

                        @Override
                        public void onFinished(News newsObj) {

                        }

                        @Override
                        public void onFinished(ArrayList<Sources> sourceList) {

                        }

                        @Override
                        public void onFinished(int userID) {

                        }

                        @Override
                        public void onFinished(final boolean success) {
                            MainActivity activity = (MainActivity) context;
                            activity.runOnUiThread(new Runnable() {
                                public void run() {

                                    if (success) {
                                        //
                                        mSource.setGeo_id(sourcesList.get(position).getGeo_id());
                                        mSource.setSub_id(sourcesList.get(position).getSub_id());
                                        mSource.setSource_id(sourcesList.get(position).getSource_id());
                                        mSource.setSource_name(sourcesList.get(position).getSource_name());
                                        mSource.setDetails(sourcesList.get(position).getDetails());
                                        mSource.setLogo_url(sourcesList.get(position).getLogo_url());
                                        mSource.setChange_type(sourcesList.get(position).getChange_type());
                                        mSource.setNumber_followers(sourcesList.get(position).getNumber_followers());
                                        mSource.setTimeStamp(0);
                                        mSource.setNotifiable(sourcesList.get(position).getNotifiable());
                                        mSource.setSelected_or_not(1);
//                                        dp.updateSelected(source.getSource_id(), source.getNumber_followers() + 1, 1);
                                        dp.insertInSources(mSource);
                                        selectionProgressBar.setVisibility(View.GONE);
                                        sourcesList.get(position).setSelected_or_not(1);
                                        follow.setImageResource(R.drawable.source_news_check);
                                        follow.setVisibility(View.VISIBLE);
                                        try {
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "recentNews");
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "hotNews");
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "urgentNews");
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else
                                        Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }
            }
        });

        return convertView;
    }

}
