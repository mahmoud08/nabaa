package com.madarsoft.nabaa.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.firebase.messaging.FirebaseMessaging;
import com.madarsoft.nabaa.GCM.RegisterApp;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.OnOffSwitch;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Colossus on 3/14/2016.
 */
public class NotificationsAdapter extends ArrayAdapter {
    private final MainControl mainControl;
    private final boolean isGeneralNotification;
    private final int maxSize;
    DataBaseAdapter db;
    ArrayList<Category> categories;
    //    NotificationSubscription notificationSubscription;
    //    Sources source;
    List<Sources> mySources;
    private Context context;
    private JsonParser jsonParser;
    private boolean test;
    private boolean isRemove;
    private boolean isRemoveFromSub;
    private boolean isUrgentFirst;
    private boolean isNormalFirst;
//    private NotificationSubscription anotificationSubscription;

    public NotificationsAdapter(Context context, List<Sources> mySources, boolean isGeneralNotification, int maxSize) {
        super(context, R.layout.notified_source_item, mySources);
        this.mySources = mySources;
        this.context = context;
        this.maxSize = maxSize;
        this.isGeneralNotification = isGeneralNotification;
        db = new DataBaseAdapter(context);
        mainControl = new MainControl(context);
    }
    //    @Override
//    public int getCount() {
//        return myResources.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return myResources.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MyResourcesHolder myResourcesHolder;
//        source = myResources.get(position);
//        if (convertView == null) {
//            myResourcesHolder = new MyResourcesHolder();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.notified_source_item, null);


        final OnOffSwitch sourceAlert = (OnOffSwitch) convertView.findViewById(R.id.sources_notify_on_off);
        final TextView text = (TextView) convertView.findViewById(R.id.source_item_name_tv);
        final TextView detailText = (TextView) convertView.findViewById(R.id.source_item_disc_tv);
        final TextView numberFollowers = (TextView) convertView.findViewById(R.id.my_source_item_followers_number_tv);
        final ImageView image = (ImageView) convertView.findViewById(R.id.source_item_img);
        // checkedButton = (ToggleButton) rowView.findViewById(R.id.my_resource_item_add);
        final ProgressBar selectionProgressBar = (ProgressBar) convertView.findViewById(R.id.select_progress);
        final LinearLayout sectionHeader = (LinearLayout) convertView.findViewById(R.id.my_resource_header);
        final TextView textHeader = (TextView) convertView.findViewById(R.id.my_resource_header_tv);
//            convertView.setTag(myResourcesHolder);
//        } else {
//            myResourcesHolder = (MyResourcesHolder) convertView.getTag();
//        }
        // checkedButton.setClickable(false);
        Picasso.with(context)
                .load(mySources.get(position).getLogo_url())
                .placeholder(R.anim.progress_animation)
                .error(R.drawable.launcher)
                .into(image);

        text.setText(mySources.get(position).getSource_name());

        detailText.setText(mySources.get(position).getDetails());
        numberFollowers.setText(mySources.get(position).getNumber_followers() + context.getString(R.string.followers));

        /*if (myResources.get(position).getSelected_or_not() == 1) {
             checkedButton.setChecked(true);
        } else {
             checkedButton.setChecked(false);
        }*/

        if (isGeneralNotification) {
            if (mySources.get(position).getNotifiable() > 0)
                sourceAlert.setSwitch(true);
            else
                sourceAlert.setSwitch(false);

        } else {
            if (mySources.get(position).getUrgent_notify() > 0)
                sourceAlert.setSwitch(true);
            else
                sourceAlert.setSwitch(false);
        }

        isGeo(sectionHeader, textHeader, position);
//        rowView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //onRowClick(myResourcesHolder, position, source);
//            }
//        });
        sourceAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isGeneralNotification) {
                    if (mySources.get(position).getNotifiable() > 0) {
//                     sourceAlert.setSwitch(false);
//                    source.setNotifiable(-1);
//                    db.updateSources(source);
                        selectionProgressBar.setVisibility(View.VISIBLE);
                        sourceAlert.setVisibility(View.GONE);
//                    Toast.makeText(context, context.getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
                        callJson(URLs.REMOVE_NOTIFICATION_URL, URLs.REMOVE_NOTIFICATION_REQUEST_TYPE, URLs.getUserID(), mySources.get
                                        (position)
                                        .getSource_id()
                                , context, true, position, selectionProgressBar, sourceAlert);
                    } else {
//                     sourceAlert.setSwitch(true);
//                    source.setNotifiable(1);
//                    db.updateSources(source);
                        sourceAlert.setVisibility(View.GONE);
                        selectionProgressBar.setVisibility(View.VISIBLE);
//                    Toast.makeText(context, context.getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
                        callJson(URLs.ADD_NOTIFICATION_URL, URLs.ADD_NOTIFICATION_REQUEST_TYPE, URLs.getUserID(), mySources.get(position)
                                        .getSource_id(),
                                context, false, position, selectionProgressBar, sourceAlert);
                    }
                } else {
                    //for urgent
                    if (mySources.get(position).getUrgent_notify() > 0) {
//                     sourceAlert.setSwitch(false);
//                    source.setNotifiable(-1);
//                    db.updateSources(source);
                        selectionProgressBar.setVisibility(View.VISIBLE);
                        sourceAlert.setVisibility(View.GONE);
//                    Toast.makeText(context, context.getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
                        callJson(URLs.REMOVE_URGENT_URL, URLs.REMOVE_URGENT_REQUEST_TYPE, URLs.getUserID(), mySources.get(position)
                                        .getSource_id()
                                , context, true, position, selectionProgressBar, sourceAlert);
                    } else {
//                     sourceAlert.setSwitch(true);
//                    source.setNotifiable(1);
//                    db.updateSources(source);
                        sourceAlert.setVisibility(View.GONE);
                        selectionProgressBar.setVisibility(View.VISIBLE);
//                    Toast.makeText(context, context.getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
                        callJson(URLs.ADD_URGENT_URL, URLs.ADD_URGENT_REQUEST_TYPE, URLs.getUserID(), mySources.get(position)
                                        .getSource_id(),
                                context, false, position, selectionProgressBar, sourceAlert);
                    }
                }
            }
        });

//        convertView.setTag(R.id.sources_notify_on_off,  sourceAlert);
//        convertView.setTag(R.id.source_item_name_tv,  text);
//        convertView.setTag(R.id.source_item_disc_tv,  detailText);
//        convertView.setTag(R.id.my_source_item_followers_number_tv,  numberFollowers);
//        convertView.setTag(R.id.source_item_img,  image);
//        //rowView.setTag(R.id.my_resource_item_add,  checkedButton);
//        convertView.setTag(R.id.my_resource_header_tv,  textHeader);
//        convertView.setTag(R.id.my_resource_header,  sectionHeader);
//        convertView.setTag(R.id.select_progress,  selectionProgressBar);

        return convertView;
    }

    String getCatNameByGeo(int cat) {
        categories = db.getCategoryNamebyGeoId(cat);
        return categories.get(0).getCategory_name();
    }

    String getCatNameBySubId(int cat) {
        categories = db.getCategoryNameBySubId(cat);
        return categories.get(0).getCategory_name();
    }

    private void isGeo(final LinearLayout sectionHeader, final TextView textHeader, int position) {
        if (position >= maxSize) {
            isSub(sectionHeader, textHeader, position);
        } else {
            try {
                if (position == 0) {
                    sectionHeader.setVisibility(View.VISIBLE);
                    textHeader.setText(getCatNameByGeo(mySources.get(position).getGeo_id()));
                }
            } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews0", "exception", e);
            }
            try {
                if (getCatNameByGeo(mySources.get(position).getGeo_id()).equalsIgnoreCase(getCatNameByGeo(mySources.get(position - 1)
                        .getGeo_id()))) {
                    sectionHeader.setVisibility(View.GONE);

                }
            } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews1", "exception", e);
            }
            try {
                if (!getCatNameByGeo(mySources.get(position).getGeo_id()).equalsIgnoreCase(getCatNameByGeo(mySources.get(position - 1)
                        .getGeo_id()))) {
                    sectionHeader.setVisibility(View.VISIBLE);
                    textHeader.setText(getCatNameByGeo(mySources.get(position).getGeo_id()));

                }
            } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews2", "exception", e);
            }
        }
    }

    private void isSub(final LinearLayout sectionHeader, final TextView textHeader, int position) {
        try {
            if (position == maxSize) {
                sectionHeader.setVisibility(View.VISIBLE);
                textHeader.setText(getCatNameBySubId(mySources.get(position).getSub_id()));
                return;
            }
        } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews3", "exception", e);
        }
        try {
            if (getCatNameBySubId(mySources.get(position).getSub_id()).equalsIgnoreCase(getCatNameBySubId(mySources.get(position - 1)
                    .getSub_id()))) {
                sectionHeader.setVisibility(View.GONE);

            }
        } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews4", "exception", e);
        }
        try {
            if (!getCatNameBySubId(mySources.get(position).getSub_id()).equalsIgnoreCase(getCatNameBySubId(mySources.get(position - 1)
                    .getSub_id()))) {
                sectionHeader.setVisibility(View.VISIBLE);
                textHeader.setText(getCatNameBySubId(mySources.get(position).getSub_id()));

            }
        } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews5", "exception", e);
        }
    }

    private void callJson(String url, int requestType, int id, int sourceId, final Context context, final boolean isRemove, final int i,
                          final ProgressBar selectionProgressBar, final OnOffSwitch sourceAlert) {

        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }
        HashMap<String, String> dataObj = new HashMap<>();
//        dataObj.put(tag, id + "");
        if (requestType == URLs.ADD_NOTIFICATION_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_SOURCE_ID, sourceId + "");
        } else if (requestType == URLs.REMOVE_NOTIFICATION_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_SOURCE_ID, sourceId + "");
        }
        if (requestType == URLs.ADD_URGENT_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_ADD_NOTIFICATION_SOURCE_ID, sourceId + "");
        } else if (requestType == URLs.REMOVE_URGENT_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_USER_ID, id + "");
            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_SOURCE_ID, sourceId + "");
        }
        jsonParser = new JsonParser(context, url, requestType, dataObj);
        jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {

            }

            @Override
            public void onFinished(List<Integer> likeResponse) {

            }

            @Override
            public void onFinished(News newsObj) {
            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int id) {
            }

            @Override
            public void onFinished(boolean success) {
                if (success) {
                    if (isGeneralNotification) {
                        if (isRemove) {
                            selectionProgressBar.setVisibility(View.GONE);
                            sourceAlert.setSwitch(false);
                            mySources.get(i).setNotifiable(-1);
                            int sourceNumb = mySources.get(i).getSource_id();
                            for (int j = 0; j < mySources.size(); j++) {
                                if (sourceNumb == mySources.get(j).getSource_id()) {
                                    mySources.get(j).setNotifiable(-1);
                                }
                            }
                            sourceAlert.setVisibility(View.VISIBLE);
                            isRemoveFromSub = true;
                            FirebaseMessaging.getInstance().unsubscribeFromTopic(mySources.get(i)
                                    .getSource_id() + "");
                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
                                    .getSource_id() + "");
                            db.updateSources(mySources.get(i));
                        } else {
                            selectionProgressBar.setVisibility(View.GONE);
                            sourceAlert.setSwitch(true);
                            mySources.get(i).setNotifiable(1);
                            int sourceNumb = mySources.get(i).getSource_id();
                            for (int j = 0; j < mySources.size(); j++) {
                                if (sourceNumb == mySources.get(j).getSource_id()) {
                                    mySources.get(j).setNotifiable(1);
                                }
                            }
                            sourceAlert.setVisibility(View.VISIBLE);
                            isRemoveFromSub = false;
                            FirebaseMessaging.getInstance().subscribeToTopic(mySources.get(i)
                                    .getSource_id() + "");
                            if (!isNormalFirst) {
                                isNormalFirst = true;
                                FirebaseMessaging.getInstance().subscribeToTopic("True");
                            }
//                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
//                                    .getSource_id() + "");
                            db.updateSources(mySources.get(i));
                        }

                    } else {
                        if (isRemove) {
                            selectionProgressBar.setVisibility(View.GONE);
                            sourceAlert.setSwitch(false);
                            mySources.get(i).setUrgent_notify(-1);
                            sourceAlert.setVisibility(View.VISIBLE);
                            int sourceNumb = mySources.get(i).getSource_id();
                            for (int j = 0; j < mySources.size(); j++) {
                                if (sourceNumb == mySources.get(j).getSource_id()) {
                                    mySources.get(j).setUrgent_notify(-1);
                                }
                            }
                            isRemoveFromSub = true;
                            FirebaseMessaging.getInstance().unsubscribeFromTopic(mySources.get(i)
                                    .getSource_id() + "U");
                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
                                    .getSource_id() + "");
                            db.updateSources(mySources.get(i));
                        } else {
                            selectionProgressBar.setVisibility(View.GONE);
                            sourceAlert.setSwitch(true);
                            mySources.get(i).setUrgent_notify(1);
                            int sourceNumb = mySources.get(i).getSource_id();
                            for (int j = 0; j < mySources.size(); j++) {
                                if (sourceNumb == mySources.get(j).getSource_id()) {
                                    mySources.get(j).setUrgent_notify(1);
                                }
                            }
                            sourceAlert.setVisibility(View.VISIBLE);
                            isRemoveFromSub = false;
                            FirebaseMessaging.getInstance().subscribeToTopic(mySources.get(i)
                                    .getSource_id() + "U");
                            if (!isUrgentFirst) {
                                isUrgentFirst = true;
                                FirebaseMessaging.getInstance().subscribeToTopic("Urgent");
                            }
//                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
//                                    .getSource_id() + "");
                            db.updateSources(mySources.get(i));
                        }

                    }
//                    Toast.makeText(context, success + "", Toast.LENGTH_SHORT).show();
                } else {
                    if (isGeneralNotification) {
                        Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                        if (isRemove) {
                            selectionProgressBar.setVisibility(View.GONE);
                            sourceAlert.setVisibility(View.VISIBLE);
                            sourceAlert.setSwitch(true);
                            mySources.get(i).setNotifiable(1);
                            int sourceNumb = mySources.get(i).getSource_id();
                            for (int j = 0; j < mySources.size(); j++) {
                                if (sourceNumb == mySources.get(j).getSource_id()) {
                                    mySources.get(j).setNotifiable(1);
                                }
                            }
                            isRemoveFromSub = false;
                            FirebaseMessaging.getInstance().subscribeToTopic(mySources.get(i)
                                    .getSource_id() + "");
                            FirebaseMessaging.getInstance().subscribeToTopic("True");
//                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
//                                    .getSource_id() + "");
                            db.updateSources(mySources.get(i));
                        } else {
                            selectionProgressBar.setVisibility(View.GONE);
                            sourceAlert.setVisibility(View.VISIBLE);
                            sourceAlert.setSwitch(false);
                            mySources.get(i).setNotifiable(-1);
                            int sourceNumb = mySources.get(i).getSource_id();
                            for (int j = 0; j < mySources.size(); j++) {
                                if (sourceNumb == mySources.get(j).getSource_id()) {
                                    mySources.get(j).setNotifiable(-1);
                                }
                            }
                            isRemoveFromSub = true;
                            FirebaseMessaging.getInstance().unsubscribeFromTopic(mySources.get(i)
                                    .getSource_id() + "");
                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
                                    .getSource_id() + "");
                            db.updateSources(mySources.get(i));
                        }

                    } else {
                        Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                        if (isRemove) {
                            selectionProgressBar.setVisibility(View.GONE);
                            sourceAlert.setVisibility(View.VISIBLE);
                            sourceAlert.setSwitch(true);
                            mySources.get(i).setUrgent_notify(1);
                            int sourceNumb = mySources.get(i).getSource_id();
                            for (int j = 0; j < mySources.size(); j++) {
                                if (sourceNumb == mySources.get(j).getSource_id()) {
                                    mySources.get(j).setUrgent_notify(1);
                                }
                            }
                            isRemoveFromSub = false;
                            FirebaseMessaging.getInstance().subscribeToTopic(mySources.get(i)
                                    .getSource_id() + "U");
                            FirebaseMessaging.getInstance().subscribeToTopic("Urgent");
//                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
//                                    .getSource_id() + "");
                            db.updateSources(mySources.get(i));
                        } else {
                            selectionProgressBar.setVisibility(View.GONE);
                            sourceAlert.setVisibility(View.VISIBLE);
                            sourceAlert.setSwitch(false);
                            mySources.get(i).setUrgent_notify(-1);
                            int sourceNumb = mySources.get(i).getSource_id();
                            for (int j = 0; j < mySources.size(); j++) {
                                if (sourceNumb == mySources.get(j).getSource_id()) {
                                    mySources.get(j).setUrgent_notify(-1);
                                }
                            }
                            isRemoveFromSub = true;
                            FirebaseMessaging.getInstance().unsubscribeFromTopic(mySources.get(i)
                                    .getSource_id() + "U");

                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mySources.get(i)
                                    .getSource_id() + "U");
                            db.updateSources(mySources.get(i));
                        }
                    }
                }
            }
        });
    }

    class MyResourcesHolder {
        ProgressBar selectionProgressBar;
        TextView text;
        TextView detailText;
        TextView numberFollowers;
        TextView textHeader;
        LinearLayout sectionHeader;
        ImageView image;
        OnOffSwitch sourceAlert;
    }

    //for Sub
    class NotificationSubscription extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final GcmPubSub pubSub = GcmPubSub.getInstance(context.getApplicationContext());
            final SharedPreferences prefs = context.getSharedPreferences(MainActivity.MyPREFERENCES,
                    Context.MODE_PRIVATE);
            String token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
            try {
                if (isRemoveFromSub) {
                    if (isGeneralNotification)
                        pubSub.unsubscribe(token, "/topics/" + params[0] + "");
                    else
                        pubSub.unsubscribe(token, "/topics/" + params[0] + "U");

                } else {
                    if (isGeneralNotification) {
                        pubSub.subscribe(token, "/topics/" + params[0] + "", null);
//                        pubSub.subscribe(token, "/topics/abdallah", null);
                    } else
                        pubSub.subscribe(token, "/topics/" + params[0] + "U", null);
                }
            } catch (IOException e) {

            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals("Executed")) {
            }
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }
    }
}