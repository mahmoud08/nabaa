package com.madarsoft.nabaa.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.CircleTransform;
import com.madarsoft.nabaa.activities.LoginPopup;
import com.squareup.picasso.Picasso;

/**
 * Created by Colossus on 3/23/2016.
 */
public class DrawerAdapter extends ArrayAdapter {

    public static String imgUrl;
    private final LayoutInflater inflater;
    private final Typeface headerFont;
    private final Context context;
    private final SharedPreferences sharedpreferences;
    TypedArray typedArray;
    String[] val;
    //    private DrawerHolder drawerHolder;
    private String userName;
    private ImageView headerImg;
    private View redView;
    private TextView headerText;
    private TextView headerLogOut;
    private TextView text;
    private ImageView image;


    public DrawerAdapter(Context context, int resource, TypedArray typedArray, String[] val, String imgUrl, String userName) {
        super(context, resource, val);
        this.typedArray = typedArray;
        this.context = context;
        this.val = val;
        this.userName = userName;
        this.imgUrl = imgUrl;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        headerFont = Typeface.createFromAsset(context.getAssets(), "fonts/arial.ttf");
        sharedpreferences = context.getSharedPreferences(LoginPopup.MyPREFERENCES, Context.MODE_PRIVATE);
    }


    @Override
    public Object getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//        drawerHolder = new DrawerHolder();
//        View rowView = null;
//        if (convertView == null) {
        if (position == 0) {
            convertView = inflater.inflate(R.layout.drawer_header, parent, false);
            headerImg = (ImageView) convertView.findViewById(R.id.drawer_header_img);
            redView = (View) convertView.findViewById(R.id.red_view);

            try {
                Picasso.with(context).load(sharedpreferences.getString(LoginPopup.imgUrl, "")).transform(new CircleTransform()).into
                        (headerImg);
                headerText.setVisibility(View.VISIBLE);
                headerText.setText(sharedpreferences.getString(LoginPopup.userName, ""));
            } catch (Exception e) {
            }
            try {
                Picasso.with(context).load(imgUrl).transform(new CircleTransform()).placeholder(R.anim.progress_animation).into(headerImg);
            } catch (Exception e) {
            }

//                convertView.setTag(drawerHolder);
            //UserINfo
        } else if (position == 1) {
            convertView = inflater.inflate(R.layout.drawer_user_info, parent, false);
            headerText = (TextView) convertView.findViewById(R.id.header_text_slider);
            headerLogOut = (TextView) convertView.findViewById(R.id.header_log_out);
            redView = (View) convertView.findViewById(R.id.red_view);
//               line = (View) rowView.findViewById(R.id.login_line);
            headerText.setTypeface(headerFont);
            headerText.setVisibility(View.VISIBLE);
            headerText.setText(userName);
            convertView.setBackgroundColor(Color.parseColor("#EBECEC"));
            if (sharedpreferences.getString(LoginPopup.userName, "").isEmpty()) {
                headerText.setTextColor(Color.parseColor("#000000"));
//                   line.setVisibility(View.GONE);
            } else if (sharedpreferences.getString(LoginPopup.userName, "").length() > 1) {
                headerText.setTextColor(Color.parseColor("#000000"));
//                   line.setVisibility(View.VISIBLE);
            }
//                convertView.setTag(drawerHolder);

        } else {

            convertView = inflater.inflate(R.layout.drawer_list_item, parent, false);
//               line = (View) rowView.findViewById(R.id.login_line);
            text = (TextView) convertView.findViewById(R.id.drawer_list_text);
            image = (ImageView) convertView.findViewById(R.id.drawer_img);
            redView = (View) convertView.findViewById(R.id.red_view);
            text.setTypeface(headerFont);
//                convertView.setTag(drawerHolder);
        }


//        } else {
////            rowView = convertView;
//            drawerHolder = (DrawerHolder) convertView.getTag();
//        }

        if (position == 0) {

        } else if (position == 1) {

            if (sharedpreferences.getString(LoginPopup.userName, "").length() < 1 && sharedpreferences.getString(LoginPopup.imgUrl, "")
                    .length() < 1) {
                headerText.setText(val[position]);

            } else {
                try {
                    headerText.setText(sharedpreferences.getString(LoginPopup.userName, ""));
                    headerLogOut.setVisibility(View.VISIBLE);
//                   line.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                }
            }

        } else {
            try {
                text.setText(val[position]);
                image.setImageResource(typedArray.getResourceId(position - 2, -1));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (position == 9) {
                redView.setVisibility(View.GONE);
            }

        }

//
//
//        convertView.setTag(R.id.native_ad_icon,drawerHolder.redView);
//        convertView.setTag(R.id.native_ad_title, drawerHolder.nativeAdTitle);
//        convertView.setTag(R.id.native_ad_body, drawerHolder.nativeAdBody);
//        convertView.setTag(R.id.native_ad_media, drawerHolder.nativeAdMedia);
//        convertView.setTag(R.id.native_ad_social_context, drawerHolder.headerLogOut);
//        convertView.setTag(R.id.native_ad_call_to_action, drawerHolder.headerImg);
        return convertView;

    }
//    public class DrawerHolder {
//        public View redView;
//        TextView text;
//        ImageView image;
//        TextView headerText;
//        TextView headerLogOut;
//        ImageView headerImg;
////        View line;
//    }
}
