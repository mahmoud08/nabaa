//package com.madarsoft.hinewz.adapters;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.os.AsyncTask;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.android.gms.gcm.GcmPubSub;
//import com.madarsoft.hinewz.GCM.RegisterApp;
//import com.madarsoft.hinewz.R;
//import com.madarsoft.hinewz.Views.OnOffSwitch;
//import com.madarsoft.hinewz.activities.MainActivity;
//import com.madarsoft.hinewz.controls.JsonParser;
//import com.madarsoft.hinewz.controls.MainControl;
//import com.madarsoft.hinewz.database.DataBaseAdapter;
//import com.madarsoft.hinewz.entities.News;
//import com.madarsoft.hinewz.entities.Profile;
//import com.madarsoft.hinewz.entities.Sources;
//import com.madarsoft.hinewz.entities.URLs;
//import com.squareup.picasso.Picasso;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//
///**
// * Created by Colossus on 14-Aug-16.
// */
//public class NotificationAdapter extends BaseAdapter {
//    private static final String URGENT_TOPIC = "urgent";
//    private final DataBaseAdapter db;
//    private final List<Sources> myResources;
//    private final Context context;
//    private final MainControl mainControl;
//    private ArrayList<Profile> profile;
//    private JsonParser jsonParser;
//    private boolean isRemoveFromSub;
//    private Profile objProfile;
//    private boolean isRemove;
//    private boolean isUtility;
//
//
//    public NotificationAdapter(Context context, ArrayList<Sources> myResource) {
//        this.myResources = myResource;
//        this.context = context;
//        db = new DataBaseAdapter(context);
//        mainControl = new MainControl(context);
////        inflater = (LayoutInflater) context.
////                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    }
//
//    @Override
//    public int getCount() {
//        return myResources.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return myResources.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        final MyResourcesHolder myResourcesHolder;
//        myResourcesHolder = new MyResourcesHolder();
//        View rowView;
//
//        LayoutInflater inflater = (LayoutInflater) context.
//                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        if (position == 0)
//            rowView = inflater.inflate(R.layout.notification_header, parent, false);
//        else
//            rowView = inflater.inflate(R.layout.notified_source_item, parent, false);
//
////        if (convertView == null) {
////            if (position == 0)
////                convertView = inflater.inflate(R.layout.notification_header, parent, false);
////            else convertView = inflater.inflate(R.layout.notified_source_item, parent, false);
////
//////            myResourcesHolder = new MyResourcesHolder();
//
////        myResourcesHolder.generalNotify = (OnOffSwitch) rowView.findViewById(R.id.notify_on_off_general);
//        myResourcesHolder.urgentNotify = (OnOffSwitch) rowView.findViewById(R.id.notify_on_off_urgent);
//        myResourcesHolder.soundRadioGroup = (RadioGroup) rowView.findViewById(R.id.notification_sound_type);
//        myResourcesHolder.soundRadio = (RadioButton) rowView.findViewById(R.id.notification_sound);
//        myResourcesHolder.vibrationRadio = (RadioButton) rowView.findViewById(R.id.notification_vibration);
//        myResourcesHolder.bothRadio = (RadioButton) rowView.findViewById(R.id.notification_both);
//        myResourcesHolder.holder = (RelativeLayout) rowView.findViewById(R.id.notification_holder);
//        myResourcesHolder.progress = (ProgressBar) rowView.findViewById(R.id.select_progress);
//        myResourcesHolder.greyView = (View) rowView.findViewById(R.id.notification_view);
//
//        myResourcesHolder.sourceAlert = (OnOffSwitch) rowView.findViewById(R.id.sources_notify_on_off);
//        myResourcesHolder.text = (TextView) rowView.findViewById(R.id.source_item_name_tv);
//        myResourcesHolder.detailText = (TextView) rowView.findViewById(R.id.source_item_disc_tv);
//        myResourcesHolder.numberFollowers = (TextView) rowView.findViewById(R.id.my_source_item_followers_number_tv);
//        myResourcesHolder.image = (ImageView) rowView.findViewById(R.id.source_item_img);
//        //myResourcesHolder.checkedButton = (ToggleButton) rowView.findViewById(R.id.my_resource_item_add);
//        myResourcesHolder.selectionProgressBar = (ProgressBar) rowView.findViewById(R.id.select_progress);
//        myResourcesHolder.sectionHeader = (LinearLayout) rowView.findViewById(R.id.my_resource_header);
//        myResourcesHolder.textHeader = (TextView) rowView.findViewById(R.id.my_resource_header_tv);
//        rowView.setTag(myResourcesHolder);
//
////        convertView.setTag(myResourcesHolder);
//
//
////        } else {
////            myResourcesHolder = (MyResourcesHolder) convertView.getTag();
////        }
//        final Sources source = myResources.get(position);
//        if (position == 0) {
//            profile = db.getAllProfiles();
//            objProfile = profile.get(0);
//
////        urgentNotificationStatus = dp.getAllProfiles().get(0).getUrgentFlag();
////        generalNotification = dp.getAllProfiles().get(0).getSoundType();
//
//
////        notificationsListView.setFocusable(false);
//
//            if (objProfile.getUrgentFlag() > 0) {
//                isRemove = false;
//                try {
//                    isUtility = false;
//                    new Subscription().execute(URGENT_TOPIC);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                myResourcesHolder.urgentNotify.setSwitch(true);
//            } else {
//                isRemove = true;
//                try {
//                    isUtility = false;
//                    new Subscription().execute(URGENT_TOPIC);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                myResourcesHolder.urgentNotify.setSwitch(false);
//            }
//            if (objProfile.getSoundType() > 0) {
////                myResourcesHolder.generalNotify.setSwitch(true);
//                myResourcesHolder.greyView.setVisibility(View.GONE);
//            } else {
////                myResourcesHolder.generalNotify.setSwitch(false);
//                myResourcesHolder.greyView.setVisibility(View.VISIBLE);
//                myResourcesHolder.greyView.bringToFront();
//                myResourcesHolder.holder.setClickable(false);
//            }
//            switch (objProfile.getSoundType()) {
//                case 1:
//                    myResourcesHolder.soundRadio.setChecked(true);
//                    break;
//                case 2:
//                    myResourcesHolder.vibrationRadio.setChecked(true);
//                    break;
//                case 3:
//                    myResourcesHolder.bothRadio.setChecked(true);
//                    break;
//            }
//            isUtility = true;
//            new Subscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//
//            myResourcesHolder.greyView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(context, context.getString(
//                            R.string.enable_notification_first
//                    ), Toast.LENGTH_SHORT).show();
//                }
//            });
////            myResourcesHolder.generalNotify.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View v) {
////                    if (objProfile.getSoundType() > 0) {
////                        myResourcesHolder.generalNotify.setSwitch(false);
////                        onGreyClicked.onGreyClicked();
////                        myResourcesHolder.greyView.setVisibility(View.VISIBLE);
////                        myResourcesHolder.greyView.bringToFront();
////                        myResourcesHolder.holder.setClickable(false);
////                        objProfile.setSoundType(-1);
////                        objProfile.setUrgentFlag(-1);
////                        db.updateProfiles(objProfile);
////                    } else {
////                        myResourcesHolder.generalNotify.setSwitch(true);
////                        myResourcesHolder.greyView.setVisibility(View.GONE);
////                        objProfile.setSoundType(1);
////                        myResourcesHolder.soundRadioGroup.check(R.id.notification_sound);
////                        objProfile.setUrgentFlag(1);
////                        db.updateProfiles(objProfile);
////                    }
////                }
////            });
//
//            myResourcesHolder.urgentNotify.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (objProfile.getUrgentFlag() > 0) {
////                    urgentNotify.setSwitch(false);
////                    profile.get(0).setUrgentFlag(-1);
////                    dp.updateProfiles(profile.get(0));
////                    Toast.makeText(getActivity(), getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
//                        isRemove = true;
//                        isUtility = false;
//                        myResourcesHolder.urgentNotify.setVisibility(View.GONE);
//                        myResourcesHolder.progress.setVisibility(View.VISIBLE);
//                        new Subscription().execute(URGENT_TOPIC);
////                    sub.execute(URGENT_TOPIC);
//                        callJson(URLs.REMOVE_URGENT_URL, URLs.REMOVE_URGENT_REQUEST_TYPE, URLs.getUserID(), context, URLs.TAG_REMOVE_URGENT_USER_ID, true, myResourcesHolder);
//                    } else {
////                    urgentNotify.setSwitch(true);
////                    profile.get(0).setUrgentFlag(1);
////                    dp.updateProfiles(profile.get(0));
//                        isUtility = false;
//                        myResourcesHolder.urgentNotify.setVisibility(View.GONE);
//                        myResourcesHolder.progress.setVisibility(View.VISIBLE);
////                    Toast.makeText(getActivity(), getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
//                        isRemove = false;
//                        new Subscription().execute(URGENT_TOPIC);
////                    subscribeTopics(URGENT_TOPIC);
//                        callJson(URLs.ADD_URGENT_URL, URLs.ADD_URGENT_REQUEST_TYPE, URLs.getUserID(), context, URLs.TAG_ADD_URGENT_USER_ID, false, myResourcesHolder);
//
//                    }
//                }
//            });
//            myResourcesHolder.soundRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(RadioGroup group, int checkedId) {
//                    switch (checkedId) {
//                        case R.id.notification_sound:
//                            myResourcesHolder.soundRadio.setChecked(true);
//                            objProfile.setSoundType(1);
//                            db.updateProfiles(objProfile);
//                            break;
//                        case R.id.notification_vibration:
//                            myResourcesHolder.vibrationRadio.setChecked(true);
//                            objProfile.setSoundType(2);
//                            db.updateProfiles(objProfile);
//                            break;
//                        case R.id.notification_both:
//                            myResourcesHolder.bothRadio.setChecked(true);
//                            objProfile.setSoundType(3);
//                            db.updateProfiles(objProfile);
//                            break;
//                    }
//                }
//            });
//
//
//        } else {
//
//            Picasso.with(context)
//                    .load(myResources.get(position).getLogo_url())
//                    .placeholder(R.anim.progress_animation)
//                    .error(R.drawable.launcher)
//                    .into(myResourcesHolder.image);
//
//            myResourcesHolder.text.setText(myResources.get(position).getSource_name());
//
//            myResourcesHolder.detailText.setText(myResources.get(position).getDetails());
//            myResourcesHolder.numberFollowers.setText(myResources.get(position).getNumber_followers() + context.getString(R.string.followers));
//
//        /*if (myResources.get(position).getSelected_or_not() == 1) {
//            myResourcesHolder.checkedButton.setChecked(true);
//        } else {
//            myResourcesHolder.checkedButton.setChecked(false);
//        }*/
//
//            if (myResources.get(position).getNotifiable() > 0)
//                myResourcesHolder.sourceAlert.setSwitch(true);
//            else
//                myResourcesHolder.sourceAlert.setSwitch(false);
////        rowView.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                //onRowClick(myResourcesHolder, position, source);
////            }
////        });
//
//            myResourcesHolder.sourceAlert.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (myResources.get(position).getNotifiable() > 0) {
////                    myResourcesHolder.sourceAlert.setSwitch(false);
////                    source.setNotifiable(-1);
////                    db.updateSources(source);
//                        myResourcesHolder.selectionProgressBar.setVisibility(View.VISIBLE);
//                        myResourcesHolder.sourceAlert.setVisibility(View.GONE);
////                    Toast.makeText(context, context.getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
//                        callJson(URLs.REMOVE_NOTIFICATION_URL, URLs.REMOVE_NOTIFICATION_REQUEST_TYPE, URLs.getUserID(), source.getSource_id(), context, true, myResourcesHolder, position);
//                    } else {
////                    myResourcesHolder.sourceAlert.setSwitch(true);
////                    source.setNotifiable(1);
////                    db.updateSources(source);
//                        myResourcesHolder.sourceAlert.setVisibility(View.GONE);
//                        myResourcesHolder.selectionProgressBar.setVisibility(View.VISIBLE);
////                    Toast.makeText(context, context.getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
//                        callJson(URLs.ADD_NOTIFICATION_URL, URLs.ADD_NOTIFICATION_REQUEST_TYPE, URLs.getUserID(), source.getSource_id(), context, false, myResourcesHolder, position);
//                    }
//                }
//            });
//
//
//        }
//        rowView.setTag(R.id.sources_notify_on_off, myResourcesHolder.sourceAlert);
//        rowView.setTag(R.id.source_item_name_tv, myResourcesHolder.text);
//        rowView.setTag(R.id.source_item_disc_tv, myResourcesHolder.detailText);
//        rowView.setTag(R.id.my_source_item_followers_number_tv, myResourcesHolder.numberFollowers);
//        rowView.setTag(R.id.source_item_img, myResourcesHolder.image);
//        //rowView.setTag(R.id.my_resource_item_add, myResourcesHolder.checkedButton);
//        rowView.setTag(R.id.my_resource_header_tv, myResourcesHolder.textHeader);
//        rowView.setTag(R.id.my_resource_header, myResourcesHolder.sectionHeader);
//        rowView.setTag(R.id.select_progress, myResourcesHolder.selectionProgressBar);
//
////        rowView.setTag(R.id.notify_on_off_general, myResourcesHolder.generalNotify);
//        rowView.setTag(R.id.notify_on_off_urgent, myResourcesHolder.urgentNotify);
//        rowView.setTag(R.id.notification_sound_type, myResourcesHolder.soundRadioGroup);
//        rowView.setTag(R.id.notification_sound, myResourcesHolder.soundRadio);
//        rowView.setTag(R.id.notification_both, myResourcesHolder.bothRadio);
//        rowView.setTag(R.id.notification_holder, myResourcesHolder.holder);
//        rowView.setTag(R.id.select_progress, myResourcesHolder.progress);
//        rowView.setTag(R.id.notification_view, myResourcesHolder.greyView);
//        rowView.setTag(R.id.notification_vibration, myResourcesHolder.vibrationRadio);
//
//        return rowView;
//    }
//
//
//    private void callJson(String url, int requestType, int id, int sourceId, final Context context, final boolean isRemove, final MyResourcesHolder myResourcesHolder, final int i) {
//
//        try {
//            if (!mainControl.checkInternetConnection()) {
//                Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
//                return;
//            }
//        } catch (NullPointerException e) {
//
//        }
//        HashMap<String, String> dataObj = new HashMap<>();
////        dataObj.put(tag, id + "");
//        if (requestType == URLs.ADD_NOTIFICATION_REQUEST_TYPE) {
//            dataObj.put(URLs.TAG_ADD_NOTIFICATION_USER_ID, id + "");
//            dataObj.put(URLs.TAG_ADD_NOTIFICATION_SOURCE_ID, sourceId + "");
//        } else if (requestType == URLs.REMOVE_NOTIFICATION_REQUEST_TYPE) {
//            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_USER_ID, id + "");
//            dataObj.put(URLs.TAG_REMOVE_NOTIFICATION_SOURCE_ID, sourceId + "");
//        }
//
//        jsonParser = new JsonParser(context, url, requestType, dataObj);
//        jsonParser.execute();
//        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
//            @Override
//            public void onFinished() {
//
//            }
//
//            @Override
//            public void onFinished(List<Integer> likeResponse) {
//
//            }
//
//            @Override
//            public void onFinished(News newsObj) {
//            }
//
//            @Override
//            public void onFinished(ArrayList<Sources> sourceList) {
//
//            }
//
//            @Override
//            public void onFinished(int id) {
//            }
//
//            @Override
//            public void onFinished(boolean success) {
//                if (success) {
////                    Toast.makeText(context, success + "", Toast.LENGTH_SHORT).show();
//                    if (isRemove) {
//                        myResourcesHolder.selectionProgressBar.setVisibility(View.GONE);
//                        myResourcesHolder.sourceAlert.setSwitch(false);
//                        myResources.get(i).setNotifiable(-1);
//                        myResourcesHolder.sourceAlert.setVisibility(View.VISIBLE);
//                        isRemoveFromSub = true;
//                        new NotificationSubscription().execute(myResources.get(i).getSource_id() + "");
//                        db.updateSources(myResources.get(i));
//                    } else {
//                        myResourcesHolder.selectionProgressBar.setVisibility(View.GONE);
//                        myResourcesHolder.sourceAlert.setSwitch(true);
//                        myResources.get(i).setNotifiable(1);
//                        myResourcesHolder.sourceAlert.setVisibility(View.VISIBLE);
//                        isRemoveFromSub = false;
//                        new NotificationSubscription().execute(myResources.get(i).getSource_id() + "");
//                        db.updateSources(myResources.get(i));
//                    }
//                } else {
//                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
//                    if (isRemove) {
//                        myResourcesHolder.selectionProgressBar.setVisibility(View.GONE);
//                        myResourcesHolder.sourceAlert.setVisibility(View.VISIBLE);
//                        myResourcesHolder.sourceAlert.setSwitch(true);
//                        myResources.get(i).setNotifiable(1);
//                        isRemoveFromSub = false;
//                        new NotificationSubscription().execute(myResources.get(i).getSource_id() + "");
//                        db.updateSources(myResources.get(i));
//                    } else {
//                        myResourcesHolder.selectionProgressBar.setVisibility(View.GONE);
//                        myResourcesHolder.sourceAlert.setVisibility(View.VISIBLE);
//                        myResourcesHolder.sourceAlert.setSwitch(false);
//                        myResources.get(i).setNotifiable(-1);
//                        isRemoveFromSub = true;
//                        new NotificationSubscription().execute(myResources.get(i).getSource_id() + "");
//                        db.updateSources(myResources.get(i));
//                    }
//                }
//            }
//        });
//    }
//
//    private void callJson(String url, int requestType, int id, final Context context, String tag, final boolean isRemove, final MyResourcesHolder myResourcesHolder) {
//
//        try {
//            if (!mainControl.checkInternetConnection()) {
//                Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
//                return;
//            }
//        } catch (NullPointerException e) {
//
//        }
//        HashMap<String, String> dataObj = new HashMap<>();
//        dataObj.put(tag, id + "");
//        if (requestType == URLs.ADD_URGENT_REQUEST_TYPE) {
//            dataObj.put(URLs.TAG_ADD_URGENT_USER_ID, id + "");
//        } else {
//            dataObj.put(URLs.TAG_REMOVE_URGENT_USER_ID, id + "");
//        }
//
//        jsonParser = new JsonParser(context, url, requestType, dataObj);
//        jsonParser.execute();
//        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
//            @Override
//            public void onFinished() {
//
//            }
//
//            @Override
//            public void onFinished(List<Integer> likeResponse) {
//
//            }
//
//            @Override
//            public void onFinished(News newsObj) {
//            }
//
//            @Override
//            public void onFinished(ArrayList<Sources> sourceList) {
//
//            }
//
//            @Override
//            public void onFinished(int id) {
//            }
//
//            @Override
//            public void onFinished(boolean success) {
//                if (success) {
//                    if (isRemove) {
//                        myResourcesHolder.urgentNotify.setSwitch(false);
//                        objProfile.setUrgentFlag(-1);
//                        myResourcesHolder.urgentNotify.setVisibility(View.VISIBLE);
//                        myResourcesHolder.progress.setVisibility(View.GONE);
//                        db.updateProfiles(objProfile);
//                    } else {
//                        myResourcesHolder.urgentNotify.setSwitch(true);
//                        objProfile.setUrgentFlag(1);
//                        myResourcesHolder.urgentNotify.setVisibility(View.VISIBLE);
//                        myResourcesHolder.progress.setVisibility(View.GONE);
//                        db.updateProfiles(objProfile);
//                    }
//                } else {
//                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
//                    if (isRemove) {
//                        myResourcesHolder.urgentNotify.setSwitch(true);
//                        myResourcesHolder.urgentNotify.setVisibility(View.VISIBLE);
//                        myResourcesHolder.progress.setVisibility(View.GONE);
//                        objProfile.setUrgentFlag(1);
//                        db.updateProfiles(objProfile);
//                    } else {
//                        myResourcesHolder.urgentNotify.setSwitch(false);
//                        myResourcesHolder.urgentNotify.setVisibility(View.VISIBLE);
//                        myResourcesHolder.progress.setVisibility(View.GONE);
//                        objProfile.setUrgentFlag(-1);
//                        db.updateProfiles(objProfile);
//                    }
//                }
//            }
//        });
//    }
//
//
//    class MyResourcesHolder {
//        //        public OnOffSwitch generalNotify;
//        public OnOffSwitch urgentNotify;
//        public RadioGroup soundRadioGroup;
//        public RadioButton soundRadio;
//        public RadioButton vibrationRadio;
//        public RadioButton bothRadio;
//        public RelativeLayout holder;
//        public View greyView;
//        public ProgressBar progress;
//        public ProgressBar selectionProgressBar;
//        public TextView text;
//        public TextView detailText;
//        public TextView numberFollowers;
//        public TextView textHeader;
//        public LinearLayout sectionHeader;
//        public ImageView image;
//        public OnOffSwitch sourceAlert;
//    }
//
//    //for Sub
//    class NotificationSubscription extends AsyncTask<String, Void, String> {
//
//
//        @Override
//        protected String doInBackground(String... params) {
//            final GcmPubSub pubSub = GcmPubSub.getInstance(context.getApplicationContext());
//            final SharedPreferences prefs = context.getSharedPreferences(MainActivity.MyPREFERENCES,
//                    Context.MODE_PRIVATE);
//            String token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
//            try {
//                if (isRemoveFromSub) {
//                    pubSub.unsubscribe(token, "/topics/" + params[0] + "");
//                } else {
//                    pubSub.subscribe(token, "/topics/" + params[0] + "", null);
//                }
//            } catch (IOException e) {
//
//            }
//            return "Executed";
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//
//            if (result.equals("Executed")) {
//
//
//            }
//            // might want to change "executed" for the returned string passed
//            // into onPostExecute() but that is upto you
//        }
//
//    }
//
//    class Subscription extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected String doInBackground(String... params) {
//            if (isUtility) {
//                for (int i = 0; i < 1; i++) {
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        Thread.interrupted();
//                    }
//                }
//
//                return "Executed";
//            }
//            GcmPubSub pubSub = null;
//            SharedPreferences prefs = null;
//            try {
//                pubSub = GcmPubSub.getInstance(context.getApplicationContext());
//                prefs = context.getSharedPreferences(MainActivity.MyPREFERENCES,
//                        Context.MODE_PRIVATE);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            String token = null;
//            try {
//                token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            try {
//                if (isRemove) {
//                    pubSub.unsubscribe(token, "/topics/" + params[0] + "");
//                } else {
//                    pubSub.subscribe(token, "/topics/" + params[0] + "", null);
//                }
//            } catch (Exception e) {
//
//            }
//            return "";
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//
//            if (result.equals("Executed")) {
//
////                Utility.setListViewHeightBasedOnChildren(notificationsListView);
//                // Helper.getListViewSize(notificationsListView);
////                gifMovieView.setVisibility(View.GONE);
////                gifMovieView.setPaused(true);
////                notificationsListView.setVisibility(View.VISIBLE);
////                NotificationsFragment.Helper.getListViewSize(notificationsListView);
//
//            }
//            // might want to change "executed" for the returned string passed
//            // into onPostExecute() but that is upto you
//        }
//
//    }
//
//}
