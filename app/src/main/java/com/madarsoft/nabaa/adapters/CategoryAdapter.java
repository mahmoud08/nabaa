package com.madarsoft.nabaa.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.fragments.SourceNews;
import com.madarsoft.nabaa.fragments.SourcesFragmentV2;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoryAdapter extends ArrayAdapter {

    private final Typeface font;
    private final boolean isCategoryNews;
    Context context;
    int type;
    private LayoutInflater inflater;
    private List<Category> categories;
    private CategoryHolder categoryHolder;
    private int lastPosition = -1;
    private OnSourcesSelected onSourceUpdated;
    private long mLastClickTime = 0;
    private DataBaseAdapter dataBaseAdapter;

    public CategoryAdapter(Context context, int resource, List<Category> categories, int type, boolean isCategoryNews) {

        super(context, resource, categories);
        this.context = context;
        this.categories = categories;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.type = type;
        this.isCategoryNews = isCategoryNews;
        font = Typeface.createFromAsset(context.getAssets(), "fonts/Hacen_Liner_Screen.ttf");
        dataBaseAdapter = new DataBaseAdapter(context);
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        categoryHolder = new CategoryHolder();
        final View rowView;

        if (convertView == null) {
            rowView = inflater.inflate(R.layout.category_list_item, parent, false);
            categoryHolder.img = (ImageView) rowView.findViewById(R.id.category_img);
            categoryHolder.text = (TextView) rowView.findViewById(R.id.category_text);
        } else {
            rowView = convertView;
            categoryHolder = (CategoryHolder) rowView
                    .getTag();
        }

        categoryHolder.text.setTypeface(font);

        Picasso.with(context)
                .load(categories.get(position).getLogo_url())
                .placeholder(R.anim.progress_animation)
                .error(R.drawable.item_icon)
                .into(categoryHolder.img);
        rowView.setTag(categoryHolder);

        categoryHolder.text.setText(categories.get(position).getCategory_name());

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                dataBaseAdapter.increment(categories.get(position).getID());
                FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
//1--Countries
                if (!isCategoryNews) {
                    transaction.add(R.id.parent, new SourcesFragmentV2(categories.get(position), type));
                    transaction.commit();
                    //
                } else {
                    //filtered Country
                    if (categories.get(position).getCategory_type() == 2)
                        transaction.add(R.id.parent, new SourceNews(categories.get(position).getCategory_id(), false, false,
                                categories.get(position).getCategory_name(), true));
                        //filtered Subjects
                    else
                        transaction.add(R.id.parent, new SourceNews(categories.get(position).getCategory_id(), false, true,
                                categories.get(position).getCategory_name(), true));
                    transaction.commit();
                }
//                SubjectListFragment.generalPosition = position;
//                onSourceUpdated.onFinished(position);
            }
        });

        return rowView;
    }

    public void setOnSourceUpdated(OnSourcesSelected onSourceUpdated) {
        this.onSourceUpdated = onSourceUpdated;
    }

    public interface OnSourcesSelected {
        void onFinished(int index);

    }

    public class CategoryHolder {
        TextView text;
        ImageView img;
    }
}
