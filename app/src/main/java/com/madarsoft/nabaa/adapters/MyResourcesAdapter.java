package com.madarsoft.nabaa.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.firebase.messaging.FirebaseMessaging;
import com.madarsoft.nabaa.GCM.RegisterApp;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.fragments.AlertDialogFrag;
import com.madarsoft.nabaa.fragments.SourceNews;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Colossus on 3/14/2016.
 */
public class MyResourcesAdapter extends BaseAdapter {
    public static boolean isGeo;
    private final int maxSize;
    private final boolean isEmpty;
    DataBaseAdapter db;
    ArrayList<Category> categories;
    LayoutInflater inflater;
    Typeface font;
    OnRowSelectedListener onRowSelectedListener;
    Toast errotToast;
    boolean ignoreDisabled = false;
    private Typeface headerFont;
    private Context context;
    private List<Sources> mySources;
    private JsonParser jsonParser;
    private int lastPosition = -1;
    private boolean isServerResponsed;
    private boolean isRemoveFromSub;
    private int rankMax;

//    public OnRowSelectedListener getOnRowSelectedListener() {
//        return onRowSelectedListener;
//    }

    public MyResourcesAdapter(Context context, List<Sources> myResource, boolean isGeo, int maxSize, boolean isEmpty, int rankMax) {
        this.mySources = myResource;
        this.maxSize = maxSize;
        this.context = context;
        this.isEmpty = isEmpty;
        db = new DataBaseAdapter(context);
        this.rankMax = rankMax;
        this.isGeo = isGeo;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        font = Typeface.createFromAsset(context.getAssets(), "fonts/Hacen_Liner_Screen.ttf");
        headerFont = Typeface.createFromAsset(context.getAssets(), "fonts/Hacen_Liner_Screen_Bd.ttf");
        errotToast = Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT);

    }

    public void setOnRowSelectedListener(OnRowSelectedListener onRowSelectedListener) {
        this.onRowSelectedListener = onRowSelectedListener;
    }

    @Override
    public int getCount() {
        return mySources.size();
    }

    @Override
    public Object getItem(int position) {
        return mySources.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final MyResourcesHolder myResourcesHolder;
        myResourcesHolder = new MyResourcesHolder();
        final View rowView;
        if (convertView == null) {
            rowView = inflater.inflate(R.layout.my_sources_item, parent, false);
        } else {
            rowView = convertView;
        }
        final Sources source = mySources.get(position);

        myResourcesHolder.text = (TextView) rowView.findViewById(R.id.source_item_name_tv);
        myResourcesHolder.detailText = (TextView) rowView.findViewById(R.id.source_item_disc_tv);
        myResourcesHolder.numberFollowers = (TextView) rowView.findViewById(R.id.my_source_item_followers_number_tv);
        myResourcesHolder.image = (ImageView) rowView.findViewById(R.id.source_item_img);
        myResourcesHolder.checkedButton = (ImageView) rowView.findViewById(R.id.my_resource_item_add);
        myResourcesHolder.selectionProgressBar = (ProgressBar) rowView.findViewById(R.id.select_progress);
        myResourcesHolder.sectionHeader = (LinearLayout) rowView.findViewById(R.id.my_resource_header);
        myResourcesHolder.textHeader = (TextView) rowView.findViewById(R.id.my_resource_header_tv);
        myResourcesHolder.preview = (ImageView) rowView.findViewById(R.id.my_sources_preview);
        myResourcesHolder.selCell = (RelativeLayout) rowView.findViewById(R.id.my_sources_item_sel);
        myResourcesHolder.text.setTypeface(headerFont);
        myResourcesHolder.textHeader.setTypeface(headerFont);
        myResourcesHolder.detailText.setTypeface(font);
        myResourcesHolder.numberFollowers.setTypeface(font);
        myResourcesHolder.checkedButton.setClickable(false);
        rowView.setTag(myResourcesHolder);

        Picasso.with(context)
                .load(mySources.get(position).getLogo_url())
                .placeholder(R.anim.progress_animation)
                .error(R.drawable.launcher)
                .into(myResourcesHolder.image);

        myResourcesHolder.text.setText(mySources.get(position).getSource_name());

        myResourcesHolder.detailText.setText(mySources.get(position).getDetails());
        myResourcesHolder.numberFollowers.setText(mySources.get(position).getNumber_followers() + context.getString(R.string.followers));

        if (mySources.get(position).getSelected_or_not() == 1) {
            myResourcesHolder.checkedButton.setImageResource(R.drawable.source_news_check);

        } else {
            myResourcesHolder.checkedButton.setImageResource(R.drawable.add);
        }

        myResourcesHolder.selCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainControl mainControl = new MainControl(context);
                if (!mainControl.checkInternetConnection()) {
                    Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (lastPosition == position && !isServerResponsed)
                    return;
                lastPosition = position;
                isServerResponsed = false;
                onRowClick(parent, myResourcesHolder, position, source);
            }
        });
        myResourcesHolder.preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                transaction.add(R.id.parent, new SourceNews(source.getSource_id(), true, false, "", true));
                //transaction.add(R.id.parent, new SourceNews(news.getSourceID(), true, false, ""));
                transaction.commit();
            }
        });
//        detectSourceType(myResourcesHolder, position);
        isGeo(myResourcesHolder, position);
//        isSub(myResourcesHolder, position);

        rowView.setTag(R.id.source_item_name_tv, myResourcesHolder.text);
        rowView.setTag(R.id.source_item_disc_tv, myResourcesHolder.detailText);
        rowView.setTag(R.id.my_source_item_followers_number_tv, myResourcesHolder.numberFollowers);
        rowView.setTag(R.id.source_item_img, myResourcesHolder.image);
        rowView.setTag(R.id.my_resource_item_add, myResourcesHolder.checkedButton);
        rowView.setTag(R.id.my_resource_header_tv, myResourcesHolder.textHeader);
        rowView.setTag(R.id.my_resource_header, myResourcesHolder.sectionHeader);
        rowView.setTag(R.id.select_progress, myResourcesHolder.selectionProgressBar);

        return rowView;
    }

    private void showConfirmationDialoge(final int position , final MyResourcesHolder myResourcesHolder, final Sources source) {
        FragmentManager fm = ((FragmentActivity)context).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        final AlertDialogFrag commentFrag = new AlertDialogFrag(context.getString(R.string.source_delete_confirmation), context.getString(R.string.yes),
                context.getString(R.string.cancel), AlertDialogFrag.DIALOG_TWO_BUTTONS);
        commentFrag.show(ft, "dialog");
        commentFrag.setCancelable(false);
        commentFrag.OnDialogClickListener(new AlertDialogFrag.OnDialogListener() {
            @Override
            public void onPositiveClickListener() {
                HashMap<String, String> dataObj = new HashMap<>();
                dataObj.put(URLs.TAG_ADD_SOURCE_USERID, URLs.getUserID() + "");
                dataObj.put(URLs.TAG_SOURCE_SOURCEID, mySources.get(position).getSource_id() + "");
                jsonParser = new JsonParser(context, URLs.DELETE_SOURCE_URL, URLs.SOURCE_DELETE_REQUEST_TYPE, dataObj);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                else
                    jsonParser.execute();
                jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                    @Override
                    public void onFinished() {

//                    mySources.remove(position);
//                    onRowSelectedListener.onSelected(true, position);
                    }

                    @Override
                    public void onFinished(List<Integer> likeResponse) {

                    }

                    @Override
                    public void onFinished(News newsObj) {

                    }

                    @Override
                    public void onFinished(ArrayList<Sources> sourceList) {

                    }

                    @Override
                    public void onFinished(int userID) {

                    }

                    @Override
                    public void onFinished(boolean success) {
                        commentFrag.dismiss();
                        isServerResponsed = true;
                        myResourcesHolder.checkedButton.setVisibility(View.VISIBLE);
                        myResourcesHolder.selectionProgressBar.setVisibility(View.GONE);
                        if (success) {
                            myResourcesHolder.checkedButton.setImageResource(R.drawable.add);
                            source.setSelected_or_not(0);
                            source.setNumber_followers(source.getNumber_followers() - 1);
                            FirebaseMessaging.getInstance().unsubscribeFromTopic(source.getSource_id() + "");
//                        new UrgentSubscription().execute(source.getSource_id() + "");
                            FirebaseMessaging.getInstance().unsubscribeFromTopic(source
                                    .getSource_id() + "U");
                            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, source
                                    .getSource_id() + "");
                            source.setUrgent_notify(-1);
                            db.updateSources(source);
                            myResourcesHolder.numberFollowers.setText(source.getNumber_followers() + " " + context.getString(R.string
                                    .followers));
                            try {
                                MainControl.writeCacheFile(context, new ArrayList<News>(), "recentNews");
                                MainControl.writeCacheFile(context, new ArrayList<News>(), "hotNews");
                                MainControl.writeCacheFile(context, new ArrayList<News>(), "urgentNews");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            /////
                            errotToast.show();
                            myResourcesHolder.checkedButton.setImageResource(R.drawable.source_news_check);
                        }
                    }
                });
            }

            @Override
            public void onNegativeClickListener() {
                commentFrag.dismiss();
                myResourcesHolder.checkedButton.setVisibility(View.VISIBLE);
                myResourcesHolder.selectionProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void onRowClick(final ViewGroup parent, final MyResourcesHolder myResourcesHolder, final int position, final Sources source) {
//        MainControl mainControl = new MainControl(context);
//        if (!mainControl.checkInternetConnection()) {
//            Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
//            return;
//        }

        myResourcesHolder.checkedButton.setVisibility(View.INVISIBLE);
        myResourcesHolder.selectionProgressBar.setVisibility(View.VISIBLE);
        if (mySources.get(position).getSelected_or_not() == 1) {
            //change it on the server
           showConfirmationDialoge(position, myResourcesHolder, source);

        } else {
            //change it on the server
            HashMap<String, String> dataObj = new HashMap<>();
            dataObj.put(URLs.TAG_ADD_SOURCE_USERID, URLs.getUserID() + "");
            dataObj.put(URLs.TAG_SOURCE_SOURCEID, mySources.get(position).getSource_id() + "");
            jsonParser = new JsonParser(context, URLs.ADD_SOURCE_URL, URLs.SOURCE_ADD_REQUEST_TYPE, dataObj);
            //change it on locale
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                @Override
                public void onFinished() {
                }

                @Override
                public void onFinished(List<Integer> likeResponse) {

                }

                @Override
                public void onFinished(News newsObj) {

                }

                @Override
                public void onFinished(ArrayList<Sources> sourceList) {

                }

                @Override
                public void onFinished(int userID) {

                }

                @Override
                public void onFinished(boolean success) {
                    isServerResponsed = true;
                    myResourcesHolder.checkedButton.setVisibility(View.VISIBLE);
                    try {
                        myResourcesHolder.selectionProgressBar.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (success) {
                        myResourcesHolder.checkedButton.setImageResource(R.drawable.source_news_check);

                        source.setSelected_or_not(1);
                        source.setNumber_followers(source.getNumber_followers() + 1);
                        source.setUrgent_notify(1);
                        //FirebaseMessaging.getInstance().subscribeToTopic("Urgent");
                        FirebaseMessaging.getInstance().subscribeToTopic(source.getSource_id() + "U");
                        db.updateSources(source);
                        myResourcesHolder.numberFollowers.setText(source.getNumber_followers() + " " + context.getString(R.string
                                .followers));
                        try {
                            MainControl.writeCacheFile(context, new ArrayList<News>(), "recentNews");
                            MainControl.writeCacheFile(context, new ArrayList<News>(), "urgentNews");
                            MainControl.writeCacheFile(context, new ArrayList<News>(), "hotNews");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        errotToast.show();
                        myResourcesHolder.checkedButton.setImageResource(R.drawable.add);
                    }
                }
            });
        }
    }

    private void isGeo(MyResourcesHolder myResourcesHolder, int position) {
        if (!isEmpty) {
            if (position <= rankMax - 1) {
                try {
                    if (position == 0) {
                        myResourcesHolder.sectionHeader.setVisibility(View.VISIBLE);
                        myResourcesHolder.textHeader.setText("الأكثر قراءة");
                        return;
                    } else {
                        myResourcesHolder.sectionHeader.setVisibility(View.GONE);
                        return;
//                    myResourcesHolder.textHeader.setText("الأكثر قراءة");
                    }
                } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews0", "exception", e);
                }

            }
        }

        if (position >= maxSize) {
            isSub(myResourcesHolder, position);
        } else {
            if (isEmpty) {
                try {
                    if (position == 0) {
                        myResourcesHolder.sectionHeader.setVisibility(View.VISIBLE);
                        myResourcesHolder.textHeader.setText(getCatNameByGeo(mySources.get(position).getGeo_id()));
                    }
                } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews0", "exception", e);
                }
            } else {
                try {
                    if (position == rankMax) {
                        myResourcesHolder.sectionHeader.setVisibility(View.VISIBLE);
                        myResourcesHolder.textHeader.setText(getCatNameByGeo(mySources.get(position).getGeo_id()));
                        return;
                    }
                } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews0", "exception", e);
                }
            }
            try {
                if (getCatNameByGeo(mySources.get(position).getGeo_id()).equalsIgnoreCase(getCatNameByGeo(mySources.get(position - 1)
                        .getGeo_id()))) {
                    myResourcesHolder.sectionHeader.setVisibility(View.GONE);
                }
            } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews1", "exception", e);
            }
            try {
                if (!getCatNameByGeo(mySources.get(position).getGeo_id()).equalsIgnoreCase(getCatNameByGeo(mySources.get(position - 1)
                        .getGeo_id()))) {
                    myResourcesHolder.sectionHeader.setVisibility(View.VISIBLE);
                    myResourcesHolder.textHeader.setText(getCatNameByGeo(mySources.get(position).getGeo_id()));
                }
            } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews2", "exception", e);
            }
        }

    }

    private void isSub(MyResourcesHolder myResourcesHolder, int position) {
        try {
            if (position == maxSize) {
                myResourcesHolder.sectionHeader.setVisibility(View.VISIBLE);
                myResourcesHolder.textHeader.setText(getCatNameBySubId(mySources.get(position).getSub_id()));
                return;
            }
        } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews3", "exception", e);
        }
        try {
            if (getCatNameBySubId(mySources.get(position).getSub_id()).equalsIgnoreCase(getCatNameBySubId(mySources.get(position - 1)
                    .getSub_id()))) {
                myResourcesHolder.sectionHeader.setVisibility(View.GONE);

            }
        } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews4", "exception", e);
        }
        try {
            if (!getCatNameBySubId(mySources.get(position).getSub_id()).equalsIgnoreCase(getCatNameBySubId(mySources.get(position - 1)
                    .getSub_id()))) {
                myResourcesHolder.sectionHeader.setVisibility(View.VISIBLE);
                myResourcesHolder.textHeader.setText(getCatNameBySubId(mySources.get(position).getSub_id()));

            }
        } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews5", "exception", e);
        }
    }

    private void detectSourceType(MyResourcesHolder myResourcesHolder, int position) {
        //Geo Selected First
        if (isGeo) {
            isGeo(myResourcesHolder, position);
            //Sub
        } else {
            isSub(myResourcesHolder, position);
        }
    }

    String getCatNameByGeo(int cat) {
        categories = db.getCategoryNamebyGeoId(cat);
        return categories.get(0).getCategory_name();
    }

    String getCatNameBySubId(int cat) {
        categories = db.getCategoryNameBySubId(cat);
            return categories.get(0).getCategory_name();
    }

    public interface OnRowSelectedListener {
        void onSelected(boolean isGeo, int position);
    }

    class MyResourcesHolder {
        RelativeLayout selCell;
        ProgressBar selectionProgressBar;
        TextView text;
        TextView detailText;
        TextView numberFollowers;
        TextView textHeader;
        LinearLayout sectionHeader;
        ImageView checkedButton;
        ImageView image;
        ImageView preview;
    }

    class NotificationSubscription extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final GcmPubSub pubSub = GcmPubSub.getInstance(context);
            final SharedPreferences prefs = context.getSharedPreferences(MainActivity.MyPREFERENCES,
                    Context.MODE_PRIVATE);
            String token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
            try {
                pubSub.unsubscribe(token, "/topics/" + params[0]);
                Log.d("topicGCM ", "Subscribed");
            } catch (IOException e) {

            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals("Executed")) {

            }
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }
    }

}