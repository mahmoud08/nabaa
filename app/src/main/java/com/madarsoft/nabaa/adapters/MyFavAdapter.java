package com.madarsoft.nabaa.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.ExpandableView;
import com.madarsoft.nabaa.controls.GPlusListAdapter;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.controls.SpeedScrollListener;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.fragments.AddCommentFragment;
import com.madarsoft.nabaa.fragments.NewsDetail2;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Colossus on 15-May-16.
 */
public class MyFavAdapter extends GPlusListAdapter {

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    public static List<News> newsList;
    private final Context context;
    private final DataBaseAdapter dp;
    private final Locale locale;
    private final SimpleDateFormat print;
    News news;
    Handler handler = new Handler();
    private JsonParser jsonParser;
    private String[] monthsName = {"يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر",
            "ديسمبر"};
    private int myPos;
    private Animation pop;
    private MainControl mainControl;
    private int diff;
    private int seconds;
    private String today;
    private String year;
    private String time;
    private int month;
    private Date parsedDate;

    public MyFavAdapter(Context context, SpeedScrollListener scrollListener, List<News> newsList) {
        super(context, scrollListener, newsList);
        this.context = context;
        mainControl = new MainControl(context);
        this.newsList = newsList;
        dp = new DataBaseAdapter(context);
        locale = new Locale("ar", "SA");
        print = new SimpleDateFormat("hh:mm a dd-MMMM-yyyy ", locale);
        try {
            /*URLs.setLastNewsID(newsList.get(newsList.size() - 1).getID());
            URLs.setFirstNewsID(newsList.get(0).getID());*/
            URLs.setFavNewsID(newsList.get(newsList.size() - 1).getFavouriteID());
        } catch (IndexOutOfBoundsException e) {
        } catch (NullPointerException e) {
        }
    }

    public static String getCurrentTimezoneOffset() {

        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        int offsetInMillis = tz.getOffset(cal.getTimeInMillis());

        String offset = String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
        offset = (offsetInMillis >= 0 ? "+" : "-") + offset;

        return offset;
    }

    @Override
    public int getCount() {
        try {
            return newsList.size();
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    protected View getRowView(final int position, View convertView, ViewGroup parent) {
        myPos = position;
        final Holder holder;
        holder = new Holder();
        View rowView;
        final News news = newsList.get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.fav_list_item, parent, false);
        } else {
            rowView = convertView;
        }

        holder.actionsView = (ExpandableView) rowView.findViewById(R.id.actions_expanded_view);
        holder.deselectFav = (ImageView) rowView.findViewById(R.id.deselect_fav);
        //holder.actionsView.setTag(position);
        holder.likesView = (LinearLayout) rowView.findViewById(R.id.likes_view);
        //holder.likesView.setTag(position);
        holder.commentsView = (LinearLayout) rowView.findViewById(R.id.comments_view);
        //holder.commentsView.setTag(position);
        holder.sharesView = (LinearLayout) rowView.findViewById(R.id.shares_view);
        //holder.sharesView.setTag(position);

        holder.likeBtn = (LinearLayout) rowView.findViewById(R.id.like_btn);
        //holder.likeBtn.setTag(position);
        holder.commentBtn = (LinearLayout) rowView.findViewById(R.id.comment_btn);
        //holder.commentBtn.setTag(position);
        holder.shareBtn = (LinearLayout) rowView.findViewById(R.id.share_btn);
        //holder.shareBtn.setTag(position);

        holder.sourceTitle = (TextView) rowView.findViewById(R.id.source_title);
        holder.newsTime = (TextView) rowView.findViewById(R.id.news_time);
        holder.newsTitle = (TextView) rowView.findViewById(R.id.news_title);
        holder.sharingNumbers = (TextView) rowView.findViewById(R.id.sharing_number);
        holder.commentsNumber = (TextView) rowView.findViewById(R.id.comments_number);
        holder.likesNumber = (TextView) rowView.findViewById(R.id.likes_number);
        holder.likeTitle = (TextView) rowView.findViewById(R.id.like_txt);
        holder.commentTitle = (TextView) rowView.findViewById(R.id.comment_txt);
        holder.shareTitle = (TextView) rowView.findViewById(R.id.share_txt);

        holder.sourceImg = (ImageView) rowView.findViewById(R.id.source_image);
        holder.likeImg = (ImageView) rowView.findViewById(R.id.like_img);
        holder.commentImg = (ImageView) rowView.findViewById(R.id.comment_img);
        holder.shareImg = (ImageView) rowView.findViewById(R.id.share_img);
        holder.newsLogo = (ImageView) rowView.findViewById(R.id.news_logo);
        holder.headerView = rowView.findViewById(R.id.header_view);
        holder.footerView = rowView.findViewById(R.id.footer_view);
        holder.loading = (ProgressBar) rowView.findViewById(R.id.loading_fav);
        holder.sourceTitle.setText(news.getSourceTitle());
        holder.sourceTitle.setGravity(Gravity.RIGHT);
        holder.progressBar = (ProgressBar) rowView.findViewById(R.id.like_loading);
        holder.gifLoading = (GifMovieView) rowView.findViewById(R.id.loading_spinner);
        holder.reload = (ImageView) rowView.findViewById(R.id.reload);
        holder.gifLoading.setMovieResource(R.drawable.gif_reload);
        holder.gifLoading.bringToFront();
        //Date date = new Date(news.getTime_stamp());
        setViewGone(holder, news);
        setViewsVisible(holder, news);

        if (position == 0) {
            holder.headerView.setVisibility(View.VISIBLE);
        } else {
            holder.headerView.setVisibility(View.GONE);
        }
        if (position == getCount() - 1) {
            holder.footerView.setVisibility(View.VISIBLE);
        } else {
            holder.footerView.setVisibility(View.GONE);
        }

        try {
            Calendar cal = mainControl.setTimeZone(Long.parseLong(news.getArticleDate()), news.getTimeOffset());

            holder.newsTime.setText(context.getString(R.string.updated) + " " + print.format(cal.getTime()));
        } catch (NumberFormatException e) {
        }
        holder.newsTitle.setText(news.getNewsTitle());
        holder.newsTitle.setEllipsize(TextUtils.TruncateAt.END);
        holder.newsTitle.setMaxLines(2);
        if (news.getIsRTL().equalsIgnoreCase("false"))
            holder.newsTitle.setGravity(Gravity.LEFT);
        rowView.setTag(holder);

        Picasso.with(context)
                .load(news.getSourceLogoUrl())
                .placeholder(R.anim.progress_animation)
                .error(R.drawable.item_icon)
                .into(holder.sourceImg);
//        try {
//            Picasso.with(context)
//                    .load(news.getLogo_url())
//                    .error(R.drawable.default_img)
//                    .into(holder.newsLogo);
//        } catch (IllegalArgumentException e) {
//            holder.newsLogo.setImageResource(R.drawable.default_img);
//        }

        try {
            if (news.getIsBlocked() > 0) {
                holder.gifLoading.setVisibility(View.GONE);
                holder.reload.setVisibility(View.VISIBLE);
                holder.newsLogo.setImageResource(R.drawable.default_img);
            } else {

                Picasso.with(context)
                        .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                        .error(R.drawable.default_img)
                        .into(holder.newsLogo, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.gifLoading.setVisibility(View.GONE);
                                holder.reload.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                holder.gifLoading.setVisibility(View.GONE);
                                holder.reload.setVisibility(View.GONE);
                            }
                        });

                holder.gifLoading.setVisibility(View.GONE);
                holder.reload.setVisibility(View.GONE);
                //holder.isVisible = true;
            }
        } catch (IllegalArgumentException ee) {
            holder.newsLogo.setImageResource(R.drawable.default_img);
            holder.gifLoading.setVisibility(View.GONE);
        }


        rowView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {

            }
        });

        holder.newsLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToDetails(newsList.get(position), position);
            }
        });
        holder.newsTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToDetails(newsList.get(position), position);
            }
        });
        holder.likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mainControl.checkInternetConnection()) {
                    Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }

                holder.likeBtn.setEnabled(false);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.likeImg.setVisibility(View.GONE);
                if (news.getLikeID() == 0)
                    likeEvent(holder, URLs.getUserID(), position, news);
                else
                    UNLikeEvent(holder, position, news);
            }
        });
        holder.reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.gifLoading.setVisibility(View.VISIBLE);
                try {
                    Picasso.with(context)
                            .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                            .error(R.drawable.default_img)
                            .into(holder.newsLogo, new Callback() {
                                @Override
                                public void onSuccess() {
                                    holder.gifLoading.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    holder.gifLoading.setVisibility(View.GONE);
                                }
                            });
                    //holder.isVisible = true;

                } catch (IllegalArgumentException e) {
                    holder.newsLogo.setImageResource(R.drawable.default_img);
                    holder.gifLoading.setVisibility(View.GONE);
                }
                holder.reload.setVisibility(View.GONE);
                news.setIsBlocked(-1);


            }
        });
        holder.commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = (Integer) v.getTag();
                //commentsState.add(pos, 1);
                pop = AnimationUtils.loadAnimation(context, R.anim.pop);
                holder.commentImg.startAnimation(pop);
                showDialog(holder, news, position);
            }
        });

        holder.commentsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(holder, news, position);
            }
        });

        holder.shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = (Integer) v.getTag();
                //sharesState.add(pos, 1);
                pop = AnimationUtils.loadAnimation(context, R.anim.pop);
                holder.shareImg.startAnimation(pop);

                shareEvent(holder, position, news);
            }
        });

        holder.deselectFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
////                int pos = (Integer) v.getTag();
//                //sharesState.add(pos, 1);
                final MainControl ma = new MainControl(context);
                if (!ma.checkInternetConnection()) {
                    Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
                    return;
                    //noSourcesSelectedLayout.setVisibility(View.GONE);
                }
                if (newsList.get(position).getFavouriteID() > 0) {
                    callJson(URLs.DELETE_FAVOURITE_URL, URLs.DELETE_FAVOURITE_REQUEST_TYPE, URLs.getUserID(), newsList.get(position)
                            .getFavouriteID(), context, URLs.TAG_DELETE_FAVOURITE_FAVOURITE_ARTICLE_ID, holder, position);
                } else
                    callJson(URLs.ADD_FAVOURITE_URL, URLs.ADD_FAVOURITE_REQUEST_TYPE, URLs.getUserID(), newsList.get(position).getID(),
                            context, URLs.TAG_ADD_FAVOURITE_ARTICLE_ID, holder, position);
            }
        });

        rowView.setTag(R.id.like_loading, holder.progressBar);
        rowView.setTag(R.id.favourite_loading, holder.loading);
        rowView.setTag(R.id.like_btn, holder.likeBtn);
        rowView.setTag(R.id.comment_btn, holder.commentBtn);
        rowView.setTag(R.id.share_btn, holder.shareBtn);
        rowView.setTag(R.id.source_title, holder.sourceTitle);
        rowView.setTag(R.id.news_title, holder.newsTime);
        rowView.setTag(R.id.news_time, holder.newsTitle);
        rowView.setTag(R.id.sharing_number, holder.sharingNumbers);
        rowView.setTag(R.id.comments_number, holder.commentsNumber);
        rowView.setTag(R.id.likes_number, holder.likesNumber);
        rowView.setTag(R.id.like_txt, holder.likeTitle);
        rowView.setTag(R.id.comment_txt, holder.commentTitle);
        rowView.setTag(R.id.share_txt, holder.shareTitle);
        rowView.setTag(R.id.source_image, holder.sourceImg);
        rowView.setTag(R.id.like_img, holder.likeImg);
        rowView.setTag(R.id.comment_img, holder.commentImg);
        rowView.setTag(R.id.share_img, holder.shareImg);
        rowView.setTag(R.id.news_logo, holder.newsLogo);
        rowView.setTag(R.id.deselect_fav, holder.deselectFav);
        rowView.setTag(R.id.header_view, holder.headerView);
        rowView.setTag(R.id.footer_view, holder.footerView);
        rowView.setTag(R.id.gif_loading, holder.gifLoading);
        rowView.setTag(R.id.reload, holder.reload);

        return rowView;
    }

    private void shareEvent(final Holder holder, final int position, final News newsObj) {
        MainControl mainControl = new MainControl(context);
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, newsObj.getNewsTitle());
        i.putExtra(Intent.EXTRA_TEXT, "http://nabaapp.com/" + newsObj.getID() + "" + " \n " + "حمل تطبيق نبأ الاخباري مجاناً " +
                "واستمتع " +
                "بمتابعة العالم لحظة بلحظة" +
                "\n" + context.getString(R
                .string.app_domain));
        context.startActivity(Intent.createChooser(i, context.getString(R.string.share)));

        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_SHARE_ARTICLE_ID, newsObj.getID() + "");
        dataObj.put(URLs.TAG_SHARE_ARTICLE_URL, newsObj.getNewsUrl() + "");

        jsonParser = new JsonParser(context, URLs.SHARE_URL, URLs.SHARE_REQUEST_TYPE, dataObj);
        jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSharesUpdatedListener() {
            @Override
            public void onFinished(int sharesCount, String shortLink) {
                if (sharesCount == -1) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_LONG).show();
                    return;
                }
                newsObj.setSharesNumber(sharesCount);
                newsList.set(position, newsObj);
                if (holder.actionsView.getVisibility() == View.GONE) {
                    holder.actionsView.setVisibility(View.VISIBLE);
                }

                if (holder.sharesView.getVisibility() == View.GONE || holder.sharesView.getVisibility() == View.INVISIBLE)
                    holder.sharesView.setVisibility(View.VISIBLE);

                holder.sharingNumbers.setText(newsObj.getSharesNumber() + "");
//                Intent i = new Intent(Intent.ACTION_SEND);
//                i.setType("text/plain");
//                i.putExtra(Intent.EXTRA_SUBJECT, newsObj.getNewsTitle());
//                i.putExtra(Intent.EXTRA_TEXT, Uri.encode(newsObj.getNewsUrl(), ALLOWED_URI_CHARS));
//                context.startActivity(Intent.createChooser(i, newsObj.getNewsTitle()));


            }
        });
    }

    void showDialog(final Holder holder, News newsObj, int index) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }
        FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        AddCommentFragment commentFrag = new AddCommentFragment(newsObj, index);
        commentFrag.show(ft, "dialog");
        commentFrag.onFinishUpdates(new AddCommentFragment.OnCommentsUpdate() {
            @Override
            public void onFinished(News newsObj, int index) {
                newsList.set(index, newsObj);
                if (holder.commentsView.getVisibility() == View.GONE) {
                    holder.commentsView.setVisibility(View.VISIBLE);
                }
                if (holder.actionsView.getVisibility() == View.GONE)
                    holder.actionsView.setVisibility(View.VISIBLE);

                holder.commentsNumber.setText(newsObj.getCommentsNumber() + "");
            }
        });
    }

    public void addData(final ListView listView, List<News> newItems) {

        //some fictitious objectList where we're populating data
        List<News> thingsToBeAdd = new ArrayList<News>();
        /*List<Integer> tempLikesState = new ArrayList<>();
        List<Integer> tempCommentsState = new ArrayList<>();
        List<Integer> tempSharesState = new ArrayList<>();*/

        for (Iterator<News> it = newItems.iterator(); it.hasNext(); ) {
            News element = it.next();
            // mElements.add(new Element("crack",getResources(), (int)touchX,(int)touchY));
            thingsToBeAdd.add(element);
        }

        /*for (int i = 0; i < newsList.size(); i++) {
            tempLikesState.add(i, 0);
            tempCommentsState.add(i, 0);
            tempSharesState.add(i, 0);
        }*/
        final int pos = listView.getFirstVisiblePosition();
        newsList.addAll(0, thingsToBeAdd);
        /*likesState.addAll(0, tempLikesState);
        commentsState.addAll(0, tempCommentsState);
        sharesState.addAll(0, tempSharesState);*/

        //URLs.setFirstNewsID(newsList.get(0).getID());
        notifyDataSetChanged();

        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.smoothScrollToPosition(pos);
            }
        });
    }

    public void appendData(List<News> newItems) {
        List<News> thingsToBeAdd = new ArrayList<News>();

        try {
            for (Iterator<News> it = newItems.iterator(); it.hasNext(); ) {
                News element = it.next();
                // mElements.add(new Element("crack",getResources(), (int)touchX,(int)touchY));
                thingsToBeAdd.add(element);
            }
            if (!newsList.containsAll(thingsToBeAdd))
                newsList.addAll(thingsToBeAdd);
            URLs.setFavNewsID(newsList.get(newsList.size() - 1).getFavouriteID());
            notifyDataSetChanged();
        } catch (NullPointerException e) {
        }
    }


    private void callJson(String url, final int requestType, int userID, int id, final Context context, String tag, final Holder holder,
                          final int position) {
        MainControl mainControl = new MainControl(context);
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {
        }
        holder.loading.setVisibility(View.VISIBLE);
        holder.deselectFav.setVisibility(View.GONE);
        HashMap<String, String> dataObj = new HashMap<>();
        if (requestType == URLs.ADD_FAVOURITE_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_ADD_FAVOURITE_USERID, userID + "");
            dataObj.put(URLs.TAG_ADD_FAVOURITE_ARTICLE_ID, id + "");
        } else if (requestType == URLs.DELETE_FAVOURITE_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_DELETE_FAVOURITE_FAVOURITE_ARTICLE_ID, id + "");
        }

        jsonParser = new JsonParser(context, url, requestType, dataObj);
        jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {

            }

            @Override
            public void onFinished(List<Integer> likeResponse) {

            }

            @Override
            public void onFinished(News newsObj) {
            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int id) {
                if (id > 1) {
                    newsList.get(position).setFavouriteID(id);
                    holder.deselectFav.setImageResource(R.drawable.star);
                    holder.loading.setVisibility(View.GONE);
                    holder.deselectFav.setVisibility(View.VISIBLE);

                } else if (id > 0) {
                    holder.deselectFav.setImageResource(R.drawable.new_fav);
                    holder.loading.setVisibility(View.GONE);
                    holder.deselectFav.setVisibility(View.VISIBLE);
                    newsList.get(position).setFavouriteID(0);
                } else {
                    //
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                    holder.loading.setVisibility(View.GONE);
                    holder.deselectFav.setVisibility(View.VISIBLE);
                    if (requestType == URLs.ADD_FAVOURITE_REQUEST_TYPE)
                        holder.deselectFav.setImageResource(R.drawable.new_fav);
                    else
                        holder.deselectFav.setImageResource(R.drawable.star);
                }
            }

            @Override
            public void onFinished(boolean success) {

            }
        });
    }

    private void goToDetails(News mNews, int index) {
        FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        transaction.addToBackStack(null);
        NewsDetail2 nesDetails = new NewsDetail2(mNews, index);
        transaction.add(R.id.parent, nesDetails, "newsDetail");
        transaction.commit();
        nesDetails.onFinishUpdates(new NewsDetail2.OnDetailsUpdate() {
            @Override
            public void onFinished(int index, News obj) {
                newsList.set(index, obj);
                notifyDataSetChanged();

//                if (obj.getFavouriteID() <= 0) {
//                    newsList.remove(index);
//                    notifyDataSetChanged();
//                } else {
//                    for (int i = 0; i < newsList.size(); i++) {
//                        if (newsList.get(i).getID() == obj.getID()) {
//                            newsList.add(index+1, obj);
//                            notifyDataSetChanged();
//                        } else {
//                            return;
//                        }
//                    }
//                }
            }

        });

    }

    private void likeEvent(final Holder holder, int userID, final int position, final News newsObj) {
       /* MainControl mainControl = new MainControl(context);
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }*/

        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_USERID, userID + "");
        dataObj.put(URLs.TAG_REQUEST_ARTICLE_ID, newsObj.getID() + "");
        jsonParser = new JsonParser(context, URLs.ADD_LIKE_URL, URLs.ADD_LIKE_REQUEST_TYPE, dataObj);
        jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {
            }

            @Override
            public void onFinished(List<Integer> likeResponse) {
                holder.likeBtn.setEnabled(true);
                holder.progressBar.setVisibility(View.GONE);
                holder.likeImg.setVisibility(View.VISIBLE);
                try {
                    if (likeResponse.get(0) > 0) {
                        newsObj.setLikeID(likeResponse.get(0));
                        newsObj.setLikesNumber(likeResponse.get(1));
                        holder.likesNumber.setText(newsObj.getLikesNumber() + " ");
                        newsObj.setLikesNumber(newsObj.getLikesNumber());
                        newsList.set(position, newsObj);
                        setViewsVisible(holder, newsObj);
                    }
                } catch (IndexOutOfBoundsException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                } catch (NullPointerException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFinished(News newsObj) {

            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int userID) {

            }

            @Override
            public void onFinished(boolean success) {

            }
        });
    }

    private void UNLikeEvent(final Holder holder, final int position, final News newsObj) {
        /*MainControl mainControl = new MainControl(context);
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }*/

        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_ADD_LIKE_LIKE_ID, newsObj.getLikeID() + "");

        jsonParser = new JsonParser(context, URLs.DELETE_LIKE_URL, URLs.DELETE_LIKE_REQUEST_TYPE, dataObj);
        jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {
            }

            @Override
            public void onFinished(List<Integer> likeResponse) {
                holder.likeBtn.setEnabled(true);
                holder.progressBar.setVisibility(View.GONE);
                holder.likeImg.setVisibility(View.VISIBLE);
                try {
                    if (likeResponse.get(0) > -1) {
                        newsObj.setLikesNumber(likeResponse.get(0));
                        holder.likesNumber.setText(newsObj.getLikesNumber() + " ");
                        newsObj.setLikesNumber(newsObj.getLikesNumber());
                        newsObj.setLikeID(0);
                        newsList.set(position, newsObj);
                        setViewGone(holder, newsObj);
                        setViewsVisible(holder, newsObj);
                    }
                } catch (IndexOutOfBoundsException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                } catch (NullPointerException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFinished(News newsObj) {

            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int userID) {

            }

            @Override
            public void onFinished(boolean success) {

            }
        });
    }

    private void setViewGone(Holder holder, News newsObj) {
        if (newsObj.getLikesNumber() == 0)
            holder.likesView.setVisibility(View.GONE);
        if (newsObj.getCommentsNumber() == 0)
            holder.commentsView.setVisibility(View.GONE);
        if (newsObj.getSharesNumber() == 0)
            holder.sharesView.setVisibility(View.INVISIBLE);

        if (newsObj.getLikesNumber() == 0 && newsObj.getCommentsNumber() == 0 && newsObj.getSharesNumber() == 0)
            holder.actionsView.setVisibility(View.GONE);
    }

    private void setViewsVisible(Holder holder, News newsObj) {
        if (newsObj.getLikesNumber() > 0 || newsObj.getCommentsNumber() > 0 || newsObj.getSharesNumber() > 0)
            holder.actionsView.setVisibility(View.VISIBLE);

        if (newsObj.getLikeID() > 0) {
            holder.likeImg.setImageResource(R.drawable.like);
            holder.likeTitle.setTextColor(Color.parseColor("#CC0000"));

        } else {
            holder.likeImg.setImageResource(R.drawable.like_unactive);
            holder.likeTitle.setTextColor(Color.parseColor("#9EA1A8"));
        }

        if (newsObj.getLikesNumber() > 0)
            holder.likesView.setVisibility(View.VISIBLE);
        if (newsObj.getCommentsNumber() > 0)
            holder.commentsView.setVisibility(View.VISIBLE);
        if (newsObj.getSharesNumber() > 0)
            holder.sharesView.setVisibility(View.VISIBLE);

        if (newsObj.getSharesNumber() < 1000)
            holder.sharingNumbers.setText(newsObj.getSharesNumber() + " ");
        else
            holder.sharingNumbers.setText(newsObj.getSharesNumber() / 1000 + "K ");

        if (newsObj.getCommentsNumber() < 1000)
            holder.commentsNumber.setText(newsObj.getCommentsNumber() + " ");
        else
            holder.commentsNumber.setText(newsObj.getCommentsNumber() / 1000 + "K ");

        if (newsObj.getLikesNumber() < 1000)
            holder.likesNumber.setText(newsObj.getLikesNumber() + " ");
        else
            holder.likesNumber.setText(newsObj.getLikesNumber() / 1000 + "K ");
    }


    private void setTextSizes(Holder holder) {
        //holder.sourceTitle.setTextSize(MainControl.pixelsToSp(context, 35.0f));
        holder.sourceTitle.setTextSize(6.0f * context.getResources().getDisplayMetrics().density);
        holder.newsTime.setTextSize(4.0f * context.getResources().getDisplayMetrics().density);
        holder.newsTitle.setTextSize(5.0f * context.getResources().getDisplayMetrics().density);
        holder.sharingNumbers.setTextSize(4.0f * context.getResources().getDisplayMetrics().density);
        holder.commentsNumber.setTextSize(4.0f * context.getResources().getDisplayMetrics().density);
        holder.likesNumber.setTextSize(4.0f * context.getResources().getDisplayMetrics().density);
        holder.likeTitle.setTextSize(4.0f * context.getResources().getDisplayMetrics().density);
        holder.commentTitle.setTextSize(4.0f * context.getResources().getDisplayMetrics().density);
        holder.shareTitle.setTextSize(4.0f * context.getResources().getDisplayMetrics().density);
    }


    public class Holder {

        public GifMovieView gifLoading;
        public ImageView reload;
        ImageView deselectFav;
        ExpandableView actionsView;
        LinearLayout likesView;
        LinearLayout commentsView;
        LinearLayout sharesView;
        LinearLayout likeBtn;
        LinearLayout commentBtn;
        LinearLayout shareBtn;
        TextView sourceTitle;
        TextView newsTime;
        TextView newsTitle;
        TextView sharingNumbers;
        TextView commentsNumber;
        TextView likesNumber;
        TextView likeTitle;
        TextView commentTitle;
        TextView shareTitle;
        ImageView sourceImg;
        ImageView likeImg;
        ImageView commentImg;
        ImageView shareImg;
        ImageView newsLogo;
        View headerView;
        View footerView;
        ProgressBar loading;
        ProgressBar progressBar;
    }
}