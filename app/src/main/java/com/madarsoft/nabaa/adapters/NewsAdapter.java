package com.madarsoft.nabaa.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appodeal.ads.MrecView;
import com.appodeal.ads.NativeAd;
import com.basv.gifmoviewview.widget.GifMovieView;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.ExpandableView;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.controls.AdsControlNabaa;
import com.madarsoft.nabaa.controls.GoogleAdMob;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.controls.SpeedScrollListener;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.History;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.fragments.AddCommentFragment;
import com.madarsoft.nabaa.fragments.MadarFragment;
import com.madarsoft.nabaa.fragments.NewsDetail2;
import com.madarsoft.nabaa.fragments.NewsFragment;
import com.madarsoft.nabaa.fragments.NewsHolderFragment;
import com.madarsoft.nabaa.fragments.SourceNews;
import com.madarsoft.nabaa.interfaces.AdViewLoadListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

//import com.facebook.ads.Ad;
//import com.facebook.ads.AdError;
//import com.facebook.ads.AdListener;
//import com.facebook.ads.AdSettings;
//import com.facebook.ads.MediaView;
//import com.facebook.ads.NativeAd;

public class NewsAdapter extends ArrayAdapter {

    public static final int REQUEST_SHARE_RESULT = 1;
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    public static final int MREC_AD_POSITION=0;
    private final boolean isSourcesNews;
    private final boolean isHistory;
    public List<News> newsList = new ArrayList<>();
    //public ArrayList<RectangleBannerAd> ads = new ArrayList<>();
    public List<News> setviceNewsList = new ArrayList<>();
    //private final Typeface font;
    Context context;
    DataBaseAdapter dp;
    MainControl mainControl;
    Locale locale;
    SimpleDateFormat print2;
    boolean ignoreDisabled = false;
    /*  Holder holder;
      AnalyticsApplication application;*/
    HashSet<Integer> intArray;
    AdsControlNabaa adsControl;
    private ArrayList<History> articleIds;
    private LayoutInflater inflater;

    //private JsonParser jsonParser;
/*    private Typeface headerFont;
    private String[] monthsName = {"يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر",
            "ديسمبر"};*/
    private Animation pop;
    /*    private int myPos;
        private int lastPosition = -1;
        private int pos;
        private LinearLayout currentAdView;*/
    private int blocked;
    // private com.google.android.gms.analytics.Tracker mTracker;
    private long mLastClickTime = 0;
    //private RelativeLayout adsWithUs;
    private OnListIsEmpty onListIsEmpty;
    private History historyObj;
    private boolean fromHistory;
    HashMap<Integer,View> adMobMap =new HashMap<>();
    HashMap<Integer,View> adNativeMap=new HashMap<>();
    MadarFragment sourceNewsFrg;
    private boolean isBannerCalled=false;

    public NewsAdapter(MainActivity mainActivity, Context context, List<News> newsList, SpeedScrollListener scrollListener, boolean
            isSourceNews, boolean isHistory, MadarFragment sourceNewsFrg) {
        super(context, R.layout.news_feed_item_card, newsList);
//        super(context, scrollListener, newsList);
        this.context = context;
        adsControl = new AdsControlNabaa(mainActivity);
        mainControl = new MainControl(context);
        this.newsList = newsList;
        historyObj = new History();
        this.isHistory = isHistory;
        intArray = new HashSet<>();
        this.isSourcesNews = isSourceNews;
        dp = new DataBaseAdapter(context);

        this.sourceNewsFrg = sourceNewsFrg;
        locale = new Locale("ar", "SA");
        print2 = new SimpleDateFormat("hh:mm a dd-MMMM-yyyy ", locale);
        // AnalyticsApplication application = (AnalyticsApplication) ((FragmentActivity) context).getApplication();
        // mTracker = application.getDefaultTracker();

    }


    public OnListIsEmpty getOnListIsEmpty() {
        return onListIsEmpty;
    }

    public void setOnListIsEmpty(OnListIsEmpty onListIsEmpty) {
        this.onListIsEmpty = onListIsEmpty;
    }

    public void setAllItemsEnabled(boolean ignoreDisabled) {
        this.ignoreDisabled = ignoreDisabled;
    }

    void showDialog(final Holder holder, News newsObj, int index) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }

        FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        AddCommentFragment commentFrag = new AddCommentFragment(newsObj, index);
        commentFrag.show(ft, "dialog");
        commentFrag.onFinishUpdates(new AddCommentFragment.OnCommentsUpdate() {
            @Override
            public void onFinished(News newsObj, int index) {
                newsList.set(index, newsObj);

                if (holder.commentsView.getVisibility() == View.GONE) {
                    holder.commentsView.setVisibility(View.VISIBLE);
                }
                if (holder.actionsView.getVisibility() == View.GONE) {
                    holder.actionsView.setVisibility(View.VISIBLE);
                    holder.seperator.setVisibility(View.VISIBLE);
                }

                if (newsObj.getCommentsNumber() == 0) {
                    holder.commentsView.setVisibility(View.GONE);
                }
                if (newsObj.getLikesNumber() == 0 && newsObj.getCommentsNumber() == 0 && newsObj.getSharesNumber() == 0)
                    holder.actionsView.setVisibility(View.GONE);

                holder.commentsNumber.setText(newsObj.getCommentsNumber() + "");
            }
        });
    }

    @Override
    public int getCount() {
        try {
            return newsList.size();
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // myPos = position;
        final Holder holder;
//        View rowView = null;
        final News news = newsList.get(position);

        if (convertView == null) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.news_feed_item_card, parent, false);
            holder = new Holder();
            holder.actionsView = (ExpandableView) convertView.findViewById(R.id.actions_expanded_view);
            //holder.actionsView.setTag(position);
            holder.likesView = (LinearLayout) convertView.findViewById(R.id.likes_view);
            //holder.likesView.setTag(position);
            holder.commentsView = (LinearLayout) convertView.findViewById(R.id.comments_view);
            //holder.commentsView.setTag(position);
            holder.sharesView = (LinearLayout) convertView.findViewById(R.id.shares_view);
            //holder.sharesView.setTag(position);
            holder.ticked = (RelativeLayout) convertView.findViewById(R.id.news_feed_ticked);
            holder.likeBtn = (LinearLayout) convertView.findViewById(R.id.like_btn);
            //holder.likeBtn.setTag(position);
            holder.commentBtn = (LinearLayout) convertView.findViewById(R.id.comment_btn);
            //holder.commentBtn.setTag(position);
            holder.shareBtn = (LinearLayout) convertView.findViewById(R.id.share_btn);
            //holder.shareBtn.setTag(position);
            holder.adWithUs = (RelativeLayout) convertView.findViewById(R.id.adWithUs);
            holder.sourceTitle = (TextView) convertView.findViewById(R.id.source_title);
            holder.newsTime = (TextView) convertView.findViewById(R.id.news_time);
            holder.newsTitle = (TextView) convertView.findViewById(R.id.news_title);
            holder.sharingNumbers = (TextView) convertView.findViewById(R.id.sharing_number);
            holder.commentsNumber = (TextView) convertView.findViewById(R.id.comments_number);
            holder.likesNumber = (TextView) convertView.findViewById(R.id.likes_number);
            holder.likeTitle = (TextView) convertView.findViewById(R.id.like_txt);
            holder.commentTitle = (TextView) convertView.findViewById(R.id.comment_txt);
            holder.shareTitle = (TextView) convertView.findViewById(R.id.share_txt);
            holder.seperator = (View) convertView.findViewById(R.id.seperator);
            holder.sourceImg = (ImageView) convertView.findViewById(R.id.source_image);
            holder.likeImg = (ImageView) convertView.findViewById(R.id.like_img);
            holder.commentImg = (ImageView) convertView.findViewById(R.id.comment_img);
            holder.shareImg = (ImageView) convertView.findViewById(R.id.share_img);
            holder.newsLogo = (ImageView) convertView.findViewById(R.id.news_logo);
            holder.reload = (ImageView) convertView.findViewById(R.id.reload);
            holder.newsFeedSourceHeader = (LinearLayout) convertView.findViewById(R.id.news_feed_source_header);
            holder.sourceNewsDateHolder = (LinearLayout) convertView.findViewById(R.id.source_news_date_holder);
            holder.sourceNewsTime = (FontTextView) convertView.findViewById(R.id.source_news_time);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.like_loading);
            holder.roundedBackground = (LinearLayout) convertView.findViewById(R.id.rounded_background);
            holder.urgentLabel = (FontTextView) convertView.findViewById(R.id.urgent_label);
            holder.spinnerAd = (GifMovieView) convertView.findViewById(R.id.spinner_ad);
            holder.mRecAd = (MrecView) convertView.findViewById(R.id.mrec_ad);
            holder.adContainer = (LinearLayout) convertView.findViewById(R.id.ads_container_basma);
            //holder.nativeAdTitle = (TextView) convertView.findViewById(R.id.native_ad_title);
            //holder.nativeAdBody = (TextView) convertView.findViewById(R.id.native_ad_body);
            //holder.nativeAdMedia = (MediaView) convertView.findViewById(R.id.native_ad_media);
            //holder.nativeAdSocialContext = (TextView) convertView.findViewById(R.id.native_ad_social_context);
            //holder.nativeAdCallToAction = (Button) convertView.findViewById(R.id.native_ad_call_to_action);
            holder.gifHolder = (RelativeLayout) convertView.findViewById(R.id.gif_holder);
            //holder.holder_hh = (RelativeLayout) convertView.findViewById(R.id.ll_header);
//            holder.adView = (LinearLayout) convertView.findViewById(R.id.holder_me);
            //holder.adChoices = (ImageView) convertView.findViewById(R.id.ad_choices_container);
            holder.greyView = (View) convertView.findViewById(R.id.grey_view);
            //holder.adCover = (ImageView) convertView.findViewById(R.id.ad_cover);
            holder.loading = (GifMovieView) convertView.findViewById(R.id.loading_spinner);
            holder.deleteIcon = (ImageView) convertView.findViewById(R.id.delete_trash);
            holder.loading.setMovieResource(R.drawable.gif_reload);
            holder.spinnerAd.setMovieResource(R.drawable.gif_reload);
            holder.cardHolder = (LinearLayout) convertView.findViewById(R.id.news_feed_item_holder);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        if (mainControl.checkInternetConnection()) {


            if (adsControl.isAdPosition(position, false)) {
                getAdmobAd(position, holder, adMobMap,false);
            } else if (adsControl.isAdPosition(position, true))
            {
                if (!NewsHolderFragment.isUseAdMobAds) {


                    NativeAd nativead = adsControl.getNativeAdForCertainPosition(position, NewsFragment.AllNativeAds);


                    if (nativead != null) {

                        try {
//                    holder.holder_hh.setVisibility(View.VISIBLE);
                            holder.adContainer.setVisibility(View.VISIBLE);

                            //  if (holder.adContainer.getChildCount() == 0) {
                            holder.gifHolder.setVisibility(View.VISIBLE);
                            holder.spinnerAd.setVisibility(View.VISIBLE);
                            holder.mRecAd.setVisibility(View.GONE);
                            //Appodeal.hide((Activity) context, Appodeal.MREC);


                            holder.adContainer.removeAllViews();
                            holder.adContainer.setBackgroundResource(R.color.white);

                            showNativeAd(position, holder.adContainer, holder.gifHolder, nativead);

                        } catch (Exception e) {
                            hideAdView(holder);
                            e.printStackTrace();
                        }
                        // }

                    } else {


                        hideAdView(holder);
                    }
                } else {

                    //if (adsControl.isAdPosition(position, true)) {
                        getAdmobAd(position, holder,adNativeMap, true);
                   // getAdmobAd(position, holder, adMobMap,false);

                    //  }


                }
        }
           else {
                hideAdView(holder);
            }
       /*     if (adMobMap.containsKey("" + position)) {
                try {
                    holder.adContainer.removeAllViews();
                    holder.gifHolder.setVisibility(View.GONE);
                    //solve this for both cases native and admob
                    holder.adContainer.addView(adMobMap.get("" + position));
                    holder.adContainer.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    throw e;
                }

            } else

            {
                if (!isUseAdMobAds) {


                    NativeAd nativead = adsControl.getNativeAdForCertainPosition(position, NewsFragment.AllNativeAds);


                    if (nativead != null) {

                        try {
//                    holder.holder_hh.setVisibility(View.VISIBLE);
                            holder.adContainer.setVisibility(View.VISIBLE);

                            //  if (holder.adContainer.getChildCount() == 0) {
                            holder.gifHolder.setVisibility(View.VISIBLE);
                            holder.spinnerAd.setVisibility(View.VISIBLE);
                            holder.mRecAd.setVisibility(View.GONE);
                            Appodeal.hide((Activity) context, Appodeal.MREC);


                            holder.adContainer.removeAllViews();
                            holder.adContainer.setBackgroundResource(R.color.white);

                            showNativeAd(position, holder.adContainer, holder.gifHolder, nativead);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // }

                    } *//*else {
                    if (holder.adContainer.getChildCount() > 0) {
                        holder.adContainer.removeAllViews();
                    }

                    holder.gifHolder.setVisibility(View.GONE);
                    holder.spinnerAd.setVisibility(View.GONE);
                    holder.adWithUs.setVisibility(View.GONE);
                    holder.adContainer.setVisibility(View.GONE);
                    holder.mRecAd.setVisibility(View.GONE);
                    holder.greyView.setVisibility(View.GONE);
                }
            }*//* }else if (adsControl.isAdPosition(position)) {
                        holder.adContainer.setVisibility(View.VISIBLE);

                        //  if (holder.adContainer.getChildCount() == 0) {
                        holder.gifHolder.setVisibility(View.VISIBLE);
                        holder.spinnerAd.setVisibility(View.VISIBLE);
                        holder.mRecAd.setVisibility(View.GONE);
                        //    Appodeal.hide((Activity) context, Appodeal.MREC);


                        //  if (holder.adContainer.getChildCount() == 0) {
                        try {
//                    holder.holder_hh.setVisibility(View.VISIBLE);
                            // holder.adContainer.removeAllViews();
                            holder.adContainer.setBackground(null);
                            showAdmobAd(position, holder.adContainer, holder.gifHolder);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                *//*} else {
                    try {
                        ((AdView) holder.adContainer.getChildAt(0)).loadAd(new AdRequest.Builder().build());

                    } catch (IndexOutOfBoundsException e) {
                    } catch (ClassCastException e) {

                    }
                }*//*
                        // }


                    } else {
              *//*  if (holder.adContainer.getChildCount() > 0) {
                    holder.adContainer.removeAllViews();
                }*//*

                        holder.gifHolder.setVisibility(View.GONE);
                        holder.spinnerAd.setVisibility(View.GONE);
                        holder.adWithUs.setVisibility(View.GONE);
                        holder.adContainer.setVisibility(View.GONE);
                        holder.mRecAd.setVisibility(View.GONE);
                        holder.greyView.setVisibility(View.GONE);
                    }
                }*/
            }

            holder.sourceTitle.setText(news.getSourceTitle());
            if (isSourcesNews) {
                holder.newsFeedSourceHeader.setVisibility(View.GONE);
                if (android.os.Build.VERSION.SDK_INT >= 21) {
                    holder.roundedBackground.setBackground(context.getResources().getDrawable(R.drawable.rounded_border, context.getTheme()));

                } else {
                    holder.roundedBackground.setBackground(context.getResources().getDrawable(R.drawable.rounded_border));
                }
            } else {
                holder.newsFeedSourceHeader.setVisibility(View.VISIBLE);
            }
            holder.sourceTitle.setGravity(Gravity.RIGHT);
            //Date date = new Date(news.getTime_stamp());
            setViewGone(holder, news);
            setViewsVisible(holder, news);

        /*if (likesState.get(position) == 1) {
            holder.likeImg.setImageResource(R.drawable.like);
        } else {
            holder.likeImg.setImageResource(R.drawable.like_unactive);
        }*/
            try {
                Calendar cal = mainControl.setTimeZone(Long.parseLong(news.getArticleDate()), news.getTimeOffset());

                holder.newsTime.setText(context.getString(R.string.updated) + " " + print2.format(cal.getTime()));
                holder.sourceNewsTime.setText(context.getString(R.string.updated) + " " + print2.format(cal.getTime()));

            } catch (Exception e) {
            }
            if (isSourcesNews)
                holder.sourceNewsDateHolder.setVisibility(View.VISIBLE);
            else
                holder.sourceNewsDateHolder.setVisibility(View.GONE);

            holder.newsTitle.setText(news.getNewsTitle());
            holder.newsTitle.setEllipsize(TextUtils.TruncateAt.END);
            holder.newsTitle.setMaxLines(2);
            if (news.getIsRTL() != null && news.getIsRTL().equalsIgnoreCase("false"))
                holder.newsTitle.setGravity(Gravity.LEFT);
            else
                holder.newsTitle.setGravity(Gravity.RIGHT);
            //setTextSizes(holder);
//        convertView.setTag(holder);

            final Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.slide_out);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    newsList.remove(position);
                    try {
                        if (newsList.size() == 0) {
                            onListIsEmpty.isEmpty();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    notifyDataSetChanged();
                }
            });

            try {
                holder.deleteIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.cardHolder.startAnimation(animation);
                        dp.deleteArticleByArticleId(news.getID());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            Picasso.with(context)
                    .load(news.getSourceLogoUrl())
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(holder.sourceImg, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            //Try again online if cache failed
                            Picasso.with(context)
                                    .load(news.getSourceLogoUrl())
                                    .error(R.drawable.item_icon)
                                    .into(holder.sourceImg, new Callback() {
                                        @Override
                                        public void onSuccess() {

                                        }

                                        @Override
                                        public void onError() {
                                            Log.v("Picasso", "Could not fetch image");
                                        }
                                    });
                        }
                    });

        /*Picasso.with(context)
                .load(news.getSourceLogoUrl())
                .placeholder(R.anim.progress_animation)
                .error(R.drawable.item_icon)
                .into(holder.sourceImg);*/

            if (news.getID() == 24001) {
                Log.e("ASDA", "ASDSAd");
            }

//        if (holder.isVisible) {
//            Picasso.with(context)
//                    .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
//                    .error(R.drawable.no_img)
//                    .into(holder.newsLogo);
//            holder.reload.setVisibility(View.GONE);
//        }
            try {
                if (news.getIsBlocked() > 0) {
                    holder.reload.setVisibility(View.VISIBLE);
                    holder.newsLogo.setImageResource(R.drawable.default_img);
                } else {

                    Picasso.with(context)
                            .load(Uri.decode(news.getLogo_url()))
                            .networkPolicy(NetworkPolicy.OFFLINE)
                            .into(holder.newsLogo, new Callback() {
                                @Override
                                public void onSuccess() {
                                    holder.loading.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    //Try again online if cache failed
                                    Picasso.with(context)
                                            .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS)).error(R.drawable.default_img)
                                            .into(holder.newsLogo, new Callback() {
                                                @Override
                                                public void onSuccess() {
//                                                .setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.FILL_PARENT,height));
                                                    holder.loading.setVisibility(View.GONE);
                                                }

                                                @Override
                                                public void onError() {
                                                    holder.loading.setVisibility(View.GONE);
                                                    Log.v("Picasso", "Could not fetch image");
                                                }
                                            });
                                }

                                ;
                            });
                    holder.reload.setVisibility(View.GONE);
                    //holder.isVisible = true;
                }
       /*Picasso.with(context)
                    .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                    .error(R.drawable.no_img)
                    .into(holder.newsLogo);*/

            } catch (IllegalArgumentException e) {
                holder.newsLogo.setImageResource(R.drawable.default_img);
            }

            holder.reload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        holder.reload.setVisibility(View.GONE);
                        holder.loading.bringToFront();
                        holder.loading.setVisibility(View.VISIBLE);
                        Picasso.with(context)
                                .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                                .error(R.drawable.default_img)
                                .networkPolicy(NetworkPolicy.OFFLINE)
                                .into(holder.newsLogo, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        holder.loading.setVisibility(View.GONE);
                                        holder.reload.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {
                                        //Try again online if cache failed
                                        Picasso.with(context)
                                                .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                                                .error(R.drawable.default_img).into(holder.newsLogo, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                                holder.reload.setVisibility(View.GONE);
                                                holder.loading.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {
                                                holder.reload.setVisibility(View.GONE);
                                                holder.loading.setVisibility(View.GONE);
                                            }
                                        });

                                    }
                                });

                    } catch (Exception e) {
                        holder.reload.setVisibility(View.GONE);
                        holder.newsLogo.setImageResource(R.drawable.default_img);
                        holder.loading.setVisibility(View.GONE);
                        holder.newsLogo.bringToFront();
                    }
                    news.setIsBlocked(-1);
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View view) {

                }
            });
            holder.newsLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    goToDetails(newsList.get(position), position, holder);
                }
            });
            holder.newsTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    goToDetails(newsList.get(position), position, holder);
                }
            });
            holder.likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!mainControl.checkInternetConnection()) {
                        Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    holder.likeBtn.setEnabled(false);
                    holder.progressBar.setVisibility(View.VISIBLE);
                    holder.likeImg.setVisibility(View.GONE);
                    if (news.getLikeID() == 0)
                        likeEvent(holder, URLs.getUserID(), position, news);
                    else
                        UNLikeEvent(holder, position, news);

                /*pop.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        if (news.getLikeID() == 0)
                            likeEvent(holder, URLs.getUserID(), position, news);
                        else
                            UNLikeEvent(holder, position, news);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });*/

                }
            });

            holder.newsFeedSourceHeader.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
                            FragmentTransaction transaction = manager.beginTransaction();
                            transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                            transaction.addToBackStack(null);
                            //
                            transaction.replace(R.id.parent, new SourceNews(news.getSourceID(), true, false, "", false));
                            //transaction.add(R.id.parent, new SourceNews(news.getSourceID(), true, false, ""));
                            transaction.commit();
                        }
                    }
            );

            holder.commentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //int pos = (Integer) v.getTag();
                    //commentsState.add(pos, 1);
                    pop = AnimationUtils.loadAnimation(context, R.anim.pop);
                    holder.commentImg.startAnimation(pop);
                    showDialog(holder, news, position);
                }
            });

            holder.shareBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //int pos = (Integer) v.getTag();
                    //sharesState.add(pos, 1);
                    pop = AnimationUtils.loadAnimation(context, R.anim.pop);
                    holder.shareImg.startAnimation(pop);
                    shareEvent(holder, position, news);
                }
            });

            holder.commentsView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialog(holder, news, position);
                }
            });

            convertView.setTag(R.id.urgent_label, holder.urgentLabel);
            convertView.setTag(R.id.seperator, holder.seperator);
            convertView.setTag(R.id.rounded_background, holder.roundedBackground);
            convertView.setTag(R.id.like_loading, holder.progressBar);
            convertView.setTag(R.id.like_btn, holder.likeBtn);
            convertView.setTag(R.id.comment_btn, holder.commentBtn);
            convertView.setTag(R.id.share_btn, holder.shareBtn);
            convertView.setTag(R.id.source_title, holder.sourceTitle);
            convertView.setTag(R.id.news_title, holder.newsTime);
            convertView.setTag(R.id.news_time, holder.newsTitle);
            convertView.setTag(R.id.sharing_number, holder.sharingNumbers);
            convertView.setTag(R.id.comments_number, holder.commentsNumber);
            convertView.setTag(R.id.likes_number, holder.likesNumber);
            convertView.setTag(R.id.like_txt, holder.likeTitle);
            convertView.setTag(R.id.comment_txt, holder.commentTitle);
            convertView.setTag(R.id.share_txt, holder.shareTitle);
            convertView.setTag(R.id.source_image, holder.sourceImg);
            convertView.setTag(R.id.like_img, holder.likeImg);
            convertView.setTag(R.id.comment_img, holder.commentImg);
            convertView.setTag(R.id.share_img, holder.shareImg);
            convertView.setTag(R.id.news_logo, holder.newsLogo);
            convertView.setTag(R.id.news_feed_source_header, holder.newsFeedSourceHeader);
            convertView.setTag(R.id.source_news_date_holder, holder.sourceNewsDateHolder);
            convertView.setTag(R.id.source_news_time, holder.sourceNewsTime);
            convertView.setTag(R.id.news_feed_item_holder, holder.cardHolder);
            convertView.setTag(R.id.news_feed_ticked, holder.ticked);
            convertView.setTag(R.id.spinner_ad, holder.spinnerAd);
            convertView.setTag(R.id.mrec_ad, holder.mRecAd);
            convertView.setTag(R.id.ads_container_basma, holder.adContainer);
            convertView.setTag(R.id.gif_holder, holder.gifHolder);

            return convertView;
        }

    public void getAdmobAd(int position, Holder holder,final HashMap<Integer,View> adsMap ,boolean isnative) {
        View admobAd = adsControl.getAdmobAd(position, adsMap,isnative);
        if ( admobAd==null) {
            holder.adContainer.setVisibility(View.VISIBLE);

            holder.gifHolder.setVisibility(View.VISIBLE);
            holder.spinnerAd.setVisibility(View.VISIBLE);
            holder.mRecAd.setVisibility(View.GONE);
            try {
                if (android.os.Build.VERSION.SDK_INT >= 21) {
                    // holder.roundedBackground.setBackground(null, context.getTheme()));
                    holder.adContainer.setBackground(null);

                } else {
                    holder.adContainer.setBackgroundDrawable(null);
                }
                if (adsMap.size()<adsControl.getNoOfCahedAds()) {
                    adsMap.put( adsControl.getInstreamPosition(position,isnative), showAdmobAd(position, holder));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder.adContainer.setVisibility(View.VISIBLE);

            if (android.os.Build.VERSION.SDK_INT >= 21) {
               // holder.roundedBackground.setBackground(null, context.getTheme()));
                holder.adContainer.setBackground(null);

            } else {
                holder.adContainer.setBackgroundDrawable(null);
            }

            try {
                holder.adContainer.removeAllViews();
                holder.adContainer.addView(admobAd);
                admobAd.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

            }catch (Exception e)
            {
                Log.e("position of crash",""+position);
e.printStackTrace();
            }



        }
    }

    public void hideAdView(Holder holder) {
        holder.adContainer.removeAllViews();
        holder.gifHolder.setVisibility(View.GONE);
        holder.spinnerAd.setVisibility(View.GONE);
        holder.adWithUs.setVisibility(View.GONE);
        holder.adContainer.setVisibility(View.GONE);
        holder.mRecAd.setVisibility(View.GONE);
        holder.greyView.setVisibility(View.GONE);
    }


    /*
    void showMobFoxNative(final RelativeLayout nativeViewContainer, final View loading)
    {
        final LinearLayout nativeView =  (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.use_native, null);
        // Native aNative;

        Native.setDebug(true);
        loading.setVisibility(View.GONE);
        //assigning xml components to our layout
        final TextView headline = (TextView) nativeView.findViewById(R.id.headline);
        final ImageView nativeIcon = (ImageView) nativeView.findViewById(R.id.nativeIcon);
        final ImageView  nativeMainImg = (ImageView) nativeView.findViewById(R.id.nativeMainImg);
        final RelativeLayout layout = (RelativeLayout) nativeView.findViewById(R.id.nativeLayout);

       // spinnerInvh = (MySpinner) findViewById(R.id.spinnerInvh);

        //spinnerInvh.setAdapter(adapter);






        final Button loadBtn = (Button) nativeView.findViewById(R.id.loadBtn);
        //final loadBtn.setOnClickListener(context);

        Button btnAdd = (Button) nativeView.findViewById(R.id.btnAdd);
       // btnAdd.setOnClickListener(context);

        EditText etvalue = (EditText) nativeView.findViewById(R.id.etvalue);

        TextView tvParams = (TextView) nativeView.findViewById(R.id.tvParams);

        EditText etInvh = (EditText) nativeView.findViewById(R.id.etInvh);


        Native  aNative = new Native(context);

        //params.setParams(new JSONObject());

        //we must set a listener for native

        NativeListener listener = new NativeListener() {


            @Override
            public void onNativeReady(Native aNative, CustomEventNative customEventNative, com.mobfox.sdk.nativeads.NativeAd ad) {
                Toast.makeText(context, "on native ready", Toast.LENGTH_SHORT).show();

                //register custom layout click
                customEventNative.registerViewForInteraction(layout);
                ad.fireTrackers(context);

                headline.setText(ad.getTexts().get(0).getText());

                ad.loadImages(context, new com.mobfox.sdk.nativeads.NativeAd.ImagesLoadedListener() {
                    @Override
                    public void onImagesLoaded(com.mobfox.sdk.nativeads.NativeAd ad) {
                        Toast.makeText(context, "on images ready", Toast.LENGTH_SHORT).show();
                        nativeIcon.setImageBitmap(ad.getImages().get(0).getImg());
                    }
                });
    nativeViewContainer.addView(nativeView);
            }

            @Override
            public void onNativeError(Exception e) {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNativeClick(com.mobfox.sdk.nativeads.NativeAd nativeAd) {

            }



        };

        aNative.setListener(listener);
        aNative.load("80187188f458cfde788d961b6882fd53");
    }
    */
    PublisherAdView showAdmobAd(final int position ,final Holder holder){


       PublisherAdView ad = GoogleAdMob.loadContentAd(context,position,holder.adContainer, adsControl, new AdViewLoadListener() {


            @Override
            public void onAdLoaded() {
                //nativeViewContainer.addView(nativeView);
                holder.gifHolder.setVisibility(View.GONE);
                holder.spinnerAd.setVisibility(View.GONE);
                if (android.os.Build.VERSION.SDK_INT >= 21) {
                    // holder.roundedBackground.setBackground(null, context.getTheme()));
                    holder.adContainer.setBackground(null);

                } else {
                    holder.adContainer.setBackgroundDrawable(null);
                }
            }

            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdError() {
                adMobMap.remove(adsControl.getInstreamPosition(position,false));
                hideAdView(holder);
                //holder.adWithUs.setVisibility(View.VISIBLE);

            }
        });
       return ad;

    }
    void showNativeAd(int position,final LinearLayout nativeViewContainer, final View loading,NativeAd nativeAd)
    {
      /*  Appodeal.setAutoCacheNativeIcons(true);
        Appodeal.setAutoCacheNativeMedia(false);


        Appodeal.initialize((Activity) context, AdsControlNabaa.NABAA_ID, Appodeal.NATIVE);
        Appodeal.show((Activity) context, Appodeal.NATIVE);
        Appodeal.setNativeCallbacks(new NativeCallbacks() {
            @Override
            public void onNativeLoaded(List<NativeAd> nativeAds) {*/
        // Toast.makeText(context, "onNativeLoaded", Toast.LENGTH_SHORT).show();
        loading.setVisibility(View.GONE);
        LinearLayout nativeView =  (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.ad_view, null);
        //   adMobMap.put(""+position,nativeView);
        //  NativeAd nativeAd = nativeAds.get(0);

        TextView nativeAdSign = (TextView) nativeView.findViewById(R.id.sponsored_label);
        // nativeAdSign.setText("Ad");

        TextView nativeTitle = (TextView) nativeView.findViewById(R.id.native_ad_title);
        nativeTitle.setText(nativeAd.getTitle());

        TextView nativeDescription = (TextView) nativeView.findViewById(R.id.native_ad_body);
        nativeDescription.setMaxLines(2);
        nativeDescription.setEllipsize(TextUtils.TruncateAt.END);
        nativeDescription.setText(nativeAd.getDescription());

        RatingBar nativeRating = (RatingBar) nativeView.findViewById(R.id.rating_bar);
               /* if (nativeAd.getRating() == 0) {
                    nativeRating.setVisibility(View.GONE);
                } else {
                    nativeRating.setVisibility(View.VISIBLE);
                    nativeRating.setRating(nativeAd.getRating());
                    nativeRating.setIsIndicator(true);
                    nativeRating.setStepSize(0.05f);
                }*/

        Button nativeCta = (Button) nativeView.findViewById(R.id.native_ad_call_to_action);
        nativeCta.setText(nativeAd.getCallToAction());

        ((ImageView) nativeView.findViewById(R.id.native_ad_icon)).setImageBitmap(nativeAd.getIcon());
        ((ImageView) nativeView.findViewById(R.id.ad_cover)).setImageBitmap(nativeAd.getImage());

        View providerView = nativeAd.getProviderView(context);
        if (providerView != null) {
            FrameLayout providerViewContainer = (FrameLayout) nativeView.findViewById(R.id.native_provider_view);
            providerViewContainer.addView(providerView);
        }

        nativeAd.registerViewForInteraction(nativeView);
        nativeView.setVisibility(View.VISIBLE);
             /*   holder.gifHolder.setVisibility(View.GONE);
                holder.spinnerAd.setVisibility(View.GONE);*/
          nativeViewContainer.removeAllViews();
        nativeViewContainer.addView(nativeView);
        //adMobMap.put(""+position,nativeView);
        nativeView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
               /* TextView text=new TextView(context);text.setText("test");
                nativeViewContainer.addView(text);*/
           /* }

            @Override
            public void onNativeFailedToLoad() {
                Toast.makeText(context, "onNativeFailedToLoad", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNativeShown(NativeAd nativeAd)
            {

                     *//*   holder.gifHolder.setVisibility(View.GONE);
                holder.spinnerAd.setVisibility(View.GONE);*//*
                Toast.makeText(context, "onNativeShown", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNativeClicked(NativeAd nativeAd) {

                Toast.makeText(context, "onNativeClicked", Toast.LENGTH_SHORT).show();
            }
        });*/
    }
  /*  public void getBannerAd(final MrecView view, final View loading,final int position) {



      //  Appodeal.show((Activity) context, Appodeal.MREC);


        Appodeal.setMrecCallbacks(new MrecCallbacks() {
            @Override
            public void onMrecLoaded( boolean b) {

               // loading.setVisibility(View.GONE);
                if (position== MREC_AD_POSITION)
                {
                    view.setVisibility(View.VISIBLE);
                    Appodeal.show((Activity) context, Appodeal.MREC);
                    //Appodeal.show((Activity) context, Appodeal.MREC);
                }
                else{
                    Appodeal.hide((Activity) context, Appodeal.MREC);


                }
               // Toast.makeText(context, "onMediumRecShown", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMrecFailedToLoad() {
                Toast.makeText(context, "onMediumRecFailure", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onMrecShown() {
                if (position== MREC_AD_POSITION)
                {
                    view.setVisibility(View.VISIBLE);
                    //Appodeal.show((Activity) context, Appodeal.MREC);
                }
                else{
                    Appodeal.hide((Activity) context, Appodeal.MREC);


                }
            }

            @Override
            public void onMrecClicked() {

            }
        });


    }*/


    private void goToDetails(News mNews, int index, Holder holderM) {
        if (ignoreDisabled) {
            return;
        }
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, R.string.no_internet, Toast.LENGTH_SHORT).show();
            return;
        } else {
            holderM.ticked.setVisibility(View.VISIBLE);
            FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            transaction.addToBackStack(null);
            NewsDetail2 nesDetails = new NewsDetail2(mNews, index);
            //NewsDetail2 nesDetails = new NewsDetail2("-8505763082654550151");
            transaction.add(R.id.parent, nesDetails, "newsDetail");
            transaction.commit();
            nesDetails.onFinishUpdates(new NewsDetail2.OnDetailsUpdate() {
                @Override
                public void onFinished(int index, News obj) {
                    newsList.set(index, obj);
                    notifyDataSetChanged();
                    if (sourceNewsFrg != null) {
                        if (dp.getSourcesBySourceId(obj.getSourceID()).getSelected_or_not() > 0)
                            ((SourceNews) sourceNewsFrg).setSelction();
                        if (dp.getSourcesBySourceId(obj.getSourceID()).getUrgent_notify() > 0)
                            ((SourceNews) sourceNewsFrg).setNotification();
                    }
                }
            });
        }


    }

    private void setViewGone(Holder holder, News newsObj) {
        /*if (newsObj.getLikesNumber() == 0)
            holder.likesView.setVisibility(View.GONE);
        if (newsObj.getCommentsNumber() == 0)
            holder.commentsView.setVisibility(View.GONE);
        if (newsObj.getSharesNumber() == 0)
            holder.sharesView.setVisibility(View.INVISIBLE);

        if (newsObj.getLikesNumber() == 0 && newsObj.getCommentsNumber() == 0 && newsObj.getSharesNumber() == 0) {
            holder.actionsView.setVisibility(View.GONE);
            if (isSourcesNews) {
                holder.seperator.setVisibility(View.GONE);
            }
        }*/
        if (newsObj.getLikesNumber() == 0)
            holder.likesView.setVisibility(View.GONE);
        if (newsObj.getCommentsNumber() == 0)
            holder.commentsView.setVisibility(View.GONE);
        if (newsObj.getSharesNumber() == 0)
            holder.sharesView.setVisibility(View.INVISIBLE);

        if (newsObj.getLikesNumber() == 0 && newsObj.getCommentsNumber() == 0 && newsObj.getSharesNumber() == 0) {
            holder.actionsView.setVisibility(View.GONE);
            if (isSourcesNews) {
                holder.seperator.setVisibility(View.GONE);
            }
        }
    }

    private void setViewsVisible(Holder holder, News newsObj) {

        articleIds = dp.getArticlesId();
        for (int i = 0; i < articleIds.size(); i++) {
            if (articleIds.get(i).getArticleId() == newsObj.getID()) {
                fromHistory = true;
                historyObj.setLike_Id(newsObj.getLikeID());
                historyObj.setLikesCount(newsObj.getLikesNumber());
                historyObj.setFavouriteId(newsObj.getFavouriteID());
                historyObj.setShareCount(newsObj.getSharesNumber());
                historyObj.setCommentsCount(newsObj.getCommentsNumber());
                dp.updateArticleById(newsObj.getID(), historyObj);
                break;
            } else {
                fromHistory = false;
            }
//            break;
        }

        if (newsObj.getLikesNumber() > 0 || newsObj.getCommentsNumber() > 0 || newsObj.getSharesNumber() > 0) {
            holder.actionsView.setVisibility(View.VISIBLE);
            if (isSourcesNews)
                holder.seperator.setVisibility(View.VISIBLE);
        }
        if (isHistory)
            holder.deleteIcon.setVisibility(View.VISIBLE);
        else
            holder.deleteIcon.setVisibility(View.GONE);
        if (newsObj.isUrgent())
            holder.urgentLabel.setVisibility(View.VISIBLE);
        else
            holder.urgentLabel.setVisibility(View.GONE);


        if (newsObj.getLikeID() > 0) {
            holder.likeImg.setImageResource(R.drawable.like);
            holder.likeTitle.setTextColor(Color.parseColor("#CC0000"));
        } else {
            holder.likeImg.setImageResource(R.drawable.like_unactive);
            holder.likeTitle.setTextColor(Color.parseColor("#9EA1A8"));
        }
        if (fromHistory) {
            holder.ticked.setVisibility(View.VISIBLE);
            notifyDataSetChanged();
            if (historyObj.getLike_Id() > 0) {
                holder.likeImg.setImageResource(R.drawable.like);
                holder.likeTitle.setTextColor(Color.parseColor("#CC0000"));
            } else {
                holder.likeImg.setImageResource(R.drawable.like_unactive);
                holder.likeTitle.setTextColor(Color.parseColor("#9EA1A8"));
            }
            if (historyObj.getLikesCount() > 0)
                holder.likesView.setVisibility(View.VISIBLE);
            if (historyObj.getCommentsCount() > 0)
                holder.commentsView.setVisibility(View.VISIBLE);
            if (historyObj.getShareCount() > 0)
                holder.sharesView.setVisibility(View.VISIBLE);
            if (historyObj.getShareCount() < 1000)
                holder.sharingNumbers.setText(newsObj.getSharesNumber() + " ");
            else
                holder.sharingNumbers.setText(newsObj.getSharesNumber() / 1000 + "K ");

            if (historyObj.getCommentsCount() < 1000)
                holder.commentsNumber.setText(newsObj.getCommentsNumber() + " ");
            else
                holder.commentsNumber.setText(newsObj.getCommentsNumber() / 1000 + "K ");

            if (historyObj.getLikesCount() < 1000)
                holder.likesNumber.setText(newsObj.getLikesNumber() + " ");
            else
                holder.likesNumber.setText(newsObj.getLikesNumber() / 1000 + "K ");
        } else {
            holder.ticked.setVisibility(View.GONE);
            if (newsObj.getLikesNumber() > 0)
                holder.likesView.setVisibility(View.VISIBLE);
            if (newsObj.getCommentsNumber() > 0)
                holder.commentsView.setVisibility(View.VISIBLE);
            if (newsObj.getSharesNumber() > 0)
                holder.sharesView.setVisibility(View.VISIBLE);

            if (newsObj.getSharesNumber() < 1000)
                holder.sharingNumbers.setText(newsObj.getSharesNumber() + " ");
            else
                holder.sharingNumbers.setText(newsObj.getSharesNumber() / 1000 + "K ");

            if (newsObj.getCommentsNumber() < 1000)
                holder.commentsNumber.setText(newsObj.getCommentsNumber() + " ");
            else
                holder.commentsNumber.setText(newsObj.getCommentsNumber() / 1000 + "K ");

            if (newsObj.getLikesNumber() < 1000)
                holder.likesNumber.setText(newsObj.getLikesNumber() + " ");
            else
                holder.likesNumber.setText(newsObj.getLikesNumber() / 1000 + "K ");
        }
    }
   /* private void shareIntent(final Holder holder, int position, final News newsObj) {

        shareEvent(holder, position, newsObj);
    }*/

    private void shareEvent(final Holder holder, final int position, final News newsObj) {
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        } else {

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, newsObj.getNewsTitle());
            i.putExtra(Intent.EXTRA_TEXT, "http://nabaapp.com/" + newsObj.getID() + "" + " \n " + "حمل تطبيق نبأ الاخباري مجاناً واستمتع " +
                    "بمتابعة العالم لحظة بلحظة" +
                    "\n" + context.getString(R
                    .string.app_domain));
            context.startActivity(Intent.createChooser(i, context.getString(R.string.share)));


            HashMap<String, String> dataObj = new HashMap<>();

            dataObj.put(URLs.TAG_SHARE_ARTICLE_ID, newsObj.getID() + "");
            dataObj.put(URLs.TAG_SHARE_ARTICLE_URL, newsObj.getShareUrl() + "");

            JsonParser jsonParser = new JsonParser(context, URLs.SHARE_URL, URLs.SHARE_REQUEST_TYPE, dataObj);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnSharesUpdatedListener() {
                @Override
                public void onFinished(int sharesCount, String shortLink) {
                    if (sharesCount == -1) {
                        Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_LONG).show();
                        return;
                    }
                    newsObj.setSharesNumber(sharesCount);
                    newsList.set(position, newsObj);
                    if (holder.actionsView.getVisibility() == View.GONE) {
                        holder.seperator.setVisibility(View.VISIBLE);
                        holder.actionsView.setVisibility(View.VISIBLE);
                    }

                    if (holder.sharesView.getVisibility() == View.GONE || holder.sharesView.getVisibility() == View.INVISIBLE)
                        holder.sharesView.setVisibility(View.VISIBLE);

                    holder.sharingNumbers.setText(newsObj.getSharesNumber() + "");

                    for (int i = 0; i < articleIds.size(); i++) {
                        if (articleIds.get(i).getArticleId() == newsObj.getID()) {
                            historyObj.setLike_Id(newsObj.getLikeID());
                            historyObj.setLikesCount(newsObj.getLikesNumber());
                            historyObj.setFavouriteId(newsObj.getFavouriteID());
                            historyObj.setShareCount(newsObj.getSharesNumber());
                            historyObj.setCommentsCount(newsObj.getCommentsNumber());
                            dp.updateArticleById(newsObj.getID(), historyObj);
                        }
                        break;
                    }
//                    historyObj.setLike_Id(newsObj.getLikeID());
//                    historyObj.setLikesCount(newsObj.getLikesNumber());
//                    historyObj.setFavouriteId(newsObj.getFavouriteID());
//                    historyObj.setShareCount(newsObj.getSharesNumber());
//                    dp.updateArticleById(newsObj.getID(), historyObj);

//                    Intent i = new Intent(Intent.ACTION_SEND);
//                    i.setType("text/plain");
////                i.putExtra(Intent.EXTRA_SUBJECT, shortLink + " \n " + "حمل تطبيق نبأ الاخباري مجاناً واستمتع بمتابعة العالم لحظة
/// بلحظة" +
////                        "\n" + "http://nabaapp.com/");
//                    i.putExtra(Intent.EXTRA_TEXT, newsObj.getNewsTitle() + "\n" + shortLink + " \n " + "حمل تطبيق نبأ الاخباري مجاناً
// واستمتع" +
//
//                            " بمتابعة العالم لحظة بلحظة" +
//                            "\n" + "http://nabaapp.com/");
////                Toast.makeText(context, shortLink, Toast.LENGTH_LONG).show();
////                i.putExtra(Intent.EXTRA_TEXT, "Hi Newz World");
//                    context.startActivity(Intent.createChooser(i, context.getString(R.string.share)));
                }
            });
        }
    }

    private void likeEvent(final Holder holder, int userID, final int position, final News newsObj) {

        /*MainControl mainControl = new MainControl(context);
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }*/

        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_USERID, userID + "");
        dataObj.put(URLs.TAG_REQUEST_ARTICLE_ID, newsObj.getID() + "");
        JsonParser jsonParser = new JsonParser(context, URLs.ADD_LIKE_URL, URLs.ADD_LIKE_REQUEST_TYPE, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();

        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {
            }

            @Override
            public void onFinished(List<Integer> likeResponse) {
                holder.likeBtn.setEnabled(true);
                holder.progressBar.setVisibility(View.GONE);
                holder.likeImg.setVisibility(View.VISIBLE);
                try {
                    if (likeResponse.get(0) > 0) {
                        newsObj.setLikeID(likeResponse.get(0));
                        newsObj.setLikesNumber(likeResponse.get(1));
                        holder.likesNumber.setText(newsObj.getLikesNumber() + " ");

                        for (int i = 0; i < articleIds.size(); i++) {
                            if (articleIds.get(i).getArticleId() == newsObj.getID()) {
                                historyObj.setLike_Id(likeResponse.get(0));
                                historyObj.setLikesCount(newsObj.getLikesNumber());
                                historyObj.setFavouriteId(newsObj.getFavouriteID());
                                historyObj.setShareCount(newsObj.getSharesNumber());
                                historyObj.setCommentsCount(newsObj.getCommentsNumber());
                                dp.updateArticleById(newsObj.getID(), historyObj);
                            }
                            break;
                        }

                        //newsList.set(position, newsObj);
                        setViewsVisible(holder, newsObj);
                    }
                } catch (IndexOutOfBoundsException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                } catch (NullPointerException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFinished(News newsObj) {

            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int userID) {

            }

            @Override
            public void onFinished(boolean success) {

            }
        });
    }

    private void UNLikeEvent(final Holder holder, final int position, final News newsObj) {
        /*MainControl mainControl = new MainControl(context);
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }*/

        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_ADD_LIKE_LIKE_ID, newsObj.getLikeID() + "");

        JsonParser jsonParser = new JsonParser(context, URLs.DELETE_LIKE_URL, URLs.DELETE_LIKE_REQUEST_TYPE, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {
            }

            @Override
            public void onFinished(List<Integer> likeResponse) {
                holder.likeBtn.setEnabled(true);
                holder.progressBar.setVisibility(View.GONE);
                holder.likeImg.setVisibility(View.VISIBLE);
                try {
                    if (likeResponse.get(0) > -1) {
                        newsObj.setLikesNumber(likeResponse.get(0));
                        holder.likesNumber.setText(newsObj.getLikesNumber() + " ");
                        newsObj.setLikesNumber(newsObj.getLikesNumber());
                        newsObj.setLikeID(0);
                        newsList.set(position, newsObj);
                        setViewGone(holder, newsObj);
                        setViewsVisible(holder, newsObj);
                    }
                } catch (IndexOutOfBoundsException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                } catch (NullPointerException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFinished(News newsObj) {

            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int userID) {

            }

            @Override
            public void onFinished(boolean success) {

            }
        });
    }

    public void addList(List<News> list) {
        blocked = dp.getAllProfiles().get(0).getBlockImg();
        if (blocked > 0) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(1);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(-1);
            }
        }
        newsList.addAll(0, list);
    }

    public void appendList(List<News> list) {
        blocked = dp.getAllProfiles().get(0).getBlockImg();
        if (blocked > 0) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(1);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(-1);
            }
        }
        newsList.addAll(list);
    }

    public interface OnListIsEmpty {
        void isEmpty();
    }

    private static class Holder {
        public LinearLayout sourceNewsDateHolder;
        public FontTextView sourceNewsTime;
        // public ImageView nativeAdIcon;
        //public TextView nativeAdTitle;
        //  public TextView nativeAdBody;
        //        public TextView nativeAdSocialContext;
        // public Button nativeAdCallToAction;
        //        public LinearLayout adView;
        // LinearLayout newsFeedHeader;
        public ExpandableView actionsView;
        public LinearLayout likesView;
        public LinearLayout commentsView;
        public LinearLayout sharesView;
        public LinearLayout likeBtn;
        public LinearLayout commentBtn;
        public LinearLayout shareBtn;
        public TextView sourceTitle;
        public TextView newsTime;
        public TextView newsTitle;
        public TextView sharingNumbers;
        public TextView commentsNumber;
        public TextView likesNumber;
        public TextView likeTitle;
        public TextView commentTitle;
        public TextView shareTitle;
        public ImageView sourceImg;
        public ImageView likeImg;
        public ImageView commentImg;
        public ImageView shareImg;
        public ImageView newsLogo;
        public LinearLayout newsFeedSourceHeader;
        public ProgressBar progressBar;
        MrecView mRecAd;
        //public MediaView nativeAdMedia;
        //        public MediaView nativeAdMedia;
        public LinearLayout roundedBackground;
        public View seperator;
        public FontTextView urgentLabel;
        public ImageView reload;
        public boolean isVisible;
        public GifMovieView loading;
        public View greyView;
        //        public NativeAd nativeAd;
        public RelativeLayout adWithUs;
        public GifMovieView spinnerAd;
        LinearLayout adContainer;
        // public TextView nativeAdSocialContext;
        public RelativeLayout gifHolder;
        // public ImageView adCover;
        public ImageView deleteIcon;
        public LinearLayout cardHolder;
        public RelativeLayout ticked;
        // public RelativeLayout holder_hh;
        //  public ImageView adChoices;
    }

    private class ShowAdsViews extends AsyncTask<String, Void, String> {
        private final Holder holder;
        private final int position;
        boolean serviceFlag;

        public ShowAdsViews(Holder holder, int position) {
            this.holder = holder;
            this.position = position;
        }

        @Override
        protected String doInBackground(String... params) {

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
//            showNativeAd(holder, currentAdView);View);
        }

        @Override
        protected void onPreExecute() {
            if (position % 4 == 0 && position != 0) {
//                holder.adView.setVisibility(View.VISIBLE);
                serviceFlag = true;
            } else {
//                holder.adView.setVisibility(View.GONE);
                serviceFlag = false;
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}
