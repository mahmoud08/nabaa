/*
package com.madarsoft.nabaa.adapters;

*/
/**
 * Created by basma on 8/7/2017.
 *//*


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.MrecView;
import com.appodeal.ads.NativeAd;
import com.basv.gifmoviewview.widget.GifMovieView;
import com.facebook.ads.MediaView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.ExpandableView;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.controls.AdsControlNabaa;
import com.madarsoft.nabaa.controls.GoogleAdMob;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.controls.SpeedScrollListener;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.History;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.fragments.AddCommentFragment;
import com.madarsoft.nabaa.fragments.MadarFragment;
import com.madarsoft.nabaa.fragments.NewsDetail2;
import com.madarsoft.nabaa.fragments.NewsFragment;
import com.madarsoft.nabaa.fragments.SourceNews;
import com.madarsoft.nabaa.interfaces.AdViewLoadListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;


*/
/**
 * Created by basma on 3/16/2017.
 *//*


public class BasmaNewsAdapter extends RecyclerView.Adapter{




    public static final int REQUEST_SHARE_RESULT = 1;
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    public static final int MREC_AD_POSITION=0;
    public static boolean isUseAdMobAds=false;
    private final boolean isSourcesNews;
    private final boolean isHistory;
    HashMap<String,View> adMobMap=new HashMap<>();
    public List<News> newsList = new ArrayList<>();
    //public ArrayList<RectangleBannerAd> ads = new ArrayList<>();
    public List<News> setviceNewsList = new ArrayList<>();
    //private final Typeface font;
    Context context;
    DataBaseAdapter dp;
    MainControl mainControl;
    Locale locale;
    SimpleDateFormat print2;
    boolean ignoreDisabled = false;
    */
/*  Holder ((ViewHolder) holder);
      AnalyticsApplication application;*//*

    HashSet<Integer> intArray;
    AdsControlNabaa adsControl;
    private ArrayList<History> articleIds;
    private LayoutInflater inflater;

    //private JsonParser jsonParser;
*/
/*    private Typeface headerFont;
    private String[] monthsName = {"يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر",
            "ديسمبر"};*//*

    private Animation pop;
    */
/*    private int myPos;
        private int lastPosition = -1;
        private int pos;
        private LinearLayout currentAdView;*//*

    private int blocked;
    // private com.google.android.gms.analytics.Tracker mTracker;
    private long mLastClickTime = 0;
    //private RelativeLayout adsWithUs;
    private NewsAdapter.OnListIsEmpty onListIsEmpty;
    private History historyObj;
    private boolean fromHistory;

    MadarFragment sourceNewsFrg;
    private boolean isBannerCalled=false;

    public BasmaNewsAdapter(MainActivity mainActivity, Context context, List<News> newsList, SpeedScrollListener scrollListener, boolean
            isSourceNews, boolean isHistory, MadarFragment sourceNewsFrg) {
       
//        super(context, scrollListener, newsList);
        this.context = context;
        adsControl = new AdsControlNabaa(mainActivity);
        mainControl = new MainControl(context);
        this.newsList = newsList;
        historyObj = new History();
        this.isHistory = isHistory;
        intArray = new HashSet<>();
        this.isSourcesNews = isSourceNews;
        dp = new DataBaseAdapter(context);

        this.sourceNewsFrg = sourceNewsFrg;
        locale = new Locale("ar", "SA");
        print2 = new SimpleDateFormat("hh:mm a dd-MMMM-yyyy ", locale);
        // AnalyticsApplication application = (AnalyticsApplication) ((FragmentActivity) context).getApplication();
        // mTracker = application.getDefaultTracker();

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.news_feed_item_card, parent, false);

        return new BasmaNewsAdapter.ViewHolder(root);
    }
    @Override

    public void onBindViewHolder(final RecyclerView.ViewHolder  holder, final int position) {
        final News news = newsList.get(position);

        if (mainControl.checkInternetConnection()) {

            if (!isUseAdMobAds) {
                NativeAd nativead = adsControl.getNativeAdForCertainPosition(position, NewsFragment.AllNativeAds);


                if (nativead != null) {

                    try {
//                    ((ViewHolder) holder).holder_hh.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).adContainer.setVisibility(View.VISIBLE);

                        //  if (((ViewHolder) holder).adContainer.getChildCount() == 0) {
                        ((ViewHolder) holder).gifHolder.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).spinnerAd.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).mRecAd.setVisibility(View.GONE);
                        Appodeal.hide((Activity) context, Appodeal.MREC);


                        ((ViewHolder) holder).adContainer.removeAllViews();
                        ((ViewHolder) holder).adContainer.setBackgroundResource(R.color.white);

                        showNativeAd(((ViewHolder) holder).adContainer, ((ViewHolder) holder).gifHolder, nativead);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // }

                } else {
                    if (((ViewHolder) holder).adContainer.getChildCount() > 0) {
                        ((ViewHolder) holder).adContainer.removeAllViews();
                    }

                    ((ViewHolder) holder).gifHolder.setVisibility(View.GONE);
                    ((ViewHolder) holder).spinnerAd.setVisibility(View.GONE);
                    ((ViewHolder) holder).adWithUs.setVisibility(View.GONE);
                    ((ViewHolder) holder).adContainer.setVisibility(View.GONE);
                    ((ViewHolder) holder).mRecAd.setVisibility(View.GONE);
                    ((ViewHolder) holder).greyView.setVisibility(View.GONE);
                }
            } else if (adsControl.isAdPosition(position,true)) {
                ((ViewHolder) holder).adContainer.setVisibility(View.VISIBLE);

                //  if (((ViewHolder) holder).adContainer.getChildCount() == 0) {
                ((ViewHolder) holder).gifHolder.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).spinnerAd.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).mRecAd.setVisibility(View.GONE);
                //    Appodeal.hide((Activity) context, Appodeal.MREC);



                if (((ViewHolder) holder).adContainer.getChildCount()  ==0) {
                    try {
//                    ((ViewHolder) holder).holder_hh.setVisibility(View.VISIBLE);
                        // ((ViewHolder) holder).adContainer.removeAllViews();
                        ((ViewHolder) holder).adContainer.setBackground(null);
                        showAdmobAd(((ViewHolder) holder).adContainer, ((ViewHolder) holder).gifHolder);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    try {
                        ((AdView)((ViewHolder) holder).adContainer.getChildAt(0)).loadAd( new AdRequest.Builder().build());

                    }catch (IndexOutOfBoundsException e)
                    {}catch (ClassCastException e)
                    {

                    }
                }
                // }


            }else {
              */
/*  if (((ViewHolder) holder).adContainer.getChildCount() > 0) {
                    ((ViewHolder) holder).adContainer.removeAllViews();
                }*//*


                ((ViewHolder) holder).gifHolder.setVisibility(View.GONE);
                ((ViewHolder) holder).spinnerAd.setVisibility(View.GONE);
                ((ViewHolder) holder).adWithUs.setVisibility(View.GONE);
                ((ViewHolder) holder).adContainer.setVisibility(View.GONE);
                ((ViewHolder) holder).mRecAd.setVisibility(View.GONE);
                ((ViewHolder) holder).greyView.setVisibility(View.GONE);
            }
        }
        ((ViewHolder) holder).sourceTitle.setText(news.getSourceTitle());
        if (isSourcesNews) {
            ((ViewHolder) holder).newsFeedSourceHeader.setVisibility(View.GONE);
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                ((ViewHolder) holder).roundedBackground.setBackground(context.getResources().getDrawable(R.drawable.rounded_border, context.getTheme()));
            } else {
                ((ViewHolder) holder).roundedBackground.setBackground(context.getResources().getDrawable(R.drawable.rounded_border));
            }
        } else {
            ((ViewHolder) holder).newsFeedSourceHeader.setVisibility(View.VISIBLE);
        }
        ((ViewHolder) holder).sourceTitle.setGravity(Gravity.RIGHT);
        //Date date = new Date(news.getTime_stamp());
        setViewGone(((ViewHolder) holder), news);
        setViewsVisible(((ViewHolder) holder), news);

        */
/*if (likesState.get(position) == 1) {
            ((ViewHolder) holder).likeImg.setImageResource(R.drawable.like);
        } else {
            ((ViewHolder) holder).likeImg.setImageResource(R.drawable.like_unactive);
        }*//*

        try {
            Calendar cal = mainControl.setTimeZone(Long.parseLong(news.getArticleDate()), news.getTimeOffset());

            ((ViewHolder) holder).newsTime.setText(context.getString(R.string.updated) + " " + print2.format(cal.getTime()));
            ((ViewHolder) holder).sourceNewsTime.setText(context.getString(R.string.updated) + " " + print2.format(cal.getTime()));

        } catch (Exception e) {
        }
        if (isSourcesNews)
            ((ViewHolder) holder).sourceNewsDateHolder.setVisibility(View.VISIBLE);
        else
            ((ViewHolder) holder).sourceNewsDateHolder.setVisibility(View.GONE);

        ((ViewHolder) holder).newsTitle.setText(news.getNewsTitle());
        ((ViewHolder) holder).newsTitle.setEllipsize(TextUtils.TruncateAt.END);
        ((ViewHolder) holder).newsTitle.setMaxLines(2);
        if (news.getIsRTL().equalsIgnoreCase("false"))
            ((ViewHolder) holder).newsTitle.setGravity(Gravity.LEFT);
        else
            ((ViewHolder) holder).newsTitle.setGravity(Gravity.RIGHT);
        //setTextSizes(((ViewHolder) holder));
//        convertView.setTag(((ViewHolder) holder));

        final Animation animation = AnimationUtils.loadAnimation(context,
                R.anim.slide_out);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                newsList.remove(position);
                try {
                    if (newsList.size() == 0) {
                        onListIsEmpty.isEmpty();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                notifyDataSetChanged();
            }
        });

        try {
            ((ViewHolder) holder).deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ViewHolder) holder).cardHolder.startAnimation(animation);
                    dp.deleteArticleByArticleId(news.getID());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        Picasso.with(context)
                .load(news.getSourceLogoUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(((ViewHolder) holder).sourceImg, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(context)
                                .load(news.getSourceLogoUrl())
                                .error(R.drawable.item_icon)
                                .into(((ViewHolder) holder).sourceImg, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        Log.v("Picasso", "Could not fetch image");
                                    }
                                });
                    }
                });

        */
/*Picasso.with(context)
                .load(news.getSourceLogoUrl())
                .placeholder(R.anim.progress_animation)
                .error(R.drawable.item_icon)
                .into(((ViewHolder) holder).sourceImg);*//*


        if (news.getID() == 24001) {
            Log.e("ASDA", "ASDSAd");
        }

//        if (((ViewHolder) holder).isVisible) {
//            Picasso.with(context)
//                    .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
//                    .error(R.drawable.no_img)
//                    .into(((ViewHolder) holder).newsLogo);
//            ((ViewHolder) holder).reload.setVisibility(View.GONE);
//        }
        try {
            if (news.getIsBlocked() > 0) {
                ((ViewHolder) holder).reload.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).newsLogo.setImageResource(R.drawable.default_img);
            } else {

                Picasso.with(context)
                        .load(Uri.decode(news.getLogo_url()))
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(((ViewHolder) holder).newsLogo, new Callback() {
                            @Override
                            public void onSuccess() {
                                ((ViewHolder) holder).loading.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                //Try again online if cache failed
                                Picasso.with(context)
                                        .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS)).error(R.drawable.default_img)
                                        .into(((ViewHolder) holder).newsLogo, new Callback() {
                                            @Override
                                            public void onSuccess() {
//                                                .setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.FILL_PARENT,height));
                                                ((ViewHolder) holder).loading.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {
                                                ((ViewHolder) holder).loading.setVisibility(View.GONE);
                                                Log.v("Picasso", "Could not fetch image");
                                            }
                                        });
                            }

                            ;
                        });
                ((ViewHolder) holder).reload.setVisibility(View.GONE);
                //((ViewHolder) holder).isVisible = true;
            }
       */
/*Picasso.with(context)
                    .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                    .error(R.drawable.no_img)
                    .into(((ViewHolder) holder).newsLogo);*//*


        } catch (IllegalArgumentException e) {
            ((ViewHolder) holder).newsLogo.setImageResource(R.drawable.default_img);
        }

        ((ViewHolder) holder).reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ((ViewHolder) holder).reload.setVisibility(View.GONE);
                    ((ViewHolder) holder).loading.bringToFront();
                    ((ViewHolder) holder).loading.setVisibility(View.VISIBLE);
                    Picasso.with(context)
                            .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                            .error(R.drawable.default_img)
                            .networkPolicy(NetworkPolicy.OFFLINE)
                            .into(((ViewHolder) holder).newsLogo, new Callback() {
                                @Override
                                public void onSuccess() {
                                    ((ViewHolder) holder).loading.setVisibility(View.GONE);
                                    ((ViewHolder) holder).reload.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    //Try again online if cache failed
                                    Picasso.with(context)
                                            .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                                            .error(R.drawable.default_img).into(((ViewHolder) holder).newsLogo, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            ((ViewHolder) holder).reload.setVisibility(View.GONE);
                                            ((ViewHolder) holder).loading.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onError() {
                                            ((ViewHolder) holder).reload.setVisibility(View.GONE);
                                            ((ViewHolder) holder).loading.setVisibility(View.GONE);
                                        }
                                    });

                                }
                            });

                } catch (Exception e) {
                    ((ViewHolder) holder).reload.setVisibility(View.GONE);
                    ((ViewHolder) holder).newsLogo.setImageResource(R.drawable.default_img);
                    ((ViewHolder) holder).loading.setVisibility(View.GONE);
                    ((ViewHolder) holder).newsLogo.bringToFront();
                }
                news.setIsBlocked(-1);
            }
        });

      */
/*  convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {

            }
        });*//*

        ((ViewHolder) holder).newsLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                goToDetails(newsList.get(position), position, ((ViewHolder) holder));
            }
        });
        ((ViewHolder) holder).newsTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                goToDetails(newsList.get(position), position, ((ViewHolder) holder));
            }
        });
        ((ViewHolder) holder).likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mainControl.checkInternetConnection()) {
                    Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }

                ((ViewHolder) holder).likeBtn.setEnabled(false);
                ((ViewHolder) holder).progressBar.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).likeImg.setVisibility(View.GONE);
                if (news.getLikeID() == 0)
                    likeEvent(((ViewHolder) holder), URLs.getUserID(), position, news);
                else
                    UNLikeEvent(((ViewHolder) holder), position, news);

                */
/*pop.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        if (news.getLikeID() == 0)
                            likeEvent(((ViewHolder) holder), URLs.getUserID(), position, news);
                        else
                            UNLikeEvent(((ViewHolder) holder), position, news);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });*//*


            }
        });

        ((ViewHolder) holder).newsFeedSourceHeader.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                        transaction.addToBackStack(null);
                        //
                        transaction.replace(R.id.parent, new SourceNews(news.getSourceID(), true, false, "", false));
                        //transaction.add(R.id.parent, new SourceNews(news.getSourceID(), true, false, ""));
                        transaction.commit();
                    }
                }
        );

        ((ViewHolder) holder).commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = (Integer) v.getTag();
                //commentsState.add(pos, 1);
                pop = AnimationUtils.loadAnimation(context, R.anim.pop);
                ((ViewHolder) holder).commentImg.startAnimation(pop);
                showDialog(((ViewHolder) holder), news, position);
            }
        });

        ((ViewHolder) holder).shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = (Integer) v.getTag();
                //sharesState.add(pos, 1);
                pop = AnimationUtils.loadAnimation(context, R.anim.pop);
                ((ViewHolder) holder).shareImg.startAnimation(pop);
                shareEvent(((ViewHolder) holder), position, news);
            }
        });

        ((ViewHolder) holder).commentsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(((ViewHolder) holder), news, position);
            }
        });

 

       

    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public LinearLayout sourceNewsDateHolder;
        public FontTextView sourceNewsTime;
        // public ImageView nativeAdIcon;
        //public TextView nativeAdTitle;
        //  public TextView nativeAdBody;
        //        public TextView nativeAdSocialContext;
        // public Button nativeAdCallToAction;
        //        public LinearLayout adView;
        // LinearLayout newsFeedHeader;
        public ExpandableView actionsView;
        public LinearLayout likesView;
        public LinearLayout commentsView;
        public LinearLayout sharesView;
        public LinearLayout likeBtn;
        public LinearLayout commentBtn;
        public LinearLayout shareBtn;
        public TextView sourceTitle;
        public TextView newsTime;
        public TextView newsTitle;
        public TextView sharingNumbers;
        public TextView commentsNumber;
        public TextView likesNumber;
        public TextView likeTitle;
        public TextView commentTitle;
        public TextView shareTitle;
        public ImageView sourceImg;
        public ImageView likeImg;
        public ImageView commentImg;
        public ImageView shareImg;
        public ImageView newsLogo;
        public LinearLayout newsFeedSourceHeader;
        public ProgressBar progressBar;
        MrecView mRecAd;
        //public MediaView nativeAdMedia;
        //        public MediaView nativeAdMedia;
        public LinearLayout roundedBackground;
        public View seperator;
        public FontTextView urgentLabel;
        public ImageView reload;
        public boolean isVisible;
        public GifMovieView loading;
        public View greyView;
        //        public NativeAd nativeAd;
        public RelativeLayout adWithUs;
        public GifMovieView spinnerAd;
        LinearLayout adContainer;
        // public TextView nativeAdSocialContext;
        public RelativeLayout gifHolder;
        // public ImageView adCover;
        public ImageView deleteIcon;
        public LinearLayout cardHolder;
        public RelativeLayout ticked;

        public ViewHolder(View convertView) {

            super(convertView);
          actionsView = (ExpandableView) convertView.findViewById(R.id.actions_expanded_view);
            //holder.actionsView.setTag(position);
            likesView = (LinearLayout) convertView.findViewById(R.id.likes_view);
            //holder.likesView.setTag(position);
            commentsView = (LinearLayout) convertView.findViewById(R.id.comments_view);
            //holder.commentsView.setTag(position);
            sharesView = (LinearLayout) convertView.findViewById(R.id.shares_view);
            //holder.sharesView.setTag(position);
            ticked = (RelativeLayout) convertView.findViewById(R.id.news_feed_ticked);
            likeBtn = (LinearLayout) convertView.findViewById(R.id.like_btn);
            //holder.likeBtn.setTag(position);
            commentBtn = (LinearLayout) convertView.findViewById(R.id.comment_btn);
            //holder.commentBtn.setTag(position);
            shareBtn = (LinearLayout) convertView.findViewById(R.id.share_btn);
            //holder.shareBtn.setTag(position);
            adWithUs = (RelativeLayout) convertView.findViewById(R.id.adWithUs);
            sourceTitle = (TextView) convertView.findViewById(R.id.source_title);
            newsTime = (TextView) convertView.findViewById(R.id.news_time);
            newsTitle = (TextView) convertView.findViewById(R.id.news_title);
            sharingNumbers = (TextView) convertView.findViewById(R.id.sharing_number);
            commentsNumber = (TextView) convertView.findViewById(R.id.comments_number);
            likesNumber = (TextView) convertView.findViewById(R.id.likes_number);
            likeTitle = (TextView) convertView.findViewById(R.id.like_txt);
            commentTitle = (TextView) convertView.findViewById(R.id.comment_txt);
            shareTitle = (TextView) convertView.findViewById(R.id.share_txt);
            seperator = (View) convertView.findViewById(R.id.seperator);
            sourceImg = (ImageView) convertView.findViewById(R.id.source_image);
            likeImg = (ImageView) convertView.findViewById(R.id.like_img);
            commentImg = (ImageView) convertView.findViewById(R.id.comment_img);
            shareImg = (ImageView) convertView.findViewById(R.id.share_img);
            newsLogo = (ImageView) convertView.findViewById(R.id.news_logo);
            reload = (ImageView) convertView.findViewById(R.id.reload);
            newsFeedSourceHeader = (LinearLayout) convertView.findViewById(R.id.news_feed_source_header);
            sourceNewsDateHolder = (LinearLayout) convertView.findViewById(R.id.source_news_date_holder);
            sourceNewsTime = (FontTextView) convertView.findViewById(R.id.source_news_time);
            progressBar = (ProgressBar) convertView.findViewById(R.id.like_loading);
            roundedBackground = (LinearLayout) convertView.findViewById(R.id.rounded_background);
            urgentLabel = (FontTextView) convertView.findViewById(R.id.urgent_label);
            spinnerAd = (GifMovieView) convertView.findViewById(R.id.spinner_ad);
            mRecAd = (MrecView) convertView.findViewById(R.id.mrec_ad);
            adContainer = (LinearLayout) convertView.findViewById(R.id.ads_container_basma);
            //holder.nativeAdTitle = (TextView) convertView.findViewById(R.id.native_ad_title);
            //holder.nativeAdBody = (TextView) convertView.findViewById(R.id.native_ad_body);
            //holder.nativeAdMedia = (MediaView) convertView.findViewById(R.id.native_ad_media);
            //holder.nativeAdSocialContext = (TextView) convertView.findViewById(R.id.native_ad_social_context);
            //holder.nativeAdCallToAction = (Button) convertView.findViewById(R.id.native_ad_call_to_action);
            gifHolder = (RelativeLayout) convertView.findViewById(R.id.gif_holder);
            //holder.holder_hh = (RelativeLayout) convertView.findViewById(R.id.ll_header);
//            holder.adView = (LinearLayout) convertView.findViewById(R.id.holder_me);
            //holder.adChoices = (ImageView) convertView.findViewById(R.id.ad_choices_container);
            greyView = (View) convertView.findViewById(R.id.grey_view);
            //holder.adCover = (ImageView) convertView.findViewById(R.id.ad_cover);
            loading = (GifMovieView) convertView.findViewById(R.id.loading_spinner);
            deleteIcon = (ImageView) convertView.findViewById(R.id.delete_trash);
            loading.setMovieResource(R.drawable.gif_reload);
            spinnerAd.setMovieResource(R.drawable.gif_reload);
            cardHolder = (LinearLayout) convertView.findViewById(R.id.news_feed_item_holder);
        }


    }
    public NewsAdapter.OnListIsEmpty getOnListIsEmpty() {
        return onListIsEmpty;
    }

    public void setOnListIsEmpty(NewsAdapter.OnListIsEmpty onListIsEmpty) {
        this.onListIsEmpty = onListIsEmpty;
    }

    public void setAllItemsEnabled(boolean ignoreDisabled) {
        this.ignoreDisabled = ignoreDisabled;
    }

    void showDialog(final BasmaNewsAdapter.ViewHolder holder, News newsObj, int index) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }

        FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        AddCommentFragment commentFrag = new AddCommentFragment(newsObj, index);
        commentFrag.show(ft, "dialog");
        commentFrag.onFinishUpdates(new AddCommentFragment.OnCommentsUpdate() {
            @Override
            public void onFinished(News newsObj, int index) {
                newsList.set(index, newsObj);

                if (((ViewHolder) holder).commentsView.getVisibility() == View.GONE) {
                    ((ViewHolder) holder).commentsView.setVisibility(View.VISIBLE);
                }
                if (((ViewHolder) holder).actionsView.getVisibility() == View.GONE) {
                    ((ViewHolder) holder).actionsView.setVisibility(View.VISIBLE);
                    ((ViewHolder) holder).seperator.setVisibility(View.VISIBLE);
                }

                if (newsObj.getCommentsNumber() == 0) {
                    ((ViewHolder) holder).commentsView.setVisibility(View.GONE);
                }
                if (newsObj.getLikesNumber() == 0 && newsObj.getCommentsNumber() == 0 && newsObj.getSharesNumber() == 0)
                    ((ViewHolder) holder).actionsView.setVisibility(View.GONE);

                ((ViewHolder) holder).commentsNumber.setText(newsObj.getCommentsNumber() + "");
            }
        });
    }

    @Override
    public int getItemCount() {
        try {
            return newsList.size();
        } catch (NullPointerException e) {
            return 0;
        }
    }

 */
/*   @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }*//*


*/
/*    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // myPos = position;
        final NewsAdapter.Holder ((ViewHolder) holder);
//        View rowView = null;
        final News news = newsList.get(position);

        if (convertView == null) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.news_feed_item_card, parent, false);
            ((ViewHolder) holder) = new NewsAdapter.Holder();
            ((ViewHolder) holder).actionsView = (ExpandableView) convertView.findViewById(R.id.actions_expanded_view);
            //((ViewHolder) holder).actionsView.setTag(position);
            ((ViewHolder) holder).likesView = (LinearLayout) convertView.findViewById(R.id.likes_view);
            //((ViewHolder) holder).likesView.setTag(position);
            ((ViewHolder) holder).commentsView = (LinearLayout) convertView.findViewById(R.id.comments_view);
            //((ViewHolder) holder).commentsView.setTag(position);
            ((ViewHolder) holder).sharesView = (LinearLayout) convertView.findViewById(R.id.shares_view);
            //((ViewHolder) holder).sharesView.setTag(position);
            ((ViewHolder) holder).ticked = (RelativeLayout) convertView.findViewById(R.id.news_feed_ticked);
            ((ViewHolder) holder).likeBtn = (LinearLayout) convertView.findViewById(R.id.like_btn);
            //((ViewHolder) holder).likeBtn.setTag(position);
            ((ViewHolder) holder).commentBtn = (LinearLayout) convertView.findViewById(R.id.comment_btn);
            //((ViewHolder) holder).commentBtn.setTag(position);
            ((ViewHolder) holder).shareBtn = (LinearLayout) convertView.findViewById(R.id.share_btn);
            //((ViewHolder) holder).shareBtn.setTag(position);
            ((ViewHolder) holder).adWithUs = (RelativeLayout) convertView.findViewById(R.id.adWithUs);
            ((ViewHolder) holder).sourceTitle = (TextView) convertView.findViewById(R.id.source_title);
            ((ViewHolder) holder).newsTime = (TextView) convertView.findViewById(R.id.news_time);
            ((ViewHolder) holder).newsTitle = (TextView) convertView.findViewById(R.id.news_title);
            ((ViewHolder) holder).sharingNumbers = (TextView) convertView.findViewById(R.id.sharing_number);
            ((ViewHolder) holder).commentsNumber = (TextView) convertView.findViewById(R.id.comments_number);
            ((ViewHolder) holder).likesNumber = (TextView) convertView.findViewById(R.id.likes_number);
            ((ViewHolder) holder).likeTitle = (TextView) convertView.findViewById(R.id.like_txt);
            ((ViewHolder) holder).commentTitle = (TextView) convertView.findViewById(R.id.comment_txt);
            ((ViewHolder) holder).shareTitle = (TextView) convertView.findViewById(R.id.share_txt);
            ((ViewHolder) holder).seperator = (View) convertView.findViewById(R.id.seperator);
            ((ViewHolder) holder).sourceImg = (ImageView) convertView.findViewById(R.id.source_image);
            ((ViewHolder) holder).likeImg = (ImageView) convertView.findViewById(R.id.like_img);
            ((ViewHolder) holder).commentImg = (ImageView) convertView.findViewById(R.id.comment_img);
            ((ViewHolder) holder).shareImg = (ImageView) convertView.findViewById(R.id.share_img);
            ((ViewHolder) holder).newsLogo = (ImageView) convertView.findViewById(R.id.news_logo);
            ((ViewHolder) holder).reload = (ImageView) convertView.findViewById(R.id.reload);
            ((ViewHolder) holder).newsFeedSourceHeader = (LinearLayout) convertView.findViewById(R.id.news_feed_source_header);
            ((ViewHolder) holder).sourceNewsDateHolder = (LinearLayout) convertView.findViewById(R.id.source_news_date_holder);
            ((ViewHolder) holder).sourceNewsTime = (FontTextView) convertView.findViewById(R.id.source_news_time);
            ((ViewHolder) holder).progressBar = (ProgressBar) convertView.findViewById(R.id.like_loading);
            ((ViewHolder) holder).roundedBackground = (LinearLayout) convertView.findViewById(R.id.rounded_background);
            ((ViewHolder) holder).urgentLabel = (FontTextView) convertView.findViewById(R.id.urgent_label);
            ((ViewHolder) holder).spinnerAd = (GifMovieView) convertView.findViewById(R.id.spinner_ad);
            ((ViewHolder) holder).mRecAd=(MrecView)convertView.findViewById(R.id.mrec_ad);
            ((ViewHolder) holder).adContainer = (LinearLayout) convertView.findViewById(R.id.ads_container_basma);
            //((ViewHolder) holder).nativeAdTitle = (TextView) convertView.findViewById(R.id.native_ad_title);
            //((ViewHolder) holder).nativeAdBody = (TextView) convertView.findViewById(R.id.native_ad_body);
            //((ViewHolder) holder).nativeAdMedia = (MediaView) convertView.findViewById(R.id.native_ad_media);
            //((ViewHolder) holder).nativeAdSocialContext = (TextView) convertView.findViewById(R.id.native_ad_social_context);
            //((ViewHolder) holder).nativeAdCallToAction = (Button) convertView.findViewById(R.id.native_ad_call_to_action);
            ((ViewHolder) holder).gifHolder = (RelativeLayout) convertView.findViewById(R.id.gif_holder);
            //((ViewHolder) holder).holder_hh = (RelativeLayout) convertView.findViewById(R.id.ll_header);
//            ((ViewHolder) holder).adView = (LinearLayout) convertView.findViewById(R.id.holder_me);
            //((ViewHolder) holder).adChoices = (ImageView) convertView.findViewById(R.id.ad_choices_container);
            ((ViewHolder) holder).greyView = (View) convertView.findViewById(R.id.grey_view);
            //((ViewHolder) holder).adCover = (ImageView) convertView.findViewById(R.id.ad_cover);
            ((ViewHolder) holder).loading = (GifMovieView) convertView.findViewById(R.id.loading_spinner);
            ((ViewHolder) holder).deleteIcon = (ImageView) convertView.findViewById(R.id.delete_trash);
            ((ViewHolder) holder).loading.setMovieResource(R.drawable.gif_reload);
            ((ViewHolder) holder).spinnerAd.setMovieResource(R.drawable.gif_reload);
            ((ViewHolder) holder).cardHolder = (LinearLayout) convertView.findViewById(R.id.news_feed_item_holder);
            convertView.setTag(((ViewHolder) holder));
        } else {
            ((ViewHolder) holder) = (NewsAdapter.Holder) convertView.getTag();
        }
        if (mainControl.checkInternetConnection()) {

            if (!isUseAdMobAds) {
                NativeAd nativead = adsControl.getNativeAdForCertainPosition(position, NewsFragment.AllNativeAds);


                if (nativead != null) {

                    try {
//                    ((ViewHolder) holder).holder_hh.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).adContainer.setVisibility(View.VISIBLE);

                        //  if (((ViewHolder) holder).adContainer.getChildCount() == 0) {
                        ((ViewHolder) holder).gifHolder.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).spinnerAd.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).mRecAd.setVisibility(View.GONE);
                        Appodeal.hide((Activity) context, Appodeal.MREC);


                        ((ViewHolder) holder).adContainer.removeAllViews();
                        ((ViewHolder) holder).adContainer.setBackgroundResource(R.color.white);

                        showNativeAd(((ViewHolder) holder).adContainer, ((ViewHolder) holder).gifHolder, nativead);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // }

                } else {
                    if (((ViewHolder) holder).adContainer.getChildCount() > 0) {
                        ((ViewHolder) holder).adContainer.removeAllViews();
                    }

                    ((ViewHolder) holder).gifHolder.setVisibility(View.GONE);
                    ((ViewHolder) holder).spinnerAd.setVisibility(View.GONE);
                    ((ViewHolder) holder).adWithUs.setVisibility(View.GONE);
                    ((ViewHolder) holder).adContainer.setVisibility(View.GONE);
                    ((ViewHolder) holder).mRecAd.setVisibility(View.GONE);
                    ((ViewHolder) holder).greyView.setVisibility(View.GONE);
                }
            } else if (adsControl.isAdPosition(position)) {
                ((ViewHolder) holder).adContainer.setVisibility(View.VISIBLE);

                //  if (((ViewHolder) holder).adContainer.getChildCount() == 0) {
                ((ViewHolder) holder).gifHolder.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).spinnerAd.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).mRecAd.setVisibility(View.GONE);
                //    Appodeal.hide((Activity) context, Appodeal.MREC);



                if (((ViewHolder) holder).adContainer.getChildCount()  ==0) {
                    try {
//                    ((ViewHolder) holder).holder_hh.setVisibility(View.VISIBLE);
                        // ((ViewHolder) holder).adContainer.removeAllViews();
                        ((ViewHolder) holder).adContainer.setBackground(null);
                        showAdmobAd(((ViewHolder) holder).adContainer, ((ViewHolder) holder).gifHolder);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    try {
                        ((AdView)((ViewHolder) holder).adContainer.getChildAt(0)).loadAd( new AdRequest.Builder().build());

                    }catch (IndexOutOfBoundsException e)
                    {}catch (ClassCastException e)
                    {

                    }
                }
                // }


            }else {
              *//*
*/
/*  if (((ViewHolder) holder).adContainer.getChildCount() > 0) {
                    ((ViewHolder) holder).adContainer.removeAllViews();
                }*//*
*/
/*

                ((ViewHolder) holder).gifHolder.setVisibility(View.GONE);
                ((ViewHolder) holder).spinnerAd.setVisibility(View.GONE);
                ((ViewHolder) holder).adWithUs.setVisibility(View.GONE);
                ((ViewHolder) holder).adContainer.setVisibility(View.GONE);
                ((ViewHolder) holder).mRecAd.setVisibility(View.GONE);
                ((ViewHolder) holder).greyView.setVisibility(View.GONE);
            }
        }
        ((ViewHolder) holder).sourceTitle.setText(news.getSourceTitle());
        if (isSourcesNews) {
            ((ViewHolder) holder).newsFeedSourceHeader.setVisibility(View.GONE);
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                ((ViewHolder) holder).roundedBackground.setBackground(context.getResources().getDrawable(R.drawable.rounded_border, context.getTheme()));
            } else {
                ((ViewHolder) holder).roundedBackground.setBackground(context.getResources().getDrawable(R.drawable.rounded_border));
            }
        } else {
            ((ViewHolder) holder).newsFeedSourceHeader.setVisibility(View.VISIBLE);
        }
        ((ViewHolder) holder).sourceTitle.setGravity(Gravity.RIGHT);
        //Date date = new Date(news.getTime_stamp());
        setViewGone(((ViewHolder) holder), news);
        setViewsVisible(((ViewHolder) holder), news);

        *//*
*/
/*if (likesState.get(position) == 1) {
            ((ViewHolder) holder).likeImg.setImageResource(R.drawable.like);
        } else {
            ((ViewHolder) holder).likeImg.setImageResource(R.drawable.like_unactive);
        }*//*
*/
/*
        try {
            Calendar cal = mainControl.setTimeZone(Long.parseLong(news.getArticleDate()), news.getTimeOffset());

            ((ViewHolder) holder).newsTime.setText(context.getString(R.string.updated) + " " + print2.format(cal.getTime()));
            ((ViewHolder) holder).sourceNewsTime.setText(context.getString(R.string.updated) + " " + print2.format(cal.getTime()));

        } catch (Exception e) {
        }
        if (isSourcesNews)
            ((ViewHolder) holder).sourceNewsDateHolder.setVisibility(View.VISIBLE);
        else
            ((ViewHolder) holder).sourceNewsDateHolder.setVisibility(View.GONE);

        ((ViewHolder) holder).newsTitle.setText(news.getNewsTitle());
        ((ViewHolder) holder).newsTitle.setEllipsize(TextUtils.TruncateAt.END);
        ((ViewHolder) holder).newsTitle.setMaxLines(2);
        if (news.getIsRTL().equalsIgnoreCase("false"))
            ((ViewHolder) holder).newsTitle.setGravity(Gravity.LEFT);
        else
            ((ViewHolder) holder).newsTitle.setGravity(Gravity.RIGHT);
        //setTextSizes(((ViewHolder) holder));
//        convertView.setTag(((ViewHolder) holder));

        final Animation animation = AnimationUtils.loadAnimation(context,
                R.anim.slide_out);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                newsList.remove(position);
                try {
                    if (newsList.size() == 0) {
                        onListIsEmpty.isEmpty();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                notifyDataSetChanged();
            }
        });

        try {
            ((ViewHolder) holder).deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ViewHolder) holder).cardHolder.startAnimation(animation);
                    dp.deleteArticleByArticleId(news.getID());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        Picasso.with(context)
                .load(news.getSourceLogoUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(((ViewHolder) holder).sourceImg, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(context)
                                .load(news.getSourceLogoUrl())
                                .error(R.drawable.item_icon)
                                .into(((ViewHolder) holder).sourceImg, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        Log.v("Picasso", "Could not fetch image");
                                    }
                                });
                    }
                });

        *//*
*/
/*Picasso.with(context)
                .load(news.getSourceLogoUrl())
                .placeholder(R.anim.progress_animation)
                .error(R.drawable.item_icon)
                .into(((ViewHolder) holder).sourceImg);*//*
*/
/*

        if (news.getID() == 24001) {
            Log.e("ASDA", "ASDSAd");
        }

//        if (((ViewHolder) holder).isVisible) {
//            Picasso.with(context)
//                    .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
//                    .error(R.drawable.no_img)
//                    .into(((ViewHolder) holder).newsLogo);
//            ((ViewHolder) holder).reload.setVisibility(View.GONE);
//        }
        try {
            if (news.getIsBlocked() > 0) {
                ((ViewHolder) holder).reload.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).newsLogo.setImageResource(R.drawable.default_img);
            } else {

                Picasso.with(context)
                        .load(Uri.decode(news.getLogo_url()))
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(((ViewHolder) holder).newsLogo, new Callback() {
                            @Override
                            public void onSuccess() {
                                ((ViewHolder) holder).loading.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                //Try again online if cache failed
                                Picasso.with(context)
                                        .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS)).error(R.drawable.default_img)
                                        .into(((ViewHolder) holder).newsLogo, new Callback() {
                                            @Override
                                            public void onSuccess() {
//                                                .setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.FILL_PARENT,height));
                                                ((ViewHolder) holder).loading.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {
                                                ((ViewHolder) holder).loading.setVisibility(View.GONE);
                                                Log.v("Picasso", "Could not fetch image");
                                            }
                                        });
                            }

                            ;
                        });
                ((ViewHolder) holder).reload.setVisibility(View.GONE);
                //((ViewHolder) holder).isVisible = true;
            }
       *//*
*/
/*Picasso.with(context)
                    .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                    .error(R.drawable.no_img)
                    .into(((ViewHolder) holder).newsLogo);*//*
*/
/*

        } catch (IllegalArgumentException e) {
            ((ViewHolder) holder).newsLogo.setImageResource(R.drawable.default_img);
        }

        ((ViewHolder) holder).reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ((ViewHolder) holder).reload.setVisibility(View.GONE);
                    ((ViewHolder) holder).loading.bringToFront();
                    ((ViewHolder) holder).loading.setVisibility(View.VISIBLE);
                    Picasso.with(context)
                            .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                            .error(R.drawable.default_img)
                            .networkPolicy(NetworkPolicy.OFFLINE)
                            .into(((ViewHolder) holder).newsLogo, new Callback() {
                                @Override
                                public void onSuccess() {
                                    ((ViewHolder) holder).loading.setVisibility(View.GONE);
                                    ((ViewHolder) holder).reload.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    //Try again online if cache failed
                                    Picasso.with(context)
                                            .load(Uri.encode(news.getLogo_url(), ALLOWED_URI_CHARS))
                                            .error(R.drawable.default_img).into(((ViewHolder) holder).newsLogo, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            ((ViewHolder) holder).reload.setVisibility(View.GONE);
                                            ((ViewHolder) holder).loading.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onError() {
                                            ((ViewHolder) holder).reload.setVisibility(View.GONE);
                                            ((ViewHolder) holder).loading.setVisibility(View.GONE);
                                        }
                                    });

                                }
                            });

                } catch (Exception e) {
                    ((ViewHolder) holder).reload.setVisibility(View.GONE);
                    ((ViewHolder) holder).newsLogo.setImageResource(R.drawable.default_img);
                    ((ViewHolder) holder).loading.setVisibility(View.GONE);
                    ((ViewHolder) holder).newsLogo.bringToFront();
                }
                news.setIsBlocked(-1);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {

            }
        });
        ((ViewHolder) holder).newsLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                goToDetails(newsList.get(position), position, ((ViewHolder) holder));
            }
        });
        ((ViewHolder) holder).newsTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                goToDetails(newsList.get(position), position, ((ViewHolder) holder));
            }
        });
        ((ViewHolder) holder).likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mainControl.checkInternetConnection()) {
                    Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }

                ((ViewHolder) holder).likeBtn.setEnabled(false);
                ((ViewHolder) holder).progressBar.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).likeImg.setVisibility(View.GONE);
                if (news.getLikeID() == 0)
                    likeEvent(((ViewHolder) holder), URLs.getUserID(), position, news);
                else
                    UNLikeEvent(((ViewHolder) holder), position, news);

                *//*
*/
/*pop.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        if (news.getLikeID() == 0)
                            likeEvent(((ViewHolder) holder), URLs.getUserID(), position, news);
                        else
                            UNLikeEvent(((ViewHolder) holder), position, news);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });*//*
*/
/*

            }
        });

        ((ViewHolder) holder).newsFeedSourceHeader.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                        transaction.addToBackStack(null);
                        //
                        transaction.replace(R.id.parent, new SourceNews(news.getSourceID(), true, false, "", false));
                        //transaction.add(R.id.parent, new SourceNews(news.getSourceID(), true, false, ""));
                        transaction.commit();
                    }
                }
        );

        ((ViewHolder) holder).commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = (Integer) v.getTag();
                //commentsState.add(pos, 1);
                pop = AnimationUtils.loadAnimation(context, R.anim.pop);
                ((ViewHolder) holder).commentImg.startAnimation(pop);
                showDialog(((ViewHolder) holder), news, position);
            }
        });

        ((ViewHolder) holder).shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int pos = (Integer) v.getTag();
                //sharesState.add(pos, 1);
                pop = AnimationUtils.loadAnimation(context, R.anim.pop);
                ((ViewHolder) holder).shareImg.startAnimation(pop);
                shareEvent(((ViewHolder) holder), position, news);
            }
        });

        ((ViewHolder) holder).commentsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(((ViewHolder) holder), news, position);
            }
        });

        convertView.setTag(R.id.urgent_label, ((ViewHolder) holder).urgentLabel);
        convertView.setTag(R.id.seperator, ((ViewHolder) holder).seperator);
        convertView.setTag(R.id.rounded_background, ((ViewHolder) holder).roundedBackground);
        convertView.setTag(R.id.like_loading, ((ViewHolder) holder).progressBar);
        convertView.setTag(R.id.like_btn, ((ViewHolder) holder).likeBtn);
        convertView.setTag(R.id.comment_btn, ((ViewHolder) holder).commentBtn);
        convertView.setTag(R.id.share_btn, ((ViewHolder) holder).shareBtn);
        convertView.setTag(R.id.source_title, ((ViewHolder) holder).sourceTitle);
        convertView.setTag(R.id.news_title, ((ViewHolder) holder).newsTime);
        convertView.setTag(R.id.news_time, ((ViewHolder) holder).newsTitle);
        convertView.setTag(R.id.sharing_number, ((ViewHolder) holder).sharingNumbers);
        convertView.setTag(R.id.comments_number, ((ViewHolder) holder).commentsNumber);
        convertView.setTag(R.id.likes_number, ((ViewHolder) holder).likesNumber);
        convertView.setTag(R.id.like_txt, ((ViewHolder) holder).likeTitle);
        convertView.setTag(R.id.comment_txt, ((ViewHolder) holder).commentTitle);
        convertView.setTag(R.id.share_txt, ((ViewHolder) holder).shareTitle);
        convertView.setTag(R.id.source_image, ((ViewHolder) holder).sourceImg);
        convertView.setTag(R.id.like_img, ((ViewHolder) holder).likeImg);
        convertView.setTag(R.id.comment_img, ((ViewHolder) holder).commentImg);
        convertView.setTag(R.id.share_img, ((ViewHolder) holder).shareImg);
        convertView.setTag(R.id.news_logo, ((ViewHolder) holder).newsLogo);
        convertView.setTag(R.id.news_feed_source_header, ((ViewHolder) holder).newsFeedSourceHeader);
        convertView.setTag(R.id.source_news_date_holder, ((ViewHolder) holder).sourceNewsDateHolder);
        convertView.setTag(R.id.source_news_time, ((ViewHolder) holder).sourceNewsTime);
        convertView.setTag(R.id.news_feed_item_holder, ((ViewHolder) holder).cardHolder);
        convertView.setTag(R.id.news_feed_ticked, ((ViewHolder) holder).ticked);
        convertView.setTag(R.id.spinner_ad, ((ViewHolder) holder).spinnerAd);
        convertView.setTag(R.id.mrec_ad, ((ViewHolder) holder).mRecAd);
        convertView.setTag(R.id.ads_container_basma,((ViewHolder) holder).adContainer);
        convertView.setTag(R.id.gif_holder, ((ViewHolder) holder).gifHolder);

        return convertView;
    }*//*



    */
/*
    void showMobFoxNative(final RelativeLayout nativeViewContainer, final View loading)
    {
        final LinearLayout nativeView =  (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.use_native, null);
        // Native aNative;

        Native.setDebug(true);
        loading.setVisibility(View.GONE);
        //assigning xml components to our layout
        final TextView headline = (TextView) nativeView.findViewById(R.id.headline);
        final ImageView nativeIcon = (ImageView) nativeView.findViewById(R.id.nativeIcon);
        final ImageView  nativeMainImg = (ImageView) nativeView.findViewById(R.id.nativeMainImg);
        final RelativeLayout layout = (RelativeLayout) nativeView.findViewById(R.id.nativeLayout);

       // spinnerInvh = (MySpinner) findViewById(R.id.spinnerInvh);

        //spinnerInvh.setAdapter(adapter);






        final Button loadBtn = (Button) nativeView.findViewById(R.id.loadBtn);
        //final loadBtn.setOnClickListener(context);

        Button btnAdd = (Button) nativeView.findViewById(R.id.btnAdd);
       // btnAdd.setOnClickListener(context);

        EditText etvalue = (EditText) nativeView.findViewById(R.id.etvalue);

        TextView tvParams = (TextView) nativeView.findViewById(R.id.tvParams);

        EditText etInvh = (EditText) nativeView.findViewById(R.id.etInvh);


        Native  aNative = new Native(context);

        //params.setParams(new JSONObject());

        //we must set a listener for native

        NativeListener listener = new NativeListener() {


            @Override
            public void onNativeReady(Native aNative, CustomEventNative customEventNative, com.mobfox.sdk.nativeads.NativeAd ad) {
                Toast.makeText(context, "on native ready", Toast.LENGTH_SHORT).show();

                //register custom layout click
                customEventNative.registerViewForInteraction(layout);
                ad.fireTrackers(context);

                headline.setText(ad.getTexts().get(0).getText());

                ad.loadImages(context, new com.mobfox.sdk.nativeads.NativeAd.ImagesLoadedListener() {
                    @Override
                    public void onImagesLoaded(com.mobfox.sdk.nativeads.NativeAd ad) {
                        Toast.makeText(context, "on images ready", Toast.LENGTH_SHORT).show();
                        nativeIcon.setImageBitmap(ad.getImages().get(0).getImg());
                    }
                });
    nativeViewContainer.addView(nativeView);
            }

            @Override
            public void onNativeError(Exception e) {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNativeClick(com.mobfox.sdk.nativeads.NativeAd nativeAd) {

            }



        };

        aNative.setListener(listener);
        aNative.load("80187188f458cfde788d961b6882fd53");
    }
    *//*

      void showAdmobAd(final LinearLayout nativeViewContainer, final View loading){

        GoogleAdMob.loadContentAd(context, 0,adMobMap,nativeViewContainer, adsControl, new AdViewLoadListener() {
            @Override
            public void onAdLoaded() {
                //nativeViewContainer.addView(nativeView);
                loading.setVisibility(View.GONE);

            }

            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdError() {

            }
        });


    }
    void showNativeAd(final LinearLayout nativeViewContainer, final View loading,NativeAd nativeAd)
    {
      */
/*  Appodeal.setAutoCacheNativeIcons(true);
        Appodeal.setAutoCacheNativeMedia(false);


        Appodeal.initialize((Activity) context, AdsControlNabaa.NABAA_ID, Appodeal.NATIVE);
        Appodeal.show((Activity) context, Appodeal.NATIVE);
        Appodeal.setNativeCallbacks(new NativeCallbacks() {
            @Override
            public void onNativeLoaded(List<NativeAd> nativeAds) {*//*

        // Toast.makeText(context, "onNativeLoaded", Toast.LENGTH_SHORT).show();
        loading.setVisibility(View.GONE);
        LinearLayout nativeView =  (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.ad_view, null);
        //   adMobMap.put(""+position,nativeView);
        //  NativeAd nativeAd = nativeAds.get(0);

        TextView nativeAdSign = (TextView) nativeView.findViewById(R.id.sponsored_label);
        // nativeAdSign.setText("Ad");

        TextView nativeTitle = (TextView) nativeView.findViewById(R.id.native_ad_title);
        nativeTitle.setText(nativeAd.getTitle());

        TextView nativeDescription = (TextView) nativeView.findViewById(R.id.native_ad_body);
        nativeDescription.setMaxLines(2);
        nativeDescription.setEllipsize(TextUtils.TruncateAt.END);
        nativeDescription.setText(nativeAd.getDescription());

        RatingBar nativeRating = (RatingBar) nativeView.findViewById(R.id.rating_bar);
               */
/* if (nativeAd.getRating() == 0) {
                    nativeRating.setVisibility(View.GONE);
                } else {
                    nativeRating.setVisibility(View.VISIBLE);
                    nativeRating.setRating(nativeAd.getRating());
                    nativeRating.setIsIndicator(true);
                    nativeRating.setStepSize(0.05f);
                }*//*


        Button nativeCta = (Button) nativeView.findViewById(R.id.native_ad_call_to_action);
        nativeCta.setText(nativeAd.getCallToAction());

        ((ImageView) nativeView.findViewById(R.id.native_ad_icon)).setImageBitmap(nativeAd.getIcon());
        ((ImageView) nativeView.findViewById(R.id.ad_cover)).setImageBitmap(nativeAd.getImage());

        View providerView = nativeAd.getProviderView(context);
        if (providerView != null) {
            FrameLayout providerViewContainer = (FrameLayout) nativeView.findViewById(R.id.native_provider_view);
            providerViewContainer.addView(providerView);
        }

        nativeAd.registerViewForInteraction(nativeView);
        nativeView.setVisibility(View.VISIBLE);
             */
/*   ((ViewHolder) holder).gifHolder.setVisibility(View.GONE);
                ((ViewHolder) holder).spinnerAd.setVisibility(View.GONE);*//*

        //  nativeViewContainer.removeAllViews();
        nativeViewContainer.addView(nativeView);
        nativeView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
               */
/* TextView text=new TextView(context);text.setText("test");
                nativeViewContainer.addView(text);*//*

           */
/* }

            @Override
            public void onNativeFailedToLoad() {
                Toast.makeText(context, "onNativeFailedToLoad", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNativeShown(NativeAd nativeAd)
            {

                     *//*
*/
/*   ((ViewHolder) holder).gifHolder.setVisibility(View.GONE);
                ((ViewHolder) holder).spinnerAd.setVisibility(View.GONE);*//*
*/
/*
                Toast.makeText(context, "onNativeShown", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNativeClicked(NativeAd nativeAd) {

                Toast.makeText(context, "onNativeClicked", Toast.LENGTH_SHORT).show();
            }
        });*//*

    }
  */
/*  public void getBannerAd(final MrecView view, final View loading,final int position) {



      //  Appodeal.show((Activity) context, Appodeal.MREC);


        Appodeal.setMrecCallbacks(new MrecCallbacks() {
            @Override
            public void onMrecLoaded( boolean b) {

               // loading.setVisibility(View.GONE);
                if (position== MREC_AD_POSITION)
                {
                    view.setVisibility(View.VISIBLE);
                    Appodeal.show((Activity) context, Appodeal.MREC);
                    //Appodeal.show((Activity) context, Appodeal.MREC);
                }
                else{
                    Appodeal.hide((Activity) context, Appodeal.MREC);


                }
               // Toast.makeText(context, "onMediumRecShown", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onMrecFailedToLoad() {
                Toast.makeText(context, "onMediumRecFailure", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onMrecShown() {
                if (position== MREC_AD_POSITION)
                {
                    view.setVisibility(View.VISIBLE);
                    //Appodeal.show((Activity) context, Appodeal.MREC);
                }
                else{
                    Appodeal.hide((Activity) context, Appodeal.MREC);


                }
            }

            @Override
            public void onMrecClicked() {

            }
        });


    }*//*



    private void goToDetails(News mNews, int index, BasmaNewsAdapter.ViewHolder  holderM) {
        if (ignoreDisabled) {
            return;
        }
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, R.string.no_internet, Toast.LENGTH_SHORT).show();
            return;
        } else {
            holderM.ticked.setVisibility(View.VISIBLE);
            FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            transaction.addToBackStack(null);
            NewsDetail2 nesDetails = new NewsDetail2(mNews, index);
            //NewsDetail2 nesDetails = new NewsDetail2("-8505763082654550151");
            transaction.add(R.id.parent, nesDetails, "newsDetail");
            transaction.commit();
            nesDetails.onFinishUpdates(new NewsDetail2.OnDetailsUpdate() {
                @Override
                public void onFinished(int index, News obj) {
                    newsList.set(index, obj);
                    notifyDataSetChanged();
                    if (sourceNewsFrg != null) {
                        if (dp.getSourcesBySourceId(obj.getSourceID()).getSelected_or_not() > 0)
                            ((SourceNews) sourceNewsFrg).setSelction();
                        if (dp.getSourcesBySourceId(obj.getSourceID()).getUrgent_notify() > 0)
                            ((SourceNews) sourceNewsFrg).setNotification();
                    }
                }
            });
        }


    }

    private void setViewGone(BasmaNewsAdapter.ViewHolder  holder, News newsObj) {
        */
/*if (newsObj.getLikesNumber() == 0)
            ((ViewHolder) holder).likesView.setVisibility(View.GONE);
        if (newsObj.getCommentsNumber() == 0)
            ((ViewHolder) holder).commentsView.setVisibility(View.GONE);
        if (newsObj.getSharesNumber() == 0)
            ((ViewHolder) holder).sharesView.setVisibility(View.INVISIBLE);

        if (newsObj.getLikesNumber() == 0 && newsObj.getCommentsNumber() == 0 && newsObj.getSharesNumber() == 0) {
            ((ViewHolder) holder).actionsView.setVisibility(View.GONE);
            if (isSourcesNews) {
                ((ViewHolder) holder).seperator.setVisibility(View.GONE);
            }
        }*//*

        if (newsObj.getLikesNumber() == 0)
            ((ViewHolder) holder).likesView.setVisibility(View.GONE);
        if (newsObj.getCommentsNumber() == 0)
            ((ViewHolder) holder).commentsView.setVisibility(View.GONE);
        if (newsObj.getSharesNumber() == 0)
            ((ViewHolder) holder).sharesView.setVisibility(View.INVISIBLE);

        if (newsObj.getLikesNumber() == 0 && newsObj.getCommentsNumber() == 0 && newsObj.getSharesNumber() == 0) {
            ((ViewHolder) holder).actionsView.setVisibility(View.GONE);
            if (isSourcesNews) {
                ((ViewHolder) holder).seperator.setVisibility(View.GONE);
            }
        }
    }

    private void setViewsVisible(BasmaNewsAdapter.ViewHolder  holder, News newsObj) {

        articleIds = dp.getArticlesId();
        for (int i = 0; i < articleIds.size(); i++) {
            if (articleIds.get(i).getArticleId() == newsObj.getID()) {
                fromHistory = true;
                historyObj.setLike_Id(newsObj.getLikeID());
                historyObj.setLikesCount(newsObj.getLikesNumber());
                historyObj.setFavouriteId(newsObj.getFavouriteID());
                historyObj.setShareCount(newsObj.getSharesNumber());
                historyObj.setCommentsCount(newsObj.getCommentsNumber());
                dp.updateArticleById(newsObj.getID(), historyObj);
                break;
            } else {
                fromHistory = false;
            }
//            break;
        }

        if (newsObj.getLikesNumber() > 0 || newsObj.getCommentsNumber() > 0 || newsObj.getSharesNumber() > 0) {
            ((ViewHolder) holder).actionsView.setVisibility(View.VISIBLE);
            if (isSourcesNews)
                ((ViewHolder) holder).seperator.setVisibility(View.VISIBLE);
        }
        if (isHistory)
            ((ViewHolder) holder).deleteIcon.setVisibility(View.VISIBLE);
        else
            ((ViewHolder) holder).deleteIcon.setVisibility(View.GONE);
        if (newsObj.isUrgent())
            ((ViewHolder) holder).urgentLabel.setVisibility(View.VISIBLE);
        else
            ((ViewHolder) holder).urgentLabel.setVisibility(View.GONE);


        if (newsObj.getLikeID() > 0) {
            ((ViewHolder) holder).likeImg.setImageResource(R.drawable.like);
            ((ViewHolder) holder).likeTitle.setTextColor(Color.parseColor("#CC0000"));
        } else {
            ((ViewHolder) holder).likeImg.setImageResource(R.drawable.like_unactive);
            ((ViewHolder) holder).likeTitle.setTextColor(Color.parseColor("#9EA1A8"));
        }
        if (fromHistory) {
            ((ViewHolder) holder).ticked.setVisibility(View.VISIBLE);
            notifyDataSetChanged();
            if (historyObj.getLike_Id() > 0) {
                ((ViewHolder) holder).likeImg.setImageResource(R.drawable.like);
                ((ViewHolder) holder).likeTitle.setTextColor(Color.parseColor("#CC0000"));
            } else {
                ((ViewHolder) holder).likeImg.setImageResource(R.drawable.like_unactive);
                ((ViewHolder) holder).likeTitle.setTextColor(Color.parseColor("#9EA1A8"));
            }
            if (historyObj.getLikesCount() > 0)
                ((ViewHolder) holder).likesView.setVisibility(View.VISIBLE);
            if (historyObj.getCommentsCount() > 0)
                ((ViewHolder) holder).commentsView.setVisibility(View.VISIBLE);
            if (historyObj.getShareCount() > 0)
                ((ViewHolder) holder).sharesView.setVisibility(View.VISIBLE);
            if (historyObj.getShareCount() < 1000)
                ((ViewHolder) holder).sharingNumbers.setText(newsObj.getSharesNumber() + " ");
            else
                ((ViewHolder) holder).sharingNumbers.setText(newsObj.getSharesNumber() / 1000 + "K ");

            if (historyObj.getCommentsCount() < 1000)
                ((ViewHolder) holder).commentsNumber.setText(newsObj.getCommentsNumber() + " ");
            else
                ((ViewHolder) holder).commentsNumber.setText(newsObj.getCommentsNumber() / 1000 + "K ");

            if (historyObj.getLikesCount() < 1000)
                ((ViewHolder) holder).likesNumber.setText(newsObj.getLikesNumber() + " ");
            else
                ((ViewHolder) holder).likesNumber.setText(newsObj.getLikesNumber() / 1000 + "K ");
        } else {
            ((ViewHolder) holder).ticked.setVisibility(View.GONE);
            if (newsObj.getLikesNumber() > 0)
                ((ViewHolder) holder).likesView.setVisibility(View.VISIBLE);
            if (newsObj.getCommentsNumber() > 0)
                ((ViewHolder) holder).commentsView.setVisibility(View.VISIBLE);
            if (newsObj.getSharesNumber() > 0)
                ((ViewHolder) holder).sharesView.setVisibility(View.VISIBLE);

            if (newsObj.getSharesNumber() < 1000)
                ((ViewHolder) holder).sharingNumbers.setText(newsObj.getSharesNumber() + " ");
            else
                ((ViewHolder) holder).sharingNumbers.setText(newsObj.getSharesNumber() / 1000 + "K ");

            if (newsObj.getCommentsNumber() < 1000)
                ((ViewHolder) holder).commentsNumber.setText(newsObj.getCommentsNumber() + " ");
            else
                ((ViewHolder) holder).commentsNumber.setText(newsObj.getCommentsNumber() / 1000 + "K ");

            if (newsObj.getLikesNumber() < 1000)
                ((ViewHolder) holder).likesNumber.setText(newsObj.getLikesNumber() + " ");
            else
                ((ViewHolder) holder).likesNumber.setText(newsObj.getLikesNumber() / 1000 + "K ");
        }
    }
   */
/* private void shareIntent(final Holder ((ViewHolder) holder), int position, final News newsObj) {

        shareEvent(((ViewHolder) holder), position, newsObj);
    }*//*


    private void shareEvent(final BasmaNewsAdapter.ViewHolder   holder, final int position, final News newsObj) {
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        } else {

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, newsObj.getNewsTitle());
            i.putExtra(Intent.EXTRA_TEXT, "http://nabaapp.com/" + newsObj.getID() + "" + " \n " + "حمل تطبيق نبأ الاخباري مجاناً واستمتع " +
                    "بمتابعة العالم لحظة بلحظة" +
                    "\n" + context.getString(R
                    .string.app_domain));
            context.startActivity(Intent.createChooser(i, context.getString(R.string.share)));


            HashMap<String, String> dataObj = new HashMap<>();

            dataObj.put(URLs.TAG_SHARE_ARTICLE_ID, newsObj.getID() + "");
            dataObj.put(URLs.TAG_SHARE_ARTICLE_URL, newsObj.getShareUrl() + "");

            JsonParser jsonParser = new JsonParser(context, URLs.SHARE_URL, URLs.SHARE_REQUEST_TYPE, dataObj);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnSharesUpdatedListener() {
                @Override
                public void onFinished(int sharesCount, String shortLink) {
                    if (sharesCount == -1) {
                        Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_LONG).show();
                        return;
                    }
                    newsObj.setSharesNumber(sharesCount);
                    newsList.set(position, newsObj);
                    if (((ViewHolder) holder).actionsView.getVisibility() == View.GONE) {
                        ((ViewHolder) holder).seperator.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).actionsView.setVisibility(View.VISIBLE);
                    }

                    if (((ViewHolder) holder).sharesView.getVisibility() == View.GONE || ((ViewHolder) holder).sharesView.getVisibility() == View.INVISIBLE)
                        ((ViewHolder) holder).sharesView.setVisibility(View.VISIBLE);

                    ((ViewHolder) holder).sharingNumbers.setText(newsObj.getSharesNumber() + "");

                    for (int i = 0; i < articleIds.size(); i++) {
                        if (articleIds.get(i).getArticleId() == newsObj.getID()) {
                            historyObj.setLike_Id(newsObj.getLikeID());
                            historyObj.setLikesCount(newsObj.getLikesNumber());
                            historyObj.setFavouriteId(newsObj.getFavouriteID());
                            historyObj.setShareCount(newsObj.getSharesNumber());
                            historyObj.setCommentsCount(newsObj.getCommentsNumber());
                            dp.updateArticleById(newsObj.getID(), historyObj);
                        }
                        break;
                    }
//                    historyObj.setLike_Id(newsObj.getLikeID());
//                    historyObj.setLikesCount(newsObj.getLikesNumber());
//                    historyObj.setFavouriteId(newsObj.getFavouriteID());
//                    historyObj.setShareCount(newsObj.getSharesNumber());
//                    dp.updateArticleById(newsObj.getID(), historyObj);

//                    Intent i = new Intent(Intent.ACTION_SEND);
//                    i.setType("text/plain");
////                i.putExtra(Intent.EXTRA_SUBJECT, shortLink + " \n " + "حمل تطبيق نبأ الاخباري مجاناً واستمتع بمتابعة العالم لحظة
/// بلحظة" +
////                        "\n" + "http://nabaapp.com/");
//                    i.putExtra(Intent.EXTRA_TEXT, newsObj.getNewsTitle() + "\n" + shortLink + " \n " + "حمل تطبيق نبأ الاخباري مجاناً
// واستمتع" +
//
//                            " بمتابعة العالم لحظة بلحظة" +
//                            "\n" + "http://nabaapp.com/");
////                Toast.makeText(context, shortLink, Toast.LENGTH_LONG).show();
////                i.putExtra(Intent.EXTRA_TEXT, "Hi Newz World");
//                    context.startActivity(Intent.createChooser(i, context.getString(R.string.share)));
                }
            });
        }
    }

    private void likeEvent(final BasmaNewsAdapter.ViewHolder  holder, int userID, final int position, final News newsObj) {

        */
/*MainControl mainControl = new MainControl(context);
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }*//*


        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_USERID, userID + "");
        dataObj.put(URLs.TAG_REQUEST_ARTICLE_ID, newsObj.getID() + "");
        JsonParser jsonParser = new JsonParser(context, URLs.ADD_LIKE_URL, URLs.ADD_LIKE_REQUEST_TYPE, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();

        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {
            }

            @Override
            public void onFinished(List<Integer> likeResponse) {
                ((ViewHolder) holder).likeBtn.setEnabled(true);
                ((ViewHolder) holder).progressBar.setVisibility(View.GONE);
                ((ViewHolder) holder).likeImg.setVisibility(View.VISIBLE);
                try {
                    if (likeResponse.get(0) > 0) {
                        newsObj.setLikeID(likeResponse.get(0));
                        newsObj.setLikesNumber(likeResponse.get(1));
                        ((ViewHolder) holder).likesNumber.setText(newsObj.getLikesNumber() + " ");

                        for (int i = 0; i < articleIds.size(); i++) {
                            if (articleIds.get(i).getArticleId() == newsObj.getID()) {
                                historyObj.setLike_Id(likeResponse.get(0));
                                historyObj.setLikesCount(newsObj.getLikesNumber());
                                historyObj.setFavouriteId(newsObj.getFavouriteID());
                                historyObj.setShareCount(newsObj.getSharesNumber());
                                historyObj.setCommentsCount(newsObj.getCommentsNumber());
                                dp.updateArticleById(newsObj.getID(), historyObj);
                            }
                            break;
                        }

                        //newsList.set(position, newsObj);
                        setViewsVisible(((ViewHolder) holder), newsObj);
                    }
                } catch (IndexOutOfBoundsException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                } catch (NullPointerException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFinished(News newsObj) {

            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int userID) {

            }

            @Override
            public void onFinished(boolean success) {

            }
        });
    }

    private void UNLikeEvent(final BasmaNewsAdapter.ViewHolder  holder, final int position, final News newsObj) {
        */
/*MainControl mainControl = new MainControl(context);
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (NullPointerException e) {

        }*//*


        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_ADD_LIKE_LIKE_ID, newsObj.getLikeID() + "");

        JsonParser jsonParser = new JsonParser(context, URLs.DELETE_LIKE_URL, URLs.DELETE_LIKE_REQUEST_TYPE, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {
            }

            @Override
            public void onFinished(List<Integer> likeResponse) {
                ((ViewHolder) holder).likeBtn.setEnabled(true);
                ((ViewHolder) holder).progressBar.setVisibility(View.GONE);
                ((ViewHolder) holder).likeImg.setVisibility(View.VISIBLE);
                try {
                    if (likeResponse.get(0) > -1) {
                        newsObj.setLikesNumber(likeResponse.get(0));
                        ((ViewHolder) holder).likesNumber.setText(newsObj.getLikesNumber() + " ");
                        newsObj.setLikesNumber(newsObj.getLikesNumber());
                        newsObj.setLikeID(0);
                        newsList.set(position, newsObj);
                        setViewGone(((ViewHolder) holder), newsObj);
                        setViewsVisible(((ViewHolder) holder), newsObj);
                    }
                } catch (IndexOutOfBoundsException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                } catch (NullPointerException e) {
                    Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFinished(News newsObj) {

            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int userID) {

            }

            @Override
            public void onFinished(boolean success) {

            }
        });
    }

    public void addList(List<News> list) {
        blocked = dp.getAllProfiles().get(0).getBlockImg();
        if (blocked > 0) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(1);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(-1);
            }
        }
        newsList.addAll(0, list);
    }

    public void appendList(List<News> list) {
        blocked = dp.getAllProfiles().get(0).getBlockImg();
        if (blocked > 0) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(1);
            }
        } else {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsBlocked(-1);
            }
        }
        newsList.addAll(list);
    }

    public interface OnListIsEmpty {
        void isEmpty();
    }

   */
/* private static class Holder {
        public LinearLayout sourceNewsDateHolder;
        public FontTextView sourceNewsTime;
        // public ImageView nativeAdIcon;
        //public TextView nativeAdTitle;
        //  public TextView nativeAdBody;
        //        public TextView nativeAdSocialContext;
        // public Button nativeAdCallToAction;
        //        public LinearLayout adView;
        // LinearLayout newsFeedHeader;
        public ExpandableView actionsView;
        public LinearLayout likesView;
        public LinearLayout commentsView;
        public LinearLayout sharesView;
        public LinearLayout likeBtn;
        public LinearLayout commentBtn;
        public LinearLayout shareBtn;
        public TextView sourceTitle;
        public TextView newsTime;
        public TextView newsTitle;
        public TextView sharingNumbers;
        public TextView commentsNumber;
        public TextView likesNumber;
        public TextView likeTitle;
        public TextView commentTitle;
        public TextView shareTitle;
        public ImageView sourceImg;
        public ImageView likeImg;
        public ImageView commentImg;
        public ImageView shareImg;
        public ImageView newsLogo;
        public LinearLayout newsFeedSourceHeader;
        public ProgressBar progressBar;
        MrecView mRecAd;
        //public MediaView nativeAdMedia;
        //        public MediaView nativeAdMedia;
        public LinearLayout roundedBackground;
        public View seperator;
        public FontTextView urgentLabel;
        public ImageView reload;
        public boolean isVisible;
        public GifMovieView loading;
        public View greyView;
        //        public NativeAd nativeAd;
        public RelativeLayout adWithUs;
        public GifMovieView spinnerAd;
        LinearLayout adContainer;
        // public TextView nativeAdSocialContext;
        public RelativeLayout gifHolder;
        // public ImageView adCover;
        public ImageView deleteIcon;
        public LinearLayout cardHolder;
        public RelativeLayout ticked;
        // public RelativeLayout holder_hh;
        //  public ImageView adChoices;
    }*//*


   */
/* private class ShowAdsViews extends AsyncTask<String, Void, String> {
        private final BasmaNewsAdapter.ViewHolder  holder;
        private final int position;
        boolean serviceFlag;

        public ShowAdsViews( BasmaNewsAdapter.ViewHolder  holder, int position) {
             BasmaNewsAdapter.ViewHolder  holder = ((ViewHolder) holder);
            this.position = position;
        }

        @Override
        protected String doInBackground(String... params) {

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
//            showNativeAd(((ViewHolder) holder), currentAdView);View);
        }

        @Override
        protected void onPreExecute() {
            if (position % 4 == 0 && position != 0) {
//                ((ViewHolder) holder).adView.setVisibility(View.VISIBLE);
                serviceFlag = true;
            } else {
//                ((ViewHolder) holder).adView.setVisibility(View.GONE);
                serviceFlag = false;
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }*//*

}

*/
