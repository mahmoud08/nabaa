package com.madarsoft.nabaa.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.firebase.messaging.FirebaseMessaging;
import com.madarsoft.nabaa.GCM.RegisterApp;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.fragments.SourceNews;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Colossus on 14-Jun-17.
 */

public class DefaultSourcesAdapter extends ArrayAdapter {

    private final Context context;
    private final List<Sources> sourcesList;
    private final DataBaseAdapter dp;
    private final Typeface font;
    private final Typeface headerFont;
    private DefaultHolder holder;
    private int lastPosition = -1;
    private boolean isServerResponsed;
    private JsonParser jsonParser;
    private ArrayList<Category> mCategories;


    public DefaultSourcesAdapter(Context context, List<Sources> sourcesList) {
        super(context, R.layout.sources_list_item_v2, sourcesList);
        this.context = context;
        this.sourcesList = sourcesList;
        dp = new DataBaseAdapter(context);
        font = Typeface.createFromAsset(context.getAssets(), "fonts/Hacen_Liner_Screen.ttf");
        headerFont = Typeface.createFromAsset(context.getAssets(), "fonts/Hacen_Liner_Screen_Bd.ttf");
    }


    @Override
    public int getCount() {
        return sourcesList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        holder = new DefaultHolder();
        View rowView;


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.sources_list_item_v2, parent, false);
        } else {
            rowView = convertView;
        }

        final Sources source = sourcesList.get(position);

        holder.title = (TextView) rowView.findViewById(R.id.source_item_name_tv);
        holder.details = (TextView) rowView.findViewById(R.id.source_item_disc_tv);
        holder.numbers = (TextView) rowView.findViewById(R.id.source_item_followers_number_tv);
        holder.sourceImg = (ImageView) rowView.findViewById(R.id.source_item_img);
        holder.selectItem = (ImageView) rowView.findViewById(R.id.source_item_add);
        holder.progressBar = (ProgressBar) rowView.findViewById(R.id.image_loads_progress);
        holder.selectionProgressBar = (ProgressBar) rowView.findViewById(R.id.select_progress);
        holder.selectCell = (RelativeLayout) rowView.findViewById(R.id.sources_item_select);
        holder.preview = (ImageView) rowView.findViewById(R.id.sources_preview);
        holder.sectionHeader = (LinearLayout) rowView.findViewById(R.id.sources_header);
        holder.textHeader = (TextView) rowView.findViewById(R.id.sources_header_tv);
        holder.selectItem.bringToFront();
        holder.title.setText(source.getSource_name());
        holder.details.setText(source.getDetails());
        holder.numbers.setText(source.getNumber_followers() + " " + context.getString(R.string.followers));
        holder.title.setTypeface(headerFont);
        holder.details.setTypeface(font);
        holder.numbers.setTypeface(font);
//        holder.sectionHeader.setVisibility(View.GONE);
        rowView.setTag(holder);
//        if (type == 1)
//            isSub(holder, position);
//        else
        isGeo(holder, position);
        if (source.getSelected_or_not() == 1)
            holder.selectItem.setImageResource(R.drawable.source_news_check);
        else
            holder.selectItem.setImageResource(R.drawable.add);
        Picasso.with(context)
                .load(source.getLogo_url())
                .placeholder(R.anim.progress_animation)
                .error(R.drawable.item_icon)
                .into(holder.sourceImg);

        holder.preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                SourceNews sourceNews = new SourceNews(sourcesList.get(position).getSource_id(), true, false, "", true);
                transaction.add(R.id.parent, sourceNews);
                //transaction.add(R.id.parent, new SourceNews(news.getSourceID(), true, false, ""));
                transaction.commit();

            }
        });

        holder.selectCell.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                //parent.getChildAt(position - ((ListView)parent).getFirstVisiblePosition()).setEnabled(false);
                MainControl mainControl = new MainControl(context);
                if (!mainControl.checkInternetConnection()) {
                    Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (lastPosition == position && !isServerResponsed)
                    return;
                lastPosition = position;
                isServerResponsed = false;
                holder.selectItem.setVisibility(View.INVISIBLE);
                holder.selectionProgressBar.setVisibility(View.VISIBLE);
                if (sourcesList.get(position).getSelected_or_not() == 1) {
                    //change it on the server
                    HashMap<String, String> dataObj = new HashMap<>();
                    dataObj.put(URLs.TAG_ADD_SOURCE_USERID, URLs.getUserID() + "");
                    dataObj.put(URLs.TAG_SOURCE_SOURCEID, sourcesList.get(position).getSource_id() + "");
                    jsonParser = new JsonParser(context, URLs.DELETE_SOURCE_URL, URLs.SOURCE_DELETE_REQUEST_TYPE, dataObj);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    else
                        jsonParser.execute();
                    jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                        @Override
                        public void onFinished() {
                        }

                        @Override
                        public void onFinished(List<Integer> likeResponse) {
                        }

                        @Override
                        public void onFinished(News newsObj) {
                        }

                        @Override
                        public void onFinished(ArrayList<Sources> sourceList) {
                        }

                        @Override
                        public void onFinished(int userID) {
                        }

                        @Override
                        public void onFinished(final boolean success) {
                            MainActivity activity = (MainActivity) context;
                            activity.runOnUiThread(new Runnable() {
                                public void run() {
                                    isServerResponsed = true;
                                    holder.selectItem.setVisibility(View.VISIBLE);
                                    holder.selectionProgressBar.setVisibility(View.GONE);
                                    if (success) {
                                        holder.selectItem.setImageResource(R.drawable.add);
                                        source.setSelected_or_not(0);
                                        source.setNumber_followers(source.getNumber_followers() - 1);
                                        FirebaseMessaging.getInstance().unsubscribeFromTopic(source
                                                .getSource_id() + "U");
                                        new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, source
                                                .getSource_id() + "");
                                        source.setUrgent_notify(-1);

//                                        dp.updateSelected(source.getSource_id(), source.getNumber_followers() - 1, 0);
                                        dp.updateSources(source);
                                        FirebaseMessaging.getInstance().unsubscribeFromTopic(source.getSource_id() + "");
//                                        new UrgentSubscription().execute(source.getSource_id() + "");
                                        holder.numbers.setText(source.getNumber_followers() + " " + context.getString(R.string.followers));
                                        try {
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "recentNews");
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "hotNews");
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "urgentNews");
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else
                                        Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                } else {
                    //change it on the server
                    HashMap<String, String> dataObj = new HashMap<String, String>();
                    dataObj.put(URLs.TAG_ADD_SOURCE_USERID, URLs.getUserID() + "");
                    dataObj.put(URLs.TAG_SOURCE_SOURCEID, sourcesList.get(position).getSource_id() + "");
                    jsonParser = new JsonParser(context, URLs.ADD_SOURCE_URL, URLs.SOURCE_ADD_REQUEST_TYPE, dataObj);
                    //change it on locale
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    else
                        jsonParser.execute();
                    jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                        @Override
                        public void onFinished() {
                        }

                        @Override
                        public void onFinished(List<Integer> likeResponse) {
                        }

                        @Override
                        public void onFinished(News newsObj) {

                        }

                        @Override
                        public void onFinished(ArrayList<Sources> sourceList) {

                        }

                        @Override
                        public void onFinished(int userID) {

                        }

                        @Override
                        public void onFinished(final boolean success) {
                            MainActivity activity = (MainActivity) context;
                            activity.runOnUiThread(new Runnable() {
                                public void run() {
                                    isServerResponsed = true;
                                    holder.selectItem.setVisibility(View.VISIBLE);
                                    holder.selectionProgressBar.setVisibility(View.GONE);
                                    if (success) {
                                        holder.selectItem.setImageResource(R.drawable.source_news_check);
                                        source.setSelected_or_not(1);
                                        source.setNumber_followers(source.getNumber_followers() + 1);
//                                        dp.updateSelected(source.getSource_id(), source.getNumber_followers() + 1, 1);
                                        source.setUrgent_notify(1);
                                        dp.updateSources(source);

                                        //FirebaseMessaging.getInstance().subscribeToTopic("Urgent");
                                        FirebaseMessaging.getInstance().subscribeToTopic(source.getSource_id() + "U");
//                                        isRemoveFromSub = false;
//                                        new UrgentSubscription().execute(source.getSource_id() + "u");
                                        holder.numbers.setText(source.getNumber_followers() + " " + context.getString(R.string.followers));
                                        try {
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "recentNews");
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "hotNews");
                                            MainControl.writeCacheFile(context, new ArrayList<News>(), "urgentNews");
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else
                                        Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }
            }
        });

        rowView.setTag(R.id.source_item_name_tv, holder.title);
        rowView.setTag(R.id.source_item_disc_tv, holder.details);
        rowView.setTag(R.id.source_item_followers_number_tv, holder.numbers);
        rowView.setTag(R.id.source_item_img, holder.sourceImg);
        rowView.setTag(R.id.source_item_add, holder.selectItem);
        rowView.setTag(R.id.sources_header, holder.sectionHeader);
        rowView.setTag(R.id.sources_header_tv, holder.textHeader);
        return rowView;
    }

    private void isGeo(DefaultHolder myResourcesHolder, int position) {
        try {
            if (position == 0) {
                myResourcesHolder.sectionHeader.setVisibility(View.VISIBLE);
                myResourcesHolder.textHeader.setText(getCatNameByGeoId(sourcesList.get(position).getGeo_id()));
            }
        } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews0", "exception", e);
        }
        try {

            if (getCatNameByGeoId(sourcesList.get(position).getGeo_id()).equalsIgnoreCase(getCatNameByGeoId(sourcesList.get(position - 1)
                    .getGeo_id()))) {
                myResourcesHolder.sectionHeader.setVisibility(View.GONE);
            }
        } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews1", "exception", e);
        }
        try {
            if (!getCatNameByGeoId(sourcesList.get(position).getGeo_id()).equalsIgnoreCase(getCatNameByGeoId(sourcesList.get(position - 1)
                    .getGeo_id()))) {
                myResourcesHolder.sectionHeader.setVisibility(View.VISIBLE);
                myResourcesHolder.textHeader.setText(getCatNameByGeoId(sourcesList.get(position).getGeo_id
                        ()));

            }
        } catch (IndexOutOfBoundsException e) {
//            Log.e("HiNews2", "exception", e);
        }
    }

    String getCatNameBySub(int cat) {
        mCategories = dp.getCategoryNameBySubId(cat);
        if (cat == 0) {
            return context.getString(R.string.generalSource);
        }
//        if (db.getCategoryNameBySubId(cat).get(0).getCategory_name().equalsIgnoreCase("عالمية")) {
//            return "أندية عالميه";
//        }
        else
            return mCategories.get(0).getCategory_name();

    }

    String getCatNameByGeoId(int cat) {
        mCategories = dp.getCategoryNamebyGeoId(cat);
        if (cat == 0) {
            return context.getString(R.string.generalSource);
//        } else if (dp.getCategoryNamebyGeoId(cat).get(0).getCategory_name().equalsIgnoreCase(" عالمية") && catObj.getCategory_name()
//                .toString().equalsIgnoreCase("رياضة")) {
//            return "أندية عالميه";
        } else {
            return mCategories.get(0).getCategory_name();
        }
    }

    public class DefaultHolder {
        TextView title;
        TextView details;
        TextView numbers;
        ImageView sourceImg;
        ImageView selectItem;
        ProgressBar progressBar;
        ProgressBar selectionProgressBar;
        RelativeLayout selectCell;
        ImageView preview;
        LinearLayout sectionHeader;
        TextView textHeader;
    }

    class NotificationSubscription extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final GcmPubSub pubSub = GcmPubSub.getInstance(context);
            final SharedPreferences prefs = context.getSharedPreferences(MainActivity.MyPREFERENCES,
                    Context.MODE_PRIVATE);
            String token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
            try {
                pubSub.unsubscribe(token, "/topics/" + params[0]);
                Log.d("topicGCM ", "Subscribed");
            } catch (IOException e) {

            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals("Executed")) {

            }
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }
    }

}
