package com.madarsoft.nabaa.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.entities.Comments;
import com.squareup.picasso.Picasso;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CommentsAdapter extends ArrayAdapter {

    public List<Comments> comments;
    Context context;
    private LayoutInflater inflater;
    private CommentHolder commentHolder;
    private MainControl mainControl;
    Locale locale;

    SimpleDateFormat print;

    public CommentsAdapter(Context context, int resource, List<Comments> comments) {

        super(context, resource, comments);
        this.context = context;
        this.comments = comments;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mainControl = new MainControl(context);
        locale = new Locale("ar", "SA");
        print = new SimpleDateFormat("hh:mm a dd-MMMM-yyyy ", locale);

    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        try {
            return comments.size();
        } catch (NullPointerException e) {
            return 0;
        }

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        commentHolder = new CommentHolder();
        View rowView;

        if (convertView == null) {
            rowView = inflater.inflate(R.layout.comment_list_item, parent, false);
            commentHolder.userImg = (ImageView) rowView.findViewById(R.id.comment_user_pic);
            commentHolder.commentName = (FontTextView) rowView.findViewById(R.id.comment_name);
            commentHolder.commentText = (FontTextView) rowView.findViewById(R.id.comment_title);
            commentHolder.commentTime = (FontTextView) rowView.findViewById(R.id.comment_time);
//            commentHolder.edit = (FontTextView) rowView.findViewById(R.id.comment_edit);
//            commentHolder.delete = (FontTextView) rowView.findViewById(R.id.comment_delete);
//            commentHolder.editDelHolder = (LinearLayout) rowView.findViewById(R.id.comment_edit_del_holder);
            commentHolder.holder = (RelativeLayout) rowView.findViewById(R.id.comment_holder);
        } else {
            rowView = convertView;
            commentHolder = (CommentHolder) rowView
                    .getTag();
        }

        Picasso.with(context)
                .load(comments.get(position).getUserImg())
                .placeholder(R.anim.progress_animation)
                .error(R.drawable.profile)
                .into(commentHolder.userImg);

        commentHolder.commentName.setText(comments.get(position).getUserName() == "null" ? "محمد احمد" : comments.get(position).getUserName());
        commentHolder.commentText.setText(comments.get(position).getCommentText());

        try {
            Calendar cal = mainControl.setTimeZone(comments.get(position).getCommentDate(), comments.get(position).getTimeOffset());

            commentHolder.commentTime.setText(print.format(cal.getTime()));
        } catch (NumberFormatException e) {
        }
        rowView.setTag(commentHolder);

        return rowView;
    }

    public class CommentHolder {
        FontTextView commentText;
        FontTextView commentName;
        FontTextView commentTime;
        ImageView userImg;
        LinearLayout editDelHolder;
        FontTextView edit;
        FontTextView delete;
        View transparent;
        RelativeLayout holder;
    }


}
