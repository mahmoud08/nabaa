package com.madarsoft.nabaa.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.FontTextView;
import com.madarsoft.nabaa.controls.MainControl;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.fragments.NewsDetail2;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Colossus on 22-Jun-17.
 */

public class NewsSearchAdapter extends ArrayAdapter<News> {

    private final List<News> newsList;
    private final Context context;
    private final DataBaseAdapter dp;
    private final News news;
    private final MainControl mainControl;
    private final Locale locale;
    private final SimpleDateFormat print2;
    private ImageView newsImg;
    private FontTextView title;
    private FontTextView time;
    private ImageView navigateDetailts;

    public NewsSearchAdapter(Context context, int resource, List<News> newsList) {
        super(context, resource, newsList);
        this.context = context;
        this.newsList = newsList;
        dp = new DataBaseAdapter(context);
        mainControl = new MainControl(context);
        locale = new Locale("ar", "SA");
        print2 = new SimpleDateFormat("hh:mm a dd-MMMM-yyyy ", locale);
        news = new News();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.search_news_item, null);
        newsImg = (ImageView) convertView.findViewById(R.id.search_img);
        title = (FontTextView) convertView.findViewById(R.id.search_title);
        time = (FontTextView) convertView.findViewById(R.id.search_time);
        navigateDetailts = (ImageView) convertView.findViewById(R.id.search_go);
        try {
            Picasso.with(context)
                    .load(newsList.get(position).getSourceLogoUrl())
                    .placeholder(R.anim.progress_animation)
                    .error(R.drawable.default_img)
                    .into(newsImg);
        } catch (Exception e) {
            e.printStackTrace();
        }

        title.setText(newsList.get(position).getNewsTitle());
        title.setEllipsize(TextUtils.TruncateAt.END);
        title.setMaxLines(2);
        if (newsList.get(position).getIsRTL().equalsIgnoreCase("false"))
            title.setGravity(Gravity.LEFT);
        else
            title.setGravity(Gravity.RIGHT);

        try {
            Calendar cal = mainControl.setTimeZone(Long.parseLong(newsList.get(position).getArticleDate()), newsList.get(position)
                    .getTimeOffset());

            time.setText(context.getString(R.string.updated) + " " + print2.format(cal.getTime()));
            time.setText(context.getString(R.string.updated) + " " + print2.format(cal.getTime()));

        } catch (Exception e) {
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                transaction.addToBackStack(null);
                NewsDetail2 nesDetails = new NewsDetail2(newsList.get(position), 0);
                //NewsDetail2 nesDetails = new NewsDetail2("-8505763082654550151");
                transaction.add(R.id.parent, nesDetails, "newsDetail");
                transaction.commit();
            }
        });

        return convertView;
    }
}
