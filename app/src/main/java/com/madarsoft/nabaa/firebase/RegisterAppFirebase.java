package com.madarsoft.nabaa.firebase;

/**
 * Created by eng:karim on 6/2/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.controls.JsonParser;
import com.madarsoft.nabaa.entities.URLs;

import java.util.HashMap;

import static com.facebook.FacebookSdk.getApplicationContext;

public class RegisterAppFirebase {

    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String TAG = "FCMRelated";
    private static final String TOPIC = "new_global";
    Context context;
    private int appVersion;
    private String token;
    private SharedPreferences sharedpreferences;
    private FirebaseAnalytics mFirebaseAnalytics;
// ...

    // Obtain the FirebaseAnalytics instance.
    public RegisterAppFirebase(Context ctx) {
        this.context = ctx;
        this.appVersion = appVersion;
        registerApp();
    }

    public static String getRegistrationToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(MainActivity.PREFS_NAME, Context.MODE_PRIVATE);
        return preferences.getString(PROPERTY_REG_ID, "");
    }

//        } else {
//            Log.i("FIREBASE", "No valid Google Play Services APK found.");
//        }

    public void registerApp() {
        String mssg = "";
        //eiJH0PaQ0jo:APA91bF8M_nUvU5vAMJlZtLqG9qjkL2AHa6tYOamEq-KA_TkIm2MhExHxE8ippKkVACvTLeLDQtPyPL6iMwcpJJRrjtSzEGCacEZttTinPDUzOq2M42mSC1BPK_ocYKgLZdXz0qOxHz6
        //chRm8Ch2OvE:APA91bHHZUAM3gMQmzKY3uHR2e9-63n5ioF25_VbmWMey9PLyRyf18BZ3m2v6etVwNX6UgA4ZtTXRkhOVq2_a3WUd9UmI3ISXdRYooQLvvSTYNXvfjg6cqc9TluJW9dW-ExOTTK4D9Ui
        token = FirebaseInstanceId.getInstance().getToken();

        // Log and toast
        mssg = context.getString(R.string.msg_subscribed);
        Log.d("FIREBASE", mssg);
        mssg = "Device registered, registration ID=" + token;
        //sendRegistrationIdToBackend();
//        FirebaseMessaging.getInstance().subscribeToTopic("1U");
        try {
            FCMToServer(token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        storeRegistrationId(context, token);
        subscribeTopics(token);
    }

//    public void sendRegistrationIdToBackend() {

//        StringBuilder builder = new StringBuilder();
//        URL url = null;
//        try {
//            //
//            String urll = GCMConfig.SERVER_URL + "platform=2&programid="+GCMConfig.PROGRAM_ID+"&devicetoken=" + regid + "&udid=" +
// getDeviceId();
//            url = new URL(urll);
//            System.out.println(url);
//            Log.d("registerUrl", url.getPath());
//            Log.d("registerUrl", url.getPath());
//            Log.d("registerUrl", url.getPath());
//            HttpURLConnection http = (HttpURLConnection) url.openConnection();
//            http.setRequestMethod("GET");
//            InputStream is = http.getInputStream();
//            BufferedReader br = new BufferedReader(new InputStreamReader(is));
//            String line;
//            while ((line = br.readLine()) != null) {
//                builder.append(line);
//            }
//            http.disconnect();
//        } catch (MalformedURLException e) {
//            Log.e("PUSH", e.toString());
//        } catch (ProtocolException e) {
//            Log.e("PUSH", e.toString());
//        } catch (IOException e) {
//            Log.e("PUSH", e.toString());
//        }
//
//
//    }

    private void storeRegistrationId(Context ctx, String regid) {
        final SharedPreferences prefs = ctx.getSharedPreferences(MainActivity.PREFS_NAME,
                Context.MODE_PRIVATE);
//        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regid);
//        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private void subscribeTopics(String token) {
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC);
        FirebaseMessaging.getInstance().unsubscribeFromTopic("global");
     // FirebaseMessaging.getInstance().subscribeToTopic("ma7moud");
        //FirebaseMessaging.getInstance().subscribeToTopic("global");
        //[END subscribe_topics]
    }

    private int getMcc() {
        int mcc = 0;
        TelephonyManager tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String networkOperator = tel.getNetworkOperator();
        String countryCode = tel.getSimCountryIso();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        sharedpreferences = context.getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        mFirebaseAnalytics.setUserProperty("country", countryCode);

        editor.putString("ISO", countryCode).apply();

        if (!TextUtils.isEmpty(networkOperator)) {
            mcc = Integer.parseInt(networkOperator.substring(0, 3));
            int mnc = Integer.parseInt(networkOperator.substring(3));
        }
        return mcc;
    }

    private void FCMToServer(String regId) {
        HashMap<String, String> dataObj = new HashMap<>();
        dataObj.put(URLs.TAG_USERID, URLs.getUserID() + "");
        dataObj.put(URLs.TAG_REG_ID, regId + "");
        dataObj.put(URLs.TAG_MCC, getMcc() + "");
        JsonParser jsonParser = new JsonParser(getApplicationContext(), URLs.GCM_TO_SERVER, URLs.GET_GCM_TO_SERVER_TYPE, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();

        jsonParser.onFinishedGCM(new JsonParser.OnFinishGCM() {
            @Override
            public void onFinished(boolean success) {
                if (success) {
//                    SharedPreferences.Editor editor = gcmPrefs.edit();
//                    editor.putBoolean(GCM_RESPONSE, success).apply();
//                    Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.success), Toast.LENGTH_SHORT)
// .show();
                } else {
//                    SharedPreferences.Editor editor = gcmPrefs.edit();
//                    editor.putBoolean(GCM_RESPONSE, success).apply();
//                    Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public String getDeviceId() {
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("UNIQE", android_id);
        return android_id;
    }
}
