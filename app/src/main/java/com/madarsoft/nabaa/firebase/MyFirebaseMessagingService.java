/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.madarsoft.nabaa.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.CustomWebView;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.activities.Splash;

import java.util.Map;

//import com.madarsoft.nabaa.GCM.GCMIntentService;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final int NOTIFICATION_ID = 1;
    public static final String URL = "URL";
    public static final String WEB_VIEW_TITLE = "title";
    public static final String PAGE_ID = "pageId";
    public static final String COUNT_SHARED = "count_shared";
    public static final String FOLLOW = "follow";
    public static final String SOURCE = "source";
    public static final String CATEGORY = "category";
    private static final String TAG = "MyFirebaseMsgService";
    private static final String TYPE = "type";
    private static final String TYPE_REDIRECT = "redirect";
    private static final String TYPE_WEB_VIEW = "webView";
    private static final String TYPE_NORMAL = "normal";
    private static final String TYPE_UPDATE = "update";
    private static final String TYPE_ID = "id";
    private static final String MESSAGE = "message";
    private static final String TYPE_INNER = "inner";
    private static final String PAGE = "page";
    private static final String ID = "id";
    public static final String COUNTRY = "country";

    private NotificationManager mNotificationManager;
    private Intent myIntent;
    private RemoteMessage remoteMessage;
    private String msg;
    private NotificationCompat.Builder mBuilder;
    private SharedPreferences countSharedPref;
    private int myCount;
    private SharedPreferences sharedpreferences;


    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        Map<String, String> data = remoteMessage.getData();
        if (data.size() > 0) {
            Log.d(TAG, "Message data payload: " + data);
            String type = data.get(TYPE);
            msg = data.get(MESSAGE);
            if (msg != null) {
                if (!msg.contains("Send error:") || !msg.contains("Deleted messages on server:"))
                    try {
                        switch (type) {
                            case TYPE_REDIRECT:
                                showNotificationRedirectURL(data.get(URL));
                                break;
                            case TYPE_WEB_VIEW:
                                showNotificationWebViewURL(data.get(URL), data.get(WEB_VIEW_TITLE));
                                break;
                            case TYPE_NORMAL:
                                sendNotification();
                                break;
                            case TYPE_UPDATE:
                                sendNotification();
                                break;
                            case TYPE_ID:
                                sendPageNoNotification(data.get(TYPE_ID));
                                break;
                            case TYPE_INNER:
                                openSpecificScreen(data.get("page"), data.get("id"));
                                break;
                            default:
                                sendNotification();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }

        }
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    private void openSpecificScreen(String page, String id) {
        switch (page) {
            case FOLLOW:
                myIntent = new Intent(getApplicationContext(), Splash.class);
                myIntent.putExtra(PAGE, FOLLOW);
                showTheNotification();
                break;
            case SOURCE:
                myIntent = new Intent(getApplicationContext(), Splash.class);
                Bundle bundle = new Bundle();
                bundle.putString("page", page);
                bundle.putString("id", id);
//                i.putExtras(bundle);
//                myIntent.putExtra(PAGE, SOURCE);
//                myIntent.putExtra(ID,id );
                myIntent.putExtras(bundle);
                showTheNotification();
                break;
            case CATEGORY:
                myIntent = new Intent(getApplicationContext(), Splash.class);
                Bundle bundle1 = new Bundle();
                bundle1.putString("page", page);
                bundle1.putString("id", id);
//                i.putExtras(bundle);
//                myIntent.putExtra(PAGE, SOURCE);
//                myIntent.putExtra(ID,id );
                myIntent.putExtras(bundle1);
                showTheNotification();
                break;
            case COUNTRY:
                myIntent = new Intent(getApplicationContext(), Splash.class);
                Bundle bundle2= new Bundle();
                bundle2.putString("page", page);
                bundle2.putString("id", id);
//                i.putExtras(bundle);
//                myIntent.putExtra(PAGE, SOURCE);
//                myIntent.putExtra(ID,id );
                myIntent.putExtras(bundle2);
                showTheNotification();
                break;
            default:
                break;
        }

    }

    private void showNotificationWebViewURL(String url, String title) {
        myIntent = new Intent(getApplicationContext(), CustomWebView.class);
        myIntent.putExtra(URL, url);
        myIntent.putExtra(WEB_VIEW_TITLE, title);
        showTheNotification();
    }

    private void showNotificationRedirectURL(String url) {
        myIntent = new Intent(Intent.ACTION_VIEW);
        myIntent.setData(Uri.parse(url));
        showTheNotification();
    }

    private void sendPageNoNotification(String page) {
        myIntent = new Intent(getApplicationContext(), Splash.class);
        myIntent.putExtra(ID, page);
        showTheNotification();
    }

    public void sendNotification() {
        if (!MainActivity.active)
            showTheNotification();
        else {
//            sendBroadcast(new Intent().setAction(ConstVLS.LIVE_NOTIFICATION_POPUP_).putExtra(ConstVLS.LIVE_NOTIFICATION_POPUP_MESSAGE,
//                    msg));
        }
    }

    private void showTheNotification() {
        if (myIntent == null) {
            myIntent = new Intent(getApplicationContext(), MainActivity.class);
        }
//        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, myIntent,
//                PendingIntent.FLAG_UPDATE_CURRENT);
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//        mBuilder = new NotificationCompat.Builder(getApplicationContext());
//        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
//        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            mBuilder.setSmallIcon(R.drawable.logo4).setLargeIcon(largeIcon);
//        } else {
//            mBuilder.setSmallIcon(R.drawable.logo).setLargeIcon(largeIcon);
//        }
//        mBuilder.setContentTitle(getResources().getString(R.string.app_name)).setSmallIcon(R.drawable.logo)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).setContentText(msg)
//                .setContentIntent(contentIntent).setWhen(System.currentTimeMillis()).setAutoCancel(true);
//        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        sharedpreferences = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        countSharedPref = getSharedPreferences(MyFirebaseMessagingService.COUNT_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = countSharedPref.edit();


        boolean isInForegroundMode = sharedpreferences.getBoolean("mIsInForegroundMode", true);
        if (isInForegroundMode)
            return;
//        int soundType = dataBaseAdapter.getAllProfiles().get(0).getSoundType();
//        int urgentFlag = dataBaseAdapter.getAllProfiles().get(0).getUrgentFlag();
//        if (soundType < 0 && urgentFlag < 0) {
//            return;
//        }
        myCount = countSharedPref.getInt("Notify_count", 0);

//        Random r = new Random();
//        int i1 = r.nextInt();
//        mNotificationManager.notify(myCount, mBuilder.build());
//        editor.putInt("Notify_count", myCount + 1).apply();
        if (myCount > 40) {
            myCount = 0;
            editor.putInt("Notify_count", myCount).apply();

            for (int i = 0; i < 40; i++) {
                mNotificationManager.cancel(i);
            }
            myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            int requestID = (int) System.currentTimeMillis();
//            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), requestID, myIntent,
//                    0);
//            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
            PendingIntent mContentIntent = PendingIntent.getActivity(getApplicationContext(), requestID, new Intent(getApplicationContext
                            (), Splash.class),
                    0);
            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                    .setContentTitle(getResources().getString(R.string.app_name));
//                        setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).
//        setContentText(msg).
//                            setContentIntent(mContentIntent).setWhen(System.currentTimeMillis()).setAutoCancel(true);
            mBuilder.setColor(Color.parseColor("#CC0000"));
            NotificationCompat.BigTextStyle message = new NotificationCompat.BigTextStyle();
            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

            message.bigText(msg);
            mBuilder.setStyle(message);

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBuilder.setSmallIcon(R.drawable.logo4).setLargeIcon(largeIcon);
            } else {
                mBuilder.setSmallIcon(R.drawable.logo).setLargeIcon(largeIcon);
            }

            mBuilder.setContentText(getApplicationContext().getString(R.string.you_exceeded_normal_number)).
                    setContentIntent(mContentIntent).setWhen(System.currentTimeMillis()).setAutoCancel(true);
            mNotificationManager.notify(myCount, mBuilder.build());
            editor.putInt("Notify_count", myCount + 1).apply();
        } else {
            myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            int requestID = (int) System.currentTimeMillis();
//            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), requestID, myIntent,
//                    0);
//            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
            PendingIntent mContentIntent = PendingIntent.getActivity(getApplicationContext(), requestID, myIntent,
                    0);
            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                    .setContentTitle(getResources().getString(R.string.app_name)).setPriority(Notification.PRIORITY_MAX).
//                        setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).
        setContentText(msg).
                            setContentIntent(mContentIntent).setWhen(System.currentTimeMillis()).setAutoCancel(true);
            mBuilder.setColor(Color.parseColor("#CC0000"));
            NotificationCompat.BigTextStyle message = new NotificationCompat.BigTextStyle();
            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

            message.bigText(msg);
            mBuilder.setStyle(message);

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBuilder.setSmallIcon(R.drawable.logo4).setLargeIcon(largeIcon);
            } else {
                mBuilder.setSmallIcon(R.drawable.logo).setLargeIcon(largeIcon);
            }
            mNotificationManager.notify(myCount, mBuilder.build());
            editor.putInt("Notify_count", myCount + 1).apply();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        stopSelf();
    }


}