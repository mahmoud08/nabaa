package com.madarsoft.nabaa.controls;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.madarsoft.nabaa.interfaces.AdViewLoadListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Eng-Basma
 * use this class to get ads of content type admob
 * Note that here type admob means "google admob - flurry - mopud - mellinnial media"
 * Google mediation is totaly responsible for choosing which ad to be displayed
 *
 *
 */
public class GoogleAdMob  {






	public static void getBannerAd(Context context, final ViewGroup adContainer, String bannerAdUnit, final AdViewLoadListener adListener) {
		// TODO Auto-generated method stub


		final AdView adView = new AdView(context);

		adView.setAdSize(AdSize.BANNER);
		adView.setAdUnitId(bannerAdUnit);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {

			@Override
			public void onAdClosed() {
				// TODO Auto-generated method stub
				super.onAdClosed();
				Log.d("banner ad", "closed");
			//	tracker.sendEventMadar(GOOGLE_ADMOB_BANNER_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_CLOSE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_BANNER_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_VALUE_CLOSE);
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				// TODO Auto-generated method stub
				super.onAdFailedToLoad(errorCode);
				Log.d("banner ad", ""+errorCode);
			//	tracker.sendEventMadar(GOOGLE_ADMOB_BANNER_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_ERROR, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_BANNER_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_ERROR);

			}

			@Override
			public void onAdLeftApplication() {
				// TODO Auto-generated method stub
				super.onAdLeftApplication();
				Log.d("banner ad", "LeftApplication");
			}

			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
				adContainer.removeAllViews();

				Log.d("banner ad", "AdLoaded");
				adContainer.addView(adView);
				//tracker.sendEventMadar(GOOGLE_ADMOB_BANNER_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_DISPLAY, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_BANNER_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_VALUE_DISPLAY);
				if (adListener!=null)
				{
					adListener.onAdLoaded();
				}


			}

			@Override
			public void onAdOpened() {
				// TODO Auto-generated method stub
				super.onAdOpened();
				Log.d("banner ad", "AdOpened");
				//tracker.sendEventMadar(GOOGLE_ADMOB_BANNER_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_DISPLAY, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_BANNER_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_VALUE_CLICK);

			}
		});



		//return adView;
	}

/*
	@Override
	public void loadSplashAd() {
		// TODO Auto-generated method stub
		final InterstitialAd interstitial =new InterstitialAd(context);


		// Create ad request.
		AdRequest adRequest = new AdRequest.Builder().build();


		// Begin loading your interstitial.
		interstitial.setAdUnitId(req.getGoogleSplashAdUnitId());
		interstitial.loadAd(adRequest);


		interstitial.setAdListener(new AdListener() {

			@Override
			public void onAdClosed() {
				// TODO Auto-generated method stub
				super.onAdClosed();
				Log.d("intertitial ad", "closed");
				//tracker.sendEventMadar(GOOGLE_ADMOB_SPLASH_VIEW, MadarsoftAdsRequest.GOOGLE_ANALYTIC_CAT, MadarsoftAdsRequest.GOOGLE_ANALYTIC_ACTION_CLOSE, MadarsoftAdsRequest.GOOGLE_ANALYTIC_GOOGLE_ADMOB_SPLASH_LABLE, MadarsoftAdsRequest.GOOGLE_ANALYTIC_VALUE_CLOSE);

			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				// TODO Auto-generated method stub
				super.onAdFailedToLoad(errorCode);
				Log.d("intertitial ad", ""+errorCode);
				tracker.sendEventMadar(GOOGLE_ADMOB_SPLASH_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_ERROR, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_SPLASH_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_ERROR);


			}

			@Override
			public void onAdLeftApplication() {
				// TODO Auto-generated method stub
				super.onAdLeftApplication();
				Log.d("intertitial ad", "LeftApplication");
			}

			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
				Log.d("intertitial ad", "AdLoaded");
				interstitial.show();
				tracker.sendEventMadar(GOOGLE_ADMOB_SPLASH_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_DISPLAY, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_SPLASH_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_VALUE_DISPLAY);


			}

			@Override
			public void onAdOpened() {
				// TODO Auto-generated method stub
				super.onAdOpened();
				Log.d("intertitial ad", "AdOpened");
				tracker.sendEventMadar(GOOGLE_ADMOB_SPLASH_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_DISPLAY, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_SPLASH_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_VALUE_CLICK);

			}

		});
	}
*/

/*
    public static AdView loadContentAd(Context context, final ViewGroup adContainer,final  AdsControlNabaa adsControlNabaa, final AdViewLoadListener adListener) {
        final AdView adView = new AdView(context);
        AdSize customAdSize = new AdSize(((ViewGroup)adContainer.getParent()).getWidth(), ((ViewGroup)adContainer.getParent()).getWidth());

        adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
        adView.setAdUnitId(adsControlNabaa.getGoogleAdunit());

        AdRequest adRequest = new AdRequest.Builder().build();

        adView.loadAd(adRequest);

        adView.setAdListener(new AdListener() {

            @Override
            public void onAdClosed() {
                // TODO Auto-generated method stub
                super.onAdClosed();
                Log.d("banner ad", "closed");
                //tracker.sendEventMadar(GOOGLE_ADMOB_BANNER_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_CLOSE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_BANNER_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_VALUE_CLOSE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // TODO Auto-generated method stub
                super.onAdFailedToLoad(errorCode);
                Log.d("banner ad", ""+errorCode);

                //tracker.sendEventMadar(GOOGLE_ADMOB_BANNER_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_ERROR, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_BANNER_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_ERROR);
                if (adListener!=null)
                {
                    adListener.onAdError();
                }
                //getTempNativeAd(banner,AdData.GOOGLE_ADMOB_ADS);

            }

            @Override
            public void onAdLeftApplication() {
                // TODO Auto-generated method stub
                super.onAdLeftApplication();
                Log.d("banner ad", "LeftApplication");
            }

            @Override
            public void onAdLoaded() {
                // TODO Auto-generated method stub
                super.onAdLoaded();


                try{

                    adContainer.removeAllViews();
                    adContainer.addView(adView);

                    if (adListener!=null)
                    {
                        adListener.onAdLoaded();
                    }
                }
                catch (Exception e)
                {
                    if (adListener!=null)
                    {
                        adListener.onAdError();
                    }
                }


            }

            @Override
            public void onAdOpened() {
                // TODO Auto-generated method stub
                super.onAdOpened();
                Log.d("banner ad", "AdOpened");
                //tracker.sendEventMadar(GOOGLE_ADMOB_BANNER_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_DISPLAY, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_BANNER_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_VALUE_CLICK);

            }
        });
        return  adView;

    }
*/

	public static PublisherAdView loadContentAd(Context context,int position ,final ViewGroup adContainer,final  AdsControlNabaa adsControlNabaa, final AdViewLoadListener adListener) {
		final PublisherAdView adView = new PublisherAdView(context);

		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		//float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
		//float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
		AdSize customAdSize = new AdSize(((ViewGroup)adContainer.getParent()).getWidth()/(int)displayMetrics.density, 250);

	/*	AdSize customAdSize2 = new AdSize(300, 250);
		List<AdSize> sizeList = new ArrayList<>();
sizeList.add(customAdSize);
		sizeList.add(customAdSize2);*/
		adView.setAdSizes(customAdSize);
		adView.setAdUnitId(adsControlNabaa.getGoogleAdunit(position));
Log.e("google ad unit ",adView.getAdUnitId());
		PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {

			@Override
			public void onAdClosed() {
				// TODO Auto-generated method stub
				super.onAdClosed();
				Log.d("banner ad", "closed");
				//tracker.sendEventMadar(GOOGLE_ADMOB_BANNER_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_CLOSE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_BANNER_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_VALUE_CLOSE);
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				// TODO Auto-generated method stub
				super.onAdFailedToLoad(errorCode);
				Log.d("banner ad", ""+errorCode);


				//tracker.sendEventMadar(GOOGLE_ADMOB_BANNER_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_ERROR, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_BANNER_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_ERROR);
				if (adListener!=null)
				{
					adListener.onAdError();
				}
				//getTempNativeAd(banner,AdData.GOOGLE_ADMOB_ADS);

			}

			@Override
			public void onAdLeftApplication() {
				// TODO Auto-generated method stub
				super.onAdLeftApplication();
				Log.d("banner ad", "LeftApplication");
			}

			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
					//adView.setAdSizes(adView.getAdSize());

				try{

					adContainer.removeAllViews();

					adContainer.addView(adView);
					adView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

					if (adListener!=null)
					{
						adListener.onAdLoaded();
					}
				}
				catch (Exception e)
				{
					if (adListener!=null)
					{
						adListener.onAdError();
					}
				}


			}

			@Override
			public void onAdOpened() {
				// TODO Auto-generated method stub
				super.onAdOpened();
				Log.d("banner ad", "AdOpened");
				//tracker.sendEventMadar(GOOGLE_ADMOB_BANNER_VIEW, GoogleAnalyticConstants.GOOGLE_ANALYTIC_CAT, GoogleAnalyticConstants.GOOGLE_ANALYTIC_ACTION_DISPLAY, GoogleAnalyticConstants.GOOGLE_ANALYTIC_GOOGLE_ADMOB_BANNER_LABLE, GoogleAnalyticConstants.GOOGLE_ANALYTIC_VALUE_CLICK);

			}
		});
return  adView;

	}

}
