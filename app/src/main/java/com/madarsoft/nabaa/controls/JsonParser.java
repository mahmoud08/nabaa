package com.madarsoft.nabaa.controls;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.firebase.messaging.FirebaseMessaging;
import com.madarsoft.nabaa.GCM.RegisterApp;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.Comments;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Profile;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.fragments.CountryListFragment;
import com.madarsoft.nabaa.fragments.HotNewsFragment;
import com.madarsoft.nabaa.fragments.SourcesFragmentV2;
import com.madarsoft.nabaa.fragments.SubjectListFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ZProf on 13/03/2016.
 */
public class JsonParser extends AsyncTask<Void, Void, Void> {

    List<Profile> usersList;
    List<Category> categoriesList;
    ArrayList<Sources> sourcesList;
    List<News> newsList;
    String url;
    HashMap<String, String> params;
    int requestType;
    DataBaseAdapter dp;
    Context context;
    boolean addSourceFlag;
    boolean deleteSourceFlag;
    List<Comments> commentsList;
    List<Integer> likesResponse;
    int favouriteID;
    boolean reportResponse;
    boolean isDeleted;
    private int blocked = 0;
    private OnSelcetionUpdatedListener onSelcetionUpdatedListener;
    private OnCommentsUpdatedListener onCommentsUpdatedListener;
    private boolean isRecent;
    private int sharesCount;
    private String shortLink;
    private OnSharesUpdatedListener onSharesUpdatedListener;
    private OnRetrieveFavouriteNewsListener onRetrieveFavouriteNewsListener;
    private int userAccountID;
    private OnUserAccountListener onUserAccountListener;
    private OnUserRetrieve onUserRetrieve;
    private OnUserDataDeletionListener onUserDataDeletionListener;
    private OnCategoriesAddedListener onCategoriesAddedListener;
    private boolean editResult;
    private OnMostReadedNewsCalled onMostReadedNewsCalled;
    private OnSourceNewsListner onSourceNewsListner;
    private OnNewsRetrieveListner onNewsRetrieveListner;
    private boolean isError;
    private OnUserSourcesRetrievalError onUserSourceRetrievalError;
    private OnWebSourcesRetrieved onWebSourcesRetrieved;
    private OnFinishGCM onFinishGCM;
    private List<Sources> webSources;
    private ArrayList<Category> myCatList;
    private OnSearchedNewsRetrieved onSearchedNewsRetrieved;
    private String searchedNewsTimeStamp;
    private List<News> serachedNewsList;

    public JsonParser(Context context, String url, int requestType, HashMap<String, String> params) {
        this.url = url;
        this.requestType = requestType;
        this.params = params;
        dp = new DataBaseAdapter(context);
        this.context = context;
        /*try {
            blocked = dp.getAllProfiles().get(0).getBlockImg();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }


    public JsonParser(Context context, String url, int requestType, HashMap<String, String> params, boolean isRecent) {
        this.url = url;
        this.requestType = requestType;
        this.params = params;
        dp = new DataBaseAdapter(context);
        this.context = context;
        this.isRecent = isRecent;
        /*try {
            blocked = dp.getAllProfiles().get(0).getBlockImg();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... arg0) {
        WebRequest webreq = new WebRequest();
        //String jsonStr = webreq.makeWebServiceCall(url, WebRequest.GETRequest);
        String jsonStr = webreq.makeWebServiceCall(context, url, WebRequest.POSTREQUEST, params);
        Log.d("Response: ", ">" + jsonStr);
        ParseJSON(jsonStr);
        return null;
    }

    @Override
    protected void onPostExecute(Void requestresult) {
        super.onPostExecute(requestresult);
        try {
            switch (requestType) {
                case URLs.ADD_USER_REQUEST_TYPE:
                    try {
                        /*if (dp.getAllProfiles().isEmpty()) {
                            Toast.makeText(context, "Ø­Ø¯Ø« Ø®Ø·Ø£ Ø§Ø«Ù†Ø§Ø¡ Ø¥Ø¶Ø§ÙØ© Ù…Ø³ØªØ®Ø¯Ù…", Toast.LENGTH_SHORT).show();
                        } else {*/
                        //onSelcetionUpdatedListener.onFinished(dp.getAllProfiles().get(0).getUserId());
                        onUserRetrieve.onFinished(usersList.get(0));
                        //}
                    } catch (NullPointerException e) {
                        URLs.setUserID(dp.getAllProfiles().get(0).getUserId());
                    }
                    break;
                case URLs.GET_CATEGORIES_REQUEST_TYPE:
                    try {
                        SubjectListFragment.showData(context, dp);
                    } catch (NullPointerException e) {
                        onCategoriesAddedListener.onFinished();
                    }
                    break;
                case URLs.GET_COUNTRIES_RESOURCES_TYPE:
                    onSelcetionUpdatedListener.
                            onFinished(sourcesList);
                    break;
                case URLs.SOURCE_ADD_REQUEST_TYPE:
                    onSelcetionUpdatedListener.onFinished(addSourceFlag);
                    break;
                case URLs.GET_COUNTRIES_REQUEST_TYPE:
                    try {
                        CountryListFragment.showData(context, dp);
                    } catch (NullPointerException e) {
                    }
                    break;
                case URLs.SOURCE_DELETE_REQUEST_TYPE:
                    onSelcetionUpdatedListener.onFinished(deleteSourceFlag);
                    break;
                case URLs.GET_RECENT_NEWS_REQUEST_TYPE:
                    if (isRecent) {
                        //NewsFragment.showData(context, newsList, params.get(URLs.TAG_IS_NEW), isRecent);
                        try {
                            Intent intent = new Intent();
                            intent.setAction("com.madarsoft.displayRecentNews");
                            intent.putExtra("isNew", params.get(URLs.TAG_IS_NEW));
                            intent.putExtra("timeStamp", params.get(URLs.TAG_REQUEST_TIME_STAMP));
                            intent.putParcelableArrayListExtra("recentNews", (ArrayList<? extends Parcelable>) newsList);
                            context.sendBroadcast(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            onNewsRetrieveListner.onFinished(newsList, params.get(URLs.TAG_REQUEST_TIME_STAMP), params.get(URLs
                                    .TAG_IS_NEW));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        /*if (params.get(URLs.TAG_REQUEST_TIME_STAMP).equals("0"))
                            NewsFragment.showData(context, newsList, params.get(URLs.TAG_IS_NEW));
                        else
                            NewsFragment.showData2(context, newsList, params.get(URLs.TAG_IS_NEW));*/


//                        if (params.get(URLs.TAG_REQUEST_TIME_STAMP).equals("0"))
//                            NewsFragment.showData(context, newsList, params.get(URLs.TAG_IS_NEW));
//                        else
//                            NewsFragment.showData2(context, newsList, params.get(URLs.TAG_IS_NEW));
                    }
                    break;
                case URLs.GET_NEWS_DETAILS_REQUEST_TYPE:
                case URLs.GET_NOTIFICATION_NEWS_DETAILS__TYPE:
                    //NewsDetail.showData(context, newsList);
                    try {
                        onSelcetionUpdatedListener.onFinished(newsList.get(0));
                    } catch (NullPointerException e) {
                        News news = new News();
                        onSelcetionUpdatedListener.onFinished(news);
                    }

                    break;
                case URLs.ADD_LIKE_REQUEST_TYPE:
                    onSelcetionUpdatedListener.onFinished(likesResponse);
                    //}
                    break;
                case URLs.DELETE_LIKE_REQUEST_TYPE:
                    onSelcetionUpdatedListener.onFinished(likesResponse);
                    break;
                case URLs.GET_HOT_NEWS_REQUEST_TYPE:
                    if (isRecent) {
                        //NewsFragment.showData(context, newsList, params.get(URLs.TAG_IS_NEW), isRecent);
                        Intent intent = new Intent();
                        intent.setAction("com.madarsoft.displayUrgentNews");
                        //intent.putExtra("isNew", params.get(URLs.TAG_IS_NEW));
                        //intent.putExtra("dateTime", params.get(URLs.TAG_DATE_TIME));
                        intent.putParcelableArrayListExtra("urgentNews", (ArrayList<? extends Parcelable>) newsList);
                        context.sendBroadcast(intent);

                    } else {
                        if (Integer.parseInt(params.get(URLs.TAG_PAGE_NUM)) == 0)
                            HotNewsFragment.showData(context, newsList);
                        else
                            HotNewsFragment.showData2(context, newsList);
                    }

                    break;
                case URLs.GET_URGENT_NEWS_REQUEST_TYPE:
                    if (isRecent) {
                        Intent intent = new Intent();
                        intent.setAction("com.madarsoft.displayUrgNews");
                        intent.putExtra("isNew", params.get(URLs.TAG_IS_NEW));
                        intent.putExtra("timeStamp", params.get(URLs.TAG_REQUEST_TIME_STAMP));
                        intent.putParcelableArrayListExtra("recentNews", (ArrayList<? extends Parcelable>) newsList);
                        context.sendBroadcast(intent);
                    } else {
                        onNewsRetrieveListner.onFinished(newsList, params.get(URLs.TAG_REQUEST_TIME_STAMP), params.get(URLs.TAG_IS_NEW));
                    }
                    break;
                case URLs.ADD_FAVOURITE_REQUEST_TYPE:
                case URLs.DELETE_FAVOURITE_REQUEST_TYPE:
                    onSelcetionUpdatedListener.onFinished(favouriteID);
                    break;
                case URLs.REPORT_SOURCE_REQUEST_TYPE:
                    onSelcetionUpdatedListener.onFinished(reportResponse);
                    break;

                case URLs.EDIT_COMMENT_REQUEST_TYPE:
                    onCommentsUpdatedListener.onFinished(editResult);
                    break;
                case URLs.ADD_COMMENT_REQUEST_TYPE:
                case URLs.DELETE_COMMENT_REQUEST_TYPE:
                case URLs.RETRIEVE_COMMENT_REQUEST_TYPE:
                    onCommentsUpdatedListener.onFinished(commentsList);
                    break;
                case URLs.SHARE_REQUEST_TYPE:
                    onSharesUpdatedListener.onFinished(sharesCount, shortLink);
                    break;
                case URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE:
                    onUserAccountListener.onFinished(userAccountID);
                    break;
                case URLs.FAVOURITE_NEWS_REQUEST_TYPE:
                    onRetrieveFavouriteNewsListener.onFinished(newsList);
                    break;
                case URLs.GET_USER_SOURCES_REQUEST_TYPE:
//                    onUserDataDeletionListener.onFinished(isDeleted);
                    onUserSourceRetrievalError.onFinished(isError);
                    break;
                case URLs.DELETE_USER_SOURCES_REQUEST_TYPE:

                    onUserDataDeletionListener.onFinished(isDeleted);

                case URLs.ADD_NOTIFICATION_REQUEST_TYPE:
                    //Add notification.
                    try {
                        onSelcetionUpdatedListener.onFinished(reportResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case URLs.REMOVE_NOTIFICATION_REQUEST_TYPE:
                    //Remove notification.
                    try {
                        onSelcetionUpdatedListener.onFinished(reportResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case URLs.ADD_URGENT_REQUEST_TYPE:
                    //Add Urgent.
                    try {
                        onSelcetionUpdatedListener.onFinished(reportResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case URLs.REMOVE_URGENT_REQUEST_TYPE:
                    // Remove urgent.
                    try {
                        onSelcetionUpdatedListener.onFinished(reportResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case URLs.GET_SOURCE_NEWS_REQUEST_TYPE:
                case URLs.GET_CATEGORY_NEWS_TYPE:
                    onSourceNewsListner.onFinished(newsList);
                case URLs.GET_GCM_TO_SERVER_TYPE:
                    try {
                        onFinishGCM.onFinished(reportResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                case URLs.SEARCH_IN_SOURCES_WEB:
                    try {
                        onWebSourcesRetrieved.onFinished(webSources);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                case URLs.HOLDER_NOTIFICATION_TYPE:
                    //Add Or Remove general Or Urgent notification
                    try {
                        onSelcetionUpdatedListener.onFinished(editResult);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case URLs.RETRIEVE_NOTIFICATION_HOLDER_TYPE:
                    // Retrieve notification data
                    try {
                        onSelcetionUpdatedListener.onFinished(editResult);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case URLs.DEFAULT_SOURCES_TYPE:
                    onSelcetionUpdatedListener.
                            onFinished(sourcesList);
                    break;
                case URLs.SEARCH_BY_SOURCES_TYPE:
                    try {
                        onSearchedNewsRetrieved.onFinished(serachedNewsList, searchedNewsTimeStamp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case URLs.SEARCH_BY_USER_SOURCES_TYPE:
                    try {
                        onSearchedNewsRetrieved.onFinished(serachedNewsList, searchedNewsTimeStamp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } catch (IndexOutOfBoundsException e) {
            Log.e("HiNews", "IndexOutOfBoundsException", e);
        }
    }

    private void AddOrRemoveGeneralAndUrgentNotifcation(String json) throws JSONException {
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        editResult = json.equalsIgnoreCase("true") ? true : false;
    }

    private void editCommentResult(String json) throws JSONException {
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        editResult = json.equalsIgnoreCase("true") ? true : false;
    }

    private void deleteUserSources(String json) throws JSONException {
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        isDeleted = json.equalsIgnoreCase("true") ? true : false;
    }

    //retrieve user sources .
    private void callUserSourcesRequest(String json) throws JSONException, IndexOutOfBoundsException {

        ArrayList<Integer> ids = new ArrayList<>();
        try {
            json = json.substring(10);
//        json = json.substring(0, json.length() - 1);
//        JSONObject jsonObj = new JSONObject(json);
            sourcesList = new ArrayList<>();
//        try {
//            Boolean userUrgent = jsonObj.getBoolean(URLs.TAG_USER_URGENT);
//            Profile profile = dp.getAllProfiles().get(0);
//            profile.setUrgentFlag(userUrgent ? 1 : 0);
//            dp.updateProfiles(profile);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
            JSONArray catResults = new JSONArray(json);
//        JSONArray catResults = getJSONArray(URLs.TAG_USER_SOURCES_COUNTRIES);

//        for (int i = 0; i < catResults.length(); i++) {
//            JSONObject c = catResults.getJSONObject(i);
//            JSONArray sourcesResults = c.getJSONArray(URLs.TAG_USER_SOURCES);
//            final int sourceCatID = c.getInt(URLs.TAG_USER_SOURCES_CATEGORY_ID);
            for (int j = 0; j < catResults.length(); j++) {
                JSONObject innerC = catResults.getJSONObject(j);
                Sources sourceObj = new Sources();
                //sourceObj.setCategory_id(sourceCatID);
//                sourceObj.setCategoryName(innerC.getString(URLs.TAG_USER_SOURCES_CAT_NAME));
                sourceObj.setSource_id(innerC.getInt(URLs.TAG_USER_SOURCES_ID));
                sourceObj.setSource_name(innerC.getString(URLs.TAG_USER_SOURCES_NAME));
                sourceObj.setDetails(innerC.getString(URLs.TAG_USER_SOURCES_DETAIL));
                sourceObj.setLogo_url(innerC.getString(URLs.TAG_USER_SOURCES_IMAGE_URL));
                sourceObj.setNumber_followers(innerC.getInt(URLs.TAG_USER_SOURCES_FOLLOWERS_NUMBER));
                sourceObj.setGeo_id(innerC.getInt(URLs.TAG_USER_SOURCES_CONT_ID));
                sourceObj.setSub_id(innerC.getInt(URLs.TAG_USER_SOURCES_CAT_ID));
                sourceObj.setSelected_or_not(innerC.getInt(URLs.TAG_USER_SOURCES_SELECTED));
                sourceObj.setTimeStamp(innerC.getInt(URLs.TAG_USER_SOURCES_TIMESTAMP));
                sourceObj.setChange_type(innerC.getInt(URLs.TAG_USER_SOURCES_CHANGETYPE));
                if (innerC.getBoolean(URLs.TAG_NOTIFICATION)) {
                    ids.add(innerC.getInt(URLs.TAG_USER_SOURCES_ID));
                }
                sourceObj.setNotifiable(innerC.getBoolean(URLs.TAG_NOTIFICATION) ? 1 : 0);
                sourceObj.setUrgent_notify(innerC.getBoolean(URLs.TAG_URGENT_NOTIFICATION) ? 1 : 0);
                dp.insertInSources(sourceObj);
            }
        } catch (Exception e) {
            isError = true;
        }

        /*if(ids.size() > 0){
            FirebaseMessaging.getInstance().subscribeToTopic("true");
            new NotificationSubscription().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Integer[]) ids.toArray());
        }*/
    }

    //for Sub
    class NotificationSubscription extends AsyncTask<Integer, Void, String> {

        @Override
        protected String doInBackground(Integer... params) {
            final GcmPubSub pubSub = GcmPubSub.getInstance(context.getApplicationContext());
            final SharedPreferences prefs = context.getSharedPreferences(MainActivity.MyPREFERENCES,
                    Context.MODE_PRIVATE);
            String token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
            try {
                for (Integer param : params) {
                    pubSub.unsubscribe(token, "/topics/" + param + "");
                }

            } catch (IOException e) {

            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals("Executed")) {
            }
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }
    }

    private void getUserAccountID(String json) throws JSONException {

        JSONObject jsonObj = new JSONObject(json);
        try {
            userAccountID = jsonObj.getInt(URLs.TAG_RESULTS);
        } catch (NumberFormatException e) {
            userAccountID = -1;
        }
    }

    private void callFavouriteNews(String json) throws JSONException {
        JSONObject jsonObj = new JSONObject(json);
        newsList = new ArrayList<>();
        final String timeOffset = jsonObj.getString(URLs.TAG_TIME_OFFSET);
        JSONArray newsResults = jsonObj.getJSONArray(URLs.TAG_NEWS);

        for (int i = 0; i < newsResults.length(); i++) {
            JSONObject c = newsResults.getJSONObject(i);
            News news = new News();
            news.setTimeOffset(timeOffset);
            news.setSourceID(c.getInt(URLs.TAG_SOURCE_ID));
            news.setSourceLogoUrl(c.getString(URLs.TAG_SOURCE_LOGO));
            news.setSourceTitle(c.getString(URLs.TAG_SOURCE_NAME));
            news.setID(c.getInt(URLs.TAG_ARTICLE_ID));
            news.setNewsTitle(c.getString(URLs.TAG_ARTICLE_TITLE));
            news.setArticleDate(c.getString(URLs.TAG_ARTICLE_DATE));
            news.setLogo_url(c.getString(URLs.TAG_ARTICLE_LOGO));
            news.setLikesNumber(c.getInt(URLs.TAG_LIKES_COUNT));
            news.setCommentsNumber(c.getInt(URLs.TAG_COMMENTS_COUNT));
            news.setSharesNumber(c.getInt(URLs.TAG_SHARES_COUNT));
            news.setLikeID(c.getInt(URLs.TAG_LIKE_ID));
            news.setShareUrl(c.getString(URLs.TAG_SHARE_LINK));
            news.setFavouriteID(c.getInt(URLs.TAG_FAVOURITE_ARTICLE_ID));
            news.setIsRTL(c.getString(URLs.TAG_IS_RTL));
            if (blocked > 0)
                news.setIsBlocked(1);
            else
                news.setIsBlocked(-1);
            newsList.add(news);
        }
    }

    private void getSharesCount(String json) throws JSONException {

        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        JSONObject jsonObj = new JSONObject(json);
        try {
            if (jsonObj.has(URLs.TAG_SHARE_COUNT)) {
                sharesCount = jsonObj.getInt(URLs.TAG_SHARE_COUNT);
                shortLink = jsonObj.getString(URLs.TAG_SHARE_SHORT_URL);
            } else
                sharesCount = -1;
        } catch (NumberFormatException e) {
            sharesCount = -1;
        }
    }

    private void recievedNewsComment(String json) throws JSONException {

        JSONObject jsonObj = new JSONObject(json);
        JSONArray commentsResult = jsonObj.getJSONArray(URLs.TAG_COMMENTS);
        final String timeOffset = jsonObj.getString(URLs.TAG_TIME_OFFSET);
        commentsList = new ArrayList<>();

        for (int i = 0; i < commentsResult.length(); i++) {
            JSONObject c = commentsResult.getJSONObject(i);
            Comments comment = new Comments();
            comment.setUserName(c.getString(URLs.TAG_ADD_COMMENT_USER_NAME));
            comment.setTimeOffset(timeOffset);
            comment.setUserImg(c.getString(URLs.TAG_ADD_COMMENT_USER_IMG));
            comment.setCommentText(c.getString(URLs.TAG_ADD_COMMENT_RESPONSE_TEXT));
            comment.setCommentDate(c.getLong(URLs.TAG_ADD_COMMENT_COMMENT_DATE));
            comment.setCommentID(c.getInt(URLs.TAG_ADD_COMMENT_COMMENT_ID));
            comment.setAccountID(c.getInt(URLs.TAG_ADD_COMMENT_USER_ID));
            comment.setChangeType(!c.isNull(URLs.TAG_ADD_COMMENT_CHANGE_TYPE) ? c.getInt(URLs.TAG_ADD_COMMENT_CHANGE_TYPE) : 0);
            commentsList.add(comment);
        }
    }

    private void reportSource(String json) throws JSONException {
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        reportResponse = json.equalsIgnoreCase("true") ? true : false;
        //if (result)
    }

    private void addNotification(String json) {
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        reportResponse = json.equalsIgnoreCase("true") ? true : false;
    }

    private void removeNotification(String json) {
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        reportResponse = json.equalsIgnoreCase("true") ? true : false;
    }

    private void addUrgent(String json) {
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        reportResponse = json.equalsIgnoreCase("true") ? true : false;
    }

    private void removeUrgent(String json) {
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        reportResponse = json.equalsIgnoreCase("true") ? true : false;
    }

    private void deleteNewsFavourite(String json) throws JSONException {

        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        boolean result = json.equalsIgnoreCase("true") ? true : false;
        if (result)
            favouriteID = 1;
        else
            favouriteID = -1;
    }

    private void addNewsFavourite(String json) throws JSONException {

        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        try {
            favouriteID = Integer.parseInt(json);
        } catch (NumberFormatException e) {
            favouriteID = -1;
        }
    }

    private void deleteNewsLike(String json) throws JSONException {

        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        likesResponse = new ArrayList<>();
        int likesCount;

        JSONObject jsonObj = new JSONObject(json);
        if (jsonObj.has(URLs.TAG_ADD_LIKE_LIKES_COUNT)) {
            likesCount = jsonObj.getInt(URLs.TAG_ADD_LIKE_LIKES_COUNT);
        } else {
            likesCount = -1;
        }
        likesResponse.add(likesCount);
    }

    private void addNewsLike(String json) throws JSONException {

        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        likesResponse = new ArrayList<>();
        JSONObject jsonObj = new JSONObject(json);

        if (jsonObj.has(URLs.TAG_ADD_LIKE_LIKE_ID)) {
            int likeID = jsonObj.getInt(URLs.TAG_ADD_LIKE_LIKE_ID);
            int likesCount = jsonObj.getInt(URLs.TAG_ADD_LIKE_LIKES_COUNT);
            likesResponse.add(likeID);
            likesResponse.add(likesCount);
        }
    }

    private void callNewsDetails(String json) throws JSONException {
        JSONObject c = new JSONObject(json);
        newsList = new ArrayList<>();
        JSONObject newsResults = c.getJSONObject(URLs.TAG_NEWS);
        final String timeOffset = c.getString(URLs.TAG_TIME_OFFSET);
        //for (int i = 0; i < newsResults.length(); i++) {
        //JSONObject c = newsResults.getJSONObject(i);
        News news = new News();
        news.setTimeOffset(timeOffset);
        if (newsResults.has(URLs.TAG_SOURCE_ID))
            news.setSourceID(newsResults.getInt(URLs.TAG_SOURCE_ID));
        if (newsResults.has(URLs.TAG_SOURCE_LOGO)){
            news.setSourceLogoUrl(newsResults.getString(URLs.TAG_SOURCE_LOGO));
            news.setSourceImage(newsResults.getString(URLs.TAG_SOURCE_LOGO));
        }
        if (newsResults.has(URLs.TAG_SOURCE_NAME))
            news.setSourceTitle(newsResults.getString(URLs.TAG_SOURCE_NAME));
        if (newsResults.has(URLs.TAG_ARTICLE_ID))
            news.setID(newsResults.getInt(URLs.TAG_ARTICLE_ID));
        if (newsResults.has(URLs.TAG_ID))
            news.setID(newsResults.getInt(URLs.TAG_ID));
        if (newsResults.has(URLs.TAG_ARTICLE_TITLE))
            news.setNewsTitle(newsResults.getString(URLs.TAG_ARTICLE_TITLE));
        if (newsResults.has(URLs.TAG_ARTICLE_DATE))
            news.setArticleDate(newsResults.getString(URLs.TAG_ARTICLE_DATE));
        if (newsResults.has(URLs.TAG_ARTICLE_DATE2))
            news.setArticleDate(newsResults.getString(URLs.TAG_ARTICLE_DATE2));
        if (newsResults.has(URLs.TAG_ARTICLE_LOGO))
            news.setLogo_url(newsResults.getString(URLs.TAG_ARTICLE_LOGO));
        if (newsResults.has(URLs.TAG_LIKES_COUNT))
            news.setLikesNumber(newsResults.getInt(URLs.TAG_LIKES_COUNT));
        if (newsResults.has(URLs.TAG_COMMENTS_COUNT))
            news.setCommentsNumber(newsResults.getInt(URLs.TAG_COMMENTS_COUNT));
        if (newsResults.has(URLs.TAG_SHARES_COUNT))
            news.setSharesNumber(newsResults.getInt(URLs.TAG_SHARES_COUNT));
        if (newsResults.has(URLs.TAG_LIKE_ID))
            news.setLikeID(newsResults.getInt(URLs.TAG_LIKE_ID));
        if (newsResults.has(URLs.TAG_SHARE_LINK))
            news.setShareUrl(newsResults.getString(URLs.TAG_SHARE_LINK));

        news.setFavouriteID(newsResults.getInt(URLs.TAG_FAVOURITE_ID));
        news.setNewsDetails(newsResults.getString(URLs.TAG_ARTICLE_DETAILS));
        news.setNewsUrl(newsResults.getString(URLs.TAG_ARTICLE_URL));
        if (blocked > 0)
            news.setIsBlocked(1);
        else
            news.setIsBlocked(-1);
        try {
            news.setIsRTL(newsResults.getString(URLs.TAG_IS_RTL));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // }
        if (c.has(URLs.TAG_SOURCE)) {
            JSONObject sourceResults = c.getJSONObject(URLs.TAG_SOURCE);
            news.setSourceName(sourceResults.getString(URLs.TAG_CATEGORY_NAME));
            news.setSourceTitle(sourceResults.getString(URLs.TAG_CATEGORY_NAME));
            news.setSourceID(sourceResults.getInt(URLs.TAG_CATEGORY_ID));
            news.setSourceImage(sourceResults.getString(URLs.TAG_USER_SOURCES_IMAGE_URL));
            news.setChangeType(sourceResults.getInt(URLs.TAG_CATEGORY_CHANGE_TYPE));
            news.setSourceFollowers(sourceResults.getInt(URLs.TAG_USER_SOURCES_FOLLOWERS_NUMBER));
            news.setWebSite(sourceResults.getString(URLs.TAG_USER_SOURCES_WEBSITE));
            news.setDetails(sourceResults.getString(URLs.TAG_COUNTRIES_RESOURCES_DETAIL));
            if (sourceResults.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID) != 0)
                news.setCountryID(sourceResults.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID));
            if (sourceResults.getInt(URLs.TAG_COUNTRIES_RESOURCES_CATEGORYID) != 0)
                news.setCategoryID(sourceResults.getInt(URLs.TAG_COUNTRIES_RESOURCES_CATEGORYID));
        }
        newsList.add(news);
    }

    private void callRecentNews(String json) throws JSONException {
        Sources mSource = new Sources();
        JSONObject jsonObj = new JSONObject(json);
        newsList = new ArrayList<>();
        try {
            JSONObject sourceObj = jsonObj.getJSONObject("Source");
            //sourceObj
            if (!dp.isExist(Integer.parseInt(sourceObj.getString("Id")))) {
                if (sourceObj.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID) != 0)
                    mSource.setGeo_id(sourceObj.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID));
                if (sourceObj.getInt(URLs.TAG_COUNTRIES_RESOURCES_CATEGORYID) != 0)
                    mSource.setSub_id(sourceObj.getInt(URLs.TAG_COUNTRIES_RESOURCES_CATEGORYID));

                mSource.setSource_id(sourceObj.getInt(URLs.TAG_COUNTRIES_RESOURCES_ID));
                mSource.setSource_name(sourceObj.getString(URLs.TAG_COUNTRIES_RESOURCES_NAME));
                mSource.setDetails(sourceObj.getString(URLs.TAG_COUNTRIES_RESOURCES_DETAIL));
                mSource.setLogo_url(sourceObj.getString(URLs.TAG_COUNTRIES_RESOURCES_IMAGE_URL));
                mSource.setChange_type(sourceObj.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE));
                mSource.setNumber_followers(sourceObj.getInt(URLs.TAG_COUNTRIES_RESOURCES_FOLLOWERS_NUMBER));
                mSource.setTimeStamp(sourceObj.getLong(URLs.TAG_TIME_STAMP));
                //mSource.setNotifiable(c.getBoolean(URLs.TAG_NOTIFICATION) ? 1: 0);
                if (sourceObj.getBoolean(URLs.TAG_COUNTRIES_RESOURCES_SELECTED))
                    mSource.setSelected_or_not(1);
                else
                    mSource.setSelected_or_not(0);

                dp.insertInSources(mSource);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        final String timeOffset = jsonObj.getString(URLs.TAG_TIME_OFFSET);
        JSONArray newsResults = jsonObj.getJSONArray(URLs.TAG_NEWS);
        for (int i = 0; i < newsResults.length(); i++) {
            JSONObject c = newsResults.getJSONObject(i);
            News news = new News();
            news.setTimeOffset(timeOffset);
            news.setSourceID(c.getInt(URLs.TAG_SOURCE_ID));
            news.setSourceLogoUrl(c.getString(URLs.TAG_SOURCE_LOGO));
            news.setSourceTitle(c.getString(URLs.TAG_SOURCE_NAME));
            news.setID(c.getInt(URLs.TAG_ARTICLE_ID));
            news.setNewsTitle(c.getString(URLs.TAG_ARTICLE_TITLE));
            news.setArticleDate(c.getString(URLs.TAG_ARTICLE_DATE));
            news.setLogo_url(c.getString(URLs.TAG_ARTICLE_LOGO));
            news.setLikesNumber(c.getInt(URLs.TAG_LIKES_COUNT));
            news.setCommentsNumber(c.getInt(URLs.TAG_COMMENTS_COUNT));
            if (!c.isNull(URLs.TAG_SHARES_COUNT))
                news.setSharesNumber(c.getInt(URLs.TAG_SHARES_COUNT));
            news.setLikeID(c.getInt(URLs.TAG_LIKE_ID));
            news.setShareUrl(c.getString(URLs.TAG_SHARE_LINK));
            if (c.has(URLs.TAG_RESPONSE_READERS) && !c.isNull(URLs.TAG_RESPONSE_READERS))
                news.setReadersNum(c.getInt(URLs.TAG_RESPONSE_READERS));
            if (c.has(URLs.TAG_URGENT))
                news.setUrgent(c.getBoolean(URLs.TAG_URGENT));
            if (c.has(URLs.TAG_TIME_STAMP))
                news.setTime_stamp(c.getString(URLs.TAG_TIME_STAMP));
            if (c.has(URLs.TAG_ACTIVATE_DATE))
                news.setDateTime(c.getString(URLs.TAG_ACTIVATE_DATE));
            if (blocked > 0)
                news.setIsBlocked(1);
            else
                news.setIsBlocked(-1);
            news.setIsRTL(c.getString(URLs.TAG_IS_RTL));
            newsList.add(news);
        }
    }

    private void addNewUSer(String json) throws JSONException {
        //add user request
        JSONObject jsonObj = new JSONObject(json);
        usersList = new ArrayList<>();
        JSONObject catJsonObj = jsonObj.getJSONObject(URLs.TAG_RESULTS);
        //temp code
        /*if(!catJsonObj.has(URLs.TAG_USERID)){
            Profile user = new Profile();
            user.setUser_id(20);
            if (true)
                user.setRegFlag(1);
            else
                user.setRegFlag(0);
            dp.insertInProfile(user);
            return;
        }*/

        Profile user = new Profile();
        if (catJsonObj.has(URLs.TAG_USERID)) {
            user.setUser_id(catJsonObj.getInt(URLs.TAG_USERID));
            if (catJsonObj.getBoolean(URLs.TAG_REG_FLAG)) {
                user.setRegFlag(1);
                user.setSoundType(1);
//                user.setUrgentFlag(1);
            } else
                user.setRegFlag(0);
            dp.insertInProfile(user);
        }

        usersList.add(user);
    }




    private void callCategoriesRequest(String json) throws JSONException {
        //category request
        try {
            json = json.substring(10);
            json = json.substring(0, json.length() - 1);
            JSONObject jsonObj = new JSONObject(json);
            categoriesList = new ArrayList<Category>();
            JSONArray catResults = jsonObj.getJSONArray(URLs.TAG_CATEGORIES);

            for (int i = 0; i < catResults.length(); i++) {
                JSONObject c = catResults.getJSONObject(i);
                Category catObj = new Category();
                catObj.setTime_stamp(c.getLong(URLs.TAG_TIME_STAMP));
                catObj.setCategory_type(Integer.parseInt(URLs.TAG_CATEGORY_TYPE));
                catObj.setCategory_id(c.getInt(URLs.TAG_CATEGORY_ID));
                catObj.setCategory_name(c.getString(URLs.TAG_CATEGORY_NAME));
                catObj.setLogo_url(c.getString(URLs.TAG_CATEGORY_IMAGE_URL));
                catObj.setChange_type(c.getInt(URLs.TAG_CATEGORY_CHANGE_TYPE));
                try {
                    catObj.setIso(c.getString(URLs.TAG_ISO));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (params.get(URLs.TAG_REQUEST_TIME_STAMP).equals("0"))
                    dp.insertInCategories(catObj);
                else {
                    //insert  to database
                    if (c.getInt(URLs.TAG_CATEGORY_CHANGE_TYPE) == 1)
                        dp.insertInCategories(catObj);
                        //update category  to database
                    else if (c.getInt(URLs.TAG_CATEGORY_CHANGE_TYPE) == 2) {
                        myCatList = dp.getCategoryByCategoryType(Integer.parseInt(URLs.TAG_CATEGORY_TYPE));
                        for (int j = 0; j < myCatList.size(); j++) {
                            if (catObj.getCategory_id() == myCatList.get(j).getCategory_id()) {
                                dp.updateCategory(catObj);
                                break;
                            } else {
                                //update method for category
                                dp.insertInCategories(catObj);
                            }
//                        }
                        }
                    } else {//delete method for category
                        dp.deleteCategoryByChangedType(catObj.getCategory_id());
                    }
                }
                categoriesList.add(catObj);
            }
        } catch (Exception e) {
            try {
                Looper.prepare();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            Toast.makeText(context, "Error Retrieving sources", Toast.LENGTH_LONG).show();
        }
    }

    private void callRecommendedSources(String json) throws JSONException, IndexOutOfBoundsException {
        //sources request
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        JSONArray jsonArray = new JSONArray(json);
        List<Profile> profile = dp.getAllProfiles();
        final Profile objProfile = profile.get(0);
//        String maxTime = null;
//
//        try {
//            maxTime = jsonObj.get("maxTimeStamp").toString();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        sourcesList = new ArrayList<>();
        //JSONArray sourcesResults = jsonObj.getJSONArray(URLs.TAG_SOURCES);

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject c = jsonArray.getJSONObject(i);
            Sources sourceObj = new Sources();

            //sourceObj.setCategory_id(Integer.parseInt(params.get(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID)));
            if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID) != 0)
                sourceObj.setGeo_id(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID));
            if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CATEGORYID) != 0)
                sourceObj.setSub_id(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CATEGORYID));

            sourceObj.setSource_id(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_ID));
            sourceObj.setSource_name(c.getString(URLs.TAG_COUNTRIES_RESOURCES_NAME));
            sourceObj.setDetails(c.getString(URLs.TAG_COUNTRIES_RESOURCES_DETAIL));
            sourceObj.setLogo_url(c.getString(URLs.TAG_COUNTRIES_RESOURCES_IMAGE_URL));
            sourceObj.setChange_type(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE));
            sourceObj.setNumber_followers(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_FOLLOWERS_NUMBER));
            sourceObj.setTimeStamp(c.getLong(URLs.TAG_TIME_STAMP));
            sourceObj.setUrgent_notify(c.getBoolean(URLs.TAG_URGENT_NOTIFICATION) ? 1 : 0);
            //FirebaseMessaging.getInstance().subscribeToTopic("Urgent");
            FirebaseMessaging.getInstance().subscribeToTopic(sourceObj.getSource_id() + "U");
            if (c.getBoolean(URLs.TAG_COUNTRIES_RESOURCES_SELECTED))
                sourceObj.setSelected_or_not(1);
            else
                sourceObj.setSelected_or_not(0);
            if (c.getString(URLs.TAG_TIME_STAMP).equalsIgnoreCase("0"))
                dp.insertInSources(sourceObj);
            else {
                if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE) == 1)
                    dp.insertInSources(sourceObj);
                else if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE) == 2) {
                    //update method for source
                    dp.updateSources(sourceObj);
                } else {//delete method for source
                    dp.deleteSourceByChangedType(sourceObj.getSource_id());
                }
            }
            sourcesList.add(sourceObj);
        }

//        long highest = sourcesList.get(0).getTimeStamp();
//        int highestIndex = 0;
//        for (int s = 1; s < sourcesList.size(); s++) {
//            long curValue = sourcesList.get(s).getTimeStamp();
//            if (curValue > highest) {
//                highest = curValue;
//                highestIndex = s;
//            }
//        }
//        SourcesFragmentV2.getCatObj().setSources_time_stamp(Integer.parseInt(maxTime));
//        dp.updateCategory(SourcesFragmentV2.getCatObj());
    }

    private void callSourcesRequest(String json) throws JSONException, IndexOutOfBoundsException {
        //sources request
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
//        JSONArray jsonArray = new JSONArray(json);
        JSONObject jsonObj = new JSONObject(json);
        String maxTime = null;

        try {
            maxTime = jsonObj.get("maxTimeStamp").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sourcesList = new ArrayList<>();
        JSONArray sourcesResults = jsonObj.getJSONArray(URLs.TAG_SOURCES);

        for (int i = 0; i < sourcesResults.length(); i++) {
            JSONObject c = sourcesResults.getJSONObject(i);
            Sources sourceObj = new Sources();

            //sourceObj.setCategory_id(Integer.parseInt(params.get(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID)));
            if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID) != 0)
                sourceObj.setGeo_id(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID));
            if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CATEGORYID) != 0)
                sourceObj.setSub_id(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CATEGORYID));

            sourceObj.setSource_id(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_ID));
            sourceObj.setSource_name(c.getString(URLs.TAG_COUNTRIES_RESOURCES_NAME));
            sourceObj.setDetails(c.getString(URLs.TAG_COUNTRIES_RESOURCES_DETAIL));
            sourceObj.setLogo_url(c.getString(URLs.TAG_COUNTRIES_RESOURCES_IMAGE_URL));
            sourceObj.setChange_type(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE));
            sourceObj.setNumber_followers(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_FOLLOWERS_NUMBER));
            sourceObj.setTimeStamp(c.getLong(URLs.TAG_TIME_STAMP));
            //sourceObj.setNotifiable(c.getBoolean(URLs.TAG_NOTIFICATION) ? 1: 0);
            if (c.getBoolean(URLs.TAG_COUNTRIES_RESOURCES_SELECTED))
                sourceObj.setSelected_or_not(1);
            else
                sourceObj.setSelected_or_not(0);
            if(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID) == 54)
                Log.e("AA","AAA");

            if (params.get(URLs.TAG_REQUEST_TIME_STAMP).equals("0"))
                dp.insertInSources(sourceObj);
            else {
                if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE) == 1)
                    dp.insertInSources(sourceObj);
                else if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE) == 2) {
                    //update method for source
                    dp.updateSources(sourceObj);
                } else {//delete method for source
                    dp.deleteSourceByChangedType(sourceObj.getSource_id());
                }
            }
            sourcesList.add(sourceObj);
        }

//        long highest = sourcesList.get(0).getTimeStamp();
//        int highestIndex = 0;
//        for (int s = 1; s < sourcesList.size(); s++) {
//            long curValue = sourcesList.get(s).getTimeStamp();
//            if (curValue > highest) {
//                highest = curValue;
//                highestIndex = s;
//            }
//        }
        SourcesFragmentV2.getCatObj().setSources_time_stamp(Integer.parseInt(maxTime));
        dp.updateCategory(SourcesFragmentV2.getCatObj());
    }

    private void getSearchedSourceList(String json) throws JSONException {

        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        JSONArray jsonObj = new JSONArray(json);
        webSources = new ArrayList<>();
//        JSONArray sourcesResults = jsonObj.getJSONArray(0);

        for (int i = 0; i < jsonObj.length(); i++) {
            JSONObject c = jsonObj.getJSONObject(i);
            Sources sourceObj = new Sources();

            //sourceObj.setCategory_id(Integer.parseInt(params.get(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID)));
            if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID) != 0)
                sourceObj.setGeo_id(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_COUNTRYID));
            if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CATEGORYID) != 0)
                sourceObj.setSub_id(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CATEGORYID));
            sourceObj.setSource_id(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_ID));
            sourceObj.setSource_name(c.getString(URLs.TAG_COUNTRIES_RESOURCES_NAME));
            sourceObj.setDetails(c.getString(URLs.TAG_COUNTRIES_RESOURCES_DETAIL));
            sourceObj.setLogo_url(c.getString(URLs.TAG_COUNTRIES_RESOURCES_IMAGE_URL));
            sourceObj.setChange_type(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE));
            sourceObj.setNumber_followers(c.getInt(URLs.TAG_COUNTRIES_RESOURCES_FOLLOWERS_NUMBER));
            sourceObj.setTimeStamp(c.getLong(URLs.TAG_TIME_STAMP));
            //sourceObj.setNotifiable(c.getBoolean(URLs.TAG_NOTIFICATION) ? 1: 0);
            sourceObj.setSelected_or_not(c.getInt(URLs.TAG_USER_SOURCES_SELECTED));
//                if (params.get(URLs.TAG_REQUEST_TIME_STAMP).equals("0") && c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE) != 3)
//                    dp.insertInSources(sourceObj);
//                else {
//                    if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE) == 1)
//                        dp.insertInSources(sourceObj);
//                    else if (c.getInt(URLs.TAG_COUNTRIES_RESOURCES_CHANGE_TYPE) == 2) {
//                        //update method for source
//                        dp.updateSources(sourceObj);
//                    } else {//delete method for source
//                        dp.deleteSourceByChangedType(sourceObj.getSource_id());
//                    }
//                }
            webSources.add(sourceObj);
        }

    }

    private void callCountriesRequest(String json) throws JSONException {
        //country request
        try {
            json = json.substring(10);
            json = json.substring(0, json.length() - 1);
            JSONObject jsonObj = new JSONObject(json);
            categoriesList = new ArrayList<Category>();
            JSONArray countriesResults = jsonObj.getJSONArray(URLs.TAG_COUTRIES);

            for (int i = 0; i < countriesResults.length(); i++) {
                JSONObject c = countriesResults.getJSONObject(i);
                Category catObj = new Category();
                catObj.setTime_stamp(c.getLong(URLs.TAG_TIME_STAMP));
                catObj.setCategory_type(Integer.parseInt(URLs.TAG_COUNTRY_TYPE));
                catObj.setCategory_id(c.getInt(URLs.TAG_CATEGORY_ID));
                catObj.setCategory_name(c.getString(URLs.TAG_CATEGORY_NAME));
                catObj.setLogo_url(c.getString(URLs.TAG_CATEGORY_IMAGE_URL));
                catObj.setChange_type(c.getInt(URLs.TAG_CATEGORY_CHANGE_TYPE));
                catObj.setIso(c.getString(URLs.TAG_ISO));
                if (params.get(URLs.TAG_REQUEST_TIME_STAMP).equals("0"))
                    dp.insertInCategories(catObj);
                else {
                    if (c.getInt(URLs.TAG_CATEGORY_CHANGE_TYPE) == 1)
                        dp.insertInCategories(catObj);
                    else if (c.getInt(URLs.TAG_CATEGORY_CHANGE_TYPE) == 2) {
                        dp.updateCategory(catObj);
                    } else {//delete method for category
                        dp.deleteCategoryByChangedType(catObj.getCategory_id());
                    }
                }
                categoriesList.add(catObj);
            }
        } catch (JSONException e) {
            Looper.prepare();
            Toast.makeText(context, "Error Retrieving sources", Toast.LENGTH_LONG).show();

        }
    }

    private void ParseJSON(String json) {
        JSONObject jsonObj;
        if (json != null) {
            try {
                switch (requestType) {
                    case URLs.ADD_USER_REQUEST_TYPE:
                        addNewUSer(json);
                        break;
                    case URLs.GET_CATEGORIES_REQUEST_TYPE:
                        callCategoriesRequest(json);
                        break;
                    case URLs.GET_COUNTRIES_RESOURCES_TYPE:
                        callSourcesRequest(json);
                        break;
                    case URLs.SOURCE_ADD_REQUEST_TYPE:
                        //select/unselecet source request
                        jsonObj = new JSONObject(json);
                        addSourceFlag = jsonObj.getBoolean(URLs.TAG_RESULTS);
                        break;
                    case URLs.GET_COUNTRIES_REQUEST_TYPE:
                        callCountriesRequest(json);
                        break;
                    case URLs.SOURCE_DELETE_REQUEST_TYPE:
                        //select/unselecet source request
                        try {
                            jsonObj = new JSONObject(json);
                            if (!jsonObj.getString(URLs.TAG_RESULTS).contains("600"))
                                deleteSourceFlag = jsonObj.getBoolean(URLs.TAG_RESULTS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case URLs.GET_RECENT_NEWS_REQUEST_TYPE:
                        callRecentNews(json);
                        break;
                    case URLs.GET_NEWS_DETAILS_REQUEST_TYPE:
                    case URLs.GET_NOTIFICATION_NEWS_DETAILS__TYPE:
                        callNewsDetails(json);
                        break;
                    case URLs.ADD_LIKE_REQUEST_TYPE:
                        addNewsLike(json);
                        break;
                    case URLs.DELETE_LIKE_REQUEST_TYPE:
                        deleteNewsLike(json);
                        break;
                    case URLs.GET_HOT_NEWS_REQUEST_TYPE:
                    case URLs.GET_URGENT_NEWS_REQUEST_TYPE:
                        callRecentNews(json);
                        break;
                    case URLs.ADD_FAVOURITE_REQUEST_TYPE:
                        addNewsFavourite(json);
                        break;
                    case URLs.DELETE_FAVOURITE_REQUEST_TYPE:
                        deleteNewsFavourite(json);
                        break;
                    case URLs.REPORT_SOURCE_REQUEST_TYPE:
                        reportSource(json);
                        break;

                    case URLs.EDIT_COMMENT_REQUEST_TYPE:
                        editCommentResult(json);
                        break;
                    case URLs.ADD_COMMENT_REQUEST_TYPE:
                    case URLs.DELETE_COMMENT_REQUEST_TYPE:
                    case URLs.RETRIEVE_COMMENT_REQUEST_TYPE:
                        //add / edit / delete / retrieve
                        recievedNewsComment(json);
                        break;
                    case URLs.SHARE_REQUEST_TYPE:
                        getSharesCount(json);
                        break;
                    case URLs.EDIT_USER_ACCOUNT_REQUEST_TYPE:
                        getUserAccountID(json);
                        break;
                    case URLs.FAVOURITE_NEWS_REQUEST_TYPE:
                        callFavouriteNews(json);
                        break;
                    case URLs.GET_USER_SOURCES_REQUEST_TYPE:
                        callUserSourcesRequest(json);
                        break;
                    case URLs.DELETE_USER_SOURCES_REQUEST_TYPE:
                        deleteUserSources(json);
                        break;
                    case URLs.ADD_NOTIFICATION_REQUEST_TYPE:
                        //Add notification
                        addNotification(json);
                        break;
                    case URLs.REMOVE_NOTIFICATION_REQUEST_TYPE:
                        //Remove notification
                        removeNotification(json);
                        break;
                    case URLs.ADD_URGENT_REQUEST_TYPE:
                        //Add Urgent
                        addUrgent(json);
                        break;
                    case URLs.REMOVE_URGENT_REQUEST_TYPE:
                        // Remove urgent
                        removeUrgent(json);
                        break;
                    case URLs.GET_SOURCE_NEWS_REQUEST_TYPE:
                    case URLs.GET_CATEGORY_NEWS_TYPE:
                        callRecentNews(json);
                    case URLs.GET_GCM_TO_SERVER_TYPE:
                        gcmToServer(json);
                    case URLs.SEARCH_IN_SOURCES_WEB:
                        try {
                            getSearchedSourceList(json);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    case URLs.HOLDER_NOTIFICATION_TYPE:
                        try {
                            AddOrRemoveGeneralAndUrgentNotifcation(json);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case URLs.RETRIEVE_NOTIFICATION_HOLDER_TYPE:
                        try {
                            retrieveNotificationsData(json);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case URLs.DEFAULT_SOURCES_TYPE:
                        try {
                            callRecommendedSources(json);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case URLs.SEARCH_BY_SOURCES_TYPE:
                        try {
                            callSearchedNews(json);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case URLs.SEARCH_BY_USER_SOURCES_TYPE:
                        try {
                            callSearchedNews(json);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } catch (JSONException e) {
                Log.e("ServiceHandler", "JSONException", e);
                if (requestType == 4)
                    onSelcetionUpdatedListener.onFinished(false);
                if (requestType == 6)
                    onSelcetionUpdatedListener.onFinished(true);

            } catch (IndexOutOfBoundsException e) {
                Log.e("ServiceHandler", "IndexOutOfBoundsException", e);
            }

        } else {

            Log.e("ServiceHandler", "No data received from HTTP request");
        }
    }

    private void callSearchedNews(String json) throws JSONException {
        serachedNewsList = new ArrayList<>();
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);

        JSONObject jsonObj = new JSONObject(json);
        final String timeOffset = jsonObj.getString(URLs.TAG_TIME_OFFSET);
        JSONArray newsResults = jsonObj.getJSONArray(URLs.TAG_NEWS);
        try {
            searchedNewsTimeStamp = jsonObj.get("minTimeStamp").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < newsResults.length(); i++) {
            JSONObject c = newsResults.getJSONObject(i);
            News news = new News();
            news.setTimeOffset(timeOffset);
            news.setSourceID(c.getInt(URLs.TAG_SOURCE_ID));
            news.setSourceLogoUrl(c.getString(URLs.TAG_SOURCE_LOGO));
            news.setSourceTitle(c.getString(URLs.TAG_SOURCE_NAME));
            news.setID(c.getInt(URLs.TAG_ARTICLE_ID));
            news.setNewsTitle(c.getString(URLs.TAG_ARTICLE_TITLE));
            news.setArticleDate(c.getString(URLs.TAG_ARTICLE_DATE));
//            news.setLogo_url(c.getString(URLs.TAG_ARTICLE_LOGO));
            news.setLikesNumber(c.getInt(URLs.TAG_LIKES_COUNT));
            news.setCommentsNumber(c.getInt(URLs.TAG_COMMENTS_COUNT));
            if (!c.isNull(URLs.TAG_SHARES_COUNT))
                news.setSharesNumber(c.getInt(URLs.TAG_SHARES_COUNT));
            news.setLikeID(c.getInt(URLs.TAG_LIKE_ID));
//            news.setShareUrl(c.getString(URLs.TAG_SHARE_LINK));
            if (c.has(URLs.TAG_RESPONSE_READERS) && !c.isNull(URLs.TAG_RESPONSE_READERS))
                news.setReadersNum(c.getInt(URLs.TAG_RESPONSE_READERS));
            if (c.has(URLs.TAG_URGENT))
                news.setUrgent(c.getBoolean(URLs.TAG_URGENT));
//            if (c.has(URLs.TAG_TIME_STAMP))
//                news.setTime_stamp(c.getString(URLs.TAG_TIME_STAMP));
            if (c.has(URLs.TAG_ACTIVATE_DATE))
                news.setDateTime(c.getString(URLs.TAG_ACTIVATE_DATE));
            if (blocked > 0)
                news.setIsBlocked(1);
            else
                news.setIsBlocked(-1);
            news.setIsRTL(c.getString(URLs.TAG_IS_RTL));
            serachedNewsList.add(news);
        }
    }

    private void retrieveNotificationsData(String json) throws JSONException {
        json = json.substring(10);
        json = json.substring(0, json.length() - 1);
        JSONObject jsonObj = new JSONObject(json);

        ArrayList<Profile> profile = dp.getAllProfiles();
        if (jsonObj.get("normal").toString().equalsIgnoreCase("true")) {
            profile.get(0).setSoundType(1);
        } else {
            profile.get(0).setSoundType(-1);
        }
        if (jsonObj.get("urgent").toString().equalsIgnoreCase("true")) {
            profile.get(0).setUrgentFlag(1);
        } else {
            profile.get(0).setUrgentFlag(-1);
        }
//        profile.setSoundType();
//        profile.setUrgentFlag();
        dp.updateProfiles(profile.get(0));
    }

    private void gcmToServer(String json) {
//        json = json.substring(10);
//        json = json.substring(0, json.length() - 1);
        reportResponse = json.contains("true") ? true : false;
    }

    public void onFinishUpdates(OnSourceNewsListner onSourceNewsListner) {
        this.onSourceNewsListner = onSourceNewsListner;
    }

    public void onFinishUpdates(OnCategoriesAddedListener onCategoriesAddedListener) {
        this.onCategoriesAddedListener = onCategoriesAddedListener;
    }

    public void onFinishUpdates(OnUserDataDeletionListener onUserDataDeletionListener) {
        this.onUserDataDeletionListener = onUserDataDeletionListener;
    }

    public void onFinishUpdates(OnUserRetrieve onUserRetrieve) {
        this.onUserRetrieve = onUserRetrieve;
    }

    public void onFinishUpdates(OnSelcetionUpdatedListener onSelcetionUpdatedListener) {
        this.onSelcetionUpdatedListener = onSelcetionUpdatedListener;
    }

    public void onFinishUpdates(OnCommentsUpdatedListener onCommentsUpdatedListener) {
        this.onCommentsUpdatedListener = onCommentsUpdatedListener;
    }

    public void onFinishUpdates(OnSharesUpdatedListener onSharesUpdatedListener) {
        this.onSharesUpdatedListener = onSharesUpdatedListener;
    }

    public void onFinishUpdates(OnRetrieveFavouriteNewsListener onRetrieveFavouriteNewsListener) {
        this.onRetrieveFavouriteNewsListener = onRetrieveFavouriteNewsListener;
    }

    public void onFinishUpdates(OnUserAccountListener onUserAccountListener) {
        this.onUserAccountListener = onUserAccountListener;
    }

    public void onFinishUpdates(OnMostReadedNewsCalled onMostReadedNewsCalled) {
        this.onMostReadedNewsCalled = onMostReadedNewsCalled;
    }

    public void onFinishUpdates(OnNewsRetrieveListner onNewsRetrieveListner) {
        this.onNewsRetrieveListner = onNewsRetrieveListner;
    }

    public void onUserSourcesRetrieval(OnUserSourcesRetrievalError onUserSourceRetrievalError) {
        this.onUserSourceRetrievalError = onUserSourceRetrievalError;
    }

    public void onWebSourcesRetrieved(OnWebSourcesRetrieved onWebSourcesRetrieved) {
        this.onWebSourcesRetrieved = onWebSourcesRetrieved;
    }

    public void onSearchedNewsRetrieved(OnSearchedNewsRetrieved onSearchedNewsRetrieved) {
        this.onSearchedNewsRetrieved = onSearchedNewsRetrieved;
    }

    public void onFinishedGCM(OnFinishGCM onFinishGCM) {
        this.onFinishGCM = onFinishGCM;

    }

    public interface OnSearchedNewsRetrieved {
        void onFinished(List<News> searchedNewsList, String timeStamp);

    }

    public interface OnUserSourcesRetrievalError {
        public void onFinished(boolean isError);
    }

    public interface OnNewsRetrieveListner {
        public void onFinished(List<News> newsList, String timeStamp, String isNew);
    }

    public interface OnSourceNewsListner {
        public void onFinished(List<News> newsList);
    }


    public interface OnMostReadedNewsCalled {
        public void onFinished(List<News> newsList);
    }

    public interface OnCategoriesAddedListener {
        public void onFinished();
    }

    public interface OnUserDataDeletionListener {
        public void onFinished(boolean isDeleted);
    }


    public interface OnUserRetrieve {
        public void onFinished(Profile userObj);
    }

    public interface OnUserAccountListener {
        public void onFinished(int userAccountID);
    }

    public interface OnRetrieveFavouriteNewsListener {
        public void onFinished(List<News> favouriteNews);
    }

    public interface OnSharesUpdatedListener {
        public void onFinished(int sharesCount, String shortLink);
    }

    public interface OnCommentsUpdatedListener {
        public void onFinished(List<Comments> commentsList);
        public void onFinished(boolean result);
    }

    public interface OnFinishGCM {
        public void onFinished(boolean success);
    }

    public interface OnWebSourcesRetrieved {
        void onFinished(List<Sources> sourceList);
    }

    public interface OnSelcetionUpdatedListener {
        public void onFinished();

        public void onFinished(List<Integer> likeResponse);

        public void onFinished(News newsObj);

        public void onFinished(ArrayList<Sources> sourceList);

        public void onFinished(int id);

        public void onFinished(boolean success);
    }
}