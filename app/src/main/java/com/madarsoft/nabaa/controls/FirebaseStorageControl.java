package com.madarsoft.nabaa.controls;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.madarsoft.nabaa.entities.AdSettings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;

/**
 * Created by basma on 7/20/2017.
 */

public class FirebaseStorageControl {
public static final String ADS_DATA="ads data";
    public static final String ADS_DATA_DATE="ads data date";
    public static final String NABAA_ADS_ANDROID = "nabaa_ads_android";
    public static final String ADS_DATA_SERVER = "ads_data.json";
    // static Context context;

    /*public FirebaseStorageControl(Context context)
    {
        this.context=context;
    }*/
    public static void downloadAdsData(Context c)
    {

       AdSettings adSettings=loadAdsData(c);
        if ((adSettings.getAppInfo().getExpirationPeriodInseconds()*1000)+adSettings.getSavedDataDate()<System.currentTimeMillis()) {
            new DownloadFile(c).execute();
        }
    }

    public static AdSettings loadAdsData(Context context)
    {


        return JsonManager.getAdSettings(SharedPrefrencesMethods.loadSavedPreferencesString(context, ADS_DATA),context);


    }


 private   static class DownloadFile extends AsyncTask {
     Context context;

     DownloadFile(Context c) {
         context = c;
     }

     @Override
     protected Object doInBackground(Object[] params) {

         StorageReference reference = FirebaseConfigurations.getDatabaseRefrence(context).child("nabaa_android").child(getAdsDataServerFileName(context));


          String finalRes = "";
         final File localFile;
         try {
             localFile = File.createTempFile(NABAA_ADS_ANDROID, "json");
             FileDownloadTask task = reference.getFile(localFile);
             try {
                 Tasks.await(task); // or use await with timeout: Tasks.await(task, 30, TimeUnit.SECONDS)
                 if (task.isSuccessful()) {
           //          Toast.makeText(context, "File downloaded successfully.", Toast.LENGTH_SHORT).show();
                     try {
                             FileReader fReader = new FileReader(localFile);
                             BufferedReader bReader = new BufferedReader(fReader);

                             /** Reading the contents of the file , line by line */
                             String strLine;
                         StringBuilder result=new StringBuilder();
                             while( (strLine=bReader.readLine()) != null  ){
                                 result.append(strLine+"\n");
                             }

                         finalRes=result.toString();
                         saveAdSettingsWithDate(context,result.toString());
                     } catch (FileNotFoundException e) {
                         e.printStackTrace();
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 } else {
                     Log.e("Test", "Failed: " + task.getException().getMessage());
                     // Toast.makeText(getContext(), "File downloading failed", Toast.LENGTH_LONG).show();
                 }
             } catch (ExecutionException e) {
                 e.printStackTrace();
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }
         } catch (IOException e) {
             e.printStackTrace();
         }
         Log.e("Test", "String Downloaded- " + finalRes);
         return finalRes;
     }


 }


    public static  String getAdsDataServerFileName(Context context)
    {
        int versionCode=0;
        try {
            versionCode=context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;

        }catch (PackageManager.NameNotFoundException e)
        {

        }

        return context.getPackageName()+"_"+versionCode+"_"+ADS_DATA_SERVER;
    }
    public static void saveAdSettingsWithDate(Context context,String finalRes) {
        SharedPrefrencesMethods.savePreferencesString(context,ADS_DATA,finalRes);
        SharedPrefrencesMethods.savePreferencesLong(context,ADS_DATA_DATE,System.currentTimeMillis());
    }

}
