package com.madarsoft.nabaa.controls;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

public class SharedPrefrencesMethods {
	
	public static void savePreferences(Context context, String key, int value) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putInt(key, value);
		editor.commit();
	}
	public static void savePreferencesLong(Context context, String key, long value) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putLong(key, value);
		editor.commit();
	}

	public static void savePreferences(Context context, String key, boolean value) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	public static void savePreferences(Context context, String key, String value) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}
	public static void savePreferences(Context context, String key, JSONObject value) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString(key, value.toString());
		editor.commit();
	}
	public static JSONObject loadJsonSavedPreferences(Context context, String key) throws JSONException {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		String result = sharedPreferences.getString(key, "");

		return new JSONObject(result);
	}
	public static int loadSavedPreferences(Context context, String key) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		int result = sharedPreferences.getInt(key, 0);
		return result;
	}
	public static long loadSavedPreferencesLong(Context context, String key) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		long result = sharedPreferences.getLong(key, 0);
		return result;
	}
/*	public static boolean loadSavedPreferencesBoolean(Context context, String key) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		boolean result = sharedPreferences.getBoolean(key, false);
		return result;
	}
	public static String loadSavedPreferencesString(Context context, String key) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		int result = sharedPreferences.getInt(key, 0);
		return result;
	}*/
	public static int loadSavedPreferencesRemotly(Context context, String key) {
		SharedPreferences sharedPreferences =   context.getSharedPreferences(
				"remote",
				Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
		int result = sharedPreferences.getInt(key, 0);
		return result;
	}

	public static void saveSharedDateBetweenDifferentProcess(Context context, String key, int value)
	{


		// ...and set it in a new shared preferences file that uses Context.MODE_MULTI_PROCESS
		context.getSharedPreferences(
				"remote",
				Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS
		)
				.edit()
				.putInt(key, value)
				.commit();
	}
	public static boolean loadSavedPreferencesBoolean(Context context, String key) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		boolean result = sharedPreferences.getBoolean(key, false);
		return result;
	}


	
	public static void savePreferencesString(Context context, String key, String value) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	
	public static String loadSavedPreferencesString(Context context, String key) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		String result = sharedPreferences.getString(key, "");
		
				return result;
	}

		
	
	
	
}
