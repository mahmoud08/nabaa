package com.madarsoft.nabaa.controls;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.firebase.messaging.FirebaseMessaging;
import com.madarsoft.nabaa.GCM.RegisterApp;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Profile;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Colossus on 28-Jul-16.
 */
public class SubscribeService extends Service {
    DataBaseAdapter dataBaseAdapter;
    ArrayList<Sources> sources;

    private ArrayList<Profile> profile;
    private GcmPubSub pubSub;
    private SharedPreferences prefs;
    private String token;

    Profile objProfile;

    @Override
    public void onCreate() {
        super.onCreate();
        retrieveNotificationsData();
        pubSub = GcmPubSub.getInstance(getApplicationContext());
        prefs = getSharedPreferences(MainActivity.MyPREFERENCES,
                Context.MODE_PRIVATE);
        token = prefs.getString(RegisterApp.PROPERTY_REG_ID, "");
        dataBaseAdapter = new DataBaseAdapter(this);

        sources = dataBaseAdapter.getSelectedSources();
        int count = 0, urgentCount = 0;

        for (int i = 0; i < sources.size(); i++) {
//            new Subscribe().execute(sources.get(i).getSource_id() + "U");
            if (sources.get(i).getNotifiable() > 0) {
                FirebaseMessaging.getInstance().subscribeToTopic(sources.get(i).getSource_id() + "");
                count++;
//                new Subscribe().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, sources.get(i).getSource_id() + "");
            }
            if (sources.get(i).getUrgent_notify() > 0) {
                FirebaseMessaging.getInstance().subscribeToTopic(sources.get(i).getSource_id() + "U");
                urgentCount++;
//                new Subscribe().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, sources.get(i).getSource_id() + "U");
            }
        }
        /*profile = dataBaseAdapter.getAllProfiles();
        objProfile = profile.get(0);
        addOrRemoveNotification();*/
    }

    private void addOrRemoveNotification() {
        MainControl ma = new MainControl(this);
        if (!ma.checkInternetConnection()) {
            Toast.makeText(this, this.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        } else {

            HashMap<String, String> dataObj = new HashMap<>();

            dataObj.put(URLs.TAG_USERID, URLs.getUserID() + "");
            if (objProfile.getSoundType() > 0)
                dataObj.put(URLs.TAG_REQUEST_NORMAL, "TRUE");
            else
                dataObj.put(URLs.TAG_REQUEST_NORMAL, "FALSE");

            if (objProfile.getUrgentFlag() > 0)
                dataObj.put(URLs.TAG_REQUEST_URGENT, "TRUE");
            else
                dataObj.put(URLs.TAG_REQUEST_URGENT, "FALSE");

            JsonParser jsonParser = new JsonParser(this, URLs.NOTIFICATION_HOLDER, URLs.HOLDER_NOTIFICATION_TYPE, dataObj);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
                @Override
                public void onFinished() {

                }

                @Override
                public void onFinished(List<Integer> likeResponse) {

                }

                @Override
                public void onFinished(News newsObj) {

                }

                @Override
                public void onFinished(ArrayList<Sources> sourceList) {

                }

                @Override
                public void onFinished(int id) {

                }

                @Override
                public void onFinished(boolean success) {
//

                }
            });
        }
    }

    private void retrieveNotificationsData() {

        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_USERID, URLs.getUserID() + "");
        JsonParser jsonParser = new JsonParser(getApplicationContext(), URLs.RETRIEVE_NOTIFICATION_HOLDER, URLs
                .RETRIEVE_NOTIFICATION_HOLDER_TYPE, dataObj);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            jsonParser.execute();
        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {

            }

            @Override
            public void onFinished(List<Integer> likeResponse) {

            }

            @Override
            public void onFinished(News newsObj) {

            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {

            }

            @Override
            public void onFinished(int id) {

            }

            @Override
            public void onFinished(boolean success) {

                profile = dataBaseAdapter.getAllProfiles();
                objProfile = profile.get(0);
                addOrRemoveNotification();

                SharedPreferences settings = getSharedPreferences(MainActivity.PREFS_NAME, MODE_PRIVATE);
                if (!settings.getBoolean("first_running", false)) {
                    if (objProfile.getSoundType() > 0) {
                        FirebaseMessaging.getInstance().subscribeToTopic("True");
                        //Toast.makeText(getApplicationContext() , "Sub to True" ,Toast.LENGTH_SHORT).show();
                    } else {
                        FirebaseMessaging.getInstance().unsubscribeFromTopic("True");
                        //.makeText(getApplicationContext() , "Unsub to True" ,Toast.LENGTH_SHORT).show();
                    }

                    if (objProfile.getUrgentFlag() > 0) {
                        FirebaseMessaging.getInstance().subscribeToTopic("Urgent");

                        //Toast.makeText(getApplicationContext() , "Sub to Urgent" ,Toast.LENGTH_SHORT).show();
                    } else {
                        FirebaseMessaging.getInstance().unsubscribeFromTopic("Urgent");
                        //Toast.makeText(getApplicationContext() , "Unsub to Urgent" ,Toast.LENGTH_SHORT).show();
                    }
                    settings.edit().putBoolean("first_running" , true).commit();
                }

            }
        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSelf();
    }

//    class Subscribe extends AsyncTask<String, Void, String> {
//        @Override
//        protected String doInBackground(String... params) {
//
//            try {
//                pubSub.subscribe(token, "/topics/" + params[0] + "", null);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return "Executed";
//        }
//        @Override
//        protected void onPostExecute(String result) {
//            if (result.equals("Executed")) {
//            }
//            // might want to change "executed" for the returned string passed
//            // into onPostExecute() but that is upto you
//        }
//    }
}
