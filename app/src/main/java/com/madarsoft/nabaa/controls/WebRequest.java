package com.madarsoft.nabaa.controls;

import android.content.Context;
import android.util.Log;

import com.madarsoft.nabaa.activities.Splash;
import com.madarsoft.nabaa.entities.URLs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class WebRequest {
    public static final int GETREQUEST = 1;
    public static final int POSTREQUEST = 2;
    org.apache.commons.logging.Log logger;

    /**
     * Making web service call
     *
     * @url - url to make web request
     * @requestmethod - http request method
     */
    public String makeWebServiceCall(Context context, String url, int requestmethod) {
        return this.makeWebServiceCall(context, url, requestmethod, null);
    }

    /**
     * Making web service call
     *
     * @url - url to make web request
     * @requestmethod - http request method
     * @params - http request params
     */

    public String makeWebServiceCall(Context context, String urladdress, int requestmethod,
                                     Map<String, String> params) {
        URL url;
        String response = "";
        try {
            url = new URL(urladdress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(50000);
            conn.setConnectTimeout(50000);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            if (requestmethod == POSTREQUEST) {
                conn.setRequestMethod("POST");
            } else if (requestmethod == GETREQUEST) {
                conn.setRequestMethod("GETRequest");
            }
            if (params != null) {
                OutputStream ostream = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(ostream, "UTF-8"));
                writer.write(loopOnArray(params).toString());
                writer.flush();
                writer.close();
                ostream.close();
            }
            int reqresponseCode = conn.getResponseCode();

            if (reqresponseCode == HttpsURLConnection.HTTP_OK) {
                URLs.setIsTimoutExc(false);
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (java.net.SocketTimeoutException e) {
            URLs.setIsTimoutExc(true);
            Log.e("HiNews0", "SocketTimeoutException", e);
            final MainControl ma = new MainControl(context);
            if (ma.checkInternetConnection()) {
                if (Splash.toast != null)
                    Splash.toast.show();
            }
        } catch (UnknownHostException e) {
            URLs.setIsTimoutExc(true);
            Log.e("HiNews0", "UnknownHostException", e);
            final MainControl ma = new MainControl(context);
            if (ma.checkInternetConnection()) {
                if (Splash.toast != null)
                    Splash.toast.show();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    private StringBuilder loopOnArray(Map<String, String> params) {
        StringBuilder requestresult = new StringBuilder();
        boolean first = true;
        requestresult.append("{");
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                requestresult.append(" , ");
            requestresult.append(entry.getKey());
            requestresult.append(":");
            requestresult.append("'" + entry.getValue() + "'");
        }

        requestresult.append("}");
        return requestresult;
    }
}