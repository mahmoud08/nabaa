package com.madarsoft.nabaa.controls;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.Toast;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.fragments.AlertDialogFrag;
import com.madarsoft.nabaa.fragments.HotNewsFragment;
import com.madarsoft.nabaa.fragments.PleaseWaitFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ZProf on 28/04/2016.
 */
public class UpdateService2 extends Service {

    /**
     * indicates how to behave if the service is killed
     */
    int mStartMode;

    /**
     * interface for clients that bind
     */
    IBinder mBinder;

    /**
     * indicates whether onRebind should be used
     */
    boolean mAllowRebind;
    Toast toast;
    Timer timer;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    SharedPreferences settingsM;
    SharedPreferences.Editor settingEditor;
    private PackageInfo pInfo;
    private JsonParser jsonParesr;
    private JsonParser jsonParser;
    private int tempNewsID = -1;
    private MainControl mainControl;
    private PleaseWaitFragment pleaseWaitFragment;
    private AlertDialogFrag commentFrag;
    private List<News> cachedRecentNews = new ArrayList<>();
    private List<News> cachedHotNews = new ArrayList<>();
    private int count;

    /**
     * Called when the service is being created.
     */

    @Override
    public void onCreate() {
        toast = Toast.makeText(getApplicationContext(), "Service Started ", Toast.LENGTH_LONG);
        final Handler handler = new Handler();
        prefs = getSharedPreferences("MY_PREFS", MODE_PRIVATE);
        editor = getSharedPreferences("MY_PREFS", MODE_PRIVATE).edit();
        settingsM = getSharedPreferences(MainActivity.PREFS_NAME, MODE_PRIVATE);
        settingEditor = getSharedPreferences(MainActivity.PREFS_NAME, MODE_PRIVATE).edit();
        mainControl = new MainControl(this);
        timer = new Timer();
        /*handler.postDelayed(new Runnable() {
            @Override
            public void run() {
            }
        }, 6000);*/

        if (URLs.getUserID() != 0 && prefs.getBoolean("isExist", false)) {
//            settingsM.edit().putBoolean("my_first_time", false).apply();
//            callDefaultJson(URLs.getUserID(), getApplicationContext());
            showDialog(this);
        } else {

            final MainControl ma = new MainControl(getApplicationContext());
            if (!ma.checkInternetConnection()) {
                return;
            }
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    URLs.serviceFlag = false;
                    callJson(URLs.getUserID(), URLs.getRecentFirstTimeStamp(), 25, true, getApplicationContext(), URLs
                            .GET_RECENT_NEWS_URL, URLs.GET_RECENT_NEWS_REQUEST_TYPE);
                    callJson(URLs.getUserID(), URLs.getUrgentFirstTimeStamp(), 25, true, getApplicationContext(), URLs
                            .GET_URGENT_NEWS_URL, URLs.GET_URGENT_NEWS_REQUEST_TYPE);
                }
            }, 0, 120000);
            callJson(URLs.getUserID(), URLs.getFirstDateTime(), 25, true, getApplicationContext(), URLs.GET_HOT_NEWS_URL, URLs
                    .GET_HOT_NEWS_REQUEST_TYPE);
            //callJson(URLs.getUserID(), URLs.getFirstNewsID(), 50, true, getApplicationContext(), URLs.GET_MOST_READED_NEWS_URL, URLs
            // .GET_MOST_READED_NEWS_REQUEST_TYPE);
        }
    }

    /**
     * The service is starting, due to a call to startService()
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return mStartMode;
    }

    void showDialog(Context context) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }
        FragmentManager fm = MainActivity.mainActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        // Create and show the dialog.
        commentFrag = new AlertDialogFrag(getString(R.string.enter_scenario), getString(R.string.yes), getString(R.string.no),
                AlertDialogFrag.DIALOG_TWO_BUTTONS);
        commentFrag.show(ft, "dialog");
        commentFrag.setCancelable(false);
        commentFrag.OnDialogClickListener(new AlertDialogFrag.OnDialogListener() {
            @Override
            public void onPositiveClickListener() {
                //callJson(URLs.GET_USER_SOURCES_URL, URLs.GET_USER_SOURCES_REQUEST_TYPE, URLs.getUserID());
                if (checkConnection())
                    return;
                ///show
                commentFrag.dismiss();
                callJson(0);
//                editor.putBoolean("isExist", false);
//                editor.commit();
            }

            @Override
            public void onNegativeClickListener() {
                if (checkConnection())
                    return;

                callJson(URLs.DELETE_USER_SOURCES_URL, URLs.DELETE_USER_SOURCES_REQUEST_TYPE, URLs.getUserID());
                commentFrag.dismiss();
                pleaseWaitDialog();
                editor.putBoolean("isExist", false);
                editor.commit();
            }
        });
    }

    void showRetryDialog(Context context) {
        if (count == 3) {
            showStartOverDialog(getApplicationContext());
            return;
        }
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }
        FragmentManager fm = MainActivity.mainActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        commentFrag = new AlertDialogFrag(getString(R.string.error_while_retrieve), getString(R.string.retry), getString(R.string
                .start_over),
                AlertDialogFrag.DIALOG_TWO_BUTTONS);
        commentFrag.show(ft, "dialog");
        commentFrag.setCancelable(false);
        commentFrag.OnDialogClickListener(new AlertDialogFrag.OnDialogListener() {
            @Override
            public void onPositiveClickListener() {
                count += 1;
                //callJson(URLs.GET_USER_SOURCES_URL, URLs.GET_USER_SOURCES_REQUEST_TYPE, URLs.getUserID());
                if (checkConnection())
                    return;
                ///show

                try {
                    commentFrag.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                callJson(0);
                editor.putBoolean("isExist", false);
                editor.commit();
            }

            @Override
            public void onNegativeClickListener() {
                if (checkConnection())
                    return;
                callJson(URLs.DELETE_USER_SOURCES_URL, URLs.DELETE_USER_SOURCES_REQUEST_TYPE, URLs.getUserID());
                try {
                    commentFrag.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                editor.putBoolean("isExist", false);
                editor.commit();
            }
        });

    }

    private void showStartOverDialog(final Context context) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        if (!mainControl.checkInternetConnection()) {
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }
        FragmentManager fm = MainActivity.mainActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        commentFrag = new AlertDialogFrag(getString(R.string.many_attempts), getString(R.string.agreed), getString(R.string
                .start_over),
                AlertDialogFrag.DIALOG_ONE_BUTTON);
        commentFrag.show(ft, "dialog");
        commentFrag.setCancelable(false);
        commentFrag.OnDialogClickListener(new AlertDialogFrag.OnDialogListener() {
            @Override
            public void onPositiveClickListener() {

            }

            @Override
            public void onNegativeClickListener() {
                if (checkConnection())
                    return;
                callJson(URLs.DELETE_USER_SOURCES_URL, URLs.DELETE_USER_SOURCES_REQUEST_TYPE, URLs.getUserID());
                try {
                    commentFrag.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                editor.putBoolean("isExist", false);
                editor.commit();
            }
        });
    }

    private void pleaseWaitDialog() {
        FragmentManager fm = MainActivity.mainActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog2");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        pleaseWaitFragment = new PleaseWaitFragment(getString(R.string.loading));
        try {
            pleaseWaitFragment.show(ft, "dialog2");
        } catch (Exception e) {
            e.printStackTrace();
        }
        pleaseWaitFragment.setCancelable(false);

    }

    private boolean checkConnection() {
        try {
            if (!mainControl.checkInternetConnection()) {
                Toast.makeText(getApplicationContext(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                return true;
            }
        } catch (NullPointerException e) {

        }
        return false;
    }

//    private void callCountryCategoryAnyway() {
//        HashMap<String, String> dataObj = new HashMap<String, String>();
//        dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, 0 + "");
//        new JsonParser(this, URLs.GET_COUNTRIES_URL, URLs.GET_COUNTRIES_REQUEST_TYPE, dataObj).execute();
//        jsonParesr = new JsonParser(this, URLs.GET_CATEGORIES_URL, URLs.GET_CATEGORIES_REQUEST_TYPE, dataObj);
//        jsonParesr.execute();
//        jsonParesr.onFinishUpdates(new JsonParser.OnCategoriesAddedListener() {
//            @Override
//            public void onFinished() {
//                callDefaultJson(URLs.getUserID(), getApplicationContext());
//            }
//        });
//    }

    private void callJson(long timeStamp) {
        pleaseWaitDialog();
        HashMap<String, String> dataObj = new HashMap<String, String>();//<<<||| - ||||>>>//
        dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, timeStamp + "");
        new JsonParser(this, URLs.GET_COUNTRIES_URL, URLs.GET_COUNTRIES_REQUEST_TYPE, dataObj).execute();
        jsonParesr = new JsonParser(this, URLs.GET_CATEGORIES_URL, URLs.GET_CATEGORIES_REQUEST_TYPE, dataObj);
        jsonParesr.execute();
        jsonParesr.onFinishUpdates(new JsonParser.OnCategoriesAddedListener() {
            @Override
            public void onFinished() {
                callJson(URLs.GET_USER_SOURCES_URL, URLs.GET_USER_SOURCES_REQUEST_TYPE, URLs.getUserID());
            }
        });
    }

    private void callJson(String url, final int requestType, int userID) {
        HashMap<String, String> dataObj = new HashMap<>();
        //if (requestType == URLs.GET_USER_SOURCES_REQUEST_TYPE || requestType == URLs.DELETE_USER_SOURCES_REQUEST_TYPE)
        dataObj.put(URLs.TAG_USER_SOURCES_USER_ID, userID + "");
        jsonParser = new JsonParser(this, url, requestType, dataObj);
        jsonParser.execute();
        if (requestType == URLs.GET_USER_SOURCES_REQUEST_TYPE) {

            jsonParser.onUserSourcesRetrieval(new JsonParser.OnUserSourcesRetrievalError() {
                @Override
                public void onFinished(boolean isError) {
                    if (isError) {
                        try {
                            pleaseWaitFragment.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        showRetryDialog(getApplicationContext());
                    } else {
                        editor.putBoolean("isExist", false).apply();
//                        editor.putBoolean("isExist", true).apply();
                        callJson(URLs.getUserID(), URLs.getRecentFirstTimeStamp(), 25, true, getApplicationContext(), URLs
                                .GET_RECENT_NEWS_URL, URLs.GET_RECENT_NEWS_REQUEST_TYPE);
                        callJson(URLs.getUserID(), URLs.getUrgentFirstTimeStamp(), 25, true, getApplicationContext(), URLs
                                .GET_URGENT_NEWS_URL, URLs.GET_URGENT_NEWS_REQUEST_TYPE);
                        callJson(URLs.getUserID(), URLs.getFirstDateTime(), 25, true, getApplicationContext(), URLs.GET_HOT_NEWS_URL,
                                URLs.GET_HOT_NEWS_REQUEST_TYPE);
                        startService(new Intent(getBaseContext(), SubscribeService.class));
                        try {
                            pleaseWaitFragment.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            commentFrag.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } else if (requestType == URLs.DELETE_USER_SOURCES_REQUEST_TYPE) {

            jsonParser.onFinishUpdates(new JsonParser.OnUserDataDeletionListener() {
                @Override
                public void onFinished(boolean isDeleted) {

                    if (requestType == URLs.DELETE_USER_SOURCES_REQUEST_TYPE) {
                       // callCountryCategoryAnyway();

//                        callJson(URLs.getUserID(), URLs.getRecentFirstTimeStamp(), 0, true, getApplicationContext(), URLs
//                                .GET_RECENT_NEWS_URL, URLs.GET_RECENT_NEWS_REQUEST_TYPE);
//                        callJson(URLs.getUserID(), URLs.getUrgentFirstTimeStamp(), 0, true, getApplicationContext(), URLs
//                                .GET_URGENT_NEWS_URL, URLs.GET_URGENT_NEWS_REQUEST_TYPE);
//                        callJson(URLs.getUserID(), URLs.getFirstDateTime(), 0, true, getApplicationContext(), URLs.GET_HOT_NEWS_URL,
//                                URLs.GET_HOT_NEWS_REQUEST_TYPE);
//                        pleaseWaitFragment.dismiss();
//                        FragmentManager manager = MainActivity.mainActivity.getSupportFragmentManager();
//                        FragmentTransaction transaction = manager.beginTransaction();
//                        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
//                        transaction.addToBackStack(null);
//                        transaction.add(R.id.parent, new DefaultSourcesFragment());
//                        //transaction.add(R.id.parent, new SourceNews(news.getSourceID(), true, false, ""));
//                        transaction.commit();
                        if (isDeleted) {
                            Toast.makeText(getApplicationContext(), "تم مسح المصادر بنجاح", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "حدث خطأ في عملية المسح", Toast.LENGTH_SHORT).show();
                        }
                        callJson(URLs.getUserID(), URLs.getRecentFirstTimeStamp(), 25, true, getApplicationContext(), URLs
                                .GET_RECENT_NEWS_URL, URLs.GET_RECENT_NEWS_REQUEST_TYPE);
                        callJson(URLs.getUserID(), URLs.getUrgentFirstTimeStamp(), 25, true, getApplicationContext(), URLs
                                .GET_URGENT_NEWS_URL, URLs.GET_URGENT_NEWS_REQUEST_TYPE);
                        callJson(URLs.getUserID(), URLs.getFirstDateTime(), 25, true, getApplicationContext(), URLs.GET_HOT_NEWS_URL,
                                URLs.GET_HOT_NEWS_REQUEST_TYPE);
                        pleaseWaitFragment.dismiss();
//                    } else {
                        //hide
//                        try {
//                            pleaseWaitFragment.dismiss();
//                            //start subscribing service here and
//                            startService(new Intent(getBaseContext(), SubscribeService.class));
//                        } catch (IllegalStateException e) {
//                        }
                    }
                }
            });
        }


    }

    private int getMcc() {
        int mcc = 0;
        TelephonyManager tel = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        String networkOperator = tel.getNetworkOperator();

        if (!TextUtils.isEmpty(networkOperator)) {
            mcc = Integer.parseInt(networkOperator.substring(0, 3));
            int mnc = Integer.parseInt(networkOperator.substring(3));
        }
        return mcc;
    }

//    private void callDefaultJson(int userID, final Context context) {
////        pleaseWaitDialog();
//        if (!mainControl.checkInternetConnection()) {
//            //Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        HashMap<String, String> dataObj = new HashMap<>();
//
//        dataObj.put(URLs.TAG_COUNTRIES_RESOURCES_USERID, userID + "");
//        dataObj.put(URLs.TAG_MCC, +getMcc() + "");
//        jsonParser = new JsonParser(context, URLs.DEFAULT_SOURCES, URLs.DEFAULT_SOURCES_TYPE, dataObj);
//        jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//
//
//        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
//            @Override
//            public void onFinished() {
//
//            }
//
//            @Override
//            public void onFinished(List<Integer> likeResponse) {
//
//            }
//
//            @Override
//            public void onFinished(News newsObj) {
//            }
//
//            @Override
//            public void onFinished(ArrayList<Sources> sourceList) {
////                showDialog(getApplicationContext());
//                settingEditor.putBoolean("recommend", true).apply();
//                callJson(URLs.getUserID(), URLs.getRecentFirstTimeStamp(), 25, true, getApplicationContext(), URLs
//                        .GET_RECENT_NEWS_URL, URLs.GET_RECENT_NEWS_REQUEST_TYPE);
//                callJson(URLs.getUserID(), URLs.getUrgentFirstTimeStamp(), 25, true, getApplicationContext(), URLs
//                        .GET_URGENT_NEWS_URL, URLs.GET_URGENT_NEWS_REQUEST_TYPE);
//                callJson(URLs.getUserID(), URLs.getFirstDateTime(), 25, true, getApplicationContext(), URLs.GET_HOT_NEWS_URL,
//                        URLs.GET_HOT_NEWS_REQUEST_TYPE);
//                pleaseWaitFragment.dismiss();
//            }
//
//            @Override
//            public void onFinished(int id) {
//
//            }
//
//            @Override
//            public void onFinished(boolean success) {
//
//            }
//        });
//    }

    private void callJson(int userID, final String timeStamp, final int countArticle, final boolean isNew, final Context context, final
    String url, final int requestType) {

        HashMap<String, String> dataObj = new HashMap<>();

        if (requestType == URLs.GET_HOT_NEWS_REQUEST_TYPE) {
            dataObj.put(URLs.TAG_USERID, userID + "");
            //dataObj.put(URLs.TAG_REQUEST_ARTICLE_ID, articleID + "");
            dataObj.put(URLs.TAG_COUNT_ARTICLE, countArticle + "");
            dataObj.put(URLs.TAG_PAGE_NUM, HotNewsFragment.pageNo + "");//now it gets 50 news only (page_num not used)
            //dataObj.put(URLs.TAG_DATE_TIME, timeStamp);
        } else {
            dataObj.put(URLs.TAG_USERID, userID + "");
            //dataObj.put(URLs.TAG_REQUEST_ARTICLE_ID, articleID + "");
            dataObj.put(URLs.TAG_COUNT_ARTICLE, countArticle + "");
            dataObj.put(URLs.TAG_IS_NEW, isNew + "");//true for new news, false for paging
            dataObj.put(URLs.TAG_REQUEST_TIME_STAMP, timeStamp);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            new JsonParser(context, url, requestType, dataObj, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            new JsonParser(context, url, requestType, dataObj, true).execute();
    }

    /**
     * A client is binding to the service with bindService()
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Called when all clients have unbound with unbindService()
     */
    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    /**
     * Called when a client is binding to the service with bindService()
     */
    @Override
    public void onRebind(Intent intent) {

    }

    /**
     * Called when The service is no longer used and is being destroyed
     */
    @Override
    public void onDestroy() {
        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
        timer.cancel();
    }
}
