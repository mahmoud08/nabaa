//package com.madarsoft.nabaa.controls;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.net.Uri;
//import android.view.View;
//import android.view.Window;
//
//import com.madarsoft.nabaa.R;
//import com.madarsoft.nabaa.Views.FontTextView;
//
///**
// * Created by Colossus on 16-Nov-16.
// */
//public class AppRater {
//    private final static String APP_TITLE = "nabaa";
//    private final static String APP_PNAME = "com.madarsoft.nabaa";
//
//    private final static int DAYS_UNTIL_PROMPT = 7;
////    private final static int LAUNCHES_UNTIL_PROMPT = 7;
//
//    public static void app_launched(Context mContext) {
//        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
//        if (prefs.getBoolean("dontshowagain", false)) {
//            return;
//        }
//
//        SharedPreferences.Editor editor = prefs.edit();
//
//        // Increment launch counter
//        long launch_count = prefs.getLong("launch_count", 0) + 1;
//        editor.putLong("launch_count", launch_count);
//
//        // Get date of first launch
//        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
//        if (date_firstLaunch == 0) {
//            date_firstLaunch = System.currentTimeMillis();
//            editor.putLong("date_firstlaunch", date_firstLaunch);
//        }
////        if (date_firstLaunch == 1) {
////            date_firstLaunch = System.currentTimeMillis() + 24 * 60 * 60 * 1000;
//////            editor.putLong("date_firstlaunch", date_firstLaunch);
////            if (System.currentTimeMillis() >= date_firstLaunch) {
////                showRateDialog(mContext, editor);
////
////            }
////        }
//        // Wait at least n days before opening
////        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
//        if (!prefs.getBoolean("remind_me", false)) {
//            if (System.currentTimeMillis() >= date_firstLaunch + (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
//                showRateDialog(mContext, editor);
//            }
//        } else {
//            if (System.currentTimeMillis() >= date_firstLaunch + (24 * 60 * 60 * 1000)) {
//                showRateDialog(mContext, editor);
//            }
//        }
//        editor.apply();
//    }
//
//    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
//        final Dialog dialog = new Dialog(mContext);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.rate_app);
//
//        try {
//            dialog.getWindow().setBackgroundDrawable(null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        FontTextView rate = (FontTextView) dialog.findViewById(R.id.warning_ok);
//        FontTextView remindMeLater = (FontTextView) dialog.findViewById(R.id.warning_remind_me_later);
//        FontTextView cancel = (FontTextView) dialog.findViewById(R.id.warning_cancel);
//
//
////        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////
////        View view = inflater.inflate(R.layout.rate_app, null, true);
////
////        AlertDialog.Builder dialog1 = new AlertDialog.Builder(mContext);
////        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
////
////        dialog1.setView(view);
//
////        LinearLayout ll = new LinearLayout(mContext);
////        ll.setOrientation(LinearLayout.VERTICAL);
////        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
////                LinearLayout.LayoutParams.WRAP_CONTENT,
////                LinearLayout.LayoutParams.WRAP_CONTENT
////        );
////        params1.setMargins(50, 10, 50, 10);
//////        TextView tv = new TextView(mContext);
////////        tv.setText(mContext.getString(R.string.rate_text));
//////        tv.setTextSize(25);
//////        tv.setLayoutParams(params1);
////////        tv.setWidth(500);k
////////        tv.setPadding(50, 0, 40, 50);
//////        ll.addView(tv);
////        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
////                LinearLayout.LayoutParams.WRAP_CONTENT,
////                LinearLayout.LayoutParams.WRAP_CONTENT
////        );
////        params.setMargins(110, 10, 50, 10);
////
////        Button b1 = new Button(mContext);
////        b1.setText(mContext.getString(R.string.rate_approve));
////        b1.setLayoutParams(params);
////        b1.setBackgroundColor(Color.parseColor("#00FFFFFF"));
////        b1.setPadding(110, 5, 5, 10);
//        rate.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
//                editor.putBoolean("dontshowagain", true).apply();
//                dialog.dismiss();
//            }
//        });
////        ll.addView(b1);
//////ss//s
////
////        Button b2 = new Button(mContext);
////        b2.setText(mContext.getString(R.string.rate_remind_me));
////        b2.setPadding(110, 10, 50, 10);
////        b2.setBackgroundColor(Color.parseColor("#00FFFFFF"));
////        b2.setLayoutParams(params);
//        remindMeLater.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                editor.putLong("date_firstlaunch", 7).apply();
//                editor.commit();
//                dialog.dismiss();
//            }
//        });
////        ll.addView(b2);
////
////        Button b3 = new Button(mContext);
////        b3.setText(mContext.getString(R.string.rate_cancel));
////
////        b3.setPadding(110, 10, 50, 10);
////        b3.setBackgroundColor(Color.parseColor("#00FFFFFF"));
////        b3.setLayoutParams(params);
//        cancel.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                if (editor != null) {
//                    editor.putLong("date_firstlaunch", 7).apply();
//                    editor.putBoolean("remind_me", true).apply();
//                    editor.commit();
//                }
//                dialog.dismiss();
//            }
//        });
////        ll.addView(b3);
////        dialog.setContentView(ll);
//        dialog.setCancelable(false);
////        dialog.setTitle(R.string.rate_text);
//        dialog.show();
//    }
//}