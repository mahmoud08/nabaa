package com.madarsoft.nabaa.controls;

import android.content.Context;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by basma on 2/8/2017.
 */

public class FirebaseConfigurations {


    public static StorageReference getDatabaseRefrence( Context context)
    {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setApiKey("AIzaSyAib7QzmYu-foSm-K98iuW-rfxwama2r8s")
                .setApplicationId("1:209303939173:android:7acba76d639f7353")
                .setDatabaseUrl("https://madar-ads.firebaseio.com")
.setStorageBucket( "madar-ads.appspot.com")
                .build();
        FirebaseApp secondApp;
        try {

            secondApp = FirebaseApp.initializeApp(context, options, "madar-ads");
        }catch (IllegalStateException e)
        {
            secondApp = FirebaseApp.getInstance("madar-ads");

        }

        FirebaseStorage secondDatabase = FirebaseStorage.getInstance(secondApp);
        return secondDatabase.getReference();
    }


}
