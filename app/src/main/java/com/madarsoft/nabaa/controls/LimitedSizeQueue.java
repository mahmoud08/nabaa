//package com.madarsoft.nabaa.controls;
//
//import java.util.ArrayList;
//
///**
// * Created by ZProf on 17/07/2016.
// */
//public class LimitedSizeQueue<News> extends ArrayList<News> {
//
//
//    private int maxSize;
//
//    public LimitedSizeQueue(int size){
//        this.maxSize = size;
//    }
//
//    public boolean add(News k){
//        boolean r = super.add(k);
//        if (size() > maxSize){
//            removeRange(0, size() - maxSize - 1);
//        }
//        return r;
//    }
//
//    public News getYongest() {
//        return get(size() - 1);
//    }
//
//    public News getOldest() {
//        return get(0);
//    }
//}