package com.madarsoft.nabaa.controls;

/**
 * Created by basma on 2/5/2017.
 */


import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.InterstitialCallbacks;
import com.appodeal.ads.NativeAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.entities.AdSettings;
import com.madarsoft.nabaa.entities.AdUnit;
import com.madarsoft.nabaa.entities.AppInfo;

import java.util.HashMap;
import java.util.List;

import io.presage.IADHandler;
import io.presage.Presage;

/*import io.presage.IADHandler;
import io.presage.Presage;*/

/*import com.madarsoft.firebasedatabasereader.madarsoftAds.AdsManager;
import com.madarsoft.firebasedatabasereader.madarsoftAds.MadarsoftAdsRequest;
import com.madarsoft.firebasedatabasereader.objects.RectangleBannerAd;*/

public class AdsControlNabaa {

    public final static int PROGRAM_ID = 18;
    public static final String NABAA_ID = "ae9a657bf2d5413dbbb6b5f6819463123be0ba83af6b90e3";
   // public static final int getNoOfCahedAds = 10;


    Activity context;
    int screenId = 1;

    AdSettings adSettings;

    public AdsControlNabaa(Activity c) {
        // TODO Auto-generated constructor stub
        context = c;




        adSettings=FirebaseStorageControl.loadAdsData(c);


    }
    public    int getNoOfCahedAds()
    {
        if (adSettings!=null)
        {
            return adSettings.getAppInfo().instreamCacheSize;
        }
        return 5;
    }


public String getGoogleAdunit(int realPosition)
{if (adSettings!=null)
{
    try {
        return    adSettings.getInstremAdUnit(realPosition,AdUnit.MREC).getAdPlacementAndUnit().getCod();
    }catch (NullPointerException e)
    {

    }

}
    return "ca-app-pub-4264562862176288/8162001954";
}

   /* public String getGoogleAdunit()
    {if (adSettings!=null)
    {
        return    adSettings.getMRECAdUnit().getAdPlacementAndUnit().getCod();
    }
        return "ca-app-pub-4264562862176288/3460889057";
    }*/

    public void getBannerAd(View view, String screenName) {
        AdUnit banner=adSettings.getBannerAdUnitByScreenName(screenName);
        if (view !=null &&adSettings!=null&& banner!=null &&banner.isEnabled()) {
        Appodeal.setBannerViewId(view.getId());
        Appodeal.show(context, Appodeal.BANNER_BOTTOM);
        }
    }
public  boolean isOguryEnabled()
{
    if (adSettings!=null && adSettings.getAppInfo().isOguryEnabled())
    {
        return true;
    }
return false;
}

    public void getSplashAd(final String screenName) {
        // Appodeal.show(context, Appodeal.INTERSTITIAL);

        if (adSettings != null) {
            AdUnit splashAd = adSettings.getSplashAdUnitByScreenName(screenName);
            if (splashAd != null && splashAd.isEnabled() && splashAd.getType() != null) {
                if (System.currentTimeMillis()>SharedPrefrencesMethods.loadSavedPreferencesLong(context,(AdUnit.LAST_APPEARENCE_TIME)+screenName)+splashAd.getDurationBetweenTwoAppearnceInMin()*(60*1000)){

                    if (splashAd.getType().equalsIgnoreCase(AdUnit.TYPE_APPODEAL)) {

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            // Appodeal.cache(context, Appodeal.INTERSTITIAL);
                            Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {
                                //   private Toast mToast;

                                @Override
                                public void onInterstitialLoaded(boolean isPrecache) {
                                    //     showToast("onInterstitialLoaded");
                                    Appodeal.show(context, Appodeal.INTERSTITIAL);
                                }

                                @Override
                                public void onInterstitialFailedToLoad() {

                                    //showToast("onInterstitialFailedToLoad");
                                }

                                @Override
                                public void onInterstitialShown() {
                                    //  showToast("onInterstitialShown");
                                    SharedPrefrencesMethods.savePreferencesLong(context, AdUnit.LAST_APPEARENCE_TIME+screenName, System.currentTimeMillis());

                                }

                                @Override
                                public void onInterstitialClicked() {
                                    // showToast("onInterstitialClicked");
                                }

                                @Override
                                public void onInterstitialClosed() {
                                    //  showToast("onInterstitialClosed");
                                }

                            });
                        }
                    }, splashAd.getDisplay_delay_seconds() * 1000);


                } else if (isOguryEnabled() && splashAd.getType().equalsIgnoreCase(AdUnit.TYPE_OGURY)) {

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            AnalyticsApplication application = (AnalyticsApplication) context.getApplication();
                         final   Tracker mTracker = application.getDefaultTracker();
                            Presage.getInstance().load(new IADHandler() {

                                @Override
                                public void onAdAvailable() {
                                    Log.i("PRESAGE", "ad available");


                                    //mTracker.setScreenName(getString(R.string.country_list));
                                    mTracker.send(new HitBuilders.EventBuilder("UGORY","ad available").build());
                                }

                                @Override
                                public void onAdNotAvailable() {
                                    Log.i("PRESAGE", "no ad available");

                                    mTracker.send(new HitBuilders.EventBuilder("UGORY","No ad available").build());

                                }

                                @Override
                                public void onAdLoaded() {
                                    Log.i("PRESAGE", "an ad in loaded, ready to be shown");
                                    Presage.getInstance().show(this);
                                    mTracker.send(new HitBuilders.EventBuilder("UGORY","an ad in loaded, ready to be shown").build());

                                }

                                @Override
                                public void onAdDisplayed() {
                                    SharedPrefrencesMethods.savePreferencesLong(context, AdUnit.LAST_APPEARENCE_TIME+screenName, System
                                            .currentTimeMillis());

                                    Log.i("PRESAGE", "ad displayed");
                                    mTracker.send(new HitBuilders.EventBuilder("UGORY","ad displayed").build());

                                }

                                @Override
                                public void onAdClosed() {
                                    Log.i("PRESAGE", "ad closed");
                                    mTracker.send(new HitBuilders.EventBuilder("UGORY","ad closed").build());

                                }

                                @Override
                                public void onAdError(int code) {
                                    Log.i("PRESAGE", String.format("error with code %d", code));
                                    mTracker.send(new HitBuilders.EventBuilder("UGORY","ad error code "+code).build());

                                }
                            });
                        }
                    }, splashAd.getDisplay_delay_seconds() * 1000);


                }
            }
        }
    }
    }

    //call in on onpause
    public void unregisterAdListening() {


    }

    //call in on onresume
    public void registerForAdsListening()
    {
        Appodeal.onResume(context, Appodeal.BANNER);
        Appodeal.onResume(context, Appodeal.MREC);
    }





    public boolean isAdPosition(int realPosition,int startPosition,int repeate) {
        return (repeate>0&& (realPosition -
                startPosition >=0)&&(realPosition - startPosition) % repeate == 0);
        //return realPosition==getNativeAdsStartPosition()+((realPosition-getNativeAdsStartPosition())%getNativeAdsRepeating())*getNativeAdsRepeating();
    }
    public boolean isAdPosition(int realPosition,boolean isNative) {
        AdUnit nativeAdUnit;
        nativeAdUnit = getInstreamAdUnit(isNative,realPosition);
if (nativeAdUnit!=null) {
    return (    adSettings.getAppInfo().getInstreamAdsRepeating() > 0 && (realPosition -
                adSettings.getAppInfo().getInstreamAdsStartPosition()  >= 0) && (realPosition -     adSettings.getAppInfo().getInstreamAdsStartPosition() ) %     adSettings.getAppInfo().getInstreamAdsRepeating() == 0);
    //return realPosition==getNativeAdsStartPosition()+((realPosition-getNativeAdsStartPosition())%getNativeAdsRepeating())*getNativeAdsRepeating();


}
    return false;
    }


 /*   public  int getInstreamPosition(int position, boolean isNative)
    {
        if(adSettings!=null) {

            AdUnit nativeAdUnit ;//= adSettings.getMRECAdUnit();
            nativeAdUnit = getInstreamAdUnit(isNative);
            if (nativeAdUnit!=null) {
                return ((position - nativeAdUnit.getStart_after()) / getNoOfCahedAds) % nativeAdUnit.getRepeat_every();
            }

        }
        return -1;
    }*/
public  int getInstreamPosition(int position, boolean isNative)
{
    if(adSettings!=null) {

        AdUnit nativeAdUnit ;//= adSettings.getMRECAdUnit();
        nativeAdUnit = getInstreamAdUnit(isNative,position);
        if (nativeAdUnit!=null) {
            return ((position -     adSettings.getAppInfo().getInstreamAdsStartPosition() ) / adSettings.getAppInfo().getInstreamAdsRepeating()) % getNoOfCahedAds();
        }

    }
return -1;
}

    public AdUnit getInstreamAdUnit(boolean isNative,int realPosition) {
        AdUnit nativeAdUnit;
        if (isNative)
        {
            nativeAdUnit = adSettings.getNativeAdUnit();
        }else {
            nativeAdUnit = adSettings.getInstremAdUnit(realPosition,AdUnit.MREC);
        }
        return nativeAdUnit;
    }

    public NativeAd getNativeAdForCertainPosition(int position, List<NativeAd> nativeAds)
    {

       if(adSettings!=null) {
           AdUnit nativeAdUnit=adSettings.getNativeAdUnit();
           if (nativeAdUnit!=null && nativeAdUnit.isEnabled()) {
               int tempPosition = position;
               if (nativeAds == null) {
                   return null;
               }
               for (int i = 0; i < nativeAds.size(); i++) {
                   position = tempPosition;


                   if (isAdPosition(position,    adSettings.getAppInfo().getInstreamAdsStartPosition() ,    adSettings.getAppInfo().getInstreamAdsRepeating())) {
                       // position = (position / request.getNativeAdsRepeating());

                       if (nativeAds.size() > 0) {
                           position = ((position -     adSettings.getAppInfo().getInstreamAdsStartPosition() ) /     adSettings.getAppInfo().getInstreamAdsRepeating()) % nativeAds.size();
                           if (position < nativeAds.size() && position>0) {
                               Log.e("posiotion=" + tempPosition, "adposition=" + position);
                               return nativeAds.get(position);
                           } else return null;
                       }
                   } else {
                       return null;
                   }

               }
           }
       }
        return null;
    }


    public  View getAdmobAd(int position, HashMap<Integer,View> nativeAds,boolean isNative)
    {

        if(adSettings!=null) {
            AdUnit nativeAdUnit;
            nativeAdUnit = getInstreamAdUnit(isNative,position);

            if (nativeAdUnit!=null && nativeAdUnit.isEnabled()) {
                int tempPosition = position;
                if (nativeAds == null) {
                    return null;
                }
                Log.e("admob posiotion=" + tempPosition, "adposition=" + getInstreamPosition(position,false));

                try {
                    return nativeAds.get(getInstreamPosition(position,false));

                }catch (IndexOutOfBoundsException e)
                {

                }
              //  return nativeAds.get(getInstreamPosition(position,false));


             /*   for (int i = 0; i < nativeAds.size(); i++) {
                    position = tempPosition;


                    if (isAdPosition(position,    adSettings.getAppInfo().getInstreamAdsStartPosition() ,    adSettings.getAppInfo().getInstreamAdsRepeating())) {
                        // position = (position / request.getNativeAdsRepeating());

                        if (nativeAds.size() > 0&& position>0) {
                            position = ((position -     adSettings.getAppInfo().getInstreamAdsStartPosition() ) / getNoOfCahedAds) %     adSettings.getAppInfo().getInstreamAdsRepeating();
                           // if (position < nativeAds.size()) {
                                Log.e("admob posiotion=" + tempPosition, "adposition=" + position);
                                return nativeAds.get(position);
                           // } else return null;
                        }
                    } else {
                        return null;
                    }

                }*/
            }
        }
        return null;
    }
    public boolean isMediumRecAdPosition(int position) {
        return (position%10==0);
    }
}

