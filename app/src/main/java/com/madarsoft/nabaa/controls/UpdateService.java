//package com.madarsoft.nabaa.controls;
//
//import android.app.Service;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Handler;
//import android.os.IBinder;
//import android.os.Looper;
//import android.support.v4.content.LocalBroadcastManager;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.madarsoft.nabaa.entities.URLs;
//import com.madarsoft.nabaa.fragments.NewsFragment;
//import com.madarsoft.nabaa.interfaces.OnListIsFull;
//
//import java.util.HashMap;
//
///**
// * Created by Colossus on 4/3/2016.
// */
//public class UpdateService extends Service implements OnListIsFull {
//
//    static final public String COPA_RESULT = "com.controlj.copame.backend.COPAService.REQUEST_PROCESSED";
//    static final public String COPA_MESSAGE = "com.controlj.copame.backend.COPAService.COPA_MSG";
//    private static final String TAG = "HelloService";
//    private Handler mHandler;
//    private Thread thread;
//    private LocalBroadcastManager broadcaster;
//    private NewsFragment newsFragment = new NewsFragment();
//    private boolean isFull;
//    private Thread task;
////    private boolean isRunning;
//
//    public void sendResult(String message) {
//        Intent intent = new Intent(COPA_RESULT);
//        if (message != null)
//            intent.putExtra(COPA_MESSAGE, message);
//        broadcaster.sendBroadcast(intent);
//    }
//
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        Log.i(TAG, "Service onStartCommand");
////        mHandler = new Handler();
////        thread = new Thread(new Runnable() {
////            @Override
////            public void run() {
////                mHandler.postDelayed(thread, 10000);
////                callJson(URLs.getUserID(), 0, 25, true, getApplicationContext());
////            }
////        });
////        thread.start();
//
//
//        mHandler = new Handler();
//        task = new Thread(new Runnable() {
//            public void run() {
////                NewPostsAsyncTask newPostsAsyncTask = new NewPostsAsyncTask();
////                newPostsAsyncTask.execute();
//
//                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        broadcaster = LocalBroadcastManager.getInstance(getApplicationContext());
//                        callJson(URLs.getUserID(), 0, 2, true, getApplicationContext());
//                        if (!isFull) {
//
//                            sendResult("isFull");
//                        }
//                    }
//                }, 50000);
//            }
//        });
//        task.start();
//        return Service.START_STICKY;
//    }
//
//    @Override
//    public void onDestroy() {
//        Log.i(TAG, "Service onDestroy");
//    }
//
//    private void callJson(int userID, int articleID, int countArticle,
//                          boolean isNew, Context context) {
//        MainControl mainControl = new MainControl(context);
//        if (!mainControl.checkInternetConnection()) {
//            Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
//            return;
//
//        }
//        HashMap<String, String> dataObj = new HashMap<>();
//        dataObj.put(URLs.TAG_USERID, userID + "");
//        dataObj.put(URLs.TAG_REQUEST_ARTICLE_ID, articleID + "");
//        dataObj.put(URLs.TAG_COUNT_ARTICLE, countArticle + "");
//        dataObj.put(URLs.TAG_IS_NEW, isNew + "");
//        new JsonParser(getApplicationContext(), URLs.GET_RECENT_NEWS_URL, URLs.GET_RECENT_NEWS_REQUEST_TYPE, dataObj, true).execute();
//
//    }
//
//
//    @Override
//    public void onListIsFull() {
//        isFull = true;
//    }
//}
//
