package com.madarsoft.nabaa.controls;


import android.content.Context;

import com.google.gson.Gson;
import com.madarsoft.nabaa.entities.AdSettings;
import com.madarsoft.nabaa.entities.AdUnit;
import com.madarsoft.nabaa.entities.AppInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class JsonManager {


    public static final String RESULT = "result";

    public static boolean isUserDeviceIdSaved(String jsonStr)
    {
        try {


            return new JSONObject(jsonStr).getBoolean(RESULT);
        } catch (JSONException e) {

        }
        return false;
    }

    public static AdSettings getAdSettings(String json, Context context)
    {

        AdSettings adSettings=new AdSettings();
        try {
            JSONObject adSettingsJson=new JSONObject(json).getJSONObject("ad_settings");
            JSONObject inStreamAds= adSettingsJson.getJSONObject("in_stream");
            JSONObject splashAds= adSettingsJson.getJSONObject("Interstitial");
            JSONObject bannerAds = adSettingsJson.getJSONObject("banner");
            JSONArray nativeadUnitsJson=inStreamAds.getJSONArray("unit");
            JSONArray bannerAdUnitsJson=bannerAds.getJSONArray("unit");

            JSONArray splashAdUnitsJson=splashAds.getJSONArray("unit");

            ArrayList<AdUnit> nativeAdUnits=getAllAdUnits(nativeadUnitsJson);
            ArrayList<AdUnit> bannerAdUnits=getAllAdUnits(bannerAdUnitsJson);
            ArrayList<AdUnit> splashAdUnits=getAllAdUnits(splashAdUnitsJson);
            adSettings.setAppInfo((AppInfo) parse(adSettingsJson.getJSONObject("appInfo").toString(), AppInfo.class));
            adSettings.setSavedDataDate(SharedPrefrencesMethods.loadSavedPreferencesLong(context,FirebaseStorageControl.ADS_DATA_DATE));

            adSettings.getNativeAds().addAll(nativeAdUnits);
            adSettings.getBannerAds().addAll(bannerAdUnits);
            adSettings.getSplashAds().addAll(splashAdUnits);

        }catch (JSONException e)
        {
            e.printStackTrace();
        }
return adSettings;

    }

    public static ArrayList<AdUnit> getAllAdUnits(JSONArray adUnitsJson)
    {
        ArrayList<AdUnit> res=new ArrayList<>();
        for (int i = 0; i <adUnitsJson.length() ; i++) {
            try {
             res.add((AdUnit) parse(adUnitsJson.get(i).toString(),AdUnit.class));

            }catch (JSONException e)
            {}
        }
        return res;
    }


    public static Object parse(String json, Class typeClass) {
        Gson gson = new Gson();
        try {
            return gson.fromJson(json, typeClass);
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static Boolean getBoolean(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return jsonObject.getBoolean(RESULT);
        } catch (JSONException e) {
            e.printStackTrace();

            return false;
        }
    }


    public static Object parse(String json, Type typeClass) {
        Gson gson = new Gson();
        try {
            return gson.fromJson(json, typeClass);
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }


    public static JSONObject convertAllJsonObjectFieldsToString(String string) {


        try {
            JSONObject result = new JSONObject();
            JSONObject jsonObject = new JSONObject(string);
            Iterator<String> iterator = jsonObject.keys();
            String key;
            while (iterator.hasNext()) {
                key = iterator.next();
                result.put(key, String.valueOf(jsonObject.get(key)));
            }
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }








    public static JSONArray convertAllJsonArrayFieldsToString(JSONArray jsonArray)
    {

        try {
            JSONArray result = new JSONArray();
            //JSONArray jsonArray  = new JSONArray(string);
            String key;
            for (int i = 0; i < jsonArray.length() ;i++ )
            {
                result.put(convertAllJsonObjectFieldsToString(jsonArray.get(i).toString()));
            }
            return result;
        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }

    }
    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }
    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }


    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

  /*  public static  String getRequestResult(String response)
    {
        try
        {
            return ((new JSONObject(response)).getString(Constants.MOSQUE.RESULT_TAG)) ;
        } catch (JSONException e)
        {
            e.printStackTrace();
            return null;
        }

    }
*/























public  static void findTheMissedField(JSONObject Correct,JSONObject target)
{
    Iterator<String> iterator = Correct.keys();
    String key;
    while (iterator.hasNext())
    {
        key = iterator.next();

        try
        {
        target.get(key);
        try
        {
        JSONArray jsonArrayCorrect = new JSONArray(Correct.get(key).toString());
       JSONArray jsonArrayTarget = new JSONArray(target.get(key).toString());
        for( int i=0;i<jsonArrayCorrect.length();i++)
        {
            findTheMissedField(jsonArrayCorrect.getJSONObject(i),jsonArrayTarget.getJSONObject(i));

        }
        }catch (JSONException je)
        {
         je.printStackTrace();
        }


        }catch (JSONException e)
        {
            e.printStackTrace();
        return ;
        }
    }


}








}


/*
    public static String getobjectAsJsonAllStrings (Object obj)
    {

        JSONObject jsonObject = new JSONObject();
        Class<?> c = obj.getClass();
        Field[] fields = c.getDeclaredFields();
       // Map<String, Object> temp = new HashMap<String, Object>();

        for( Field field : fields )
        {
            try
            {
                try
                {
                    String value;

                    if(field.get(obj).getClass().isPrimitive())
                    {
                        value = getobjectAsJsonAllStrings(field.get(obj));
                    }else
                    {
                        value = String.valueOf(field.get(obj));
                    }
                    jsonObject.put(field.getName().toString(),value );

                }catch (JSONException e)
                {
                    e.printStackTrace();
                    Log.e(Constants.TAG,e.getMessage());
                }

            }
            catch (IllegalArgumentException e1)
            {
                return "";
            } catch (IllegalAccessException e1)
            {
                return "";
            }
        }
    return jsonObject.toString();
    }

*/

