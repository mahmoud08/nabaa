/*
package com.madarsoft.nabaa.controls;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.InterstitialCallbacks;
import com.appodeal.ads.NativeAd;
import com.madarsoft.nabaa.entities.AdSettings;
import com.madarsoft.nabaa.entities.AdUnit;

import java.util.List;

*/
/**
 * Created by basma on 8/1/2017.
 *//*


public class MobFoxAdsSdkControl {

    public final static int PROGRAM_ID = 18;
    public static final String NABAA_ID = "ae9a657bf2d5413dbbb6b5f6819463123be0ba83af6b90e3";


    Activity context;
    int screenId = 1;

    AdSettings adSettings;

    public MobFoxAdsSdkControl(Activity c) {
        // TODO Auto-generated constructor stub
        context = c;




        adSettings=FirebaseStorageControl.loadAdsData(c);

    }





    public void getBannerAd(View view, String screenName) {
        AdUnit banner=adSettings.getBannerAdUnitByScreenName(screenName);
        if (view !=null &&adSettings!=null&& banner!=null &&banner.isEnabled()) {
            Appodeal.setBannerViewId(view.getId());
            Appodeal.show(context, Appodeal.BANNER_BOTTOM);
        }
    }


    public void getSplashAd(String screenName) {
        // Appodeal.show(context, Appodeal.INTERSTITIAL);

        if (adSettings!=null) {
            AdUnit splashAd = adSettings.getSplashAdUnitByScreenName(screenName);
            if (splashAd!=null && splashAd.isEnabled()){

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        // Appodeal.cache(context, Appodeal.INTERSTITIAL);
                        Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {
                            //   private Toast mToast;

                            @Override
                            public void onInterstitialLoaded(boolean isPrecache) {
                                //     showToast("onInterstitialLoaded");
                                Appodeal.show(context, Appodeal.INTERSTITIAL);
                            }

                            @Override
                            public void onInterstitialFailedToLoad() {

                                //showToast("onInterstitialFailedToLoad");
                            }

                            @Override
                            public void onInterstitialShown() {
                                //  showToast("onInterstitialShown");
                            }

                            @Override
                            public void onInterstitialClicked() {
                                // showToast("onInterstitialClicked");
                            }

                            @Override
                            public void onInterstitialClosed() {
                                //  showToast("onInterstitialClosed");
                            }

                        });
                    }
                }, splashAd.getDisplay_delay_seconds()*1000);


            }
        }
    }

    //call in on onpause
    public void unregisterAdListening() {


    }

    //call in on onresume
    public void registerForAdsListening()
    {
        Appodeal.onResume(context, Appodeal.BANNER);
        Appodeal.onResume(context, Appodeal.MREC);
    }

    public void getNativeAd( int pos) {

    }



    public boolean isAdPosition(int realPosition,int startPosition,int repeate) {
        return (repeate>0&& (realPosition -
                startPosition >=0)&&(realPosition - startPosition) % repeate == 0);
        //return realPosition==getNativeAdsStartPosition()+((realPosition-getNativeAdsStartPosition())%getNativeAdsRepeating())*getNativeAdsRepeating();
    }

 */
/*   public NativeAd getNativeAdForCertainPosition(int position, List<NativeAd> nativeAds)
    {

        if(adSettings!=null) {
            AdUnit nativeAdUnit=adSettings.getNativeAdUnit();
            if (nativeAdUnit!=null && nativeAdUnit.isEnabled()) {
                int tempPosition = position;
                if (nativeAds == null) {
                    return null;
                }
                for (int i = 0; i < nativeAds.size(); i++) {
                    position = tempPosition;


                    if (isAdPosition(position,nativeAdUnit.getStart_after(),nativeAdUnit.getRepeat_every())) {
                        // position = (position / request.getNativeAdsRepeating());

                        if (nativeAds.size() > 0) {
                            position = ((position - nativeAdUnit.getStart_after()) / nativeAdUnit.getRepeat_every()) % nativeAds.size();
                            if (position < nativeAds.size()) {
                                Log.e("posiotion=" + tempPosition, "adposition=" + position);
                                return nativeAds.get(position);
                            } else return null;
                        }
                    } else {
                        return null;
                    }

                }
            }
        }
        return null;
    }*//*




    public boolean isMediumRecAdPosition(int position) {
        return (position%10==0);
    }
}

*/
