package com.madarsoft.nabaa.controls;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;

import org.jsoup.Jsoup;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Eng-Karim on 2/5/2017.
 */

public class AutoUpdateControl {
    public static final int UPDATE_NOTIFIY_DAYS = 7;
    public static final int UPDATE_NOTIFIY_DAYS_LONG = 30;
    public static final String UPDATE_NOTIFY_TARGET_DATE = "update_notify_target_date";
    public static final int UPDATE_NOTIFIY_DAYS_SHORT = 1;
    Context context;

    public AutoUpdateControl(Context c) {
        context = c;
    }

    public static void setDateToMuteUpdateNotify(Context context, int noOfDays) {
        long currentTime = System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(currentTime));
        calendar.add(Calendar.DAY_OF_YEAR, noOfDays);
        Date newDate = calendar.getTime();
        Long targetTime = getTimeLongByDate(newDate);
        SharedPreferences preferences = context.getSharedPreferences(MainActivity.PREFS_NAME, 0);
        preferences.edit().putLong(AutoUpdateControl.UPDATE_NOTIFY_TARGET_DATE, targetTime).apply();
    }

    public static boolean checkDateToShowUpdateNotifyPopup(Context context) {
        long currentTime = System.currentTimeMillis();
        SharedPreferences preferences = context.getSharedPreferences(MainActivity.PREFS_NAME, 0);
        Long targetTime = preferences.getLong(AutoUpdateControl.UPDATE_NOTIFY_TARGET_DATE, -1);
        if (targetTime != -1) {
            if (currentTime < targetTime)
                return false;
            else
                return true;
        }
        return true; // in case of the user un checked the mute box
    }

    public static Long getTimeLongByDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.getTimeInMillis();
    }

    public static void checkForUpdate(Activity mContext) {
        if (MainControl.checkInternetConnection(mContext)) {
            if (AutoUpdateControl.checkDateToShowUpdateNotifyPopup(mContext)) {
                new GetVersionCode(mContext).execute();
            }
        }
    }

    private static void showUpdatePopup(final Activity mContext) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.update_popup);

        CheckBox checkBox = (CheckBox) dialog.findViewById(R.id.update_check_box);
        TextView tv_ok = (TextView) dialog.findViewById(R.id.update_ok);
        TextView tv_cancel = (TextView) dialog.findViewById(R.id.update_later);
        TextView tv_dismiss = (TextView) dialog.findViewById(R.id.update_no);
        tv_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    AutoUpdateControl.setDateToMuteUpdateNotify(mContext, AutoUpdateControl.UPDATE_NOTIFIY_DAYS_LONG);
                } else {
                    AutoUpdateControl.setDateToMuteUpdateNotify(mContext, AutoUpdateControl.UPDATE_NOTIFIY_DAYS);
                }
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                AutoUpdateControl.setDateToMuteUpdateNotify(mContext, AutoUpdateControl.UPDATE_NOTIFIY_DAYS);
            }
        });
        tv_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=" + mContext.getPackageName()));
                mContext.startActivity(intent);
                AutoUpdateControl.setDateToMuteUpdateNotify(mContext, AutoUpdateControl.UPDATE_NOTIFIY_DAYS_SHORT);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static class GetVersionCode extends AsyncTask<Void, String, String> {
        private String currentVersion;
        private Activity mContext;

        public GetVersionCode(Activity mContext) {
            this.mContext = mContext;
            try {
                currentVersion = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + mContext.getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    showUpdatePopup(mContext);
                }
            }
            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
        }
    }

}
