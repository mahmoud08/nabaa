package com.madarsoft.nabaa.controls;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.view.Window;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.Views.FontTextView;

public class RateItDialogFragment {

    private final static String APP_PNAME = "com.madarsoft.nabaa";

    private static int DAYS_UNTIL_PROMPT = 7;
    private static int LAUNCHES_UNTIL_PROMPT = 0;
    private static long EXTRA_DAYS;
    private static long EXTRA_LAUCHES;
    private static SharedPreferences prefs;
    private static SharedPreferences.Editor editor;
    private static Activity activity;

    public static void app_launched(Activity activity1) {
        activity = activity1;

        prefs = activity.getSharedPreferences("apprater", Context.MODE_PRIVATE);
        if (prefs.getBoolean("dontshowagain", false))
            return;

        editor = prefs.edit();

        EXTRA_DAYS = prefs.getLong("extra_days", 0);
        EXTRA_LAUCHES = prefs.getLong("extra_launches", 0);

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launch_count >= (LAUNCHES_UNTIL_PROMPT + EXTRA_LAUCHES))
            if (System.currentTimeMillis() >= date_firstLaunch + (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000) + EXTRA_DAYS)
                showRateDialog();

        editor.commit();
    }

    public static void showRateDialog() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rate_app);



        try {
            dialog.getWindow().setBackgroundDrawable(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FontTextView rate = (FontTextView) dialog.findViewById(R.id.warning_ok);
        FontTextView remindMeLater = (FontTextView) dialog.findViewById(R.id.warning_remind_me_later);
        FontTextView cancel = (FontTextView) dialog.findViewById(R.id.warning_cancel);

        rate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                editor.putBoolean("dontshowagain", true);
                editor.commit();
                dialog.dismiss();
            }
        });


        remindMeLater.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                delayDays(7);
//                delayLaunches(10);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    delayDays(2);
//                    editor.commit();
                }
                dialog.dismiss();
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (editor != null) {
                    delayDays(2);
//                    editor.commit();
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
//        dialog.setTitle(R.string.rate_text);
        dialog.show();
    }


    private static void delayLaunches(int numberOfLaunches) {
        long extra_launches = prefs.getLong("extra_launches", 0) + numberOfLaunches;
        editor.putLong("extra_launches", extra_launches);
        editor.commit();
    }

    private static void delayDays(int numberOfDays) {
        Long extra_days = prefs.getLong("extra_days", 0) + (numberOfDays * 1000 * 60 * 60 * 24);
        editor.putLong("extra_days", extra_days);
        editor.apply();
    }
}