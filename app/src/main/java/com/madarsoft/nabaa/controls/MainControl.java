package com.madarsoft.nabaa.controls;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.activities.Splash;
import com.madarsoft.nabaa.database.DataBaseAdapter;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Profile;
import com.madarsoft.nabaa.entities.Sources;
import com.madarsoft.nabaa.entities.URLs;
import com.madarsoft.nabaa.fragments.AlertDialogFrag;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by ZProf on 22/03/2016.
 */
public class MainControl {

    private final SharedPreferences prefs;
    private final SharedPreferences myPrefs;
    private final SharedPreferences.Editor myPrefsEditor;
    Context context;
    SharedPreferences.Editor editor;
    private PackageInfo pInfo;
    private JsonParser jsonParser;
    private OnUserExistListner onUserExistListner;

    public MainControl(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences("MY_PREFS", context.MODE_PRIVATE);
        editor = context.getSharedPreferences("MY_PREFS", context.MODE_PRIVATE).edit();
        myPrefs = context.getSharedPreferences(MainActivity.PREFS_NAME, context.MODE_PRIVATE);
        myPrefsEditor = context.getSharedPreferences(MainActivity.PREFS_NAME, context.MODE_PRIVATE).edit();
    }

    private static Date localToUtc(Date localDate) {
        return new Date(localDate.getTime() - TimeZone.getDefault().getOffset(localDate.getTime()));
    }

    private static String timeZone() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault());
        String timeZone = new SimpleDateFormat("Z").format(calendar.getTime());
        return timeZone.substring(0, 3) + ":" + timeZone.substring(3, 5);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static boolean hasActiveInternetConnection(Context context) {

        if (isNetworkAvailable(context)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                Log.e("LOG_TAG", "Error checking internet connection", e);
            }
        } else {
            Log.d("LOG_TAG", "No network available!");
        }
        return false;
    }

    public static void writeCacheFile(Context context, ArrayList<News> newsLists, String tag) throws IOException {
        File suspend_f = new File(context.getCacheDir().getAbsoluteFile() + File.separator + tag);
        FileOutputStream fos = new FileOutputStream(suspend_f);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(newsLists);
        oos.close();
    }

    public static List<News> readCachedFile(File suspend_f) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(suspend_f);
        ObjectInputStream ois = new ObjectInputStream(fis);
        List<News> news = (List<News>) ois.readObject();
        ois.close();
        return news;
    }

    public static List<News> addToStack(List<News> cachedList, List<News> newsList) {

        int diff = cachedList.size() - newsList.size();
        for (int i = cachedList.size() - 1; i >= diff; i--) {
            cachedList.remove(i);
        }
        cachedList.addAll(0, newsList);

        return cachedList;
    }

    public static boolean checkInternetConnection(Activity context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        // ARE WE CONNECTED TO THE NET
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable()
                    && cm.getActiveNetworkInfo().isConnected();
        } else
            return false;
    }

    public static float pixelsToSp(Context context, float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px / scaledDensity;
    }

    public static void smoothScrollToPosition(final AbsListView view, final int position) {
        View child = getChildAtPosition(view, position);
        // There's no need to scroll if child is already at top or view is already scrolled to its end
        if ((child != null) && ((child.getTop() == 0) || ((child.getTop() > 0) && !view.canScrollVertically(1)))) {
            return;
        }

        view.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(final AbsListView view, final int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    view.setOnScrollListener(null);

                    // Fix for scrolling bug
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            view.setSelection(position);
                        }
                    });
                }
            }

            @Override
            public void onScroll(final AbsListView view, final int firstVisibleItem, final int visibleItemCount,
                                 final int totalItemCount) {
            }
        });

        // Perform scrolling to position
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                view.smoothScrollToPositionFromTop(position, 0);
            }
        });
    }

    public static View getChildAtPosition(final AdapterView view, final int position) {
        final int index = position - view.getFirstVisiblePosition();
        if ((index >= 0) && (index < view.getChildCount())) {
            return view.getChildAt(index);
        } else {
            return null;
        }
    }

    public Calendar setTimeZone(long articleDate, String timeOffset) throws NumberFormatException {
        long unixSeconds = articleDate;
        Date date = localToUtc(new Date(unixSeconds * 1000L));
        //TimeZone tz = TimeZone.getDefault();
        //Date ret = new Date(date.getTime() - tz.getRawOffset());
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
        cal.setTime(date);
        String timeZone = timeZone();
        Character sign = timeZone.charAt(0);
        timeZone = timeZone.substring(1);
        String aa[] = timeZone.split(":");
        int serverHours = Integer.parseInt(timeOffset.substring(0, 2));

        if (sign == '+') {
            cal.add(Calendar.HOUR, Integer.parseInt(aa[0]) - serverHours);
            cal.add(Calendar.MINUTE, Integer.parseInt(aa[1]));
        } else {
            cal.add(Calendar.HOUR, -Integer.parseInt(aa[0]) - serverHours);
            cal.add(Calendar.MINUTE, -Integer.parseInt(aa[1]));
        }

        // if we are now in DST, back off by the delta.  Note that we are checking the GMT date, this is the KEY.
        /*if (tz.inDaylightTime(date)) {
            Date dstDate = new Date(date.getTime() - tz.getDSTSavings());

            // check to make sure we have not crossed back into standard time
            // this happens when we are on the cusp of DST (7pm the day before the change for PDT)
            if (tz.inDaylightTime(dstDate)) {
                date = dstDate;
            }
        }*/

        return cal;
    }

    public boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        // ARE WE CONNECTED TO THE NET
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable()
                    && cm.getActiveNetworkInfo().isConnected();
        } else
            return false;
    }

    public void setUserID(String patform) {
        if (!checkInternetConnection()) {
            //Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            noInternetAlert();
            return;
        }
        /*HasActiveInternetConnection asynTaskObj = new HasActiveInternetConnection();
        asynTaskObj.execute();
        try {
            if (!asynTaskObj.get()) {
                Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }*/
        final DataBaseAdapter dp = new DataBaseAdapter(context);
        ArrayList<Profile> users = dp.getAllProfiles();

        if (users.isEmpty()) {
            String deviceToken = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);//f44d5070b4006d9d  //f44d5070b4006d9d
            try {
                pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                Log.e("HiNews", "exception", e);
            }

            double version = Double.parseDouble(pInfo.versionName);
            int versionInt = (int) version;
            HashMap<String, String> dataObj = new HashMap<>();
            dataObj.put(URLs.TAG_DEVICE_TOKEN, deviceToken);
            dataObj.put(URLs.TAG_UD_ID, "");
            dataObj.put(URLs.TAG_PLATFORM, patform);
            dataObj.put(URLs.TAG_VERSION, Integer.toString(versionInt));

            jsonParser = new JsonParser(context, URLs.ADD_USER_URL, URLs.ADD_USER_REQUEST_TYPE, dataObj);
            jsonParser.execute();
            jsonParser.onFinishUpdates(new JsonParser.OnUserRetrieve() {
                @Override
                public void onFinished(Profile userObj) {
                    if (userObj == null || userObj.getUserId() == 0) {
                        showDialog();
                        return;
                    }

                    URLs.setUserID(userObj.getUserId());
                    if (userObj.getRegFlag() == 1) {
                        onUserExistListner.onFinished(true);
                        editor.putBoolean("isExist", true);
                        myPrefsEditor.putBoolean("recommend", false).apply();
                        editor.commit();
                    } else {
                        callDefaultJson(userObj.getUserId(), context);
                    }
                }
            });

        } else {
            URLs.setUserID(users.get(0).getUserId());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (prefs.getBoolean("closed", false)) {
                        Intent i = new Intent(Splash.splash, MainActivity.class);
                        Splash.splash.startActivity(i);
                        Splash.splash.finish();
                    }

                }
            }, Splash.SPLASH_TIME_OUT);
        }
    }

    private int getMcc() {
        int mcc = 0;
        TelephonyManager tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String networkOperator = tel.getNetworkOperator();

        if (!TextUtils.isEmpty(networkOperator)) {
            mcc = Integer.parseInt(networkOperator.substring(0, 3));
            int mnc = Integer.parseInt(networkOperator.substring(3));
        }
        return mcc;
    }

    private void callDefaultJson(int userID, final Context context) {
//        pleaseWaitDialog();
        if (!checkInternetConnection()) {
            //Toast.makeText(context, "Check ur internet connection", Toast.LENGTH_SHORT).show();
            return;
        }
        HashMap<String, String> dataObj = new HashMap<>();

        dataObj.put(URLs.TAG_COUNTRIES_RESOURCES_USERID, userID + "");
        dataObj.put(URLs.TAG_MCC, +getMcc() + "");
        jsonParser = new JsonParser(context, URLs.DEFAULT_SOURCES, URLs.DEFAULT_SOURCES_TYPE, dataObj);
        jsonParser.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        jsonParser.onFinishUpdates(new JsonParser.OnSelcetionUpdatedListener() {
            @Override
            public void onFinished() {

            }

            @Override
            public void onFinished(List<Integer> likeResponse) {

            }

            @Override
            public void onFinished(News newsObj) {

            }

            @Override
            public void onFinished(ArrayList<Sources> sourceList) {
//                showDialog(getApplicationContext());
                myPrefsEditor.putBoolean("recommend", true).apply();
                onUserExistListner.onFinished(false);
                Intent i = new Intent(Splash.splash, MainActivity.class);
                Splash.splash.startActivity(i);
                Splash.splash.finish();
            }

            @Override
            public void onFinished(int id) {

            }

            @Override
            public void onFinished(boolean success) {

            }
        });
    }

    public void onFinishUpdates(OnUserExistListner onUserExistListner) {
        this.onUserExistListner = onUserExistListner;
    }

    void noInternetAlert() {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentManager fm = Splash.splash.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        // Create and show the dialog.
        final AlertDialogFrag commentFrag = new AlertDialogFrag(context.getString(R.string.no_internet), context.getString(R.string
                .retry), context.getString(R.string.cancel), AlertDialogFrag.DIALOG_ONE_BUTTON);
        commentFrag.show(ft, "dialog");
        commentFrag.setCancelable(false);
        commentFrag.OnDialogClickListener(new AlertDialogFrag.OnDialogListener() {
            @Override
            public void onPositiveClickListener() {
            }

            @Override
            public void onNegativeClickListener() {
                ((FragmentActivity) context).finish();
            }
        });
    }

    void showDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentManager fm = Splash.splash.getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        final AlertDialogFrag commentFrag = new AlertDialogFrag(context.getString(R.string.add_user_failure), context.getString(R.string
                .retry), context.getString(R.string.cancel), AlertDialogFrag.DIALOG_TWO_BUTTONS);
        commentFrag.show(ft, "dialog");
        commentFrag.setCancelable(false);
        commentFrag.OnDialogClickListener(new AlertDialogFrag.OnDialogListener() {
            @Override
            public void onPositiveClickListener() {
                if (!checkInternetConnection()) {
                    Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    return;
                }

                commentFrag.dismiss();
                setUserID("2");
                editor.putBoolean("closed", true);
                editor.commit();
            }

            @Override
            public void onNegativeClickListener() {
                ((FragmentActivity) context).finish();
            }
        });

    }

    public interface OnUserExistListner {
        public void onFinished(boolean flag);
    }

    class HasActiveInternetConnection extends AsyncTask<Void, Integer, Boolean> {
        private boolean isHaveAccess;

        protected Boolean doInBackground(Void... arg0) {

            return hasActiveInternetConnection(context);
        }

        public boolean onUpdateChanges() {
            return isHaveAccess;
        }

        protected void onPostExecute(Boolean result) {
            isHaveAccess = result;
        }
    }
}
