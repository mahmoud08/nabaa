package com.madarsoft.nabaa.Views;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.TransitionDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.interfaces.OnOffSwitchWithTitleSwitchClickListener;

public class OnOffSwitch extends LinearLayout {

    ImageView imBall;
    ImageView imTick;
    private LayoutInflater inflater;
    boolean isSwitchOn = true;
    ObjectAnimator animToLeft;
    ImageView imBackground;
    TransitionDrawable background;

    public OnOffSwitch(Context context, AttributeSet attrs) {

        super(context, attrs);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.on_off_switch, this);
        imBall = (ImageView) findViewById(R.id.im_ball_swith);
        imTick = (ImageView) findViewById(R.id.im_tick_swith);
        imBackground = (ImageView) findViewById(R.id.bg);
        background = (TransitionDrawable) imBackground.getDrawable();
        post(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (!isSwitchOn) {
                    switchOff();
                }
            }
        });
    }

    public void setSwitch(boolean isOn) {

        if (isSwitchOn != isOn) {
            background.reverseTransition(300);

            isSwitchOn = isOn;
            if (isSwitchOn) {

                switchON();

            } else {
                switchOff();

            }

        }
    }

    void switchON() {

        final int tickTo = getMeasuredWidth() - imTick.getMeasuredWidth();
        final ValueAnimator animTick = ValueAnimator.ofInt(tickTo, 0);
        animTick.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                android.widget.RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) imTick
                        .getLayoutParams();
                layoutParams.leftMargin = val;
                if (animTick.getAnimatedFraction() == .5) {
                    imTick.setImageResource(R.drawable.right_tick);

                }
                imTick.requestLayout();

            }
        });

        animTick.addListener(new AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                imTick.setImageResource(R.drawable.right_tick);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }
        });

        final int ballTo = getMeasuredWidth() - imBall.getMeasuredWidth();
        ValueAnimator animBall = ValueAnimator.ofInt(ballTo, 0);
        animTick.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                android.widget.RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) imBall
                        .getLayoutParams();
                layoutParams.rightMargin = val;
                imBall.requestLayout();

            }
        });

        AnimatorSet set = new AnimatorSet();
        set.setDuration(600);
        // set.playTogether(animTick);
        animTick.start();
        animBall.start();

    }

    void switchOff() {

        final int tickTo = getMeasuredWidth() - imTick.getMeasuredWidth();
        final ValueAnimator animTick = ValueAnimator.ofInt(0, tickTo);
        animTick.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                android.widget.RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) imTick
                        .getLayoutParams();
                layoutParams.leftMargin = val;
                if (animTick.getAnimatedFraction() == .5) {

                    imTick.setImageResource(R.drawable.false_sign);

                }
                imTick.requestLayout();

            }
        });
        animTick.addListener(new AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                // TODO Auto-generated method stub
                imTick.setImageResource(R.drawable.false_sign);

            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // TODO Auto-generated method stub

            }
        });

        final int ballTo = getMeasuredWidth() - imBall.getMeasuredWidth();
        ValueAnimator animBall = ValueAnimator.ofInt(0, ballTo);
        animTick.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                android.widget.RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) imBall
                        .getLayoutParams();
                layoutParams.rightMargin = val;
                imBall.requestLayout();

            }
        });

        AnimatorSet set = new AnimatorSet();
        set.setDuration(600);
        // set.playTogether(animTick);
        animTick.start();
        animBall.start();

    }
}

