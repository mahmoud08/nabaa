package com.madarsoft.nabaa.Views;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.basv.gifmoviewview.widget.GifMovieView;
import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.controls.Utils;


public class WaterDropListViewHeader extends FrameLayout {
    private static final int DISTANCE_BETWEEN_STRETCH_READY = 150;
    private LinearLayout mContainer;
    //private ProgressBar mProgressBar;
    private WaterDropView mWaterDropView;
    private STATE mState = STATE.normal;
    private IStateChangedListener mStateChangedListener;
    private int stretchHeight;
    private int readyHeight;
    private GifMovieView gifMovieView;
    RelativeLayout progCont;

    public WaterDropListViewHeader(Context context) {
        super(context);
        initView(context);
    }

    /**
     * @param context
     * @param attrs
     */
    public WaterDropListViewHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        mContainer = (LinearLayout) LayoutInflater.from(context).inflate(
                R.layout.waterdroplistview_header, null);
        //mProgressBar = (ProgressBar) mContainer.findViewById(R.id.waterdroplist_header_progressbar);
        gifMovieView = (GifMovieView)mContainer.findViewById(R.id.gif_loading);
        progCont = (RelativeLayout)mContainer.findViewById(R.id.prog_container);
        gifMovieView.setMovieResource(R.drawable.loading);
        mWaterDropView = (WaterDropView) mContainer.findViewById(R.id.waterdroplist_waterdrop);
        // 初始情况，设置下拉刷新view高度为0
        LayoutParams lp = new LayoutParams(
                LayoutParams.MATCH_PARENT, 0);
        addView(mContainer, lp);
        initHeight();
    }

    private void initHeight() {

        mContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                stretchHeight = mWaterDropView.getHeight();
                readyHeight = stretchHeight + DISTANCE_BETWEEN_STRETCH_READY;
                getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

    }

    public void updateState(STATE state) {
        if (state == mState) return;
        STATE oldState = mState;
        mState = state;
        if (mStateChangedListener != null) {
            mStateChangedListener.notifyStateChanged(oldState, mState);
        }

        switch (mState) {
            case normal:
                handleStateNormal();
                break;
            case stretch:
                handleStateStretch();
                break;
            case ready:
                handleStateReady();
                break;
            case refreshing:
                handleStateRefreshing();
                break;
            case end:
                handleStateEnd();
                break;
            default:
        }
    }

    /**
     * 处理处于normal状态的值
     */
    private void handleStateNormal() {
        mWaterDropView.setVisibility(View.VISIBLE);
        //mProgressBar.setVisibility(View.GONE);
        gifMovieView.setVisibility(View.GONE);
        progCont.setVisibility(View.GONE);
        gifMovieView.setPaused(true);
        mContainer.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
    }

    /**
     *
     */
    private void handleStateStretch() {
        mWaterDropView.setVisibility(View.VISIBLE);
        //mProgressBar.setVisibility(View.GONE);
        gifMovieView.setVisibility(View.GONE);
        progCont.setVisibility(View.GONE);
        gifMovieView.setPaused(true);
        mContainer.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
    }

    /**
     *
     */
    private void handleStateReady() {
        mWaterDropView.setVisibility(View.VISIBLE);
        //mProgressBar.setVisibility(View.GONE);
        gifMovieView.setVisibility(View.GONE);
        gifMovieView.setPaused(true);
        progCont.setVisibility(View.GONE);
        Animator shrinkAnimator = mWaterDropView.createAnimator();
        shrinkAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                updateState(STATE.refreshing);
            }
        });
        shrinkAnimator.start();//
    }

    /**
     *
     */
    private void handleStateRefreshing() {
        mWaterDropView.setVisibility(View.GONE);
        //mProgressBar.setVisibility(View.VISIBLE);
        gifMovieView.setVisibility(View.VISIBLE);
        progCont.setVisibility(View.VISIBLE);
        gifMovieView.setPaused(false);
    }

    /**
     *
     */
    private void handleStateEnd() {
        mWaterDropView.setVisibility(View.GONE);
        //mProgressBar.setVisibility(View.GONE);
        gifMovieView.setVisibility(View.GONE);
        gifMovieView.setPaused(true);
        progCont.setVisibility(View.GONE);
    }

    public int getVisiableHeight() {
        return mContainer.getHeight();
    }

    public void setVisiableHeight(int height) {
        if (height < 0)
            height = 0;
        LayoutParams lp = (LayoutParams) mContainer
                .getLayoutParams();
        lp.height = height;
        mContainer.setLayoutParams(lp);
        //通知水滴进行更新
        if (mState == STATE.stretch) {
            float pullOffset = (float) Utils.mapValueFromRangeToRange(height, stretchHeight, readyHeight, 0, 1);
            if (pullOffset < 0 || pullOffset > 1) {
                try {
                    throw new IllegalArgumentException("pullOffset should between 0 and 1!" + mState + " " + height);
                } catch (Exception e) {
                }
            }
            Log.e("pullOffset", "pullOffset:" + pullOffset);
            mWaterDropView.updateComleteState(pullOffset);
        }

    }

    public STATE getCurrentState() {
        return mState;
    }

    public int getStretchHeight() {
        return stretchHeight;
    }

    public int getReadyHeight() {
        return readyHeight;
    }

    public void setStateChangedListener(IStateChangedListener l) {
        mStateChangedListener = l;
    }

    public enum STATE {
        normal,//正常
        stretch,//准备进行拉伸
        ready,//拉伸到最大位置
        refreshing,//刷新
        end//刷新结束，回滚
    }

    public interface IStateChangedListener {
        public void notifyStateChanged(STATE oldState, STATE newState);
    }
}
