package com.madarsoft.nabaa.Views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.madarsoft.nabaa.R;
import com.madarsoft.nabaa.activities.MainActivity;

public class NoNetworkView extends RelativeLayout {

    Context context;
    Typeface face;

    public NoNetworkView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public NoNetworkView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

    }
}