package com.madarsoft.nabaa.Views;
import com.madarsoft.nabaa.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.madarsoft.nabaa.activities.MainActivity;

public class FontTextView extends TextView {

    Context context;
    Typeface face;
    TypedArray a;

    public FontTextView(Context context) {
        super(context);
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        a = context.obtainStyledAttributes(attrs, R.styleable.text_apply_fonts);
        init(a.getInteger(0, 3));
        a.recycle();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    private void init(int style) {

        //if (LocalizationControl.getDefaultLanguageIndex() == LocalizationControl.ArabicLanguage) {

        switch (style) {
            case 1:
                face = MainActivity.HacenLiner;
                break;
            case 2:
                face = MainActivity.HacenLinerPrintOut;
                break;
            /*case 3:
                face = MainActivity.HacenLiner;
                break;
            case 4:
                face = MainActivity.HacenLinerPrintOut;
                break;*/
            default:
                face = MainActivity.HacenLiner;
                break;
        }

        setTypeface(face);
    }
}