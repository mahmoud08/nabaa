//package com.madarsoft.hinewz.Views;
//
//import android.content.Context;
//import android.content.res.TypedArray;
//import android.util.AttributeSet;
//import android.util.TypedValue;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.LinearLayout;
//
//import com.madarsoft.hinewz.R;
//import com.madarsoft.hinewz.interfaces.OnOffSwitchWithTitleSwitchClickListener;
//
//public class OnOffSwitchWithTitle extends LinearLayout {
//
//	OnOffSwitch mSwitch;
//	FontTextView mtvTitle;
//	View vDivider;
//	private boolean mapplyDivider;
//	private String mtxtTitleValue;
//	private LayoutInflater inflater;
//	private boolean isSwitchOn;
//	private int mtextStyle;
//
//	public OnOffSwitchWithTitle(Context context, AttributeSet attrs) {
//		super(context, attrs);
//		// TODO Auto-generated constructor stub
//
//		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		inflater.inflate(R.layout.on_off_swich_with_title, this);
//		mtvTitle = (FontTextView) findViewById(R.id.title);
//		vDivider = (View) findViewById(R.id.divider);
//		mSwitch = (OnOffSwitch) findViewById(R.id.on_off_switch);
//		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.settings_on_off);
//		mapplyDivider = a.getBoolean(0, true);
//		mtxtTitleValue = a.getString(1);
//		mtextStyle = a.getInt(2, 0);
//		a.recycle();
//
//		init();
//	}
//
//	private void init() {
//		// TODO Auto-generated method stub
//
//		if (mtextStyle == 1) {
//			mtvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
//
//		}
//
//		if (mapplyDivider) {
//			vDivider.setVisibility(VISIBLE);
//		} else {
//			vDivider.setVisibility(INVISIBLE);
//
//		}
//		mtvTitle.setText(mtxtTitleValue);
//		mSwitch.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				if (onOffSwitchWithTitleSwitchClickListener != null) {
//					onOffSwitchWithTitleSwitchClickListener.onSwitchClick();
//				}
//			}
//		});
//	}
//
//	public void setText(String text) {
//		mtvTitle.setText(text);
//	}
//
//	public void setSwitch(boolean switchOn) {
//		isSwitchOn = switchOn;
//		mSwitch.switchTheButton(isSwitchOn);
//
//	}
//
//	public void setSwitch(boolean switchOn, boolean hideDivider) {
//
//		mapplyDivider = !hideDivider;
//		if (mapplyDivider) {
//			vDivider.setVisibility(VISIBLE);
//		} else {
//			vDivider.setVisibility(INVISIBLE);
//
//		}
//		isSwitchOn = switchOn;
//		mSwitch.switchTheButton(isSwitchOn);
//
//	}
//
//	public void setSwitch(int switchOn, boolean hideDivider) {
//		mapplyDivider = !hideDivider;
//		if (mapplyDivider) {
//			vDivider.setVisibility(VISIBLE);
//		} else {
//			vDivider.setVisibility(INVISIBLE);
//
//		}
//		isSwitchOn = Boolean.getBoolean("" + isSwitchOn);
//		mSwitch.switchTheButton(isSwitchOn);
//
//	}
//
//	public OnOffSwitchWithTitleSwitchClickListener getOnOffSwitchWithTitleSwitchClickListener() {
//		return onOffSwitchWithTitleSwitchClickListener;
//	}
//
//	public void setOnOffSwitchWithTitleSwitchClickListener(
//			OnOffSwitchWithTitleSwitchClickListener onOffSwitchWithTitleSwitchClickListener) {
//		this.onOffSwitchWithTitleSwitchClickListener = onOffSwitchWithTitleSwitchClickListener;
//	}
//
//	private OnOffSwitchWithTitleSwitchClickListener onOffSwitchWithTitleSwitchClickListener;
//
//}
