package com.madarsoft.nabaa.Views;

/**
 * Created by Colossus on 16-Jun-16.
 */

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class NonScrollListView extends ListView {

    boolean expanded = true;

    public NonScrollListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // HACK!  TAKE THAT ANDROID!
        if (isExpanded()) {
            // Calculate entire height by providing a very large height hint.
            // View.MEASURED_SIZE_MASK represents the largest height possible.
            int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK,
                    MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);

            android.view.ViewGroup.LayoutParams params = (android.view.ViewGroup.LayoutParams) getLayoutParams();
            params.height = getMeasuredHeight();
            requestLayout();
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}