//package com.madarsoft.nabaa.GCM;
//
//import android.app.Activity;
//import android.app.IntentService;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Color;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.SystemClock;
//import android.provider.Settings;
//import android.support.v4.app.NotificationCompat;
//import android.util.Log;
//
//import com.google.android.gms.gcm.GoogleCloudMessaging;
//import com.madarsoft.nabaa.R;
//import com.madarsoft.nabaa.activities.MainActivity;
//import com.madarsoft.nabaa.activities.Splash;
//import com.madarsoft.nabaa.database.DataBaseAdapter;
//
///**
// * Created by eng:Karim on 6/2/2016.
// */
//
//public class GCMIntentService extends IntentService {
//    //aa
//    public static final int NOTIFICATION_ID = 1;
//    public static final String COUNT_SHARED = "count_shared";
//    private static final String TAG = "GcmIntentService";
//    Bundle extras;
//    Activity context;
//    MainActivity mainActivity;
//    Intent myIntent;
//    //    Handler handler;
//    DataBaseAdapter dataBaseAdapter;
//    private SharedPreferences sharedpreferences;
//    private NotificationManager mNotificationManager;
//    private String msg = "error";
//    private String id;
//    private String lastId = "";
//    private SharedPreferences countSharedPref;
//    private int myCount;
//
//    public GCMIntentService() {
//        super("GcmIntentService");
//        Log.i(TAG, "GCMIntent Service constrctor!");
//    }
//
//    public GCMIntentService(Activity c) {
//        super("GcmIntentService");
//        context = c;
//    }
//
//    @Override
//    protected void onHandleIntent(Intent intent) {
//        extras = intent.getExtras();
//
//        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
//        // The getMessageType() intent parameter must be the intent you received
//        // in your BroadcastReceiver.
//        String messageType = gcm.getMessageType(intent);
//
//        if (!extras.isEmpty()) { // has effect of unparcelling Bundle
//
//            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
//                msg = "Send error: " + extras.toString();
//                try {
//                    sendNotification();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
//                msg = "Deleted messages on server: " + extras.toString();
//                try {
//                    sendNotification();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
//
//                for (int i = 0; i < 5; i++) {
//                    Log.i(TAG, "Working... " + (i + 1) + "/5 @ " + SystemClock.elapsedRealtime());
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                    }
//                }
//                Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
//                // sendNotification(extras.getString("Notice"));
//                sendNotification();
//                // extras.getString("message");
//                // URLDecoder.decode(extras.getString("message"));
//                Log.i(TAG, "Received: " + extras.toString());
//            }
//        }
//
//        //GcmBroadcastReceiver.completeWakefulIntent(intent);
//    }
//
//    public void sendNotification() {
//        sharedpreferences = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);
//        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//        if (!msg.contains("Send error:") || !msg.contains("Deleted messages on server:"))
//            if (extras.get("message") != null) {
//                msg = extras.get("message").toString();
//                id = extras.get("id").toString();
//                myIntent = new Intent(getApplicationContext(), Splash.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("articlebytitle", msg);
//                bundle.putBoolean("fromNotification", true);
//                bundle.putString("id", id);
//                myIntent.putExtras(bundle);
//                // Notification Badge
////                handler = new Handler();
//                showTheNotification();
//            }
//
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
////        Log.d("ANALYSIS_SERVICE", "Service started");
//        dataBaseAdapter = new DataBaseAdapter(this);
//    }
//
//    private void showTheNotification() {
//        countSharedPref = getSharedPreferences(GCMIntentService.COUNT_SHARED, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = countSharedPref.edit();
//
//        boolean isInForegroundMode = sharedpreferences.getBoolean("mIsInForegroundMode", true);
//        if (isInForegroundMode)
//            return;
////        int soundType = dataBaseAdapter.getAllProfiles().get(0).getSoundType();
////        int urgentFlag = dataBaseAdapter.getAllProfiles().get(0).getUrgentFlag();
////        if (soundType < 0 && urgentFlag < 0) {
////            return;
////        }
//
//        myCount = countSharedPref.getInt("Notify_count", 0);
//
////        Random r = new Random();
////        int i1 = r.nextInt();
////        mNotificationManager.notify(myCount, mBuilder.build());
////        editor.putInt("Notify_count", myCount + 1).apply();
//        if (myCount > 40) {
//            myCount = 0;
//            editor.putInt("Notify_count", myCount).apply();
//
//            for (int i = 0; i < 40; i++) {
//                mNotificationManager.cancel(i);
//            }
//            myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            int requestID = (int) System.currentTimeMillis();
////            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), requestID, myIntent,
////                    0);
////            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//            PendingIntent mContentIntent = PendingIntent.getActivity(getApplicationContext(), requestID, new Intent(getApplicationContext
//                            (), Splash.class),
//                    0);
//            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
//            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
//                    .setContentTitle(getResources().getString(R.string.app_name));
////                        setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).
////        setContentText(msg).
////                            setContentIntent(mContentIntent).setWhen(System.currentTimeMillis()).setAutoCancel(true);
//            mBuilder.setColor(Color.parseColor("#CC0000"));
//            NotificationCompat.BigTextStyle message = new NotificationCompat.BigTextStyle();
//            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//
//            message.bigText(msg);
//            mBuilder.setStyle(message);
//
//            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                mBuilder.setSmallIcon(R.drawable.logo4).setLargeIcon(largeIcon);
//            } else {
//                mBuilder.setSmallIcon(R.drawable.logo).setLargeIcon(largeIcon);
//            }
//
//            mBuilder.setContentText(getApplicationContext().getString(R.string.you_exceeded_normal_number)).
//                    setContentIntent(mContentIntent).setWhen(System.currentTimeMillis()).setAutoCancel(true);
//            mNotificationManager.notify(myCount, mBuilder.build());
//            editor.putInt("Notify_count", myCount + 1).apply();
//        } else {
//            myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            int requestID = (int) System.currentTimeMillis();
////            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), requestID, myIntent,
////                    0);
////            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//            PendingIntent mContentIntent = PendingIntent.getActivity(getApplicationContext(), requestID, myIntent,
//                    0);
//            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
//            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
//                    .setContentTitle(getResources().getString(R.string.app_name)).
////                        setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).
//        setContentText(msg).
//                            setContentIntent(mContentIntent).setWhen(System.currentTimeMillis()).setAutoCancel(true);
//            mBuilder.setColor(Color.parseColor("#CC0000"));
//            NotificationCompat.BigTextStyle message = new NotificationCompat.BigTextStyle();
//            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//
//            message.bigText(msg);
//            mBuilder.setStyle(message);
//
//            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                mBuilder.setSmallIcon(R.drawable.logo4).setLargeIcon(largeIcon);
//            } else {
//                mBuilder.setSmallIcon(R.drawable.logo).setLargeIcon(largeIcon);
//            }
//            mNotificationManager.notify(myCount, mBuilder.build());
//            editor.putInt("Notify_count", myCount + 1).apply();
//        }
//
//    }
//
//
//}
