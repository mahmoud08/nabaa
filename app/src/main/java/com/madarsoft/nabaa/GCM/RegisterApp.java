package com.madarsoft.nabaa.GCM;

/**
 * Created by eng:karim on 6/2/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.madarsoft.nabaa.activities.MainActivity;
import com.madarsoft.nabaa.database.DataBaseAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class RegisterApp extends AsyncTask<Void, Void, String> {

    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String TAG = "GCMRelated";
    DataBaseAdapter dataBaseAdapter;
    Context context;
    GoogleCloudMessaging gcm;
    String regid = null;
    private int appVersion;

    public RegisterApp(Context ctx, GoogleCloudMessaging gcm, int appVersion) {
        this.context = ctx;
        this.gcm = gcm;
        this.appVersion = appVersion;
        dataBaseAdapter = new DataBaseAdapter(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... arg0) {
        String msg = "";
        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(context);
            }

            InstanceID instanceID = InstanceID.getInstance(context);
            instanceID.getId();
            regid = instanceID.getToken(GCMConfig.GOOGLE_SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.d("deviceTocken", regid);
            Log.d("deviceTocken", regid);
            Log.d("deviceTocken", regid);

            msg = "Device registered, registration ID=" + regid;
            sendRegistrationIdToBackend();
            storeRegistrationId(context, regid);
            subscribeTopics(regid);
        } catch (IOException ex) {
            msg = "Error :" + ex.getMessage();
        }
        return msg;
    }
    private void storeRegistrationId(Context ctx, String regid) {
        final SharedPreferences prefs = ctx.getSharedPreferences(MainActivity.MyPREFERENCES,
                Context.MODE_PRIVATE);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regid);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();

    }

    public void sendRegistrationIdToBackend() {
        StringBuilder builder = new StringBuilder();
        URL url = null;
        try {
            //
            String urll = GCMConfig.SERVER_URL + "platform=2&programid=18&devicetoken=" + regid + "&udid=" + getDeviceId();
            url = new URL(urll);
            System.out.println(url);
            Log.d("registerUrl", url.getPath());
            Log.d("registerUrl", url.getPath());
            Log.d("registerUrl", url.getPath());

            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod("GET");
            InputStream is = http.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }
            http.disconnect();
        } catch (MalformedURLException e) {
            Log.e("PUSH", e.toString());
        } catch (ProtocolException er) {
            Log.e("PUSH", er.toString());
        } catch (IOException ee) {
            Log.e("PUSH", ee.toString());
        }
    }

    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(context);
        try {
//            pubSub.subscribe(token, "/topics/global", null);
            pubSub.subscribe(token, "/topics/-----", null);
        } catch (IOException e) {
            Log.d("TOPIC ERROR", e.toString());
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
//        Toast.makeText(context, "Registration Completed. Now you can see the notifications", Toast.LENGTH_SHORT).show();
        Log.v(TAG, result);
    }

    public String getDeviceId() {
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Log.d("UNIQE", android_id);
        return android_id;
    }
}
