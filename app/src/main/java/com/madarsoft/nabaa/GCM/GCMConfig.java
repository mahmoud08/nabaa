package com.madarsoft.nabaa.GCM;

/**
 * Created by newOs on 6/2/2016.
 */
public class GCMConfig {
    // http://api.madarsoft.com/queen/v2/api/Register_deviceToken.aspx?devicetoken=qurantoken&udid=1110&platform=2&programid=17
    // CONSTANTS
    static final String SERVER_URL = "http://api.madarsoft.com/queen/v2/api/Register_deviceToken.aspx?";
    //  : Server url where you have placed your server files
    // Google project id
    static final String GOOGLE_SENDER_ID = "676325899107";  // Place here your Google project id

    /**
     * Tag used on log messages.
     */
    static final String TAG = "GCM HiNews ";

    static final String DISPLAY_MESSAGE_ACTION =
            "com.madarsoft.gcm.DISPLAY_MESSAGE";

    static final String EXTRA_MESSAGE = "message";

}
