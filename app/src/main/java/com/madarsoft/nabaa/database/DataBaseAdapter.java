package com.madarsoft.nabaa.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.madarsoft.nabaa.entities.Category;
import com.madarsoft.nabaa.entities.History;
import com.madarsoft.nabaa.entities.News;
import com.madarsoft.nabaa.entities.Profile;
import com.madarsoft.nabaa.entities.Sources;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class DataBaseAdapter extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 4;
    // Database Name
    private static final String DATABASE_NAME = "hinews";
    // Table Create Statement
    private static final String CREATE_TABLE_CATEGORY = "CREATE TABLE "
            + Category.TABLE_NAME + "(" + Category.COLUMN_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT," + Category.COLUMN_CATEGORY_NAME
            + " TEXT," + Category.COLUMN_CATEGORY_ID + " INTEGER," + Category.COLUMN_CATEGORY_TYPE + " INTEGER," + Category
            .COLUMN_LOGO_URL + " TEXT," + Category.COLUMN_TIME_STAMP + " LONG ," + Category.COLUMN_CHANGE_TYPE + " INTEGER," + Category
            .COLUMN_SOURCE_TIME_STAMP + " LONG ," + Category.COLUMN_CATEGORY_COUNT + " INTEGER , " + Category.COLUMN_CATEGORY_ISO +
            " TEXT) ;";
    private static final String CREATE_TABLE_CATEGORY_IF_NOT_EXISTS = "CREATE TABLE IF NOT EXISTS "
            + Category.TABLE_NAME + "(" + Category.COLUMN_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT," + Category.COLUMN_CATEGORY_NAME
            + " TEXT," + Category.COLUMN_CATEGORY_ID + " INTEGER," + Category.COLUMN_CATEGORY_TYPE + " INTEGER," + Category
            .COLUMN_LOGO_URL + " TEXT," + Category.COLUMN_TIME_STAMP + " LONG ," + Category.COLUMN_CHANGE_TYPE + " INTEGER," + Category
            .COLUMN_SOURCE_TIME_STAMP + " LONG ," + Category.COLUMN_CATEGORY_COUNT + " INTEGER , " + Category.COLUMN_CATEGORY_ISO +
            " TEXT) ;";
    // table create statement
    private static final String CREATE_TABLE_SOURCES = "CREATE TABLE " + Sources.TABLE_NAME + " ( " + Sources.COLUMN_ID + "  INTEGER " +
            "PRIMARY KEY AUTOINCREMENT," + Sources.COLUMN_SOURCE_NAME + "  TEXT," + Sources.COLUMN_SOURCE_ID + " INTEGER UNIQUE ," +
            Sources.COLUMN_LOGO_URL + " TEXT," + Sources.COLUMN_NUMBER_FOLLOWERS + " INTEGER ," + Sources.COLUMN_CATEGORY_ID + " INTEGER " +
            ", " + Sources.COLUMN_SELECTED_OR_NOT + " INTEGER, " + Sources.COLUMN_GEO_ID + " INTEGER ," + Sources.COLUMN_SUB_ID + " " +
            "INTEGER ," + Sources.COLUMN_BACKGROUND_URL + " TEXT ," + Sources.COLUMN_DETAIL + " TEXT ," + Sources.COLUMN_CHANGE_TYPE + " " +
            "INTEGER , " + Sources.COLUMN_TIME_STAMP + "  TEXT ," + Sources.COLUMN_IS_SOURCE_NOTIFIABLE + " INTEGER," + Sources
            .COLUMN_IS_SOURCE_URGENT_NOTIFIABLE + " INTEGER , " + Sources
            .COLUMN_RANK + " INTEGER ,FOREIGN KEY(" + Sources.COLUMN_CATEGORY_ID + ") REFERENCES " + Category.TABLE_NAME + "( " +
            Category.COLUMN_ID + " ) ) ;";
    private static final String CREATE_TABLE_PROFILE = "CREATE TABLE "
            + Profile.TABLE_NAME + "(" + Profile.COLUMN_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT,"
            + Profile.COLUMN_USER_ID + " INTEGER UNIQUE ," + Profile.COLUMN_REG_FLAG + " INTEGER," + Profile.COLUMN_URGENT_FLAG + " " +
            "INTEGER ," + Profile.COLUMN_SOUND_TYPE + " INTEGER ," + Profile.COLUMN_BLOCK_IMG + " INTEGER )  ;";

    private static final String CREATE_TABLE_HISTORY = "CREATE TABLE IF NOT EXISTS " + History.TABLE_NAME + " (" + History.SOURCE_ID + " " +
            "INTEGER A, " + History.SOURCE_LOGO + " TEXT, " +
            "" + History.SOURCE_NAME + " TEXT, " + History.ISRTL + " TEXT, " + History.ARTICLE_ID + " INTEGER PRIMARY KEY, "
            + History.ARTICLE_TITLE + " TEXT, " + History.APPREV + " TEXT , " +
            "" + History.SHARE_LINK + " TEXT, " + History.ARTICLE_DATE + " TEXT , " + History.ARTICLE_IMAGE + " TEXT, " + History
            .LIKES_COUNT + " INTEGER DEFAULT 0, " + History.COMMENTS_COUNT + " INTEGER DEFAULT 0, " +
            "" + History.SHARE_COUNT + " INTEGER DEFAULT 0, " + History.SHARE_ID + " INTEGER DEFAULT 0, " + History.URGENT + " INTEGER " +
            "DEFAULT 0 , " + History.FAVOURITE_ID + " INTEGER DEFAULT 0, " + History.LIKE_ID + " INTEGER " +
            " ) ; ";

    private static final int GEO_TYPE = 1;
    private static final int SUB_TYPE = 2;
    Context context;
    private SQLiteDatabase db;
    private SharedPreferences sharedpreferences;
    private ArrayList<History> articlesDate;
    private ArrayList<History> modifiedArticleList;
    private History historyObj;

    public DataBaseAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static List<String> GetColumns(SQLiteDatabase db, String tableName) {
        List<String> ar = null;
        Cursor c = null;
        try {
            c = db.rawQuery("select * from " + tableName + " limit 1", null);
            if (c != null) {
                ar = new ArrayList<String>(Arrays.asList(c.getColumnNames()));
            }
        } catch (Exception e) {
            Log.v(tableName, e.getMessage(), e);
            e.printStackTrace();
        } finally {
            if (c != null)
                c.close();
        }
        return ar;
    }

    public static String join(List<String> list, String delim) {
        StringBuilder buf = new StringBuilder();
        int num = list.size();
        for (int i = 0; i < num; i++) {
            if (i != 0)
                buf.append(delim);
            buf.append((String) list.get(i));
        }
        return buf.toString();
    }

    public void updateProfiles(Profile profile) {
        db = this.getReadableDatabase();
        replace(Profile.TABLE_NAME, Profile.COLUMN_ID, "" + profile.getID(), profile.fields,
                profile.getValues());
        this.close();

    }

    public ArrayList<Sources> setSelectedOrNot(int source_Id, int selectedOrNot) {
        db = this.getReadableDatabase();
        ArrayList<Sources> res = new ArrayList<>();
        String selectQuery = "UPDATE " + Sources.TABLE_NAME + " SET " + Sources.COLUMN_SELECTED_OR_NOT + " = '" + selectedOrNot + "' " +
                "WHERE " + Sources.COLUMN_SOURCE_ID + " = " + source_Id + " ;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {
                Sources temp = new Sources();
                temp.setSelected_or_not(c.getInt(c.getColumnIndex(Sources.COLUMN_SELECTED_OR_NOT)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        return res;
    }

    public void deleteArticleByArticleId(int articleId) {

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE  FROM " + History.TABLE_NAME + " WHERE " + History.ARTICLE_ID + " = " + articleId + ";";
        db.execSQL(selectQuery);

        this.getReadableDatabase().close();

    }

    public void updateSourceById(int id, Sources source) {
        db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Sources.COLUMN_RANK, source.getRank());
        db.update(Sources.TABLE_NAME, contentValues, "" + Sources.COLUMN_SOURCE_ID + "=" + id, null);
    }

    public void updateArticleById(int articleId, History history) {
        db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(History.FAVOURITE_ID, history.getFavouriteId());
        contentValues.put(History.LIKE_ID, history.getLike_Id());
        contentValues.put(History.LIKES_COUNT, history.getLikesCount());
        contentValues.put(History.COMMENTS_COUNT, history.getCommentsCount());
        contentValues.put(History.SHARE_COUNT, history.getShareCount());
        db.update(History.TABLE_NAME, contentValues, "" + History.ARTICLE_ID + "=" + articleId, null);
    }

    //1494490383364
    public void expiredNews() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Calendar.getInstance().getTime());
        calendar.add(Calendar.DAY_OF_YEAR, -7);
        articlesDate = getArticlesId();
        for (int i = 0; i < articlesDate.size(); i++) {
            Long dateInLong = Long.parseLong(articlesDate.get(i).getArticleDate());
            articlesDate.get(i).setLongDate(dateInLong);
            if (articlesDate.get(i).getLongDate() < calendar.getTime().getTime() / 1000) {
                deleteArticleByArticleId(articlesDate.get(i).getArticleId());
            }
        }
//
//
//        String selectQuery = "DELETE  FROM " + History.TABLE_NAME + " WHERE " + calendar.getTime().getTime() + "  >  " + History
//                .ARTICLE_DATE + ";";
//        db.execSQL(selectQuery);
//
//        this.getReadableDatabase().close();

    }

    public void clearHistory() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE " +
                "FROM " + History.TABLE_NAME + ";";
        db.execSQL(selectQuery);
        this.getReadableDatabase().close();
    }

    public void clearMySources(ArrayList<Sources> sources) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = null;
        for (int i = 0; i < sources.size(); i++) {
            selectQuery = "UPDATE " + Sources.TABLE_NAME + " SET " + Sources.COLUMN_SELECTED_OR_NOT + " = -1  WHERE " +
                    Sources.COLUMN_SOURCE_ID + " = " + sources
                    .get(i).getSource_id() + ";";
            db.execSQL(selectQuery);
        }
        this.getReadableDatabase().close();

    }

    public ArrayList<History> getArticlesId() {
        ArrayList<History> res = new ArrayList<>();
        db = this.getReadableDatabase();

        String selectQuery = "SELECT  " + History.ARTICLE_ID + " , " + History.ARTICLE_DATE + " , " + History.SOURCE_ID + "  FROM " +
                History.TABLE_NAME + " ORDER BY" +
                " " + History.ARTICLE_DATE + " " +
                "DESC ;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {
                History temp = new History();
                temp.setArticleId(c.getInt(c.getColumnIndex(History.ARTICLE_ID)));
                temp.setArticleDate(c.getString(c.getColumnIndex(History.ARTICLE_DATE)));
                temp.setSourceId(c.getInt(c.getColumnIndex(History.SOURCE_ID)));
                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public ArrayList<News> getAllNews() {
        ArrayList<News> res = new ArrayList<News>();
        db = this.getReadableDatabase();

        String selectQuery = "SELECT  *  FROM " + History.TABLE_NAME + " ORDER BY " + History.ARTICLE_DATE + " DESC ;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {
                News temp = new News();
                temp.setID(c.getInt(c.getColumnIndex(History.ARTICLE_ID)));
//                temp.setApprev(c.getString(c.getColumnIndex(History.APPREV)));

                /*
                *  history.setUrgent(news.isUrgent ? 0 : 1);
                    history.setSourceName(news.getSourceTitle());
                    history.setApprev("");
                    history.setShareId(0);
                    history.setSourceLogo(news.getSourceLogoUrl());
                    history.setSourceId(news.getSourceID());
                    history.setArticleDate(news.getArticleDate());
                    history.setArticleId(news.getID());
                    history.setShareLink(news.getShareUrl());
                    history.setArticleImage(news.getLogo_url());
                    history.setArticleTitle(news.getNewsTitle());
                    history.setIsRtl(news.getIsRTL());
                    history.setLikesCount(news.getLikesNumber());
                    history.setShareCount(news.getSharesNumber());
                    history.setCommentsCount(news.getCommentsNumber());
                    history.setFavouriteId(news.getFavouriteID());
                    dataBaseAdapter.insertInHistory(history);*/
                temp.setArticleDate(c.getString(c.getColumnIndex(History.ARTICLE_DATE)));
                temp.setLogo_url(c.getString(c.getColumnIndex((History.ARTICLE_IMAGE))));
                temp.setNewsTitle(c.getString(c.getColumnIndex(History.ARTICLE_TITLE)));
                temp.setCommentsNumber(c.getInt(c.getColumnIndex(History.COMMENTS_COUNT)));
                temp.setFavouriteID(c.getInt(c.getColumnIndex(History.FAVOURITE_ID)));
                temp.setIsRTL(c.getString(c.getColumnIndex(History.ISRTL)));
                temp.setLikesNumber(c.getInt(c.getColumnIndex(History.LIKES_COUNT)));
                temp.setSharesNumber(c.getInt(c.getColumnIndex(History.SHARE_COUNT)));
                temp.setLikeID(c.getInt(c.getColumnIndex(History.LIKE_ID)));
//                temp.setShareId(c.getInt(c.getColumnIndex(History.SHARE_ID)));
                temp.setTimeOffset(c.getString(c.getColumnIndex(History.APPREV)));
                temp.setSourceID(c.getInt(c.getColumnIndex(History.SOURCE_ID)));
                temp.setSourceLogoUrl(c.getString(c.getColumnIndex(History.SOURCE_LOGO)));
                temp.setSourceTitle(c.getString(c.getColumnIndex(History.SOURCE_NAME)));
                if (c.getInt(c.getColumnIndex(History.URGENT)) == 1) {
                    temp.setUrgent(true);
                } else {
                    temp.setUrgent(false);
                }
                temp.setShareUrl(c.getString(c.getColumnIndex(History.SHARE_LINK)));
                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public int getSourcesWhereNotifiableGreaterThanOne() {
        String selectQuery = null;
        int i = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        selectQuery = " SELECT COUNT (" + Sources.COLUMN_ID + ")  FROM " + Sources.TABLE_NAME + " WHERE " + Sources
                .COLUMN_IS_SOURCE_NOTIFIABLE + " > 1 ;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                i = c.getCount();
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return i;
    }

    public int getSourcesWhereUrgentGreaterThanOne() {

        String selectQuery = null;
        int i = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        selectQuery = " SELECT COUNT (" + Sources.COLUMN_ID + ")  FROM " + Sources.TABLE_NAME + " WHERE " + Sources
                .COLUMN_IS_SOURCE_URGENT_NOTIFIABLE + " > 1 ;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                i = c.getCount();
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return i;

    }

    public ArrayList<Category> getAllCategories() {
        ArrayList<Category> res = new ArrayList<Category>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM '" + Category.TABLE_NAME + "' ;";


        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {

                Category temp = new Category();
                temp.setID(c.getInt(c.getColumnIndex(Category.COLUMN_ID)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_ID)));
                temp.setCategory_type(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_TYPE)));
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Category.COLUMN_LOGO_URL)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Category.COLUMN_CHANGE_TYPE)));
                temp.setTime_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_TIME_STAMP)));
                temp.setSources_time_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_SOURCE_TIME_STAMP)));
                temp.setCount(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_COUNT)));

                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;

    }

    public ArrayList<Sources> getSourcesByRank() {
        ArrayList<Sources> res = new ArrayList<Sources>();
        String selectQuery = null;
        SQLiteDatabase db = this.getReadableDatabase();
        selectQuery = " SELECT * FROM " + Sources.TABLE_NAME + " WHERE " + Sources.COLUMN_RANK + " > 0  ORDER BY " + Sources.COLUMN_RANK
                + " DESC LIMIT 10;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Sources temp = new Sources();
                temp.setId(c.getInt(c.getColumnIndex(Sources.COLUMN_ID)));
                temp.setBackground_url(c.getString(c.getColumnIndex(Sources.COLUMN_BACKGROUND_URL)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Sources.COLUMN_CHANGE_TYPE)));
                temp.setDetails(c.getString(c.getColumnIndex(Sources.COLUMN_DETAIL)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Sources.COLUMN_LOGO_URL)));
                temp.setSub_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SUB_ID)));
                temp.setGeo_id(c.getInt(c.getColumnIndex(Sources.COLUMN_GEO_ID)));
                temp.setNumber_followers(c.getInt(c.getColumnIndex(Sources.COLUMN_NUMBER_FOLLOWERS)));
                temp.setSource_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SOURCE_ID)));
                temp.setSource_name(c.getString(c.getColumnIndex(Sources.COLUMN_SOURCE_NAME)));
                temp.setNumber_followers(c.getInt(c.getColumnIndex(Sources.COLUMN_NUMBER_FOLLOWERS)));
                temp.setTimeStamp(c.getLong(c.getColumnIndex(Sources.COLUMN_TIME_STAMP)));
                temp.setSelected_or_not(c.getInt(c.getColumnIndex(Sources.COLUMN_SELECTED_OR_NOT)));
                temp.setNotifiable(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_NOTIFIABLE)));
                temp.setRank(c.getInt(c.getColumnIndex(Sources.COLUMN_RANK)));
                temp.setUrgent_notify(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_URGENT_NOTIFIABLE)));
                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public Sources getSourcesBySourceId(int id) {
        ArrayList<Sources> res = new ArrayList<>();
        String selectQuery = null;
        SQLiteDatabase db = this.getReadableDatabase();
        selectQuery = " SELECT * FROM " + Sources.TABLE_NAME + " WHERE " + Sources.COLUMN_SOURCE_ID + " = " + id + " ;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Sources temp = new Sources();
                temp.setId(c.getInt(c.getColumnIndex(Sources.COLUMN_ID)));
                temp.setBackground_url(c.getString(c.getColumnIndex(Sources.COLUMN_BACKGROUND_URL)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Sources.COLUMN_CHANGE_TYPE)));
                temp.setDetails(c.getString(c.getColumnIndex(Sources.COLUMN_DETAIL)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Sources.COLUMN_LOGO_URL)));
                temp.setSub_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SUB_ID)));
                temp.setGeo_id(c.getInt(c.getColumnIndex(Sources.COLUMN_GEO_ID)));
                temp.setNumber_followers(c.getInt(c.getColumnIndex(Sources.COLUMN_NUMBER_FOLLOWERS)));
                temp.setSource_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SOURCE_ID)));
                temp.setSource_name(c.getString(c.getColumnIndex(Sources.COLUMN_SOURCE_NAME)));
                temp.setNumber_followers(c.getInt(c.getColumnIndex(Sources.COLUMN_NUMBER_FOLLOWERS)));
                temp.setTimeStamp(c.getLong(c.getColumnIndex(Sources.COLUMN_TIME_STAMP)));
                temp.setSelected_or_not(c.getInt(c.getColumnIndex(Sources.COLUMN_SELECTED_OR_NOT)));
                temp.setNotifiable(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_NOTIFIABLE)));
                temp.setRank(c.getInt(c.getColumnIndex(Sources.COLUMN_RANK)));
                temp.setUrgent_notify(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_URGENT_NOTIFIABLE)));
                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        if (!res.isEmpty())
            return res.get(0);
        return null;
    }
//
//    public void updateSelected(int id, int followers, int selected) {
//        db = this.getReadableDatabase();
//        String selectQuery = "UPDATE " + Sources.TABLE_NAME + " SET " + Sources
//                .COLUMN_NUMBER_FOLLOWERS + " = " + followers + " , " + Sources
//                .COLUMN_SELECTED_OR_NOT + " = " + selected + " WHERE " + Sources.COLUMN_SOURCE_ID + " = " + id + " ; ";
//        db.execSQL(selectQuery);
//        this.getReadableDatabase().close();
////        ContentValues cv = new ContentValues();
////        cv.put(Sources.COLUMN_NUMBER_FOLLOWERS, followers);
////        cv.put(Sources.COLUMN_SELECTED_OR_NOT, selected);
////        db.update(Sources.TABLE_NAME, cv, "id=" + id, null);
//
//    }

    public ArrayList<Sources> getSourcesByCategoryId(int catId, int geoOrSub) {
        ArrayList<Sources> res = new ArrayList<Sources>();
        String selectQuery = null;
        SQLiteDatabase db = this.getReadableDatabase();
        switch (geoOrSub) {
            case GEO_TYPE:
                selectQuery = " SELECT * FROM " + Sources.TABLE_NAME + " WHERE " + Sources.COLUMN_GEO_ID + " = " + catId + " ORDER BY " +
                        Sources.COLUMN_SUB_ID + " ASC ;";
                break;
            case SUB_TYPE:
                selectQuery = " SELECT * FROM " + Sources.TABLE_NAME + " WHERE " + Sources.COLUMN_SUB_ID + " = " + catId + "  ORDER BY "
                        + Sources.COLUMN_GEO_ID + " ASC;";
                break;
        }

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {

                Sources temp = new Sources();
                temp.setId(c.getInt(c.getColumnIndex(Sources.COLUMN_ID)));
                temp.setBackground_url(c.getString(c.getColumnIndex(Sources.COLUMN_BACKGROUND_URL)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Sources.COLUMN_CHANGE_TYPE)));
                temp.setDetails(c.getString(c.getColumnIndex(Sources.COLUMN_DETAIL)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Sources.COLUMN_LOGO_URL)));
                temp.setSub_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SUB_ID)));
                temp.setGeo_id(c.getInt(c.getColumnIndex(Sources.COLUMN_GEO_ID)));
                temp.setNumber_followers(c.getInt(c.getColumnIndex(Sources.COLUMN_NUMBER_FOLLOWERS)));
                temp.setSource_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SOURCE_ID)));
                temp.setSource_name(c.getString(c.getColumnIndex(Sources.COLUMN_SOURCE_NAME)));
                temp.setNumber_followers(c.getInt(c.getColumnIndex(Sources.COLUMN_NUMBER_FOLLOWERS)));
                temp.setTimeStamp(c.getLong(c.getColumnIndex(Sources.COLUMN_TIME_STAMP)));
                temp.setSelected_or_not(c.getInt(c.getColumnIndex(Sources.COLUMN_SELECTED_OR_NOT)));
                temp.setNotifiable(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_NOTIFIABLE)));
                temp.setRank(c.getInt(c.getColumnIndex(Sources.COLUMN_RANK)));
                temp.setUrgent_notify(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_URGENT_NOTIFIABLE)));
                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public void deleteSourceByChangedType(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE  FROM " + Sources.TABLE_NAME + " WHERE " + Sources.COLUMN_SOURCE_ID + " = " + id + ";";
        db.execSQL(selectQuery);

        this.getReadableDatabase().close();

    }

    public void deleteCategoryByChangedType(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "DELETE  FROM " + Category.TABLE_NAME + " WHERE " + Category.COLUMN_CATEGORY_ID + " = " + id + " ;";
        db.execSQL(selectQuery);
        this.getReadableDatabase().close();
    }

    public void updateCategory(Category category) {
        db = this.getReadableDatabase();
        int columnId = category.getID();
        int categoryId = category.getCategory_id();
        int categoryType = category.getCategory_type();
        String categoryName = category.getCategory_name();
        String logoUrl = category.getLogo_url();
        int changeType = category.getChange_type();
        long timeStamp = category.getTime_stamp();
        long sourceTimeStamp = category.getSources_time_stamp();
        int count = category.getCount();
        String iso = category.getIso();

        String selectQuery = "UPDATE " + Category.TABLE_NAME + " SET " + Category.COLUMN_CATEGORY_NAME + " = '" + categoryName + "' , " +
                Category.COLUMN_CATEGORY_ID + " = " + categoryId + " , " + Category.COLUMN_CATEGORY_TYPE + " = " + categoryType + " , " +
                Category.COLUMN_LOGO_URL + " = '" + logoUrl + "' , " + Category.COLUMN_TIME_STAMP + " = " + timeStamp + " , " +
                Category.COLUMN_CHANGE_TYPE + " = " + changeType + " , " + Category.COLUMN_SOURCE_TIME_STAMP + " = " + sourceTimeStamp
                + "," + Category.COLUMN_CATEGORY_COUNT + " = " + count + "," + Category.COLUMN_CATEGORY_ISO + " = '" + iso + "'WHERE " +
                Category.COLUMN_CATEGORY_ID + " = " + categoryId + " " +
                "AND " + Category.COLUMN_CATEGORY_TYPE + " = " + categoryType;
        db.execSQL(selectQuery);
        this.getReadableDatabase().close();
    }

    public void updateSources(Sources sources) {
        db = this.getReadableDatabase();
        int id = sources.getId();
        String sourceName = sources.getSource_name();
        int sourceId = sources.getSource_id();
        String logoUrl = sources.getLogo_url();
        int followers = sources.getNumber_followers();
        int categoryId = sources.getCategory_id();
        int selectedOrNot = sources.getSelected_or_not();
        int geoId = sources.getGeo_id();
        int subId = sources.getSub_id();
        String backGroundUrl = sources.getBackground_url();
//        String detail = sources.getDetails();
        int changeType = sources.getChange_type();
        long timeStamp = sources.getTimeStamp();
        int notifiable = sources.getNotifiable();
        int rank = sources.getRank();
        int urgentNotify = sources.getUrgent_notify();

        String selectQuery = "UPDATE " + Sources.TABLE_NAME + " SET " + Sources.COLUMN_SOURCE_NAME + " = '" + sourceName + "' , " +
                Sources.COLUMN_SOURCE_ID + " =" + sourceId + " , " + Sources.COLUMN_LOGO_URL + " = '" + logoUrl + "' , " + Sources
                .COLUMN_NUMBER_FOLLOWERS + " = " + followers + " ," + Sources.COLUMN_CATEGORY_ID + " = " + categoryId + " , " + Sources
                .COLUMN_SELECTED_OR_NOT + " = " + selectedOrNot + " , " + Sources.COLUMN_GEO_ID + " = " + geoId + " , " + Sources
                .COLUMN_SUB_ID + " = " + subId + " ," + Sources.COLUMN_BACKGROUND_URL + " = '" + backGroundUrl + "' ,  " + Sources
                .COLUMN_CHANGE_TYPE + " = " + changeType + " , " + Sources
                .COLUMN_TIME_STAMP + " = " + timeStamp + " , " + Sources.COLUMN_IS_SOURCE_NOTIFIABLE + " = " + notifiable + "," + Sources
                .COLUMN_IS_SOURCE_URGENT_NOTIFIABLE + " = " + urgentNotify + " , " + Sources
                .COLUMN_RANK + "=" + rank + " WHERE " + Sources.COLUMN_SOURCE_ID + " = " + sourceId + " ; ";
        db.execSQL(selectQuery);
        this.getReadableDatabase().close();
    }

    public ArrayList<Category> getCategoryByCategoryType(int categoryType) {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + Category.TABLE_NAME + " WHERE " + Category.COLUMN_CATEGORY_TYPE + " = " + categoryType +
                ";";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {
                Category temp = new Category();
                temp.setID(c.getInt(c.getColumnIndex(Category.COLUMN_ID)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_ID)));
                temp.setCategory_type(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_TYPE)));
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Category.COLUMN_LOGO_URL)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Category.COLUMN_CHANGE_TYPE)));
                temp.setTime_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_TIME_STAMP)));
                temp.setSources_time_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_SOURCE_TIME_STAMP)));
                temp.setCount(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_COUNT)));
                temp.setIso(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_ISO)));
                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;

    }

    public ArrayList<Category> getCategoryNameBySubId(int subId) {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT " + Category.COLUMN_CATEGORY_NAME + " FROM " + Category.TABLE_NAME + " WHERE " + Category
                .COLUMN_CATEGORY_ID + " = " + subId + " AND " + Category.COLUMN_CATEGORY_TYPE + " = 2 ; ";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {

                Category temp = new Category();
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public ArrayList<Category> getCategoryNameByCatIdGeo(int geoId) {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT " + Category.COLUMN_CATEGORY_NAME + " FROM " + Category.TABLE_NAME + " WHERE " + Category
                .COLUMN_CATEGORY_ID + " = " + geoId + " AND " + Category.COLUMN_CATEGORY_TYPE + " = 1 ; ";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {

                Category temp = new Category();
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public ArrayList<Sources> getSelectedByGeo() {

        ArrayList<Sources> res = new ArrayList<Sources>();
        db = this.getReadableDatabase();
        String selectQuery = " SELECT * FROM " + Sources.TABLE_NAME + " WHERE " + Sources.COLUMN_SELECTED_OR_NOT + " = 1  AND  " +
                Sources.COLUMN_GEO_ID + "  >  0 ORDER BY " + Sources.COLUMN_GEO_ID + " ASC ";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {
                Sources temp = new Sources();
                temp.setId(c.getInt(c.getColumnIndex(Sources.COLUMN_ID)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Sources.COLUMN_LOGO_URL)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Sources.COLUMN_CATEGORY_ID)));
                temp.setGeo_id(c.getInt(c.getColumnIndex(Sources.COLUMN_GEO_ID)));
                temp.setSub_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SUB_ID)));
                temp.setNumber_followers(c.getInt(c.getColumnIndex(Sources.COLUMN_NUMBER_FOLLOWERS)));
                temp.setSelected_or_not(c.getInt(c.getColumnIndex(Sources.COLUMN_SELECTED_OR_NOT)));
                temp.setSource_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SOURCE_ID)));
                temp.setSource_name(c.getString(c.getColumnIndex(Sources.COLUMN_SOURCE_NAME)));
                temp.setTimeStamp(c.getLong(c.getColumnIndex(Sources.COLUMN_TIME_STAMP)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Sources.COLUMN_CHANGE_TYPE)));
                temp.setDetails(c.getString(c.getColumnIndex(Sources.COLUMN_DETAIL)));
                temp.setBackground_url(c.getString(c.getColumnIndex(Sources.COLUMN_BACKGROUND_URL)));
                temp.setNotifiable(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_NOTIFIABLE)));
                temp.setRank(c.getInt(c.getColumnIndex(Sources.COLUMN_RANK)));
                temp.setUrgent_notify(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_URGENT_NOTIFIABLE)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public ArrayList<Category> getCategoryByIdGeo(int id) {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + Category.TABLE_NAME + " WHERE " + Category.COLUMN_CATEGORY_ID + " = " + id + " AND " +
                Category.COLUMN_CATEGORY_TYPE + " = 1;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {

                Category temp = new Category();
                temp.setID(c.getInt(c.getColumnIndex(Category.COLUMN_ID)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_ID)));
                temp.setCategory_type(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_TYPE)));
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Category.COLUMN_LOGO_URL)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Category.COLUMN_CHANGE_TYPE)));
                temp.setTime_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_TIME_STAMP)));
                temp.setSources_time_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_SOURCE_TIME_STAMP)));
                temp.setCount(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_COUNT)));
                temp.setIso(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_ISO)));
                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        //this.getReadableDatabase().close();
        return res;
    }

    public boolean isExist(int id) {
//        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + Sources.TABLE_NAME + " WHERE " + Sources.COLUMN_SOURCE_ID + " = " + id + " AND " +
                Sources.COLUMN_SELECTED_OR_NOT + " > 0  ;";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            db.close();
            return true;

        } else {
            db.close();

            return false;
        }
    }

    public void closeDB() {
        this.getReadableDatabase().close();
    }

    public ArrayList<Category> getCategoryByIdSub(int id) {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + Category.TABLE_NAME + " WHERE " + Category.COLUMN_CATEGORY_ID + " = " + id + " AND " +
                Category.COLUMN_CATEGORY_TYPE + " = 2;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {

                Category temp = new Category();
                temp.setID(c.getInt(c.getColumnIndex(Category.COLUMN_ID)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_ID)));
                temp.setCategory_type(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_TYPE)));
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Category.COLUMN_LOGO_URL)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Category.COLUMN_CHANGE_TYPE)));
                temp.setTime_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_TIME_STAMP)));
                temp.setSources_time_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_SOURCE_TIME_STAMP)));
                temp.setCount(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_COUNT)));
                temp.setIso(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_ISO)));
                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        //this.getReadableDatabase().close();
        return res;
    }

    public ArrayList getSubjectDesc() {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + Category.TABLE_NAME + " WHERE " + Category.COLUMN_CATEGORY_COUNT + "  >  0 And " +
                Category.COLUMN_CATEGORY_TYPE + " = 2  order by count Desc;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Category temp = new Category();
                temp.setID(c.getInt(c.getColumnIndex(Category.COLUMN_ID)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_ID)));
                temp.setCategory_type(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_TYPE)));
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Category.COLUMN_LOGO_URL)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Category.COLUMN_CHANGE_TYPE)));
                temp.setTime_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_TIME_STAMP)));
                temp.setSources_time_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_SOURCE_TIME_STAMP)));
                temp.setCount(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_COUNT)));
                temp.setIso(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_ISO)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public ArrayList getCategoryById(int id) {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + Category.TABLE_NAME + " WHERE " + Category.COLUMN_CATEGORY_ID + " = " + id + "";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Category temp = new Category();
                temp.setID(c.getInt(c.getColumnIndex(Category.COLUMN_ID)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_ID)));
                temp.setCategory_type(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_TYPE)));
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Category.COLUMN_LOGO_URL)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Category.COLUMN_CHANGE_TYPE)));
                temp.setTime_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_TIME_STAMP)));
                temp.setSources_time_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_SOURCE_TIME_STAMP)));
                temp.setCount(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_COUNT)));
                temp.setIso(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_ISO)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public ArrayList getCountriesDesc() {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + Category.TABLE_NAME + " WHERE " + Category.COLUMN_CATEGORY_COUNT + "  >  0 And " +
                Category.COLUMN_CATEGORY_TYPE + " = 1  order by count Desc;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Category temp = new Category();
                temp.setID(c.getInt(c.getColumnIndex(Category.COLUMN_ID)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_ID)));
                temp.setCategory_type(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_TYPE)));
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Category.COLUMN_LOGO_URL)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Category.COLUMN_CHANGE_TYPE)));
                temp.setTime_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_TIME_STAMP)));
                temp.setSources_time_stamp(c.getInt(c.getColumnIndex(Category.COLUMN_SOURCE_TIME_STAMP)));
                temp.setCount(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_COUNT)));
                temp.setIso(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_ISO)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    private int getRowCount(int id) {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT " + Category.COLUMN_CATEGORY_COUNT + " FROM " + Category.TABLE_NAME + " WHERE " + Category.COLUMN_ID
                + " = " + id + " ; ";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Category temp = new Category();
                temp.setCount(c.getInt(c.getColumnIndex(Category.COLUMN_CATEGORY_COUNT)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res.get(0).getCount();
    }

    public void increment(int id) {
        int countId = getRowCount(id);
        countId += 1;
        db = this.getReadableDatabase();

        String selectQuery = "UPDATE " + Category.TABLE_NAME + " SET " + Category.COLUMN_CATEGORY_COUNT + " = ' " + countId + " ' WHERE "
                + Category.COLUMN_ID + " = " + id + " ";
        try {
            db = this.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.execSQL(selectQuery);
        this.getReadableDatabase().close();
    }

    public ArrayList<Category> getCategoryByGeoSelected() {
        ArrayList<Sources> tempSource;
        ArrayList<Category> tempCat;
        ArrayList<Category> cat;
        tempSource = new ArrayList<>();
        tempCat = new ArrayList<>();
        Sources source = new Sources();
        cat = new ArrayList<>();
        tempSource = getSelectedByGeo();
        for (Sources sourcew : tempSource) {
            tempCat = getCategoryByIdGeo(sourcew.getGeo_id());
            cat.addAll(tempCat);
        }

        for (int i = cat.size() - 1; i > 0; i--) {
            if (cat.get(i - 1).getCategory_id() == cat.get(i).getCategory_id())
                cat.remove(i);
        }


        return cat;
    }

    public ArrayList<Category> getCategoryBySubSelected() {
        ArrayList<Sources> tempSource;
        ArrayList<Category> catSel = new ArrayList<>();
        ;
        ArrayList<Category> tempCat;
        tempSource = new ArrayList<>();
        tempCat = new ArrayList<>();
        tempSource = getSelectedBySub();
        for (Sources source : tempSource) {
            tempCat = getCategoryByIdSub(source.getSub_id());
            catSel.addAll(tempCat);
        }
        for (int i = catSel.size() - 1; i > 0; i--) {
            if (catSel.get(i - 1).getCategory_id() == catSel.get(i).getCategory_id())
                catSel.remove(i);
        }
        return catSel;
    }

    public ArrayList<Sources> getSelectedBySub() {

        ArrayList<Sources> res = new ArrayList<Sources>();
        db = this.getReadableDatabase();
        String selectQuery = " SELECT * FROM " + Sources.TABLE_NAME + " WHERE " + Sources.COLUMN_SELECTED_OR_NOT + " = 1  AND  " +
                Sources.COLUMN_SUB_ID + "  >  0 ORDER BY " + Sources.COLUMN_SUB_ID + " ASC  ";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {
                Sources temp = new Sources();
                temp.setId(c.getInt(c.getColumnIndex(Sources.COLUMN_ID)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Sources.COLUMN_LOGO_URL)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Sources.COLUMN_CATEGORY_ID)));
                temp.setGeo_id(c.getInt(c.getColumnIndex(Sources.COLUMN_GEO_ID)));
                temp.setSub_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SUB_ID)));
                temp.setNumber_followers(c.getInt(c.getColumnIndex(Sources.COLUMN_NUMBER_FOLLOWERS)));
                temp.setSelected_or_not(c.getInt(c.getColumnIndex(Sources.COLUMN_SELECTED_OR_NOT)));
                temp.setSource_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SOURCE_ID)));
                temp.setSource_name(c.getString(c.getColumnIndex(Sources.COLUMN_SOURCE_NAME)));
                temp.setTimeStamp(c.getLong(c.getColumnIndex(Sources.COLUMN_TIME_STAMP)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Sources.COLUMN_CHANGE_TYPE)));
                temp.setDetails(c.getString(c.getColumnIndex(Sources.COLUMN_DETAIL)));
                temp.setBackground_url(c.getString(c.getColumnIndex(Sources.COLUMN_BACKGROUND_URL)));
                temp.setNotifiable(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_NOTIFIABLE)));
                temp.setRank(c.getInt(c.getColumnIndex(Sources.COLUMN_RANK)));
                temp.setUrgent_notify(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_URGENT_NOTIFIABLE)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public ArrayList<Category> getCategoryNamebyGeoId(int geoId) {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT " + Category.COLUMN_CATEGORY_NAME + " FROM " + Category.TABLE_NAME + " WHERE " + Category
                .COLUMN_CATEGORY_ID + " = " + geoId + " AND " + Category.COLUMN_CATEGORY_TYPE + " = 1  ORDER BY " + Category
                .COLUMN_CATEGORY_ID + " ASC ; ";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Category temp = new Category();
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;
    }

    public ArrayList<Category> getCategoryNameBySourceId(int sourceId) {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT " + Category.COLUMN_CATEGORY_NAME + " FROM " + Category.TABLE_NAME + " WHERE " + Category
                .COLUMN_CATEGORY_ID + " =" + sourceId + " ; ";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {

                Category temp = new Category();
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;

    }

    public ArrayList<Category> getCategoryNamebyResourceGeoIdAndSubId(int geoOrSub, int catType) {
        ArrayList<Category> res = new ArrayList<Category>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT " + Category.COLUMN_CATEGORY_NAME + " FROM " + Category.TABLE_NAME + " WHERE " + Category
                .COLUMN_CATEGORY_ID + " = " + geoOrSub + "  AND " + Category.COLUMN_CATEGORY_TYPE + " = " + catType + " ;";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {

                Category temp = new Category();
                temp.setCategory_name(c.getString(c.getColumnIndex(Category.COLUMN_CATEGORY_NAME)));
                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;

    }

    public ArrayList<Sources> getSelectedSources() {
        ArrayList<Sources> res = new ArrayList<Sources>();
        db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + Sources.TABLE_NAME + " WHERE " + Sources.COLUMN_SELECTED_OR_NOT + " = 1  ; ";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {
                Sources temp = new Sources();
                temp.setId(c.getInt(c.getColumnIndex(Sources.COLUMN_ID)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Sources.COLUMN_LOGO_URL)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Sources.COLUMN_CATEGORY_ID)));
                temp.setGeo_id(c.getInt(c.getColumnIndex(Sources.COLUMN_GEO_ID)));
                temp.setSub_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SUB_ID)));
                temp.setNumber_followers(c.getInt(c.getColumnIndex(Sources.COLUMN_NUMBER_FOLLOWERS)));
                temp.setSelected_or_not(c.getInt(c.getColumnIndex(Sources.COLUMN_SELECTED_OR_NOT)));
                temp.setSource_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SOURCE_ID)));
                temp.setSource_name(c.getString(c.getColumnIndex(Sources.COLUMN_SOURCE_NAME)));
                temp.setTimeStamp(c.getLong(c.getColumnIndex(Sources.COLUMN_TIME_STAMP)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Sources.COLUMN_CHANGE_TYPE)));
                temp.setDetails(c.getString(c.getColumnIndex(Sources.COLUMN_DETAIL)));
                temp.setBackground_url(c.getString(c.getColumnIndex(Sources.COLUMN_BACKGROUND_URL)));
                temp.setNotifiable(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_NOTIFIABLE)));
                temp.setRank(c.getInt(c.getColumnIndex(Sources.COLUMN_RANK)));
                temp.setUrgent_notify(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_URGENT_NOTIFIABLE)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;

    }

    public ArrayList<Sources> getSourceByCategoryId(int cat_id) {
        ArrayList<Sources> res = new ArrayList<Sources>();
        db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + Sources.TABLE_NAME + " WHERE " + Sources.COLUMN_CATEGORY_ID + " = " + cat_id + "; ";


        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {

                Sources temp = new Sources();
                temp.setId(c.getInt(c.getColumnIndex(Sources.COLUMN_ID)));
                temp.setLogo_url(c.getString(c.getColumnIndex(Sources.COLUMN_LOGO_URL)));
                temp.setCategory_id(c.getInt(c.getColumnIndex(Sources.COLUMN_CATEGORY_ID)));
                temp.setGeo_id(c.getInt(c.getColumnIndex(Sources.COLUMN_GEO_ID)));
                temp.setSub_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SUB_ID)));
                temp.setNumber_followers(c.getInt(c.getColumnIndex(Sources.COLUMN_NUMBER_FOLLOWERS)));
                temp.setSelected_or_not(c.getInt(c.getColumnIndex(Sources.COLUMN_SELECTED_OR_NOT)));
                temp.setSource_id(c.getInt(c.getColumnIndex(Sources.COLUMN_SOURCE_ID)));
                temp.setSource_name(c.getString(c.getColumnIndex(Sources.COLUMN_SOURCE_NAME)));
                temp.setTimeStamp(c.getLong(c.getColumnIndex(Sources.COLUMN_TIME_STAMP)));
                temp.setChange_type(c.getInt(c.getColumnIndex(Sources.COLUMN_CHANGE_TYPE)));
                temp.setDetails(c.getString(c.getColumnIndex(Sources.COLUMN_DETAIL)));
                temp.setBackground_url(c.getString(c.getColumnIndex(Sources.COLUMN_BACKGROUND_URL)));
                temp.setNotifiable(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_NOTIFIABLE)));
                temp.setRank(c.getInt(c.getColumnIndex(Sources.COLUMN_RANK)));
                temp.setUrgent_notify(c.getInt(c.getColumnIndex(Sources.COLUMN_IS_SOURCE_URGENT_NOTIFIABLE)));
                res.add(temp);
            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;

    }

    public ArrayList<Profile> getAllProfiles() {
        ArrayList<Profile> res = new ArrayList<Profile>();
        db = this.getReadableDatabase();

        String selectQuery = "SELECT  *  FROM " + Profile.TABLE_NAME + " ;";


        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {

            do {

                Profile temp = new Profile();
                temp.setID(c.getInt(c.getColumnIndex(Profile.COLUMN_ID)));
                temp.setUser_id(c.getInt(c.getColumnIndex(Profile.COLUMN_USER_ID)));
                temp.setRegFlag(c.getInt(c.getColumnIndex(Profile.COLUMN_REG_FLAG)));
                temp.setUrgentFlag(c.getInt(c.getColumnIndex((Profile.COLUMN_URGENT_FLAG))));
                temp.setSoundType(c.getInt(c.getColumnIndex(Profile.COLUMN_SOUND_TYPE)));
                temp.setBlockImg(c.getInt(c.getColumnIndex(Profile.COLUMN_BLOCK_IMG)));
                res.add(temp);

            } while (c.moveToNext());
        }
        c.close();
        this.getReadableDatabase().close();
        return res;

    }

    public void insertInHistory(History history) {
        insert(History.TABLE_NAME, history.fields, history.getValues(), History.ARTICLE_ID);
        this.getReadableDatabase().close();
    }

    public void insertInCategories(Category category) {
        insert(Category.TABLE_NAME, category.fields, category.getValues(), Category.COLUMN_ID);
        this.getReadableDatabase().close();
    }

    public void insertInProfile(Profile profile) {
        insert(Profile.TABLE_NAME, profile.fields, profile.getValues(), Profile.COLUMN_ID);
        this.getReadableDatabase().close();
    }

    public void insertInSources(Sources sources) {
        insert(Sources.TABLE_NAME, sources.fields, sources.getValues(), sources.COLUMN_ID);
        this.getReadableDatabase().close();
    }

    public Cursor insert(String table, String[] fields, String[] values, String whereCause) {
        Cursor c = null;
        try {
            SQLiteDatabase db = null;
            db = this.getReadableDatabase();
            ContentValues vals = new ContentValues();
            for (int i = 0; i < fields.length; i++)
                vals.put(fields[i], values[i]);

            long dd = db.insert(table, null, vals);
            dd = dd;
            String query = " Select " + whereCause + " from " + table + " order by " + whereCause + " DESC limit 1 ;";
            c = db.rawQuery(query, null);

        } catch (Exception e) {
            Log.e("HiNews", "Exception", e);
        }
        return c;
    }

    public void replace(String table, String clause, String column_clause, String[] fields, String[] values) {

        SQLiteDatabase db = null;
        db = this.getWritableDatabase();

        ContentValues vals = new ContentValues();
        int i;
        for (i = 0; i < fields.length; i++)
            vals.put(fields[i], values[i]);

        long id;
        id = db.update(table, vals, clause + "=?", new String[]{column_clause});

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_TABLE_CATEGORY_IF_NOT_EXISTS);
        db.execSQL(CREATE_TABLE_SOURCES);
        db.execSQL(CREATE_TABLE_PROFILE);
        db.execSQL(CREATE_TABLE_HISTORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            GetColumns(db, Category.TABLE_NAME);
            // on upgrade drop older tables
//            db.execSQL("DROP TABLE IF EXISTS " + Category.TABLE_NAME);
//            db.execSQL("DROP TABLE IF EXISTS " + Sources.TABLE_NAME);
//            db.execSQL("DROP TABLE IF EXISTS " + Profile.TABLE_NAME);
//            db.execSQL("DROP TABLE IF EXISTS " + History.TABLE_NAME);
//            // create new tables
//            try {
//                deleteCache(context);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            try {
//                sharedpreferences = context.getSharedPreferences(LoginPopup.MyPREFERENCES, Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.clear().apply();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            try {
//                SharedPreferences msharedpreferences = context.getSharedPreferences("MY_PREFS", Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = msharedpreferences.edit();
//                editor.clear().apply();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            try {
//                SharedPreferences msharedpreferences = context.getSharedPreferences("MyPrefsFile", Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = msharedpreferences.edit();
//                editor.clear().apply();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            try {
//                SharedPreferences msharedpreferences = context.getSharedPreferences("apprater", Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = msharedpreferences.edit();
//                editor.clear().apply();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            onCreate(db);

//            run a table creation with if not exists (we are doing an upgrade, so the table might not exists yet, it will fail alter and
// drop)
            db.execSQL(CREATE_TABLE_CATEGORY_IF_NOT_EXISTS);
//            put in a list the existing columns List<String> columns = DBUtils.GetColumns(db, TableName);
            List<String> columns = GetColumns(db, Category.TABLE_NAME);

//            backup table (ALTER table " + TableName + " RENAME TO 'temp_"                    + TableName)
            db.execSQL("ALTER table " + Category.TABLE_NAME + " RENAME TO temp_" + Category.TABLE_NAME + ";");
//            create new table (the newest table creation schema)
            db.execSQL(CREATE_TABLE_CATEGORY);
//            get the intersection with the new columns, this time columns taken from the upgraded table (columns.retainAll(DBUtils
// .GetColumns(db, TableName));)
            columns.retainAll(GetColumns(db, Category.TABLE_NAME));
//            db.execSQL(String.format(
//                    "INSERT INTO %s (%s) SELECT %s from temp_%s",
//                    TableName, cols, cols, TableName));
            String cols = join(columns, ",");
            db.execSQL(String.format(
                    "INSERT INTO %s (%s) SELECT %s from temp_%s",
                    Category.TABLE_NAME, cols, cols, Category.TABLE_NAME));
            db.execSQL("DROP table temp_" + Category.TABLE_NAME + " ;");
//)
//            remove backup table (DROP table 'temp_" + TableName)
            db.execSQL(CREATE_TABLE_HISTORY);

        }
//        updateDatabase(db, oldVersion + 1, newVersion);
    }

    private void updateDatabase(SQLiteDatabase db, int fromVersion, int toVersion) {
        Log.i("Updating Database", "Updating database from version " + fromVersion + " to " + toVersion);

        for (int ver = fromVersion; ver <= toVersion; ver++) {
            switch (ver) {
                case 1:
                    handleDatabaseVersion1(db);
                    break;
                case 2:
                    handleDatabaseVersion2(db);
                    break;
            }
        }
    }

    private void handleDatabaseVersion1(SQLiteDatabase db) {
        // create initial tables and so on
    }

    private void handleDatabaseVersion2(SQLiteDatabase db) {
        // here we extend databases
    }

    public void changeName() {


    }
}