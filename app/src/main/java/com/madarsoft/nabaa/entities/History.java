package com.madarsoft.nabaa.entities;

/**
 * Created by Colossus on 08-May-17.
 */

public class History {

    public static final String TABLE_NAME = "history";
    public static final String SOURCE_ID = "SourceId";
    public static final String SOURCE_LOGO = "SourceLogo";
    public static final String SOURCE_NAME = "SourceName";
    public static final String ISRTL = "IsRtl";
    public static final String ARTICLE_ID = "ArticleId";
    public static final String ARTICLE_TITLE = "ArticleTitle";
    public static final String APPREV = "Apprev";
    public static final String SHARE_LINK = "ShareLink";
    public static final String ARTICLE_DATE = "ArticleDate";
    public static final String ARTICLE_IMAGE = "ArticleImage";
    public static final String LIKES_COUNT = "LikesCount";
    public static final String COMMENTS_COUNT = "CommentsCount";
    public static final String SHARE_COUNT = "ShareCount";
    public static final String SHARE_ID = "ShareId";
    public static final String URGENT = "Urgent";
    public static final String FAVOURITE_ID = "FavouriteId";
    public static final String LIKE_ID = "Like_Id";
    public final String fields[] = new String[]{SOURCE_ID, SOURCE_LOGO, SOURCE_NAME, ISRTL, ARTICLE_ID, ARTICLE_TITLE, APPREV,
            SHARE_LINK, ARTICLE_DATE, ARTICLE_IMAGE, LIKES_COUNT, COMMENTS_COUNT, SHARE_COUNT, SHARE_ID, URGENT, FAVOURITE_ID, LIKE_ID};
    private int SourceId;
    private String SourceLogo;
    private String SourceName;
    private String IsRtl;
    private int ArticleId;
    private String ArticleTitle;
    private String Apprev;
    private String ArticleDate;
    private String ArticleImage;
    private int LikesCount;
    private int CommentsCount;
    private int ShareCount;
    private String ShareLink;
    private int ShareId;
    private int Urgent;
    private int FavouriteId;
    private int Like_Id;
    private Long longDate;

    public int getLike_Id() {
        return Like_Id;
    }

    public void setLike_Id(int like_Id) {
        Like_Id = like_Id;
    }

    public String getSourceName() {
        return SourceName;
    }

    public void setSourceName(String sourceName) {
        SourceName = sourceName;
    }

    public String getIsRtl() {
        return IsRtl;
    }

    public void setIsRtl(String isRtl) {
        IsRtl = isRtl;
    }

    public int getArticleId() {
        return ArticleId;
    }

    public void setArticleId(int articleId) {
        ArticleId = articleId;
    }

    public String getArticleTitle() {
        return ArticleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        ArticleTitle = articleTitle;
    }

    public String getApprev() {
        return Apprev;
    }

    public void setApprev(String apprev) {
        Apprev = apprev;
    }

    public String getArticleDate() {
        return ArticleDate;
    }

    public void setArticleDate(String articleDate) {
        ArticleDate = articleDate;
    }

    public String getArticleImage() {
        return ArticleImage;
    }

    public void setArticleImage(String articleImage) {
        ArticleImage = articleImage;
    }

    public int getLikesCount() {
        return LikesCount;
    }

    public void setLikesCount(int likesCount) {
        LikesCount = likesCount;
    }

    public int getCommentsCount() {
        return CommentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        CommentsCount = commentsCount;
    }

    public int getShareCount() {
        return ShareCount;
    }

    public void setShareCount(int shareCount) {
        ShareCount = shareCount;
    }

    public String getShareLink() {
        return ShareLink;
    }

    public void setShareLink(String shareLink) {
        ShareLink = shareLink;
    }

    public int getShareId() {
        return ShareId;
    }

    public void setShareId(int shareId) {
        ShareId = shareId;
    }

    public int getUrgent() {
        return Urgent;
    }

    public void setUrgent(int urgent) {
        Urgent = urgent;
    }

    public int getFavouriteId() {
        return FavouriteId;
    }

    public void setFavouriteId(int favouriteId) {
        FavouriteId = favouriteId;
    }

    public int getSourceId() {
        return SourceId;
    }

    public void setSourceId(int sourceId) {
        SourceId = sourceId;
    }

    public String getSourceLogo() {
        return SourceLogo;
    }

    public void setSourceLogo(String sourceLogo) {
        SourceLogo = sourceLogo;
    }

    public String[] getValues() {
        return new String[]{"" + SourceId, "" + SourceLogo, "" + SourceName, "" + IsRtl, "" + ArticleId, "" + ArticleTitle, "" + Apprev,
                "" + ShareLink, "" + ArticleDate, "" + ArticleImage, "" + LikesCount, "" + CommentsCount, "" + ShareCount, "" + ShareId,
                "" + Urgent, "" + FavouriteId, "" + Like_Id};

    }


    public Long getLongDate() {
        return longDate;
    }

    public void setLongDate(Long longDate) {
        this.longDate = longDate;
    }
}
