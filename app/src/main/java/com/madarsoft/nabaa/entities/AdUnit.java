package com.madarsoft.nabaa.entities;

/**
 * Created by basma on 7/20/2017.
 */

public class AdUnit {
    public static final String NATIVE="Native";
    public static final String BANNER="banner";
    public static final String MREC="MRect";
    public static final String SPLASH="Interstitial";
    public static final String TYPE_APPODEAL="appodeal";
    public static final String TYPE_OGURY="ogury";
    public static final String LAST_APPEARENCE_TIME="adLastAppearanceTime";

    String type;
    boolean enabled;
    //int start_after;
    private AdPlacementAndUnit backup;
    public int getDisplay_delay_seconds() {
        return display_delay_seconds;
    }

    public void setDisplay_delay_seconds(int display_delay_seconds) {
        this.display_delay_seconds = display_delay_seconds;
    }

    int display_delay_seconds;
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

/*    public int getStart_after() {
        return start_after;
    }

    public void setStart_after(int start_after) {
        this.start_after = start_after;
    }

    public int getRepeat_every() {
        return repeat_every;
    }

    public void setRepeat_every(int repeat_every) {
        this.repeat_every = repeat_every;
    }*/

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public boolean isAnimate() {
        return animate;
    }

    public void setAnimate(boolean animate) {
        this.animate = animate;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public void setScreen_name(String screenName) {
        this.screen_name = screenName;
    }

/*    public int getDurationBetweenTwoAppearnceInHours() {
        return durationBetweenTwoAppearnceInHours;
    }

    public void setDurationBetweenTwoAppearnceInHours(int durationBetweenTwoAppearnceInHours) {
        this.durationBetweenTwoAppearnceInHours = durationBetweenTwoAppearnceInHours;
    }*/

    public int getDurationBetweenTwoAppearnceInMin() {
        return durationBetweenTwoAppearnceInMin;
    }

    public void setDurationBetweenTwoAppearnceInMin(int durationBetweenTwoAppearnceInMin) {
        this.durationBetweenTwoAppearnceInMin = durationBetweenTwoAppearnceInMin;
    }

    int durationBetweenTwoAppearnceInMin;
    //int repeat_every;
    String place;
    String size;
    boolean animate;
    String screen_name;

    public AdPlacementAndUnit getAdPlacementAndUnit() {
        return backup;
    }

    public void setAdPlacementAndUnit(AdPlacementAndUnit backup) {
        this.backup = backup;
    }
}
