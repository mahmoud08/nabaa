package com.madarsoft.nabaa.entities;

public class URLs {

    public static final int MORE_ITEMS = -1;
    public static final int COUNTRIES_ITEMS = -2;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public static final String ADD_USER_URL = "https://api.nabaapp.com/user/add";
    public static final String GET_CATEGORIES_URL = "https://api.nabaapp.com/category/index";
    public static final String GET_CATEGORIES_RESOURCES_URL = " https://api.nabaapp.com/source/category";
    public static final String GET_COUNTRIES_RESOURCES_URL = "https://api.nabaapp.com/source/country";
    public static final String ADD_SOURCE_URL = "https://api.nabaapp.com/source/AddWithUrgent";
    public static final String DELETE_SOURCE_URL = "https://api.nabaapp.com/source/delete";
    public static final String GET_COUNTRIES_URL = "https://api.nabaapp.com/country/index";
    public static final String GET_RECENT_NEWS_URL = "https://api.nabaapp.com/article/index";
    public static final String GET_URGENT_NEWS_URL = "https://api.nabaapp.com/article/urgent";
    public static final String GET_HOT_NEWS_URL = "https://api.nabaapp.com/article/HOTNews";
    public static final String GET_MOST_READED_NEWS_URL = "https://api.nabaapp.com/article/MoreReaders";
    public static final String GET_NEWS_DETAILS_URL = "https://api.nabaapp.com/article/DetailsBYIDAndHash";
    public static final String ADD_LIKE_URL = "https://api.nabaapp.com/like/add";
    public static final String DELETE_LIKE_URL = "https://api.nabaapp.com/like/delete";
    public static final String ADD_FAVOURITE_URL = "https://api.nabaapp.com/FavoriteArticle/ADD";
    public static final String DELETE_FAVOURITE_URL = "https://api.nabaapp.com/FavoriteArticle/delete";
    public static final String REPORT_SOURCE_URL = "https://api.nabaapp.com/report/add";
    public static final String ADD_COMMENT_URL = "https://api.nabaapp.com/comment/add";
    public static final String EDIT_COMMENT_URL = "https://api.nabaapp.com/comment/edit";
    public static final String DELETE_COMMENT_URL = "https://api.nabaapp.com/comment/delete";
    public static final String RETRIEVE_COMMENT_URL = "https://api.nabaapp.com/comment/index";
    public static final String SHARE_URL = "https://api.nabaapp.com/share/add";
    public static final String EDIT_USER_ACCOUNT_URL = "https://api.nabaapp.com/user/edit";
    public static final String FAVOURITE_NEWS_URL = "https://api.nabaapp.com/FavoriteArticle/index";
    public static final String GET_USER_SOURCES_URL = "https://api.nabaapp.com/source/user";
    public static final String DELETE_USER_SOURCES_URL = "https://api.nabaapp.com/user/DeleteSources";
    public static final String GET_SOURCE_NEWS_URL = "https://api.nabaapp.com/source/articles";
    public static final String GET_NOTIFICATION_NEWS_DETAILS_URL = "https://api.nabaapp.com/article/DetailsBYIDAndHash";
    public static final String GET_CATEGORY_NEWS_URL = "https://api.nabaapp.com/country/articles";
    public static final String GCM_TO_SERVER = "https://api.nabaapp.com/user/gcmRegister";
    public static final String SOURCES_SEARCH = "https://api.nabaapp.com/source/search";
    public static final String DEFAULT_SOURCES = "http://api.nabaapp.com/user/RecommendWithNotifyUrgent";

    //Notifications URLs.
    public static final String ADD_NOTIFICATION_URL = "https://api.nabaapp.com/Notification/add";
    public static final String REMOVE_NOTIFICATION_URL = "https://api.nabaapp.com/Notification/remove";
    public static final String ADD_URGENT_URL = "https://api.nabaapp.com/Notification/AddUrgent";
    public static final String REMOVE_URGENT_URL = "https://api.nabaapp.com/Notification/RemoveUrgent";
    public static final String NOTIFICATION_HOLDER = "http://api.nabaapp.com/Notification/DefaultNotification";
    public static final String RETRIEVE_NOTIFICATION_HOLDER = "http://api.nabaapp.com//Notification/Default";
    //search apis
    public static final String SEARCH_BY_SOURCES = "http://api.nabaapp.com/article/search";
    public static final String SEARCH_BY_USER_SOURCES = "http://api.nabaapp.com/article/searchByUser";

    public static final int ADD_USER_REQUEST_TYPE = 1;
    public static final int GET_CATEGORIES_REQUEST_TYPE = 2;
    public static final int GET_COUNTRIES_RESOURCES_TYPE = 3;
    public static final int SOURCE_ADD_REQUEST_TYPE = 4;
    public static final int GET_COUNTRIES_REQUEST_TYPE = 5;
    public static final int SOURCE_DELETE_REQUEST_TYPE = 6;
    public static final int GET_RECENT_NEWS_REQUEST_TYPE = 7;
    public static final int GET_NEWS_DETAILS_REQUEST_TYPE = 8;
    public static final int ADD_LIKE_REQUEST_TYPE = 9;
    public static final int DELETE_LIKE_REQUEST_TYPE = 10;
    public static final int GET_HOT_NEWS_REQUEST_TYPE = 11;
    public static final int GET_URGENT_NEWS_REQUEST_TYPE = 12;
    public static final int ADD_FAVOURITE_REQUEST_TYPE = 13;
    public static final int DELETE_FAVOURITE_REQUEST_TYPE = 14;
    public static final int REPORT_SOURCE_REQUEST_TYPE = 15;
    public static final int ADD_COMMENT_REQUEST_TYPE = 16;
    public static final int EDIT_COMMENT_REQUEST_TYPE = 17;
    public static final int DELETE_COMMENT_REQUEST_TYPE = 18;
    public static final int RETRIEVE_COMMENT_REQUEST_TYPE = 19;
    public static final int SHARE_REQUEST_TYPE = 20;
    public static final int EDIT_USER_ACCOUNT_REQUEST_TYPE = 21;
    public static final int FAVOURITE_NEWS_REQUEST_TYPE = 22;
    public static final int GET_USER_SOURCES_REQUEST_TYPE = 23;
    public static final int DELETE_USER_SOURCES_REQUEST_TYPE = 24;
    //notification Request Type.
    public static final int ADD_NOTIFICATION_REQUEST_TYPE = 25;
    public static final int REMOVE_NOTIFICATION_REQUEST_TYPE = 26;
    public static final int ADD_URGENT_REQUEST_TYPE = 27;
    public static final int REMOVE_URGENT_REQUEST_TYPE = 28;
    public static final int GET_SOURCE_NEWS_REQUEST_TYPE = 29;
    public static final int GET_NOTIFICATION_NEWS_DETAILS__TYPE = 30;
    public static final int GET_CATEGORY_NEWS_TYPE = 31;
    public static final int GET_GCM_TO_SERVER_TYPE = 32;
    public static final int SEARCH_IN_SOURCES_WEB = 33;
    public static final int HOLDER_NOTIFICATION_TYPE = 34;
    public static final int RETRIEVE_NOTIFICATION_HOLDER_TYPE = 35;
    public static final int DEFAULT_SOURCES_TYPE = 36;
    public static final int SEARCH_BY_SOURCES_TYPE = 37;
    public static final int SEARCH_BY_USER_SOURCES_TYPE = 38;

    // JSON Node names
    public static final String TAG_RESULTS = "result";
    public static final String TAG_REQUEST_TIME_STAMP = "timeStamp";
    public static final String TAG_REQUEST_IS_COUNTRY = "isCountry";
    public static final String TAG_TIME_STAMP = "TimeStamp";
    public static final String TAG_COUNTRID = "countryId";
    public static final String TAG_CATEGORYID = "categoryId";
    //users
    public static final String TAG_DEVICE_TOKEN = "deviceToken";
    public static final String TAG_UD_ID = "udId";
    public static final String TAG_PLATFORM = "platForm";
    public static final String TAG_VERSION = "version";
    public static final String TAG_USERID = "userId";
    public static final String TAG_REG_FLAG = "Register";
    //categories
    public static final String TAG_CATEGORIES = "Categories";
    public static final String TAG_CATEGORY_ID = "Id";
    public static final String TAG_CATEGORY_NAME = "Name";
    public static final String TAG_CATEGORY_IMAGE_URL = "ImageUrl";
    public static final String TAG_CATEGORY_CHANGE_TYPE = "ChangeType";
    public static final String TAG_CATEGORY_TYPE = "2";
    //sources
    public static final String TAG_SOURCES = "Sources";
    public static final String TAG_SOURCE = "Source";
    public static final String TAG_COUNTRIES_RESOURCES_COUNTRYID = "CountryId";
    public static final String TAG_COUNTRIES_RESOURCES_CATEGORYID = "CategoryId";
    public static final String TAG_COUNTRIES_RESOURCES_USERID = "userId";
    public static final String TAG_COUNTRIES_RESOURCES_ID = "Id";
    public static final String TAG_COUNTRIES_RESOURCES_NAME = "Name";
    public static final String TAG_COUNTRIES_RESOURCES_DETAIL = "Details";
    public static final String TAG_COUNTRIES_RESOURCES_IMAGE_URL = "ImageUrl";
    public static final String TAG_COUNTRIES_RESOURCES_CHANGE_TYPE = "ChangeType";
    public static final String TAG_COUNTRIES_RESOURCES_FOLLOWERS_NUMBER = "FollowersNo";
    public static final String TAG_COUNTRIES_RESOURCES_SELECTED = "Selected";
    //source select/unselect request
    public static final String TAG_ADD_SOURCE_USERID = "userId";
    public static final String TAG_SOURCE_SOURCEID = "sourceId";
    //countries
    public static final String TAG_COUTRIES = "Countries";
    public static final String TAG_COUNTRY_TYPE = "1";
    //news
    public static final String TAG_NEWS = "Articles";
    public static final String TAG_REQUEST_ARTICLE_ID = "articleId";
    public static final String TAG_COUNT_ARTICLE = "countArticle";//no of requested articles
    public static final String TAG_ARTICLE_IDS_LIST = "articlesIds";//no of requested articles
    public static final String TAG_READERS = "readers";//no of requested articles
    public static final String TAG_RESPONSE_READERS = "Readers";//no of requested articles
    public static final String TAG_IS_NEW = "isNew";
    public static final String TAG_SOURCE_ID = "SourceId";
    public static final String TAG_SOURCE_LOGO = "SourceLogo";
    public static final String TAG_SOURCE_NAME = "SourceName";
    public static final String TAG_ARTICLE_ID = "ArticleId";
    public static final String TAG_ID = "id";
    public static final String TAG_ARTICLE_TITLE = "ArticleTitle";
    public static final String TAG_ARTICLE_DETAILS = "ArticleDetails";
    public static final String TAG_ARTICLE_URL = "ArticleUrl";
    public static final String TAG_URGENT = "Urgent";
    public static final String TAG_REQUEST_URGENT = "urgent";
    public static final String TAG_REQUEST_NORMAL = "normal";

    public static final String TAG_ARTICLE_LOGO = "ArticleImage";
    public static final String TAG_ARTICLE_DATE = "ArticleDate";
    public static final String TAG_ARTICLE_DATE2 = "Date";
    public static final String TAG_LIKES_COUNT = "LikesCount";
    public static final String TAG_TIME_OFFSET = "TimeOffset";
    public static final String TAG_COMMENTS_COUNT = "commentsCount";
    public static final String TAG_SHARES_COUNT = "sharesCount";
    public static final String TAG_LIKE_ID = "LikeId";
    public static final String TAG_SHARE_LINK = "ShareLink";
    public static final String TAG_FAVOURITE_ID = "FavoriteId";
    public static final String TAG_ACTIVATE_DATE = "ActivateDate";
    public static final String TAG_DATE_TIME = "DateTime";
    public static final String TAG_PAGE_NUM = "pageNo";
    public static final String TAG_IS_RTL = "IsRtl";
    //add/delete favourite
    public static final String TAG_ADD_FAVOURITE_ARTICLE_ID = "articleId";
    public static final String TAG_ADD_FAVOURITE_USERID = "userId";
    public static final String TAG_DELETE_FAVOURITE_FAVOURITE_ARTICLE_ID = "favoriteArticleId";
    //add/delete likes
    public static final String TAG_ADD_LIKE_LIKE_ID = "likeId";
    public static final String TAG_ADD_LIKE_LIKES_COUNT = "likesCount";
    //report source
    public static final String TAG_REPORT_SOURCE_USER_ID = "userId";
    public static final String TAG_REPORT_SOURCE_SOURCE_ID = "sourceId";
    //returned comment
    public static final String TAG_COMMENTS = "comments";
    public static final String TAG_ADD_COMMENT_USER_NAME = "UserName";
    public static final String TAG_ADD_COMMENT_USER_IMG = "UserImage";
    public static final String TAG_ADD_COMMENT_COMMENT_ID = "CommentId";
    public static final String TAG_ADD_COMMENT_USER_ID = "AccountId";
    public static final String TAG_ADD_COMMENT_RESPONSE_TEXT = "CommentText";
    public static final String TAG_ADD_COMMENT_COMMENT_DATE = "CommentDate";
    public static final String TAG_ADD_COMMENT_CHANGE_TYPE = "ChangeType";
    //retrieve comment
    public static final String TAG_RETRIEVE_COMMENT_ID = "commentId";
    public static final String TAG_RETRIEVE_ARTICEL_ID = "articleId";
    public static final String TAG_RETRIEVE_COMMENT_COUNT = "count";
    //add comment
    public static final String TAG_ADD_COMMENT_USER_ACCOUNT_ID = "userAccountId";
    public static final String TAG_ADD_COMMENT_ARTICLE_ID = "articleId";
    public static final String TAG_ADD_COMMENT_TEXT = "text";
    public static final String TAG_ADD_COMMENT_COUNT = "count";
    //edit comment
    public static final String TAG_EDIT_COMMENT_ID = "commentId";
    public static final String TAG_EDIT_COMMENT_TEXT = "text";
    public static final String TAG_EDIT_COMMENT_COUNT = "count";
    //delete comment
    public static final String TAG_DELETE_COMMENT_ID = "commentId";
    public static final String TAG_DELETE_COMMENT_COUNT = "count";
    //share
    public static final String TAG_SHARE_ARTICLE_ID = "articleId";
    public static final String TAG_SHARE_ARTICLE_URL = "shareUrl";
    public static final String TAG_SHARE_COUNT = "ShareCount";
    public static final String TAG_SHARE_SHORT_URL = "ShortenerUrl";
    //edit user account
    public static final String TAG_EDIT_USER_ACCOUT_USER_ID = "userId";
    public static final String TAG_EDIT_USER_ACCOUT_USER_NAME = "userName";
    public static final String TAG_EDIT_USER_ACCOUT_USER_IMAGE = "image";
    public static final String TAG_EDIT_USER_ACCOUT_USER_TYPE = "accountType";
    public static final String TAG_EDIT_USER_ACCOUT_USER_PUBLIC_ID = "publicId";
    //retrieve favourite news
    public static final String TAG_FAVOURITE_NEWS_USER_ID = "userId";
    public static final String TAG_FAVOURITE_NEWS_ARTICLE_ID = "lastFavoriteArticleId";
    public static final String TAG_FAVOURITE_NEWS_COUNT = "countArticle";
    public static final String TAG_FAVOURITE_ARTICLE_ID = "FavoriteArticleId";
    //get / delete user sources
    public static final String TAG_USER_SOURCES_USER_ID = "userId";
    public static final String TAG_USER_SOURCES_COUNTRIES = "countries";
    public static final String TAG_USER_SOURCES_CATEGORIES = "categories";
    public static final String TAG_USER_SOURCES_CAT_ID = "CategoryId";
    public static final String TAG_USER_SOURCES_CATEGORY_ID = "categoryId";
    public static final String TAG_USER_SOURCES_CONT_ID = "CountryId";
    public static final String TAG_USER_SOURCES_CAT_NAME = "categoryName";
    public static final String TAG_USER_SOURCES = "sources";
    public static final String TAG_USER_SOURCES_ID = "Id";
    public static final String TAG_USER_SOURCES_NAME = "Name";
    public static final String TAG_USER_SOURCES_DETAIL = "Details";
    public static final String TAG_USER_SOURCES_IMAGE_URL = "ImageUrl";
    public static final String TAG_USER_SOURCES_FOLLOWERS_NUMBER = "FollowersNo";
    public static final String TAG_USER_SOURCES_WEBSITE = "webSite";
    public static final String TAG_USER_SOURCES_SELECTED = "Selected";
    public static final String TAG_USER_SOURCES_TIMESTAMP = "TimeStamp";
    public static final String TAG_USER_SOURCES_CHANGETYPE = "ChangeType";
    public static final String TAG_NOTIFICATION = "Notification";
    public static final String TAG_URGENT_NOTIFICATION = "UrgentNotification";
    public static final String TAG_USER_URGENT = "urgentNews";
    //notification nodes
    public static final String TAG_ADD_NOTIFICATION_USER_ID = "userId";
    public static final String TAG_ADD_NOTIFICATION_SOURCE_ID = "sourceId";
    public static final String TAG_REMOVE_NOTIFICATION_USER_ID = "userId";
    public static final String TAG_REMOVE_NOTIFICATION_SOURCE_ID = "sourceId";
    public static final String TAG_ADD_URGENT_USER_ID = "userId";
    public static final String TAG_REMOVE_URGENT_USER_ID = "userId";
    public static final String TAG_REG_ID = "regID";
    public static final String TAG_MCC = "mcc";
    public static final String TAG_TEXT = "text";
    public static final String TAG_PAGE = "page";
    public static final String TAG_ISO = "Iso";
    public static final String SEARCH_TIME_STAMP = "timestamp";


    public static boolean serviceFlag;
    private static int READERS;
    private static int USERID;
    private static String RECENT_FIRST_TIMESTAMP;
    private static String RECENT_LAST_TIMESTAMP;
    private static String URGENT_FIRST_TIMESTAMP;
    private static String URGENT_LAST_TIMESTAMP;
    private static int LASTFAVNEWSID;
    private static String LASTDATETIME;
    private static String FIRSTDATETIME = "1970-01-01 16:48:03.947";
    private static boolean isTimoutExc;

    public static String getRecnetLastTimeStamp() {
        return RECENT_LAST_TIMESTAMP;
    }

    public static void setRecentLastTimeStamp(String lastTimeStampe) {
        RECENT_LAST_TIMESTAMP = lastTimeStampe;
    }

    public static String getRecentFirstTimeStamp() {
        return RECENT_FIRST_TIMESTAMP;
    }

    public static void setRecentFirstTimeStamp(String firstTimeStampe) {
        RECENT_FIRST_TIMESTAMP = firstTimeStampe;
    }

    public static String getUrgentLastTimeStamp() {
        return URGENT_LAST_TIMESTAMP;
    }

    public static void setUrgentLastTimeStamp(String lastTimeStamp) {
        URGENT_LAST_TIMESTAMP = lastTimeStamp;
    }

    public static String getUrgentFirstTimeStamp() {
        return URGENT_FIRST_TIMESTAMP;
    }

    public static void setUrgentFirstTimeStamp(String firstTimeStamp) {
        URGENT_FIRST_TIMESTAMP = firstTimeStamp;
    }

    public static int getUserID() {
        return USERID;
    }

    public static void setUserID(int userId) {
        USERID = userId;
    }

    public static int getFavNewsID() {
        return LASTFAVNEWSID;
    }

    public static void setFavNewsID(int favNewsID) {
        LASTFAVNEWSID = favNewsID;
    }

    public static String getLastDateTime() {
        return LASTDATETIME;
    }

    public static void setLastDateTime(String lastDateTime) {
        LASTDATETIME = lastDateTime;
    }

    public static String getFirstDateTime() {
        return FIRSTDATETIME;
    }

    public static void setFirstDateTime(String firstDateTime) {
        FIRSTDATETIME = firstDateTime;
    }

    public static int getREADERS() {
        return READERS;
    }

    public static void setREADERS(int READERS) {
        URLs.READERS = READERS;
    }

    public static boolean isTimoutExc() {
        return isTimoutExc;
    }

    public static void setIsTimoutExc(boolean isTimoutExc) {
        URLs.isTimoutExc = isTimoutExc;
    }
}
