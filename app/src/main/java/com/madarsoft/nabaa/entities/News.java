package com.madarsoft.nabaa.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

//import com.facebook.ads.NativeAd;

public class News implements Parcelable, Serializable {

    // Just cut and paste this for now
    public static final Parcelable.Creator<News> CREATOR = new Parcelable.Creator<News>() {
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        public News[] newArray(int size) {
            return new News[size];
        }
    };
    private static final long serialVersionUID = 1L;
    public boolean isUrgent;
    long pawsCount; // handle marsian centipedes
    boolean sharpTeeth;
    private int ID;
    private int sourceID;
    private String sourceName;
    private String sourceImage;
    private int sourceFollowers;
    private int changeType;
    private String webSite;
    private String details;
    private int categoryID;
    private int countryID;
    private String sourceTitle;
    private String logo_url;
    private String sourceLogoUrl;
    private String time_stamp;
    private String articleDate;
    private String newsTitle;
    private String newsDetails;
    private int likesNumber;
    private int commentsNumber;
    private int sharesNumber;
    private int likeID;
    private int favouriteID;
    private String newsUrl;
    private String shareUrl;
    private String timeOffset;
    private int readersNum;
    private String dateTime;
    private int isBlocked;
    private String isRTL;
    private boolean isLoaded;
    private boolean isTicked;

    public News() {
    }

    public News(Parcel in) {
        // This order must match the order in writeToParcel()
        ID = in.readInt();
        sourceID = in.readInt();
        likesNumber = in.readInt();
        commentsNumber = in.readInt();
        sharesNumber = in.readInt();
        likeID = in.readInt();
        favouriteID = in.readInt();
        readersNum = in.readInt();

        isUrgent = in.readByte() != 0;     //myBoolean == true if byte != 0

        sourceTitle = in.readString();
        logo_url = in.readString();
        sourceLogoUrl = in.readString();
        articleDate = in.readString();
        newsTitle = in.readString();
        newsDetails = in.readString();
        newsUrl = in.readString();
        timeOffset = in.readString();
        shareUrl = in.readString();
        time_stamp = in.readString();
        dateTime = in.readString();
        isRTL = in.readString();
        // Continue doing this for the rest of your member data
    }
//    private transient NativeAd nativeAd;

    /*private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {

        in.defaultReadObject();
        ObjectInputStream.GetField fields = in.readFields();
        int paws = fields.get("paws", 0); // the 0 is a default value
        this.pawsCount = paws;
    }*/

    public boolean isLoaded() {
        return isLoaded;
    }

    public void setLoaded(boolean loaded) {
        isLoaded = loaded;
    }

    public void writeToParcel(Parcel out, int flags) {
        // Again this order must match the Question(Parcel) constructor

        out.writeInt(ID);
        out.writeInt(sourceID);
        out.writeInt(likesNumber);
        out.writeInt(commentsNumber);
        out.writeInt(sharesNumber);
        out.writeInt(likeID);
        out.writeInt(favouriteID);
        out.writeInt(readersNum);

        out.writeByte((byte) (isUrgent ? 1 : 0));

        out.writeString(sourceTitle);
        out.writeString(logo_url);
        out.writeString(sourceLogoUrl);
        out.writeString(articleDate);
        out.writeString(newsTitle);
        out.writeString(newsDetails);
        out.writeString(newsUrl);
        out.writeString(timeOffset);
        out.writeString(shareUrl);
        out.writeString(time_stamp);
        out.writeString(dateTime);
        out.writeString(isRTL);

        // Again continue doing this for the rest of your member data
    }

    // Just cut and paste this for now
    public int describeContents() {
        return 0;
    }

    public int getFavouriteID() {
        return favouriteID;
    }

    public void setFavouriteID(int favouriteD) {
        this.favouriteID = favouriteD;
    }

    public int getLikeID() {
        return likeID;
    }

    public void setLikeID(int likeID) {
        this.likeID = likeID;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsDetails() {
        return newsDetails;
    }

    public void setNewsDetails(String newsDetails) {
        this.newsDetails = newsDetails;
    }

    public String getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(String articleDate) {
        this.articleDate = articleDate;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getSourceID() {
        return sourceID;
    }

    public void setSourceID(int sourceID) {
        this.sourceID = sourceID;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getSourceLogoUrl() {
        return sourceLogoUrl;
    }

    public void setSourceLogoUrl(String sourceLogoUrl) {
        this.sourceLogoUrl = sourceLogoUrl;
    }

    public String getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(String time_stamp) {
        this.time_stamp = time_stamp;
    }

    public String getSourceTitle() {
        return sourceTitle;
    }

    public void setSourceTitle(String sourceTitle) {
        this.sourceTitle = sourceTitle;
    }

    public String getNewsUrl() {
        return newsUrl;
    }

    public void setNewsUrl(String newsUrl) {
        this.newsUrl = newsUrl;
    }

    public int getLikesNumber() {
        return likesNumber;
    }

    public void setLikesNumber(int likesNumber) {
        this.likesNumber = likesNumber;
    }

    public int getCommentsNumber() {
        return commentsNumber;
    }

    public void setCommentsNumber(int commentsNumber) {
        this.commentsNumber = commentsNumber;
    }

    public int getSharesNumber() {
        return sharesNumber;
    }

    public void setSharesNumber(int sharesNumber) {
        this.sharesNumber = sharesNumber;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getTimeOffset() {
        return timeOffset;
    }

    public void setTimeOffset(String timeOffset) {
        this.timeOffset = timeOffset;
    }

    public int getReadersNum() {
        return readersNum;
    }

    public void setReadersNum(int readersNum) {
        this.readersNum = readersNum;
    }

    public boolean isUrgent() {
        return isUrgent;
    }

    public void setUrgent(boolean urgent) {
        isUrgent = urgent;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public int getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(int isBlocked) {
        this.isBlocked = isBlocked;
    }

    public String getIsRTL() {
        return isRTL;
    }

    public void setIsRTL(String isRTL) {
        this.isRTL = isRTL;
    }

    public boolean isTicked() {
        return isTicked;
    }

    public void setTicked(boolean ticked) {
        isTicked = ticked;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSourceImage() {
        return sourceImage;
    }

    public void setSourceImage(String sourceImage) {
        this.sourceImage = sourceImage;
    }

    public int getSourceFollowers() {
        return sourceFollowers;
    }

    public void setSourceFollowers(int sourceFollowers) {
        this.sourceFollowers = sourceFollowers;
    }

    public int getChangeType() {
        return changeType;
    }

    public void setChangeType(int changeType) {
        this.changeType = changeType;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int getCountryID() {
        return countryID;
    }

    public void setCountryID(int countryID) {
        this.countryID = countryID;
    }


//    public NativeAd getNativeAd() {
//        return nativeAd;
//    }

//    public void setNativeAd(NativeAd nativeAd) {
//        this.nativeAd = nativeAd;
//    }
}
