package com.madarsoft.nabaa.entities;

import com.appodeal.ads.NativeAd;
import com.madarsoft.nabaa.fragments.NewsHolderFragment;

import java.util.ArrayList;

/**
 * Created by basma on 7/20/2017.
 */

public class AdSettings {
    //private long expiration_period;
    private ArrayList<AdUnit> nativeAds= new ArrayList<>();
    private ArrayList<AdUnit> bannerAds= new ArrayList<>();
    private ArrayList<AdUnit> splashAds= new ArrayList<>();

    public AppInfo getAppInfo() {
        return appInfo;
    }

    public void setAppInfo(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

    AppInfo appInfo=new AppInfo();
    public long getSavedDataDate() {
        return savedDataDate;
    }

    public void setSavedDataDate(long savedDataDate) {
        this.savedDataDate = savedDataDate;
    }

    long savedDataDate;
//in seconds
  /*  public long getExpiration_period() {
        return expiration_period;
    }

    public void setExpiration_period(long expiration_period) {
        this.expiration_period = expiration_period;
    }
*/
    public ArrayList<AdUnit> getNativeAds() {
        return nativeAds;
    }

    public void setNativeAds(ArrayList<AdUnit> nativeAds) {
        this.nativeAds = nativeAds;
    }

    public ArrayList<AdUnit> getBannerAds() {
        return bannerAds;
    }

    public void setBannerAds(ArrayList<AdUnit> bannerAds) {
        this.bannerAds = bannerAds;
    }

    public ArrayList<AdUnit> getSplashAds() {
        return splashAds;
    }

    public void setSplashAds(ArrayList<AdUnit> splashAds) {
        this.splashAds = splashAds;
    }

    public AdUnit getNativeAdUnit()
    {

        for (int i = 0; i <nativeAds.size() ; i++) {
            if (nativeAds.get(i).getType()!=null&&nativeAds.get(i).getType().equalsIgnoreCase(AdUnit.NATIVE)&&nativeAds.get(i).isEnabled()){
               return nativeAds.get(i);
            }
        }
        return null;
    }
    public AdUnit getSplashAdUnitByScreenName(String screenName)
    {

        for (int i = 0; i <splashAds.size() ; i++) {
            if (splashAds.get(i).getScreen_name()!=null &&splashAds.get(i).getScreen_name().equalsIgnoreCase(screenName)){
                return splashAds.get(i);
            }
        }
        return null;
    }
    public AdUnit getBannerAdUnitByScreenName(String screenName)
    {

        for (int i = 0; i <bannerAds.size() ; i++) {
            if (bannerAds.get(i).getScreen_name()!=null &&bannerAds.get(i).getScreen_name().equalsIgnoreCase(screenName)){
                return bannerAds.get(i);
            }
        }
        return null;
    }
    public AdUnit getMRECAdUnit(int realPosition)
    {

        ArrayList<AdUnit> mrecAds=new ArrayList<>();

        for (int i = 0; i <nativeAds.size() ; i++) {
            if (nativeAds.get(i).getType()!=null && nativeAds.get(i).getType().equalsIgnoreCase(AdUnit.MREC) && nativeAds.get(i).isEnabled()){
                mrecAds.add(nativeAds.get(i));
               // return nativeAds.get(i);
            }
        }
        try
        {
            return mrecAds.get(getInstreamPosition(realPosition,mrecAds));
        }catch (Exception e)
        {

        }
        return null;
    }


    public AdUnit getInstremAdUnit(int realPosition,String type)
    {

        ArrayList<AdUnit> enabledAds=new ArrayList<>();

        for (int i = 0; i <nativeAds.size() ; i++) {
            if (nativeAds.get(i).isEnabled()){
                enabledAds.add(nativeAds.get(i));
                // return nativeAds.get(i);
            }
        }
        try
        {
           AdUnit adUnit = enabledAds.get(getInstreamPosition(realPosition,enabledAds));
            if (adUnit.getType()!=null && (adUnit.getType().equalsIgnoreCase(type)|| (type.equalsIgnoreCase(AdUnit.MREC) &&
                    NewsHolderFragment.isUseAdMobAds)))
            return adUnit;
        }catch (Exception e)
        {

        }
        return null;
    }
/*    public AdUnit getMRECAdUnit()
    {

       // ArrayList<AdUnit> mrecAds=new ArrayList<>();

        for (int i = 0; i <nativeAds.size() ; i++) {
            if (nativeAds.get(i).getType()!=null&&nativeAds.get(i).getType().equalsIgnoreCase(AdUnit.MREC)){
               // mrecAds.add(nativeAds.get(i));
                 return nativeAds.get(i);
            }
        }

        return null;
    }*/
    public  int getInstreamPosition(int position, ArrayList<AdUnit> mrecAds)
    {


      /*      AdUnit nativeAdUnit ;//= adSettings.getMRECAdUnit();
            nativeAdUnit = getInstreamAdUnit(isNative,position);
            */
      if (position>=appInfo.getInstreamAdsStartPosition()&& mrecAds!=null && mrecAds.size()>0) {
          return ((position - appInfo.getInstreamAdsStartPosition()) / mrecAds.size()) % appInfo.getInstreamAdsRepeating();


      }
        return -1;
    }



}
