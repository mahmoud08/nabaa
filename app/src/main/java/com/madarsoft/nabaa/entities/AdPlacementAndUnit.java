package com.madarsoft.nabaa.entities;

/**
 * Created by basma on 8/8/2017.
 */

public class AdPlacementAndUnit {
    private String cod;
    private String type;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
