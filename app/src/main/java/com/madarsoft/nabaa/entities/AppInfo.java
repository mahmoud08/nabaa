package com.madarsoft.nabaa.entities;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by basma on 11/17/2016.
 */

@IgnoreExtraProperties
public class AppInfo {

/*    String facebookBannerAdPlacementId ="";
    String facebookNativeAdPlacementId ="";
    String facebookSplashAdPlacementId ="";
    String googleBannerAdUnitId="";
    String googleSplashAdUnitId ="";*/
/*
static  AppInfo appInfo;
private AppInfo()
{

}

    public static AppInfo getInstance() {
        if (appInfo==null)
        {
            appInfo=new AppInfo();
        }
        return appInfo;
    }
*/

/*
    public static final String COL_nativeAdsRepeating="nativeAdsRepeating";
    public static final String COL_nativeAdsStartPosition="nativeAdsStartPosition";

    public static final String COL_DATA_EXPIRATION_PERIOD="dataExpirationPeriod";
    public static final String[] FIELDS=new String[]{COL_nativeAdsRepeating,COL_nativeAdsStartPosition,COL_DATA_EXPIRATION_PERIOD};
    @Exclude
    public String[] getValues(){
        return
                new String[]{""+getInstreamAdsRepeating(),""+ getInstreamAdsStartPosition(),""+getExpirationPeriodInseconds()};}
*/

    public boolean isOguryEnabled() {
        return oguryEnabled;
    }

    public void setOguryEnabled(boolean oguryEnabled) {
        this.oguryEnabled = oguryEnabled;
    }

    boolean oguryEnabled=true;

    long expirationPeriodInseconds;
    private int instreamAdsRepeating=-1;

  /*  public int getInstreamCacheSize() {
        return instreamCacheSize;
    }

    public void setInstreamCacheSize(int instreamCacheSize) {
        this.instreamCacheSize = instreamCacheSize;
    }*/

    public  int getInstreamCacheSize() {
        return this.instreamCacheSize;
    }

    public  void setInstreamCacheSize(int instreamCacheSize) {
        this.instreamCacheSize = instreamCacheSize;
    }

    public  int  instreamCacheSize=5;

    public long getExpirationPeriodInseconds() {
        return expirationPeriodInseconds;
    }

    public void setExpirationPeriodInseconds(long expirationPeriodInseconds) {
        this.expirationPeriodInseconds = expirationPeriodInseconds;
    }

    public int getInstreamAdsRepeating() {
        return instreamAdsRepeating;
    }

    public void setInstreamAdsRepeating(int instreamAdsRepeating) {
        this.instreamAdsRepeating = instreamAdsRepeating;
    }

    public int getInstreamAdsStartPosition() {
        return instreamAdsStartPosition;
    }

    public void setInstreamAdsStartPosition(int instreamAdsStartPosition) {
        this.instreamAdsStartPosition = instreamAdsStartPosition;
    }

    public boolean isAdPosition(int realPosition) {
        return (getInstreamAdsRepeating()>0&& (realPosition - getInstreamAdsStartPosition()>=0)&&(realPosition - getInstreamAdsStartPosition()) % getInstreamAdsRepeating() == 0);
        //return realPosition==getNativeAdsStartPosition()+((realPosition-getNativeAdsStartPosition())%getNativeAdsRepeating())*getNativeAdsRepeating();
    }
    private int instreamAdsStartPosition=-1;
   /* public String getFacebookBannerAdPlacementId() {
        return facebookBannerAdPlacementId;
    }

    public void setFacebookBannerAdPlacementId(String facebookBannerAdPlacementId) {
        this.facebookBannerAdPlacementId = facebookBannerAdPlacementId;
    }

    public String getFacebookNativeAdPlacementId() {
        return facebookNativeAdPlacementId;
    }

    public void setFacebookNativeAdPlacementId(String facebookNativeAdPlacementId) {
        this.facebookNativeAdPlacementId = facebookNativeAdPlacementId;
    }

    public String getFacebookSplashAdPlacementId() {
        return facebookSplashAdPlacementId;
    }

    public void setFacebookSplashAdPlacementId(String facebookSplashAdPlacementId) {
        this.facebookSplashAdPlacementId = facebookSplashAdPlacementId;
    }

    public String getGoogleBannerAdUnitId() {
        return googleBannerAdUnitId;
    }

    public void setGoogleBannerAdUnitId(String googleBannerAdUnitId) {
        this.googleBannerAdUnitId = googleBannerAdUnitId;
    }

    public String getGoogleSplashAdUnitId() {
        return googleSplashAdUnitId;
    }

    public void setGoogleSplashAdUnitId(String googleSplashAdUnitId) {
        this.googleSplashAdUnitId = googleSplashAdUnitId;
    }*/




/*    public int getNativeAdsRepeating() {
        return nativeAdsRepeating;
    }

    public void setNativeAdsRepeating(int nativeAdsRepeating) {
        this.nativeAdsRepeating = nativeAdsRepeating;
    }

    public int getNativeAdsStartPosition() {
        return nativeAdsStartPosition;
    }

    public void setNativeAdsStartPosition(int nativeAdsStartPosition) {
        this.nativeAdsStartPosition = nativeAdsStartPosition;
    }*/
}
