package com.madarsoft.nabaa.entities;

public class Sources {
    public static final String TABLE_NAME = "Sources";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_SOURCE_NAME = "source_name";
    public static final String COLUMN_SOURCE_ID = "source_id";
    public static final String COLUMN_CATEGORY_ID = "category_id";
    public static final String COLUMN_LOGO_URL = "logo_url";
    public static final String COLUMN_NUMBER_FOLLOWERS = "number_followers";
    public static final String COLUMN_SELECTED_OR_NOT = "selected_or_not";
    public static final String COLUMN_GEO_ID = "geo_id";
    public static final String COLUMN_SUB_ID = "sub_id";
    public static final String COLUMN_BACKGROUND_URL = "background_url";
    public static final String COLUMN_DETAIL = "details";
    public static final String COLUMN_CHANGE_TYPE = "change_type";
    public static final String COLUMN_TIME_STAMP = "time_stamp";
    public static final String COLUMN_IS_SOURCE_NOTIFIABLE = "notifiable";
    public static final String COLUMN_RANK = "rank";
    public static final String COLUMN_IS_SOURCE_URGENT_NOTIFIABLE = "urgent_notify";
    public final String fields[] = new String[]{COLUMN_SOURCE_NAME, COLUMN_SOURCE_ID,
            COLUMN_LOGO_URL, COLUMN_NUMBER_FOLLOWERS, COLUMN_CATEGORY_ID,
            COLUMN_SELECTED_OR_NOT, COLUMN_GEO_ID, COLUMN_SUB_ID, COLUMN_BACKGROUND_URL, COLUMN_DETAIL, COLUMN_CHANGE_TYPE,
            COLUMN_TIME_STAMP, COLUMN_IS_SOURCE_NOTIFIABLE, COLUMN_IS_SOURCE_URGENT_NOTIFIABLE, COLUMN_RANK};
    private int id;
    private String source_name;
    private int source_id;
    private String logo_url;
    private int number_followers;
    private int category_id;
    private String categoryName;
    private int geo_id;//تصنيف الدولة
    private int sub_id;//تصنيف التخصص
    private int selected_or_not;
    private int change_type;//1= add, 2 = edit, 3= delete
    private String background_url;
    private String details;
    private long timeStamp;
    private int notifiable;  //التنبيه العادى
    private int rank;//للاكثر قراءة
    private int urgent_notify;//التنبيه العاجل للمصدر

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * 0 is turned off
     * 1 is is on
     *
     * @return int
     */
    public int getNotifiable() {
        return notifiable;
    }

    public void setNotifiable(int notifiable) {
        this.notifiable = notifiable;
    }

    public String[] getValues() {

        return new String[]{"" + source_name, "" + source_id, "" + logo_url,
                "" + number_followers, "" + category_id, "" + selected_or_not,
                "" + geo_id, "" + sub_id, "" + background_url, "" + details, "" + change_type, "" + timeStamp, "" + notifiable, "" +
                urgent_notify, "" + rank};
    }

    public int getGeo_id() {
        return geo_id;
    }

    public void setGeo_id(int geo_id) {
        this.geo_id = geo_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public int getNumber_followers() {
        return number_followers;
    }

    public void setNumber_followers(int number_followers) {
        this.number_followers = number_followers;
    }

    public int getSelected_or_not() {
        return selected_or_not;
    }

    public void setSelected_or_not(int selected_or_not) {
        this.selected_or_not = selected_or_not;
    }

    public int getSource_id() {
        return source_id;
    }

    public void setSource_id(int source_id) {
        this.source_id = source_id;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public int getSub_id() {
        return sub_id;
    }

    public void setSub_id(int sub_id) {
        this.sub_id = sub_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getBackground_url() {
        return background_url;
    }

    public void setBackground_url(String background_url) {
        this.background_url = background_url;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }


    public int getChange_type() {
        return change_type;
    }

    public void setChange_type(int change_type) {
        this.change_type = change_type;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long time_Stamp) {
        this.timeStamp = time_Stamp;
    }


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {

        this.categoryName = categoryName;
    }


    public int getUrgent_notify() {
        return urgent_notify;
    }

    public void setUrgent_notify(int urgent_notify) {
        this.urgent_notify = urgent_notify;
    }
}
