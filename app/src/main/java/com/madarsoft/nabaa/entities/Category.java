package com.madarsoft.nabaa.entities;

public class Category {
    public static final String TABLE_NAME = "Category";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_CATEGORY_NAME = "category_name";
    public static final String COLUMN_CATEGORY_ID = "category_id";
    public static final String COLUMN_CATEGORY_TYPE = "category_type";
    public static final String COLUMN_LOGO_URL = "logo_url";
    public static final String COLUMN_TIME_STAMP = "time_stamp";
    public static final String COLUMN_CHANGE_TYPE = "change_type";
    public static final String COLUMN_SOURCE_TIME_STAMP = "sources_time_stamp";
    public static final String COLUMN_CATEGORY_COUNT = "count";
    public static final String COLUMN_CATEGORY_ISO = "iso";
    public final String fields[] = new String[]{COLUMN_CATEGORY_NAME, COLUMN_CATEGORY_ID,
            COLUMN_CATEGORY_TYPE, COLUMN_LOGO_URL, COLUMN_TIME_STAMP, COLUMN_CHANGE_TYPE, COLUMN_SOURCE_TIME_STAMP,
            COLUMN_CATEGORY_COUNT, COLUMN_CATEGORY_ISO
    };
    private int ID;
    private String category_name;
    private int category_id;
    private int category_type;
    private String logo_url;
    private long time_stamp;
    private int change_type;
    private long sources_time_stamp;
    private int count;
    private String iso;

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public long getSources_time_stamp() {
        return sources_time_stamp;
    }

    public void setSources_time_stamp(long sources_time_stamp) {
        this.sources_time_stamp = sources_time_stamp;
    }

    public int getChange_type() {
        return change_type;
    }

    public void setChange_type(int change_type) {
        this.change_type = change_type;
    }

    public String[] getValues() {

        return new String[]{"" + category_name, "" + category_id, "" + category_type,
                "" + logo_url, "" + time_stamp, "" + change_type, "" + sources_time_stamp, "" + count, "" + iso};
    }


    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public int getCategory_type() {
        return category_type;
    }

    public void setCategory_type(int category_type) {
        this.category_type = category_type;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public long getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(long time_stamp) {
        this.time_stamp = time_stamp;
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
