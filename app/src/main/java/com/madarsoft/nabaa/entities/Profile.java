package com.madarsoft.nabaa.entities;

public class Profile {
    public static final String TABLE_NAME = "Profile";
    public static final String COLUMN_URGENT_FLAG = "urgent_flag";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_REG_FLAG = "reg_flag";
    public static final String COLUMN_SOUND_TYPE = "sound_type";
    public static final String COLUMN_BLOCK_IMG = "block_img";
    public final String fields[] = new String[]{COLUMN_USER_ID, COLUMN_REG_FLAG, COLUMN_URGENT_FLAG, COLUMN_SOUND_TYPE, COLUMN_BLOCK_IMG};
    private int ID;
    private int userId;
    private int regFlag;
    private int urgentFlag;//التنبيهات العاجلة
    private int soundType;// التنبيهات العادية
    private int blockImg;

    public int getBlockImg() {
        return blockImg;
    }

    public void setBlockImg(int blockImg) {
        this.blockImg = blockImg;
    }

    public int getSoundType() {
        return soundType;
    }

    public void setSoundType(int soundType) {
        this.soundType = soundType;
    }

    /**
     * @return urgentFlag
     */
    public int getUrgentFlag() {
        return urgentFlag;
    }

    public void setUrgentFlag(int urgentFlag) {
        this.urgentFlag = urgentFlag;
    }

    public int getRegFlag() {
        return regFlag;
    }

    public void setRegFlag(int regFlag) {
        this.regFlag = regFlag;
    }

    public String[] getValues() {
        return new String[]{"" + userId, "" + regFlag, "" + urgentFlag, "" + soundType, "" + blockImg};

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getUserId() {
        return userId;
    }

    public void setUser_id(int user_id) {
        this.userId = user_id;
    }


}