/*
 * Copyright (C) 2015 Karumi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.karumi.expandableselector;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.expandableselector.animation.ExpandableSelectorAnimator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * FrameLayout extension used to show a list of ExpandableItems instances represented with Button
 * or ImageButton widgets which can be collapsed and expanded using an animation. The configurable
 * elements of the class are:
 * <p/>
 * - List of items to show represented with ExpandableItem instances.
 * - Time used to perform the collapse/expand animations. Expressed in milliseconds.
 * - Show or hide the view background when the List of ExpandaleItems are collapsed.
 * - Configure a ExpandableSelectorListeners to be notified when the view is going to be
 * collapsed/expanded or has
 * been collapsed/expanded.
 * - Configure a OnExpandableItemClickListener to be notified when an item is clicked.
 */
@SuppressWarnings("ALL")
public class ExpandableSelector extends FrameLayout {

    private static final int DEFAULT_ANIMATION_DURATION = 300;

    private List<ExpandableItem> expandableItems = Collections.EMPTY_LIST;
    private List<View> buttons = new ArrayList<View>();
    private List<ImageView> imagesList = new ArrayList<ImageView>();
    private ExpandableSelectorAnimator expandableSelectorAnimator;
    private ExpandableSelectorListener listener;
    private OnExpandableItemClickListener clickListener;

    private boolean hideBackgroundIfCollapsed;
    private Drawable expandedBackground;
    private boolean isHaveCountries;

    public ExpandableSelector(Context context) {
        this(context, null);
    }

    public ExpandableSelector(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExpandableSelector(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeView(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ExpandableSelector(Context context, AttributeSet attrs, int defStyleAttr,
                              int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initializeView(attrs);
    }

    /**
     * Configures a List<ExpandableItem> to be shown. By default, the list of ExpandableItems is
     * going to be shown collapsed. Please take into account that this method creates
     * ImageButton/Button widgets based on the size of the list passed as parameter. Don't use this
     * library as a RecyclerView and take into account the number of elements to show.
     */
    public void showExpandableItems(List<ExpandableItem> expandableItems, boolean isHaveCountries, int resource) {
        validateExpandableItems(expandableItems);
        this.isHaveCountries = isHaveCountries;
        reset();
        setExpandableItems(expandableItems);
        renderExpandableItems(resource);
        hookListeners();
        bringChildsToFront(expandableItems);
    }

    /**
     * Performs different animations to show the previously configured ExpandableItems transformed
     * into Button widgets. Notifies the ExpandableSelectorListener instance there was previously
     * configured.
     */
    public void expand() {
        expandableSelectorAnimator.expand(new ExpandableSelectorAnimator.Listener() {
            @Override
            public void onAnimationFinished() {
                notifyExpanded();
            }
        });
        notifyExpand();
        updateBackground();
    }

    /**
     * Performs different animations to hide the previously configured ExpandableItems transformed
     * into Button widgets. Notifies the ExpandableSelectorListener instance there was previously
     * configured.
     */
    public void collapse() {
        expandableSelectorAnimator.collapse(new ExpandableSelectorAnimator.Listener() {
            @Override
            public void onAnimationFinished() {
                updateBackground();
                notifyCollapsed();
            }
        });
        notifyCollapse();
    }

    /**
     * Returns true if the view is collapsed and false if the view is expanded.
     */
    public boolean isCollapsed() {
        return expandableSelectorAnimator.isCollapsed();
    }

    /**
     * Returns true if the view is expanded and false if the view is collapsed.
     */
    public boolean isExpanded() {
        return expandableSelectorAnimator.isExpanded();
    }

    /**
     * Configures a ExpandableSelectorListener instance to be notified when different collapse/expand
     * animations be performed.
     */
    public void setExpandableSelectorListener(ExpandableSelectorListener listener) {
        this.listener = listener;
    }

    /**
     * Configures a OnExpandableItemClickListener instance to be notified when a Button/ImageButton
     * inside ExpandableSelector be clicked. If the component is collapsed an the first button is
     * clicked the listener will not be notified. This listener will be notified about button clicks
     * just when ExpandableSelector be collapsed.
     */
    public void setOnExpandableItemClickListener(OnExpandableItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    /**
     * Given a position passed as parameter returns the ExpandableItem associated.
     */
    public ExpandableItem getExpandableItem(int expandableItemPosition) {
        return expandableItems.get(expandableItemPosition);
    }

    /**
     * Changes the ExpandableItem associated to a given position and updates the Button widget to
     * show
     * the new ExpandableItem information.
     */
    public void updateExpandableItem(int expandableItemPosition, ExpandableItem expandableItem) {
        validateExpandableItem(expandableItem);
        expandableItems.remove(expandableItemPosition);
        expandableItems.add(expandableItemPosition, expandableItem);
        int buttonPosition = buttons.size() - 1 - expandableItemPosition;
        configureButtonContent(buttons.get(buttonPosition), expandableItem);
    }

    private void initializeView(AttributeSet attrs) {
        TypedArray attributes =
                getContext().obtainStyledAttributes(attrs, R.styleable.expandable_selector);
        initializeAnimationDuration(attributes);
        initializeHideBackgroundIfCollapsed(attributes);
        initializeHideFirstItemOnCollapse(attributes);
        attributes.recycle();

    }

    private void initializeHideBackgroundIfCollapsed(TypedArray attributes) {
        hideBackgroundIfCollapsed =
                attributes.getBoolean(R.styleable.expandable_selector_hide_background_if_collapsed, false);
        expandedBackground = getBackground();
        updateBackground();
    }

    private void initializeAnimationDuration(TypedArray attributes) {
        int animationDuration = 300;
        int expandInterpolatorId =
                attributes.getResourceId(R.styleable.expandable_selector_expand_interpolator,
                        android.R.anim.accelerate_interpolator);
        int collapseInterpolatorId =
                attributes.getResourceId(R.styleable.expandable_selector_collapse_interpolator,
                        android.R.anim.decelerate_interpolator);
        int containerInterpolatorId =
                attributes.getResourceId(R.styleable.expandable_selector_container_interpolator,
                        android.R.anim.decelerate_interpolator);
        expandableSelectorAnimator = new ExpandableSelectorAnimator(this, animationDuration, expandInterpolatorId,
                collapseInterpolatorId, containerInterpolatorId);
    }

    private void initializeHideFirstItemOnCollapse(TypedArray attributes) {
        boolean hideFirstItemOnCollapsed =
                attributes.getBoolean(R.styleable.expandable_selector_hide_first_item_on_collapse, false);
        expandableSelectorAnimator.setHideFirstItemOnCollapse(hideFirstItemOnCollapsed);
    }

    private void updateBackground() {
        if (!hideBackgroundIfCollapsed) {
            return;
        }
        if (isExpanded()) {
            setBackgroundDrawable(expandedBackground);
        } else {
            setBackgroundResource(android.R.color.transparent);
        }
    }

    private void reset() {
        this.expandableItems = Collections.EMPTY_LIST;
        for (View button : buttons) {
            removeView(button);
        }
        this.buttons = new ArrayList<View>();
        this.imagesList = new ArrayList<ImageView>();
        expandableSelectorAnimator.reset();
    }

    public void renderExpandableItems(int resource) {
        int numberOfItems = expandableItems.size();

        for (int i = numberOfItems - 1; i >= 0; i--) {
            View button = initializeButton(i, resource);
            addView(button);
            buttons.add(button);
            expandableSelectorAnimator.initializeButton(button);
            configureButtonContent(button, expandableItems.get((i)));
        }
        expandableSelectorAnimator.setButtons(buttons);
    }

    private void setAnime(final View v, int start, int end) {
        rotateAnim = new RotateAnimation(start, end,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotateAnim.setDuration(100);
        rotateAnim.setFillAfter(true);
        rotateAnim.setDuration(340L);
        v.startAnimation(rotateAnim);
        rotateAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (isCollapsed()) {
                    ((ImageView) v).setImageResource(R.drawable.floating_menu_icon);
                } else {
                    ((ImageView) v).setImageResource(R.drawable.close_menu);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    RotateAnimation rotateAnim;
    View view;

    private void hookListeners() {
        final int numberOfButtons = buttons.size();
        boolean thereIsMoreThanOneButton = numberOfButtons > 1;

        if (thereIsMoreThanOneButton) {
            imagesList.get(numberOfButtons - 1).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(final View v) {

                    if (isCollapsed()) {
                        expand();
                        setAnime(v, 0, 360);
                    } else {
                        notifyButtonClicked(0, v);
                        setAnime(v, 360, 0);
                    }
                    view = v;

                }
            });
        }
        for (int i = 0; i < numberOfButtons - 1; i++) {
            final int buttonPosition = i;
            buttons.get(i).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int buttonIndex = numberOfButtons - 1 - buttonPosition;
                    notifyButtonClicked(buttonIndex, v);
                    setAnime(view, 360, 0);
                }
            });
        }
    }

    private void notifyButtonClicked(int itemPosition, View button) {
        if (clickListener != null) {
            clickListener.onExpandableItemClickListener(itemPosition, button);
        }
    }

    Drawable sampleDrawable;

    private void setColorFilter(ImageView img, String color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            sampleDrawable = getResources().getDrawable(R.drawable.rounded_circle_lollipop);
        else
            sampleDrawable = getResources().getDrawable(R.drawable.rounded_circle);

        sampleDrawable.setColorFilter(new PorterDuffColorFilter(Color.parseColor(color), PorterDuff.Mode.MULTIPLY));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            img.setBackground(sampleDrawable);
        } else img.setBackgroundDrawable(sampleDrawable);
    }

    private View initializeButton(int expandableItemPosition, int resource) {
        ExpandableItem expandableItem = expandableItems.get(expandableItemPosition);
        View layout = null;
        Context context = getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //if (expandableItem.hasTitle()) {

        layout = layoutInflater.inflate(resource, this, false);
        TextView title = (TextView) layout.findViewById(R.id.title);
        ImageView Img = (ImageView) layout.findViewById(R.id.icon);

        if (expandableItemPosition == 0) {
            setColorFilter(Img, "#C01717");
            Img.setImageResource(R.drawable.floating_menu_icon);
            title.setText("");
            title.setBackgroundResource(0);
        } else
            title.setText(expandableItem.getTitle());


        if (expandableItemPosition == 1) {
            if (expandableItems.size() > 4) {
                setColorFilter(Img, "#053347");
                Img.setImageResource(R.drawable.more);
            }
        }
        if (expandableItemPosition == expandableItems.size() - 1) {
            if (this.isHaveCountries) {
                setColorFilter(Img, "#F9A357");
                Img.setImageResource(R.drawable.countries);
            }
        }

         /*else {
            button = layoutInflater.inflate(R.layout.expandable_item_image_button, this, false);
        }*/
        int visibility = expandableItemPosition == 0 ? View.VISIBLE : View.INVISIBLE;
        layout.setVisibility(visibility);
        Img.setId(expandableItemPosition);
        layout.setId(expandableItemPosition);
        imagesList.add(Img);
        return layout;
    }

    private void configureButtonContent(View button, ExpandableItem expandableItem) {
        if (expandableItem.hasBackgroundId()) {
            int backgroundId = expandableItem.getBackgroundId();
            button.setBackgroundResource(backgroundId);
        }
        /*if (expandableItem.hasTitle()) {
            String text = expandableItem.getTitle();
            ((TextView) button.findViewById(R.id.title)).setText(text);
        }*/
        if (expandableItem.hasResourceId()) {
            ImageButton imageButton = (ImageButton) button;
            int resourceId = expandableItem.getResourceId();
            imageButton.setImageResource(resourceId);
        }
    }

    private void notifyExpand() {
        if (hasListenerConfigured()) {
            listener.onExpand();
        }
    }

    private void notifyCollapse() {
        if (hasListenerConfigured()) {
            listener.onCollapse();
        }
    }

    private void notifyExpanded() {
        if (hasListenerConfigured()) {
            listener.onExpanded();
        }
    }

    private void notifyCollapsed() {
        if (hasListenerConfigured()) {
            listener.onCollapsed();
        }
    }

    private boolean hasListenerConfigured() {
        return listener != null;
    }

    private void validateExpandableItem(ExpandableItem expandableItem) {
        if (expandableItem == null) {
            throw new IllegalArgumentException(
                    "You can't use a null instance of ExpandableItem as parameter.");
        }
    }

    private void validateExpandableItems(List<ExpandableItem> expandableItems) {
        if (expandableItems == null) {
            throw new IllegalArgumentException(
                    "The List<ExpandableItem> passed as argument can't be null");
        }
    }

    private void setExpandableItems(List<ExpandableItem> expandableItems) {
        this.expandableItems = new ArrayList<ExpandableItem>(expandableItems);
    }

    private void bringChildsToFront(List<ExpandableItem> expandableItems) {
        int childCount = getChildCount();
        int numberOfExpandableItems = expandableItems.size();
        if (childCount > numberOfExpandableItems) {
            for (int i = 0; i < childCount - numberOfExpandableItems; i++) {
                getChildAt(i).bringToFront();
            }
        }
    }
}
